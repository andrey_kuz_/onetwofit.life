<?php


Route::group(['prefix' => 'admin'], function () {
    Route::get('products/update-slug', 'Admin\ProductController@updateSlug');
    Route::get('categories/update-slug', 'Admin\CategoryController@updateSlug');
    Route::get('brands/update-slug', 'Admin\BrandController@updateSlug');
    Route::get('update-slug', 'Admin\AjaxController@updateSlug');
    Voyager::routes();
    Route::get('orders/publish/{id}', ['uses' => 'Admin\OrdersController@publish', 'as' => 'voyager.orders.publish']);
    Route::get('products/editprices/{product}', ['uses' => 'Admin\ProductController@editPrices', 'as' => 'voyager.product.editprices'])->where('product', '[0-9]+');;
    Route::post('products/updateprices/{product}', ['uses' => 'Admin\ProductController@updatePrices', 'as' => 'voyager.product.updateprices'])->where('product', '[0-9]+');
    Route::post('products/updateprices/{product}', ['uses' => 'Admin\ProductController@updatePrices', 'as' => 'voyager.product.updateprices'])->where('product', '[0-9]+');

    Route::post('products/sort_images', ['uses' => 'Admin\ProductController@sortImages', 'as' => 'voyager.product.sortImages'])->where('product', '[0-9]+');

    Route::post('products/{id}', ['uses' => 'Admin\ProductController@saveImages', 'as' => 'voyager.product.saveImages'])->where('product', '[0-9]+');

});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::post('/check-email', 'Auth\RegisterController@checkEmail')->name('checkEmail');

Route::post('/bestsellersFindCategory', 'HomeController@getBestsellersFindCategory');
//Route::get('/webhook', 'MessengerController@webhook');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Authentication Routes...
Route::get('login/{is_checkout?}', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login_modal_form', 'Auth\LoginController@showModalForm');
Route::post('login', 'Auth\LoginController@login');
Route::match(['get', 'post'], 'logout', 'Auth\LoginController@logout')->name('logout');


// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('user/verify/{token}/{cart?}', 'Auth\RegisterController@verifyUser');

// oAuth registration, authorization
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

//save settings such as country lang currency route
Route::match(['get', 'post'], 'savesettings', 'LocaleController@savesettings');

// messangers handles
Route::get('botman/tinker', 'Messenger\BotManController@tinker');
Route::match(['get', 'post'], 'botman', 'Messenger\BotManController@handle');
Route::match(['get', 'post'], 'botman/facebook', 'Messenger\FacebookController@handle');
Route::match(['get', 'post'], 'botman/telegram', 'Messenger\TelegramController@handle');

// authorized user account
Route::get('account', 'AccountController@account')->name('account');
Route::get('account/edit', 'AccountController@edit')->name('account.edit');
Route::post('account/update', 'AccountController@update')->name('account.update');
Route::get('account/editpassword', 'AccountController@editPassword')->name('account.editpassword');
Route::post('account/updatepassword', 'AccountController@updatePassword')->name('account.updatepassword');
Route::get('account/orders', 'AccountController@orders')->name('account.orders');
Route::get('account/order/renew/{order}', 'AccountController@renewOrder')->name('account.order.renew')->where('order', '[0-9]+');;

Route::match(['get', 'post'], 'favorites/toggle/{product}', 'AccountController@toggleFavorite')->name('favorites.toggle')->where('product', '[0-9]+');
Route::match(['get', 'post'], 'favorites', 'AccountController@favorites')->name('favorites');

Route::get('compare/count', 'CompareController@getCount')->name('compare.сount');
Route::get('compare/add/{id}/', 'CompareController@add')->name('compare.add')->where(['id' => '[0-9]+']);
Route::get('compare/delete/{id}/', 'CompareController@delete')->name('compare.delete')->where(['id' => '[0-9]+']);
Route::get('compare', 'CompareController@view')->name('compare.view')->where(['id' => '[0-9]+']);

Route::get('cart/addcombo/{combo}/{isAjax?}', 'CartController@addCombo')->name('cart.addCombo')->where(['combo' => '[0-9]+', 'isAjax' => '[0-1]']);
Route::get('cart/add/{product}/{deltaQuantity?}/{isAjax?}', 'CartController@add')->name('cart.add')->where(['product' => '[0-9]+', 'isAjax' => '[0-1]']);
Route::get('cart/addAjax/{product}/{deltaQuantity?}/{isAjax?}', 'CartController@addAjax')->name('cart.addAjax')->where(['product' => '[0-9]+', 'isAjax' => '[0-1]']);
Route::get('cart/sub/{product}/{isAjax?}', 'CartController@sub')->name('cart.sub')->where(['product' => '[0-9]+', 'isAjax' => '[0-1]']);
Route::get('cart/remove/{product}/{isAjax?}', 'CartController@remove')->name('cart.remove')->where(['product' => '[0-9]+', 'isAjax' => '[0-1]']);
Route::get('cart', 'CartController@view')->name('cart.view');
Route::post('cart/addpromo', 'CartController@addPromocode')->name('cart.addpromo');
Route::post('order/addpromo', 'OrderController@addPromocode')->name('order.addpromo');


Route::post('oneclick/store', 'OrderController@oneClickStore')->name('oneclick.store');
Route::get('oneclick/{product}', 'OrderController@oneClick')->name('oneclick');

Route::match(['get', 'post'], 'checkout/{continueWithoutAuth?}', 'OrderController@checkoutStep1')->name('checkout')->where(['continueWithoutAuth' => '[0-1]']);
Route::post('checkout/post', 'OrderController@checkoutStep1Post')->name('checkout.post');
Route::get('checkout/step2/{order}', 'OrderController@checkoutStep2')->name('checkout2')->where(['order' => '[0-9]+']);
Route::post('checkout/process', 'OrderController@checkoutProcess')->name('checkout.process');
Route::match(['get', 'post'], 'shipping', 'OrderController@shipping')->name('shipping');

Route::get('order/thankyou/{order}', 'OrderController@thankyou')->name('order.thankyou')->where(['order' => '[0-9]+']);

Route::post('payment/callback/{ptid}', 'OrderController@paymentCallback')->name('payment.callback')->where(['ptid' => '[0-9]+']);
Route::get('payment/callback/{ptid}', 'OrderController@paymentCallback')->name('payment.callback')->where(['ptid' => '[0-9]+']);
Route::get('payment/return_url/{order}', 'OrderController@returnUrl')->name('payment.return_url');

Route::post('review/store', 'ProductController@reviewStore')->name('review.store');
Route::get('review/{product}', 'ProductController@review')->name('review');
Route::get('review/block/{product}', 'ProductController@reviewBlock')->name('review.block');


Route::get('search', 'SearchController@index')->name('search');

Route::get('/tag/{id}', ['as'=>'tag-old.index', 'uses' => 'TagController@index_old'])
    ->where('id', '[0-9]+');
Route::get('/tag/{slug}', ['as'=>'tag.index', 'uses' => 'TagController@index']);
Route::get('/tag/{slug}/filters/{filters?}', ['as'=>'tag.filters', 'uses' => 'TagController@filters']);
Route::match(['get', 'post'], 'ajaxcontent/tag/{id}', ['as'=>'tag.ajaxcontent', 'uses' => 'TagController@ajaxContent'])
    ->where('id', '[0-9]+');
Route::get('/tags', ['as'=>'tag.list', 'uses' => 'TagController@getList']);

Route::get('/brand/{id}', ['as'=>'brand-old.index', 'uses' => 'BrandController@index_old'])
    ->where('id', '[0-9]+');
Route::get('/brand/{slug}', ['as'=>'brand.index', 'uses' => 'BrandController@index']);
Route::get('/brand/{slug}/filters/{filters?}', ['as'=>'brand.filters', 'uses' => 'BrandController@filters']);
Route::match(['get', 'post'], 'ajaxcontent/brand/{id}', ['as'=>'brand.ajaxcontent', 'uses' => 'BrandController@ajaxContent'])
    ->where('id', '[0-9]+');
Route::get('/brands', ['as'=>'brand.list', 'uses' => 'BrandController@getList']);

Route::get('/special-offer/{type?}', ['as'=>'special', 'uses' => 'ProductController@specialProducts']);
Route::get('/special-offer/{type?}/filters/{filters?}', ['as'=>'special.filters', 'uses' => 'ProductController@filters']);
Route::match(['get', 'post'], 'ajaxcontent/special/{type?}', ['as'=>'special.ajaxcontent', 'uses' => 'ProductController@ajaxContent']);


Route::get('/category', ['as'=>'category.special', 'uses' => 'CategoryController@index']);
Route::get('/category/filters/{filters?}', ['as'=>'category.filters_all', 'uses' => 'CategoryController@filters']);
Route::get('/category/{id?}', ['as'=>'category.index-old', 'uses' => 'CategoryController@indexOld'])->where('id', '[0-9]+');
Route::get('/category/{slug?}', ['as'=>'category.index', 'uses' => 'CategoryController@index']);
Route::get('/category/{slug?}/filters/{filters?}', ['as'=>'category.filters', 'uses' => 'CategoryController@filters']);

Route::match(['get', 'post'], 'ajaxcontent/category/{id?}', ['as'=>'category.ajaxcontent', 'uses' => 'CategoryController@ajaxContent'])
    ->where('id', '[0-9]+');

Route::get('/product/{id}-{slug}', ['as'=>'product.index', 'uses' => 'ProductController@index'])
    ->where('id', '[0-9]+');
Route::get('/product/{id?}', ['as'=>'product.index-old', 'uses' => 'ProductController@indexOld']);
Route::get('/product/ajax/{id}/{isAjax?}/{email?}', ['as'=>'product.subscription', 'uses' => 'SubscribeController@subscription'])
    ->where('id', '[0-9]+');
Route::get('products/check', 'SubscribeController@check');

Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

Route::get('/session', function () {
    var_dump(Session::all());
});

Route::get('/mainCheck', 'MainController@index');

Route::get('page/{slug}', [
    'uses' => 'PageController@index'
])->name('page')->where('slug', '([A-Za-z0-9\-\/]+)');

Route::get('buffer/import/full', 'Buffer\ImportController@full');
Route::get('buffer/import/changes', 'Buffer\ImportController@changes');

// Email preview

Route::get('email-test', function () {


    return view('emails.instock');

});


Route::get('/export', 'FileController@export');
Route::get('/sitemap.xml', 'XmlMap\XmlController@generate');