<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],


    'facebook' => [
        'client_id'     => '232123760958032',
        'client_secret' => 'a2c83be3ae50196c8106c4d715649218',
        'redirect'      => 'https://onetwofit.life/auth/facebook/callback',
    ],

    /*
    'google' => [
        'client_id' => '82842844842-q799lq5feqpq37m5o7kmh0o909a2i4nr.apps.googleusercontent.com',
        'client_secret' => 'yEiKnkJDDEORiYhIDeLzllSk',
        'redirect' => 'http://localhost:8000/auth/google/callback', // Ссылка на перенаправление при удачной авторизации (3)
    ],
    
    'vkontakte' => [
        'client_id' => 'a209aa53a209aa53a209aa5315a26abf8faa209a209aa53f904976f4697e3fe106e4e32',
        'client_secret' => 'UKMbo8hUlXCeXkja6jai',
        'redirect' => 'http://localhost:8000/auth/vkontakte/callback'
    ],*/
    
    'botman' => [
        // Your driver-specific configuration
        'telegram' => [
            'token' => "591759360:AAEoCszdsEMccQUFgkymkvyMz3tcSq6Om18"
        ],
        'facebook' => [
            'token' => 'EAALhwIZC8gdEBAEjqQzMfVlwf1Ljaoi5str6feGyYK0FWK7wUqbyFvAcmrvYQrSHkOesGbEv1Qt3KG87QM9IlxPm8SpTXZCftkJx7Y8tznU2GxO2ZChVziXvWOLZAN0fxvO8vxqpZApGsdguD0RzwS6oDCR1O0LZCGMDf2gxtAogZDZD',
            'app_secret' => 'f8984b27a8b17a4c508e5fb78e212576',
            'verification'=>'eco-token',
        ]
    ],
];
