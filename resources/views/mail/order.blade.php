@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
        {{ config('app.name') }}
        @endcomponent
    @endslot

    <div class="order-page-last__number">Заказ № {{$order->id}}</div>
    <div class="attributes">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                @foreach($order->orderItems()->get() as $orderItem)
                    <tr>
                        <td>
                            @if($image=$orderItem->product->getMainImg())
                                <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif">

                            @else
                                <img src="{{ asset( 'images/bitmap.png')}}" alt="product">
                            @endif
                        </td>
                        <td>{{str_limit($orderItem->product->getTranslatedAttribute('name'), 30)}}</td>
                        <td><b>{{to_currency($orderItem->total_price_discounted)}}</b></td>
                    </tr>
                @endforeach

                @if (isset($order->promocodeUsed))
                    <tr class="total">
                        <td><b>К оплате</b></td>
                        <td></td>
                        <td><b>{{ to_currency($order->total_price_discounted_without_promo, true) }}</b></td>
                    </tr>
                    <tr class="total">
                        <td>С учетом промокода (-{{$order->discount_promo}}%)</td>
                        <td></td>
                        <td><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                    </tr>
                @else
                    <tr class="total">
                        <td><b>К оплате</b></td>
                        <td></td>
                        <td><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <div class="table-responsive attributes-info">
        <table class="table">
            <tbody>
            <tr>
                <td>Способ оплаты</td>
                <td class="text-right">{{isset($order->paymentType) ? $order->paymentType->name : ''}}</td>
            </tr>
            <tr>
                <td>Способ доставки: </td>
                <td class="text-right">{{isset($order->shippingType) ? $order->shippingType->name : ''}}, {{!is_null($order->shippingMethod) ? $order->shippingMethod->name : ''}}</td>
            </tr>
            <tr class="total totalblock">
                <td><b>Сумма:</b></td>
                <td class="text-right"><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
            </tr>
            </tbody>
        </table>
    </div>


    <div class="sms">
        В ближайшее время мы отправим Вам SMS с номером заказа и ориентировочной датой доставки.
    </div>

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. Super FOOTER!
@endcomponent
@endslot
@endcomponent