@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="account account-subscribe">
                @include('account.menu')
                <div class="col-sm-10 account__page account-personal">
                    <div class="account__title account-subscribe__title"> @lang('messages.Редактирование личных данных') </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="usereditform" role="form" method="POST" action="{{ url('/account/update') }}">
                        {{ csrf_field() }}
                        <div class="usereditform-column">
                            <div class="registration-page__row userdata-edit__rowСу">
                                <input type="text" name="name" id="username-edit" class="registration__input" value="{{ old('name') ?: $user->name }}">
                                <label for="username-edit" class="registration__label">@lang('validation.attributes.first_name')</label>
                            </div>
                            <div class="registration-page__row userdata-edit__rowСу">
                                <input type="text" name="lastname" id="lastname-edit" class="registration__input" value="{{ old('lastname') ?: $user->lastname }}">
                                <label for="username-edit" class="registration__label">@lang('validation.attributes.last_name')</label>
                            </div>
                            <div class="registration-page__row userdata-edit__row">
                                <input name="email" id="email" class="registration__input" type="text" value="{{ old('email') ?: $user->email}}">
                                <label for="email" class="registration__label">@lang('validation.attributes.email')</label>
                            </div>
                            <div class="registration-page__row userdata-edit__row">
                                <input name="phone" id="userphone-edit" class="registration__input"  type="text" value="{{ old('phone') ?: $user->phone}}">
                                <label for="userphone-edit" class="registration__label">@lang('validation.attributes.phone')</label>
                                <p id="info"></p>
                            </div>
                        </div>
                        <div class="usereditform-column">
                            <div class="registration-page__row userdata-edit__row">
                                <input type="text" name="shipping_city" id="user-city" class="registration__input" value="{{ old('shipping_city') ?: $user->shipping_city}}">
                                <label for="user-city" class="registration__label">@lang('validation.attributes.shipping_city')</label>
                            </div>
                            <div class="registration-page__row userdata-edit__rowСу">
                                <input type="text" name="shipping_street" id="user-street" class="registration__input" value="{{ old('shipping_street') ?: $user->shipping_street}}">
                                <label for="user-street" class="registration__label">@lang('validation.attributes.shipping_street')</label>
                            </div>


                            <div class="subcolumn">
                                <div class="usereditform-column">
                                    <div class="registration-page__row userdata-edit__rowСу">
                                        <input type="text" name="shipping_house" id="user-house" class="registration__input" value="{{ old('shipping_house') ?: $user->shipping_house}}">
                                        <label for="user-house" class="registration__label">@lang('validation.attributes.shipping_house')</label>
                                    </div>
                                </div>
                                <div class="usereditform-column">
                                    <div class="registration-page__row userdata-edit__rowСу">
                                        <input type="text" name="shipping_room" id="user-flat" class="registration__input" value="{{ old('shipping_room') ?: $user->shipping_room}}">
                                        <label for="user-flat" class="registration__label">@lang('validation.attributes.shipping_room')</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" value="@lang('messages.Сохранить')" class="default-btn">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
