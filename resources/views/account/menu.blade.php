<div class="col-sm-2 account__sidebar">
    <ul class="account-list">
        <li class="account-list__item"><a href="{{ route('account') }}" class="account-list__link {{ (Route::is('account') || Route::is('account.edit')) ? 'account-list__link-active' : '' }}"> @lang('messages.Личные данные')</a></li>
        {{--<li class="account-list__item"><a href="account-map.html" class="account-list__link">Мои карты</a></li>--}}
        <li class="account-list__item"><a href="{{ route('account.orders') }}" class="account-list__link {{ (Route::is('account.orders') || Route::is('account.orders')) ? 'account-list__link-active' : '' }}"> @lang('messages.Мои заказы') </a></li>
        <li class="account-list__item"><a href="{{ route('favorites') }}" class="account-list__link {{ Route::is('favorites') ? 'account-list__link-active' : '' }}"> @lang('messages.Избранное') </a></li>
        {{--<li class="account-list__item"><a href="account-referal.html" class="account-list__link">Реферальная программа</a></li>--}}
        {{--<li class="account-list__item"><a href="account-subscribe.html" class="account-list__link">Мои подписки</a></li>--}}
        <li class="account-list__item"><a href="{{ route('logout') }}" class="account-list__link"> @lang('messages.Выход') </a></li>
    </ul>
</div>