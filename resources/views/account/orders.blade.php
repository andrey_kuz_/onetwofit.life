@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="account account-subscribe">
            @include('account.menu')
            <div class="col-sm-10 account__page">
                <div class="account__title account-subscribe__title">
                    @lang('messages.Мои заказы')
                </div>
                <div class="account__info account-subscribe__info">
                    <div class="account-subscribe__content">
                        @foreach($orders as $order)
                        <div class="account-subscribe__row account-subscribe__row-open">
                            <div class="account-subscribe__icon">

                            </div>
                            <div class="account-subscribe__order-info">
                                <span> @lang('messages.Номер заказа:') {{ $order->id }}</span>
                                <span> @lang('messages.Дата заказа:') {{ $order->created_at }}</span>
                            </div>
                            <div class="account-subscribe-wrapper account-subscribe-wrapper__open">

                                <div class="account-subscribe__viewport">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            @foreach($order->orderItems as $orderItem)
                                            @if ($orderItem->product)
                                            <tr class="table-image__wrap">
                                                <td class="table-image">
                                                    @if($image=$orderItem->product->getMainImg())
                                                        <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:190px">

                                                    @else
                                                        <img src="{{ asset( 'images/bitmap.png')}}" alt="product">
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('product.index',['id'=>$orderItem->product_id, 'slug'=> $orderItem->product->slug]) }}">
                                                        {{ str_limit($orderItem->product->getTranslatedAttribute('name'), 50) }}
                                                    </a>
                                                </td>
                                                <td>{{ $orderItem->quantity }}</td>
                                                <td>{{ to_currency($orderItem->total_price_discounted, true) }}</td>
                                            </tr>
                                            @else
                                                <tr class="table-image__wrap">
                                                    <td class="" colspan="4">
                                                        @lang('messages.Товар не доступен')
                                                    </td>
                                                </tr>
                                            @endif
                                            @endforeach

                                            @if ($order->discount_promo > 0)
                                                <tr>
                                                <td class="field-total" colspan="3"><b> @lang('messages.Сумма') </b></td>
                                                <td><b>{{ to_currency($order->total_price_discounted_without_promo,true) }}</b></td>
                                            </tr>
                                            <tr>
                                                <td class="field-total" colspan="3"><b> @lang('messages.С учетом промокода') (-{{ $order->discount_promo }}%)</b></td>
                                                <td><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                                            </tr>
                                            @else
                                            <tr>
                                                <td class="field-total" colspan="3"><b> @lang('messages.Итого к оплате') </b></td>
                                                <td><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td class="field-total" colspan="3">
                                                    @if (isset($order->shippingType) && $order->shippingType)
                                                        <b> @lang('messages.Доставка') </b> {{ isset($order->shippingType) ? $order->shippingType->name : '' }}, {{ isset($order->shippingMethod) ? $order->shippingMethod->name  : '' }}</br>
                                                        {{$order->shipping_city}},
                                                    @endif
                                                    @if (isset($order->shippingMethod) && $order->shippingMethod)
                                                        @if ($order->shippingMethod->method == 'warehouse')
                                                            {{ $order->shipping_department }}
                                                        @endif
                                                        @if ($order->shippingMethod->method == 'postman')
                                                            {{$order->shipping_address}}
                                                        @endif
                                                    @endif
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="field-total" colspan="3">
                                                    @if (isset($order->paymentType) && $order->paymentType)
                                                        <b> @lang('messages.Оплата') </b> {{ isset($order->paymentType) ? $order->paymentType->name : '' }}
                                                    @endif
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="account-subscribe__status">
                                    <div class="table__product">
                                        <div class="table__product-info">
                                            <p class="text-center"> @lang('messages.Статус заказа') </p>
                                            <p class="account-subscribe__product-status">
                                                <span class="label
                                                    @if ($order->status == 'completed') label-success @endif
                                                    @if ($order->status == 'active') label-primary @endif
                                                    @if ($order->status == 'pending') label-warning @endif
                                                    @if ($order->status == 'canceled') label-danger @endif">&nbsp;</span>&nbsp;
                                                <span>{{ __('messages.order_'.$order->status) }}</span>
                                            </p>
                                            <p class="account-subscribe__product-status">
                                                <span class="label
                                                    @if ($order->shipping_status == 'completed') label-success @endif
                                                    @if ($order->shipping_status == 'active') label-primary @endif
                                                    @if ($order->shipping_status == 'pending') label-warning @endif">&nbsp;</span>&nbsp;
                                                <span>{{ __('messages.shipping_'.$order->shipping_status) }}</span>
                                            </p>

                                            <p class="account-subscribe__product-status">
                                                @if ($order->payment)
                                                <span class="label
                                                    @if ($order->payment->status == 'success') label-success
                                                    @elseif ($order->payment->status == 'error' || $order->payment->status == 'failure') label-danger
                                                    @elseif ($order->payment->status == 'pending') label-warning
                                                    @elseif ($order->payment->status == 'processing') label-primary
                                                    @else label-warning @endif">&nbsp;
                                                </span>&nbsp;
                                                <span>{{ __('messages.payment_'.$order->payment->status) }}</span>
                                                @else
                                                    <span class="label label-danger">&nbsp;
                                                    </span>&nbsp;
                                                    <span>{{ __('messages.payment_notpayed') }}</span>
                                                @endif


                                            </p>
                                            <p class="telephone">
                                                @lang('messages.Уточнить детали вы можете по телефону:')
                                                <b><p>
                                                    {{setting('site.phoneNumber')}}
                                                    <img class="info-img" src="{{asset('images/i.svg')}}" alt="info" data-toggle="tooltip" data-placement="top" title="" data-original-title="@lang('messages.Звонки бесплатные для любых операторов Украины.')">
                                                </p></b>

                                            </p>
                                            {{--<p class="auto-buy">Делать автоматическую покупку<img class="info-img" src="assets/images/r-pinfo-icon.svg" alt="info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Новинки"></p>--}}
                                            {{--<p class="count-days text-center">--}}
                                                {{--<select class="count-days__select">--}}
                                                    {{--<option value="0">через 30 дней</option>--}}
                                                    {{--<option value="1">через 12 дней</option>--}}
                                                {{--</select>--}}
                                            {{--</p>--}}
                                            {{--<div class="checkbox">--}}
                                                {{--<label class="checkbox-wrap">Подписаться на товар--}}
                                                    {{--<input type="checkbox" class="checkbox__input">--}}
                                                    {{--<span class="checkbox__text checkmark"></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                            <a href="{{ route('account.order.renew', ['order' => $order->id]) }}" class="account-subscribe__repeat-link"> @lang('messages.Повторить заказ') </a>
                                            {{--<a href="" class="account-subscribe__lost-review">Оставить заказ</a>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection