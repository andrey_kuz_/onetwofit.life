@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="account account-featured">
                @include('account.menu')
                <div class="col-sm-10 account__page account-featured__page">
                    <div class="account__title account-featured__title"> @lang('messages.Избранное')  <img src="{{ asset('images/love.svg') }}" alt="featured"></div>
                    <div class="search-page">

                        <div class="search-page__row">
                            <form method="get" action="{{ route('favorites') }}" id="formFav">
                                <label for="price"> @lang('messages.Сортировать по') </label>
                                <select class="select-search" name="sort_by" id="sortPrice" onchange="$('#formFav').submit();">
                                    <option value="price_asc" @if ($sort_by == 'price_asc') selected @endif> @lang('messages.возрастанию цены') </option>
                                    <option value="price_desc" @if ($sort_by == 'price_desc') selected @endif> @lang('messages.убыванию цены') </option>
                                </select>
                            </form>
                        </div>


                        <div class="gridcontainer cols-seven favorite-padding">
                            @foreach($favorites as $favorite)
                                @include('products.itemBlockMin', ['itemProduct'=>$favorite])
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('cart.modal')
    @include('oneclick.modal')
@endsection
