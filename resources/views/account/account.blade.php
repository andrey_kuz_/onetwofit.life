@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="account">
            @include('account.menu')
            <div class="col-sm-10 account__page account-personal">
                <div class="account__title">{{__('messages.account_title')}}</div>
                <div class="account__info">

                    <div class="account_data">
                        <div class="title">@lang('messages.Имя')</div>
                        <p class="data">{{ $user->name  }}</p>
                    </div>
                    <div class="account_data">
                        <div class="title">@lang('messages.Фамилия')</div>
                        <p class="data">{{ $user->lastname  }}</p>
                    </div>
                    <div class="account_data">
                        <div class="title">@lang('messages.email')</div>
                        <p class="data">{{ $user->email  }}</p>
                    </div>
                    <div class="account_data">
                        <div class="title">@lang('messages.Телефон')</div>
                        <p class="data">{{ $user->phone  }}</p>
                    </div>
                    <div class="account_data">
                        <div class="title">@lang('messages.Адрес для доставки')</div>
                        <p class="data">
                            {{ $user->shipping_city  }} {{  $user->shipping_street  ? ', '.$user->shipping_street  : '' }} {{ $user->shipping_house }} {{  $user->shipping_room  ? ' / '.$user->shipping_room  : '' }}
                        </p>
                    </div>

                </div>
                <div class="account__link">
                    <a href="{{ route('account.edit') }}" class="account__link-edit"> @lang('messages.Редактировать личные данные') </a>
                </div>
                <div class="account__link">
                    <a href="{{ route('account.editpassword') }}" class="account__link-edit"> @lang('messages.Редактировать пароль') </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection