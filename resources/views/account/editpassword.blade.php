@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="account account-subscribe">
                @include('account.menu')
                <div class="col-sm-10 account__page account-personal">
                    <div class="account__title account-subscribe__title"> @lang('messages.Изменить пароль') </div>

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form class="usereditform" role="form" method="POST" action="{{ route('account.updatepassword') }}">
                        {{ csrf_field() }}
                        <div class="usereditform-column">
                            <div class="registration-page__row userdata-edit__rowСу">
                                <input type="password" name="password_old" id="old-password" class="registration__input" value="">
                                <label for="old-password" class="registration__label"> @lang('messages.Старый пароль') </label>
                            </div>
                            <div class="registration-page__row userdata-edit__rowСу">
                                <input type="password" name="password" id="new-password" class="registration__input" value="">
                                <label for="new-password" class="registration__label"> @lang('messages.Новый пароль') </label>
                            </div>
                            <div class="registration-page__row userdata-edit__row">
                                <input type="password" name="password_confirmation" id="confirm-password" class="registration__input"  value="">
                                <label for="confirm-password" class="registration__label"> @lang('messages.Подтвердить пароль') </label>
                            </div>
                        </div>
                        <input type="submit" value="@lang('messages.Сохранить')" class="default-btn">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
