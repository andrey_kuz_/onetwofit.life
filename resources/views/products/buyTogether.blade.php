@if(count($buyTogethers) > 4 )
<div class="slider-category with-product-slider">
    <div class="slider-category__title">
        <span> @lang('messages.С этим товаром покупают') </span>
    </div>
    <div class="with-product-slider__arrow slider-arrow">
            <span class="with-product-slider__arrow-left slider-arrow__left">
                <span class='arrow-wrap'>
                    <i class='fa fa-long-arrow-left' aria-hidden='true'></i>
                </span>
            </span>
        <span class="with-product-slider__arrow-right slider-arrow__right">
                <span class='arrow-wrap'>
                    <i class='fa fa-long-arrow-right' aria-hidden='true'></i>
                </span>
            </span>
    </div>
    <div class="slider-category__content">
        <div class="owl-carousel owl-theme customflex">
            @foreach($buyTogethers as $buyTogether)
                @include('products.itemBlock', ['itemProduct'=>$buyTogether])
            @endforeach
        </div>
    </div>
</div>
@elseif(count($buyTogethers) > 0)
    <div class="slider-category with-product">
        <div class="slider-category__title">
            <span> @lang('messages.С этим товаром покупают') </span>
        </div>
        <div class="noslider-container">
            @foreach($buyTogethers as $buyTogether)
                @include('products.noslider', ['itemProduct'=>$buyTogether])
            @endforeach
        </div>
    </div>
@endif