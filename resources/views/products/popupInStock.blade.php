<div id="instock-modal" class="instock-modal modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <span class="close-circle"></span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">

                <div class="instock-modal__text">
                    <p>
                        Мы сообщим вам, когда товар будет доступен к заказу
                    </p>
                </div>



                <button type="button" data-dismiss="modal" class="btn-ok">
                    ok
                </button>

            </div>
        </div>
    </div>
</div>
