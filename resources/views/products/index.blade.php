@extends('layouts.app')

@section('content')
@include('products.micromarking')
@include('categories.micromarking_bread')
    <ul class="breadcrumbs">
        @include('categories.breadcrumb', ['section' => $product->getFirstCategory(), 'is_leaf' => 0, 'sectionRoute' => 'category.index'])
        <li class="breadcrumbs__item">
            <a class="breadcrumbs__link active" href="{{ route('product.index', ['id'=>$product->id, 'slug'=> $product->slug]) }}">
                {{ $product->getTranslatedAttribute('name') }}
            </a>
        </li>
    </ul>
    <div class="some-product">
        @if ($product->discount)
            <span class="some-product__super"> @lang('messages.супер цены') </span>
        @elseif($product->is_finished)
            <span class="some-product__notavailable"> @lang('messages.нет в наличии') </span>
        @elseif ($product->is_new)
            <span class="some-product__newgoods"> @lang('messages.новинка') </span>
        @elseif ($product->is_hit)
            <span class="some-product__hit"> @lang('messages.хит') </span>
        @elseif ($product->is_bestseller)
            <span class="some-product__bestseller"> @lang('messages.бестселлер') </span>
        @else

        @endif
        <div class="some-product-thumbnails-slider">
            <div class="some-product__actions">
                <div class="some-product__featured">
                    <a href="{{ route('favorites.toggle', ['product' => $product->id]) }}"
                       class="some-product__actions-link">
                        @include('products.heart', ['itemProduct' => $product])
                        <span> @lang('messages.Избранное') </span>
                    </a>
                </div>
                <div class="some-product__compare">
                    <a href="#" class="some-product__actions-link"
                       onclick="oneClickСompareInit('{{ route('compare.add', ['product' => $product->id]) }}'); return false;"
                       data-toggle="modal" data-target="#add-compare-click">
                        <img src="{{ asset('images/compare.svg') }}" alt="love">
                             <span class="number">
                             @if($compare_count > 0)
                                 {{ $compare_count }}
                             @endif
                             </span>

                        <span> @lang('messages.Сравнить') </span>
                    </a>
                </div>
            </div>
            <div class="owl-carousel" data-slider-id="1">
                @if($mainImage=$product->getMainImg())
                    <div class="some-product__item item">
                        <a href="@if( !filter_var($mainImage, FILTER_VALIDATE_URL)){{ Voyager::image( $mainImage ) }}@else{{ $mainImage }}@endif"
                           data-lightbox="example-set">
                            <img src="@if( !filter_var($mainImage, FILTER_VALIDATE_URL)){{ Voyager::image( $mainImage ) }}@else{{ $mainImage }}@endif"
                                alt="slider" class="item__image item__image-first">
                        </a>
                    </div>
                @else
                    <div class="some-product__item item">
                        <a href="{{ asset( 'images/bitmap.png')}}"
                           data-lightbox="example-set">
                            <img src="{{ asset( 'images/bitmap.png')}}" alt="slider" class="item__image">
                        </a>

                    </div>
                @endif
                @if($otherImages=$product->getOtherImg())
                    @foreach($otherImages as $image)
                        <div class="some-product__item item">
                            <a href="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif"
                               data-lightbox="example-set">
                                <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif"
                                    alt="slider" class="item__image">
                            </a>
                        </div>
                    @endforeach
                @endif

            </div>
            <div class="owl-thumbs" data-slider-id="1">
                @if($mainImage)
                    <button data-lightbox="example-set" class="owl-thumb-item"
                            style="background-image: url( @if( !filter_var($mainImage, FILTER_VALIDATE_URL)){{ Voyager::image( $mainImage ) }}@else{{ $mainImage }}@endif )"></button>
                @endif

                @if($otherImages)
                    @foreach($otherImages as $image)
                        <button data-lightbox="example-set" class="owl-thumb-item"
                                style="background-image: url( @if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif )"></button>

                    @endforeach
                @endif

            </div>
            <div class="some-product-thumbnails__social">
                <img src="{{ asset( 'images/social-share-symbol.svg') }}" alt="share"><span> @lang('messages.Поделиться')
                    : </span>

                {!! setting('site.socialCode') !!}
                {{--<script type="text/javascript">(function(w,doc) {--}}
                {{--if (!w.__utlWdgt ) {--}}
                {{--w.__utlWdgt = true;--}}
                {{--var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';--}}
                {{--s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;--}}
                {{--s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';--}}
                {{--var h=d[g]('body')[0];--}}
                {{--h.appendChild(s);--}}
                {{--}})(window,document);--}}
                {{--</script>--}}
                {{--<div data-mobile-view="true" data-share-size="20" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1784119" data-mode="share" data-background-color="#ffffff" data-share-shape="round-rectangle" data-share-counter-size="12" data-icon-color="#ffffff" data-mobile-sn-ids="fb.vk.tw.ok.wh.vb.tm." data-text-color="#000000" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="false" data-sn-ids="fb.gp.vk.ps.em.tm." data-preview-mobile="false" data-selection-enable="true" data-exclude-show-more="true" data-share-style="1" data-counter-background-alpha="1.0" data-top-button="false" class="uptolike-buttons" ></div>--}}

                {{--<script type="text/javascript">(function(w,doc) {--}}
                {{--if (!w.utlWdgt ) {--}}
                {{--w.utlWdgt = true;--}}
                {{--var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';--}}
                {{--s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;--}}
                {{--s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';--}}
                {{--var h=dg[0];--}}
                {{--h.appendChild(s);--}}
                {{--}})(window,document);--}}
                {{--</script>--}}
                {{--<div data-mobile-view="true" data-share-size="20" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1784115" data-mode="share" data-background-color="#ffffff" data-share-shape="round-rectangle" data-share-counter-size="12" data-icon-color="#ffffff" data-mobile-sn-ids="fb.vk.tw.ok.wh.vb.tm." data-text-color="#000000" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="false" data-sn-ids="fb.gp.vk.ps.em.tm." data-preview-mobile="false" data-selection-enable="true" data-exclude-show-more="true" data-share-style="1" data-counter-background-alpha="1.0" data-top-button="false" class="uptolike-buttons" ></div>--}}

            </div>
        </div>
        <div class="some-product__info">
            <div class="some-product__articul"> @lang('messages.Артикул') {{ $product->article }}</div>
            <h1 class="some-product__title">{{ $product->getTranslatedAttribute('name') }}</h1>
            <div class="some-product__rating">
                @include('reviews.stars', ['ratingValue' => $product->rating])
                <span class="rating-count">{{$product->rating_count}}</span>
            </div>
            <div class="some-product-help-block">
                <div class="some-product__status">
                @if ($product->checkAvailable())

                    <!--Individual order-->
                        @if ($product->is_indiv)
                            <p id="order-individual-status">
                                <span class="some-product__status-individual-order">
                                    <svg width="18" height="20" viewBox="0 0 18 20" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.6497 10.1577L9.73145 10.6764V20L18.0002 15.3403V6.0166L10.6497 10.1577Z"
                                              fill="#EE0058"/>
                                        <path d="M12.1461 1.79668L8.96612 0L0.467773 4.78838L3.652 6.58506L12.1461 1.79668Z"
                                              fill="#EE0058"/>
                                        <path d="M17.46 4.78832L13.7954 2.75098L5.30127 7.53936L5.78592 7.78417L8.96589 9.5767L12.1289 7.79662L17.46 4.78832Z"
                                              fill="#EE0058"/>
                                        <path d="M4.65942 10.9668L3.13746 10.2033V7.82989L0 6.06641V15.3237L8.20926 19.9502V10.693L4.65942 8.69711V10.9668Z"
                                              fill="#EE0058"/>
                                    </svg>
                                    @lang('messages.индивидуальный заказ')
                                </span>
                            </p>
                        @elseif($product->is_finished)
                            <p id="order-ready-status-finished">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" viewBox="0 0 16 15"
                                     fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M7.68937 3.95141L12 0L15.3786 3.68577L11.3891 7.34283L15.3787 11L12.0001 14.6858L7.68937 10.7342L3.37862 14.6858L0 11L3.98964 7.34283L0.000122905 3.68577L3.37875 1.19209e-07L7.68937 3.95141Z"
                                          fill="#EE0058"/>
                                </svg>
                                <span class="some-product__status-individual-order"> @lang('messages.недоступно к заказу') </span>
                            </p>
                        @else
                            <p id="order-ready-status">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <span class="some-product__status-text"> @lang('messages.доступно к заказу') </span>
                            </p>
                        @endif
                    @else
                        <i class="fa fa-remove" aria-hidden="true"></i>
                        <span class="some-product__status-text"> @lang('messages.не доступно к заказу') </span>
                    @endif
                    @if ($params = $product->getParameters())
                        <div class="some-product__characters">
                            <div class="table-responsive">
                                <table class="table">
                                    @foreach($params as $paramName=>$paramValue)
                                        @if ($paramValue)
                                            <tr>
                                                <td>{{ __('messages.' . $paramName)}}</td>
                                                @if( (__('messages.' . $paramName) == "Бренд" ))
                                                    <td>
                                                        <a href="{{ route('search')}}?search_brand={{$paramValue}}">{{ $paramValue }}</a>
                                                    </td>
                                                @elseif(__('messages.' . $paramName) == "Вкус" )
                                                    <td>
                                                        <a href="{{ route('search')}}?search_flavors={{$paramValue}}">{{ $paramValue }}</a>
                                                    </td>
                                                @else
                                                    <td>{{ $paramValue }}</td>
                                                @endif
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    @endif

                    @if ($tags)
                        <div class="some-product__tags">
                            <div class="some-product__tags-title"> @lang('messages.Теги'):</div>
                            @foreach($tags as $tag)
                                <a href="{{ route('tag.index', ['slug' => $tag->slug]) }}"
                                   class="color-tag">{{ $tag->name }}</a>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="some-product__price">
                    <span class="some-product__price-old">{{($product->discount > 0) ?  $product->price_converted : ''}}</span>
                    @if ($product->discount)<span class="some-product__sales">-{{$product->discount}}%</span>@endif
                    <div class="some-product__price-current {{$product->is_finished ? 'finished' : ''}} {{($product->discount) ? 'is_discount' : '' }}">
                        <span class="price_text">@lang('messages.Цена'):</span>
                        <span class="price_value">{{$product->price_discounted_converted['value'] }}</span>
                        <span class="price_currency">{{ $product->price_discounted_converted['currency']}}</span>
{{--                        {{$product->price_discounted_converted}}--}}
                    </div>

                    @include('products.special')

                    @if ($product->checkAvailable())
                        <div class="some-product__buttons ">
                            @if(!$product->is_finished)
                                <a href="#" class="add-to-cart addToCart"
                                   onclick="addToCartEvent('{{ route('cart.addAjax', ['product' => $product->id, 'deltaQuantity' => 1, 'isAjax' => true]) }}'); return false;">Добавить
                                    в корзину</a>
                                <a href="#" class="buy-one-click" data-toggle="modal" data-target="#buy-one-click"
                                   onclick="oneClickInit('{{ route('oneclick', ['product' => $product->id]) }}'); return false;">Купить
                                    в 1 клик</a>
                            @else
                                <input type="text" placeholder="Введите свой email">
                                <a href="#" data-toggle="modal" class="add-to-cart addToCart subcribe"
                                   onclick="subcribe('{{ route('product.subscription', ['id' => $product->id, 'isAjax' => true]) }}'); return false;">Сообщить
                                    о наличии</a>
                            @endif
                            <span class="some-product__status-individual-order subcribe"></span>
                        </div>
                    @endif
                </div>
            </div>
            @if($comboOffer)
                @include('products.combo')
            @endif
        </div>
    </div>

    <div class="product-info-full">
        <div class="col-sm-6 col-xs-12 description-product">
            <h2 class="description-product__title">Описание <span class="description-product__title__product">{{ $product->getTranslatedAttribute('name') }}</span> </h2>
            <div class="description-product__content">{!!html_entity_decode($product->description)!!}</div>
        </div>
        <div class="col-sm-6 col-xs-12 compose-product">
            <h2 class="description-product__title">Состав <span class="description-product__title__product">{{ $product->getTranslatedAttribute('name') }}</span></h2>
            <div class="table-responsive">
            {!!html_entity_decode($product->consist)!!}

            <!-- <p id="all-tr-maximize" class="compose-product__link"><span>Подробнее<img src="{{ asset('images/arrow-down-icon.png') }}"></span></p>-->
                @if (count($characteristics))

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td><b>Характеристика</b></td>
                            <td><b>Значение</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($characteristics as $characteristic)
                            <tr>
                                <td>{{ $characteristicsName[$characteristic->characteristic_id]->name }}</td>
                                <td>{{ str_replace('.',',',$characteristic->value) }} {{ $characteristicsName[$characteristic->characteristic_id]->unit }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
            <button class="show-more">Подробнее <img src="{{ asset('images/arrow-down-icon.png') }}" style="vertical-align: middle"></button>
        </div>

    </div>

    @if(!empty($product->video_iframe))
        @include('products.video')
    @endif

    @if(!empty($withProducts))
        @include('products.with')
    @endif


    @if(!empty($analogProducts))
        @include('products.buyTogether')
    @endif
    @if(!empty($analogProducts))
        @include('products.analog')
    @endif


    @include('reviews.main')
    @if(!empty($historyProducts))
        @include('products.history')
    @endif
    @include('reviews.modal.modal')
    @include('cart.modal')
    @include('oneclick.modal');
    @include('compare.modal');
    @include('products.popupInStock');
@endsection
@section('scripts')
    <script>
        fbq('track', 'ViewContent',{
            content_type: 'product',
            content_ids: '[{{ $product->id }}]',
            value:'{{$product->price_discounted_converted['value'] }}',
            currency: ' UAH '
        });
    </script>
@endsection
