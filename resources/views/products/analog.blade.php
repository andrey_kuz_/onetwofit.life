@if(count($analogProducts) > 4 )
<div class="slider-category maybe-like-slider">
    <div class="slider-category__title">
        <span> @lang('messages.Вам может понравиться') </span>
    </div>
    <div class="maybe-like-slider__arrow slider-arrow">
            <span class="maybe-like-slider__arrow-left slider-arrow__left">
                <span class='arrow-wrap'>
                    <i class='fa fa-long-arrow-left' aria-hidden='true'></i>
                </span>
            </span>
        <span class="maybe-like-slider__arrow-right slider-arrow__right">
                <span class='arrow-wrap'>
                    <i class='fa fa-long-arrow-right' aria-hidden='true'></i>
                </span>
            </span>
    </div>
    <div class="slider-category__content">
        <div class="owl-carousel owl-theme customflex">
            @foreach($analogProducts as $analogProduct)
                @include('products.itemBlock', ['itemProduct'=>$analogProduct])
            @endforeach
        </div>
    </div>
</div>
@elseif(count($analogProducts) > 0)
    <div class="slider-category maybe-like">
        <div class="slider-category__title">
            <span> @lang('messages.Вам может понравиться') </span>
        </div>
        <div class="noslider-container">
            @foreach($analogProducts as $analogProduct)
                @include('products.noslider', ['itemProduct'=>$analogProduct])
            @endforeach
        </div>
    </div>
@endif