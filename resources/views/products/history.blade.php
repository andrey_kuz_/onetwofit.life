@if(count($historyProducts) > 4 )
    <div class="slider-category history-product-slider">
        <div class="slider-category__title">
            <span> @lang('messages.Товары, которыми Вы интересовались') </span>
        </div>
        <div class="history-product-slider__arrow slider-arrow">
            <span class="history-product-slider__arrow-left slider-arrow__left">
                <span class='arrow-wrap'>
                    <i class='fa fa-long-arrow-left' aria-hidden='true'></i>
                </span>
            </span>
            <span class="history-product-slider__arrow-right slider-arrow__right">
                <span class='arrow-wrap'>
                    <i class='fa fa-long-arrow-right' aria-hidden='true'></i>
                </span>
            </span>
        </div>
        <div class="slider-category__content">
            <div class="owl-carousel owl-theme customflex">
                @foreach($historyProducts as $historyProduct)
                    @if ($historyProduct->product)
                        @include('products.itemBlock', ['itemProduct'=>$historyProduct->product])
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@elseif(count($historyProducts) > 0)
    <div class="slider-category history-product">
        <div class="slider-category__title">
            <span> @lang('messages.Товары, которыми Вы интересовались') </span>
        </div>
        <div class="noslider-container">
            @foreach($historyProducts as $historyProduct)
                @if ($historyProduct->product)
                    @include('products.noslider', ['itemProduct'=>$historyProduct->product])
                @endif
            @endforeach
        </div>
    </div>
@endif