<div class="some-product__special">
    <div class="some-product__name"> @lang('messages.Специальное предложение') </div>
    <div class="some-product__special-wrapper">
        <div class="some-product__article">
            <div class="some-product__image">
                @if($image = $comboOffer->product->getMainImg())
                    <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:80px">
                @else
                    <img src="{{ asset( 'images/bitmap.png')}}" alt="product">
                @endif
            </div>
            <div class="some-product__information">
                <div class="some-product__descr">{{str_limit($comboOffer->product->translate()->name, 50)}}</div>
                <div class="some-product__prices">{{to_currency($comboOffer->product->price_discounted, true)}}</div>
            </div>
        </div>
        <div class="some-product__action">+</div>
        <div class="some-product__article">
            <div class="some-product__image">
                @if($imageRel = $comboOffer->relatedProduct->getMainImg())
                    <img src="@if( !filter_var($imageRel, FILTER_VALIDATE_URL)){{ Voyager::image( $imageRel ) }}@else{{ $imageRel }}@endif" style="width:80px">
                @else
                    <img src="{{ asset( 'images/bitmap.png')}}" alt="product">
                @endif
            </div>
            <div class="some-product__information">
                <div class="some-product__descr">{{str_limit($comboOffer->relatedProduct->translate()->name, 50)}}</div>
                <div class="some-product__prices">{{to_currency($comboOffer->relatedProduct->price_discounted, true)}}</div>
            </div>
        </div>
        <div class="some-product__action">=</div>
        <div class="some-product__prices">
            <span class="some-product__price-current special__price-current">{{to_currency($comboOffer->getComboOfferPrice(), true)}}</span>
            <span class="some-product__price-sales">-{{to_currency($comboOffer->getComboOfferDiscount(), true)}}</span>
            <a href="#" class="add-to-cart" onclick="addToCartEvent('{{ route('cart.addCombo', ['combo' => $comboOffer->id, 'isAjax' => true]) }}'); return false;"> @lang('messages.Добавить в корзину') </a>
        </div>
    </div>
</div>