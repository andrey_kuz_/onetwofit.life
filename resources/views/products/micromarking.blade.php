<div class="micromarking">
    <div itemscope itemtype="http://schema.org/Product">
        <div itemprop="name"><h1>{{ $product->getTranslatedAttribute('name') }}</h1></div>
        @if($mainImage=$product->getMainImg())
        <div class="some-product__item item">
            <a itemprop="image" href="@if( !filter_var($mainImage, FILTER_VALIDATE_URL)){{ Voyager::image( $mainImage ) }}@else{{ $mainImage }}@endif">
                <img src="@if( !filter_var($mainImage, FILTER_VALIDATE_URL)){{ Voyager::image( $mainImage ) }}@else{{ $mainImage }}@endif"
                      title="{{ $product->getTranslatedAttribute('name') }}">
            </a>
        </div>
        @else
            <div class="some-product__item item">
                <a itemprop="image" href="{{ asset( 'images/bitmap.png')}}">
                    <img src="{{ asset( 'images/bitmap.png')}}" title="{{ $product->getTranslatedAttribute('name') }}">
                </a>

            </div>
        @endif
        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <meta itemprop="url" content="{{ route('product.index', ['id'=>$product->id, 'slug'=> $product->slug]) }}">
            <meta itemprop="price" content="{{$product->price_discounted_converted['value'] }}">
            <meta itemprop="priceCurrency" content="UAH">
            <meta itemprop="aggregateRating" ratingValue="{{ $product->rating }}">
            <meta itemprop="brand" content="{{ $product->brand->name }}">
            <link itemprop="availability" href="http://schema.org/InStock">
        </div>
        <div itemprop="description">{{ strip_tags(html_entity_decode($product->description)) }}</div>
    </div>
</div>
