<section class="videosection">
    <h2 class="videosection__header">Видео о товаре</h2>

    <div class="adaptive-wrap">
        <div class="videothumb-wrap">
            {!! html_entity_decode($product->video_iframe) !!}
        </div>
    </div>

</section>