@if (!empty($specialPricesInfo))
<div class="some-product__product-bought product-bought">
    <span class="product-bought__arrow-left disabled">
        <span class='arrow-wrap'>
            <i class='fa fa-long-arrow-left' aria-hidden='true'></i>
        </span>
    </span>
    <span class="product-bought__arrow-right">
        <span class='arrow-wrap'>
            <i class='fa fa-long-arrow-right' aria-hidden='true'></i>
        </span>
    </span>
    <div class="owl-carousel owl-theme">
        @foreach($specialPricesInfo as $cnt => $specialPriceInfo)
            <div class="item" onclick="addToCartEvent('{{ route('cart.addAjax', ['product' => $product->id, 'deltaQuantity' => $cnt, 'isAjax' => true]) }}'); return false;">
                <div class="product-bought__wrap">
                    <div class="product-bought__text"> @lang('messages.Приобретите') {{$cnt}} за</div>
                    <div class="product-bought__price">
                        <span class="price_value">{{$specialPriceInfo->toCurrency($specialPriceInfo->getDiscountedPrice()->total)['value'] }}</span> <span class="price_currency">{{ $specialPriceInfo->toCurrency($specialPriceInfo->getDiscountedPrice()->total)['currency']}}</span>

{{--                        <span class="price_value">{{$itemProduct->price_discounted_converted['value'] }}</span> <span class="price_currency">{{ $itemProduct->price_discounted_converted['currency']}}</span>--}}
{{--                        {{to_currency($specialPriceInfo->getDiscountedPrice()->total, true)}} --}}
                        (-{{$specialPriceInfo->getDiscount()}}%)</div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endif