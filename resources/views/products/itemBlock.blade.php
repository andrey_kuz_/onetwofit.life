<div class="slider-category__item product customflex-el" data-hash="">
    <div class="product-help-block">
        <div class="product-top">
            @include('products.heartLink')
            @if ($itemProduct->discount)
                {{--<span class="product__sales">-{{$itemProduct->discount}}%</span>--}}
                <span class="product__super"> @lang('messages.супер цены') </span>
            @elseif( $itemProduct->is_new)
                <span class="product__new">новинка</span>
            @elseif ($itemProduct->is_hit)
                <span class="product__hit">хит</span>
            @elseif ($itemProduct->is_bestseller)
                <span class="product__best">бестселлер</span>
            @else
        
            @endif
        </div>
        <a href="{{ route('product.index', ['id'=>$itemProduct->id, 'slug'=> $itemProduct->slug]) }}">
        <div class="product__image">
            @if($image = $itemProduct->getMainImg())
                <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:190px">

            @else
                <img src="{{ asset( 'images/bitmap.png')}}" alt="product">
            @endif
        </div>
        <div class="product__description">
            {{$itemProduct->getTranslatedAttribute('name')}}
        </div>
        </a>
        <div class="product__rating">
            @include('reviews.stars', ['ratingValue' => $itemProduct->rating])
            <span class="rating-count">{{$itemProduct->rating_count}}</span>
        </div>
        <div class="product__price">
            <div class="product__price-old">{{($itemProduct->discount) ? $itemProduct->price_converted : ''}}</div>
            <div class="product__price-current {{($itemProduct->discount) ? 'is_discount' : '' }}"><span class="price_value">{{$itemProduct->price_discounted_converted['value'] }}</span> <span class="price_currency">{{ $itemProduct->price_discounted_converted['currency']}}</span></div>
        </div>
        <div class="product__buttons">
            <a href="#" class="product__add" onclick="addToCartEvent('{{ route('cart.addAjax', ['product' => $itemProduct->id, 'deltaQuantity' => 1, 'isAjax' => true]) }}'); return false;"> @lang('messages.Добавить в корзину') </a>
            <a href="#" class="product__buy" data-toggle="modal" data-target="#buy-one-click" onclick="oneClickInit('{{ route('oneclick', ['product' => $itemProduct->id]) }}'); return false;"> @lang('messages.Купить в один клик') </a>
        </div>
    </div>
</div>