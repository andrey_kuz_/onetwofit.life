@if ($itemProduct->myFavorite->count())
    <img src="{{ asset( 'images/love-full.svg')}}" alt="like" class="product__like">
@else
    <img src="{{ asset( 'images/love.svg')}}" alt="like" class="product__like">
@endif