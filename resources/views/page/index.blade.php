<?php
    function mb_ucfirst($text) {
        return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }
?>

@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="account">
                <div class="col-sm-3 account__sidebar">
                    <ul class="account-list">
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'faq']) }}" class="account-list__link {{ Request::is('page/faq') ? 'account-list__link-active' : '' }}">
                                {{ strtoupper(trans('messages.faq')) }}
                            </a>
                        </li>
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'return']) }}" class="account-list__link {{ Request::is('page/return') ? 'account-list__link-active' : '' }}">
                                {{ mb_ucfirst(trans('messages.возврат') ) }}
                            </a>
                        </li>
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'delivery']) }}" class="account-list__link {{ Request::is('page/delivery') ? 'account-list__link-active' : '' }}">
                                {{ mb_ucfirst( trans('messages.доставка') ) }}
                            </a>
                        </li>
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'confident']) }}" class="account-list__link {{ Request::is('page/confident') ? 'account-list__link-active' : '' }}">
                                {{ mb_ucfirst( trans('messages.политика конфиденциальности') ) }}
                            </a>
                        </li>
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'agree']) }}" class="account-list__link {{ Request::is('page/agree') ? 'account-list__link-active' : '' }}">
                                {{ mb_ucfirst( trans('messages.пользовательское соглашение') ) }}
                            </a>
                        </li>
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'company']) }}" class="account-list__link {{ Request::is('page/company') ? 'account-list__link-active' : '' }}">
                                {{ mb_ucfirst( trans('messages.история компании') ) }}
                            </a>
                        </li>
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'whyonetwofit']) }}" class="account-list__link {{ Request::is('page/whyonetwofit') ? 'account-list__link-active' : '' }}">
                                {{ mb_ucfirst( trans('messages.почему onetwofit') ) }}
                            </a>
                        </li>
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'benefits']) }}" class="account-list__link {{ Request::is('page/benefits') ? 'account-list__link-active' : '' }}">
                                {{ mb_ucfirst( trans('messages.зарабатывай на покупках') ) }}
                            </a>
                        </li>
                        <li class="account-list__item">
                            <a href="{{ route('page', ['slug' => 'reviews']) }}" class="account-list__link {{ Request::is('page/reviews') ? 'account-list__link-active' : '' }}">
                                {{ mb_ucfirst( trans('messages.отзывы о магазине') ) }}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-9 account__page account-personal">
                    <div class="account__title">{{ mb_ucfirst( $page->title ) }}</div>
                    <div class="single-textcont">
                        {!! $page->body !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection