<div id="searchContent">
    <div class="gridcontainer cols-five bottom_pricepadding">

        @foreach($specialProducts as $product)
            @include('products.itemBlockGrid', ['itemProduct'=>$product])
        @endforeach
    </div>

    {{ $specialProducts->links('vendor.front_pagination.custom') }}
</div>

