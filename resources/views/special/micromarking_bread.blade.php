@php($i = 2)
<div class="micromarking">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="{{ route('home') }}">
            <span itemprop="name">@lang('messages.Главная')</span></a>
            <meta itemprop="position" content="1" />
        </li>
        @if($type && $type != 'all')
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{{ route($sectionRoute) . '/all'}}">
                    <span itemprop="name">@lang('messages.Специальное предложение')</span></a>
                <meta itemprop="position" content="{{ $i }}" />
                @php($i++)
            </li>
        @endif
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="{{ route($sectionRoute) . '/' . $type}}">
                <span itemprop="name">{{ $section }}</span></a>
            <meta itemprop="position" content="{{ $i }}" />
        </li>
     </ol>
</div>