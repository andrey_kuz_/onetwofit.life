@if (isset($section))

    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/"> @lang('messages.Главная') </a><span class="breadcrumbs__icon">/</span> </li>

    @if($type && $type != 'all')
        <li class="breadcrumbs__item"><a class="breadcrumbs__link @if (!$section) active @endif" href="{{ route($sectionRoute) . '/all'}}"> @lang('messages.Специальное предложение') </a><span class="breadcrumbs__icon">/</span> </li>
    @endif
    <li class="breadcrumbs__item"><a class="breadcrumbs__link @if ($section) active @endif" href="{{ route($sectionRoute) . '/' . $type}}">{{ $section }}</a></li>
@endif
