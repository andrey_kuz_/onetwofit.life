@extends('layouts.app')

@section('content')
    @include('special.micromarking_bread')

    <ul class="breadcrumbs">
        @include('special.breadcrumb', ['section' => $section, 'is_leaf' => 1])
    </ul>

    <div class="categories-page special_offer">
        @if (isset($section) && $section)
            <h1 class="categories-page__name">{{ $section }}</h1>
        @endif
        @include('categories.filters')

        <div class="search-page__row col-md-9 col-sm-8 col-xs-12">
            <form id="sortForm">
                <label for="price"> @lang('messages.Сортировать по') </label>
                <select class="select-search" name="sort_by" id="price" onchange="filterProducts();">
                    <option value="rating_desc"> @lang('messages.рейтингу') </option>
                    <option value="price_asc"> @lang('messages.возрастанию цены') </option>
                    <option value="price_desc"> @lang('messages.убыванию цены') </option>
                    <option value="newest"> @lang('messages.по новизне') </option>
                </select>
            </form>

            @include('special.content')

        </div>
    </div>

    @include('cart.modal')
    @include('oneclick.modal')
    @include('compare.modal')


    @include('products.history')
@endsection


