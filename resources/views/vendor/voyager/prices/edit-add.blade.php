@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', 'Edit prices')

@section('page_header')
    <h1 class="page-title">
        Edit prices
    </h1>
@stop


@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ route('voyager.product.updateprices', $product->id) }}"
                            method="POST" enctype="multipart/form-data">


                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="form-group  col-md-12">
                                <label for="name">от 2х</label>
                                <input class="form-control" name="discount2" placeholder="" value="{{$product->discount2}}" type="text">
                            </div>
                            <div class="form-group  col-md-12">
                                <label for="name">от 3х</label>
                                <input class="form-control" name="discount3" placeholder="" value="{{$product->discount3}}" type="text">
                            </div>
                            <div class="form-group  col-md-12">
                                <label for="name">от 4х</label>
                                <input class="form-control" name="discount4" placeholder="" value="{{$product->discount4}}" type="text">
                            </div>
                            <div class="form-group  col-md-12">
                                <label for="name">от 5ти</label>
                                <input class="form-control" name="discount5" placeholder="" value="{{$product->discount5}}" type="text">
                            </div>
                            <div class="form-group  col-md-12">
                                <label for="name">от 6ти</label>
                                <input class="form-control" name="discount6" placeholder="" value="{{$product->discount6}}" type="text">
                            </div>
                            <div class="form-group  col-md-12">
                                <label for="name">от 7ми</label>
                                <input class="form-control" name="discount7" placeholder="" value="{{$product->discount7}}" type="text">
                            </div>
                            <div class="form-group  col-md-12">
                                <label for="name">от 8ми</label>
                                <input class="form-control" name="discount8" placeholder="" value="{{$product->discount8}}" type="text">
                            </div>
                            <div class="form-group  col-md-12">
                                <label for="name">от 9ти</label>
                                <input class="form-control" name="discount9" placeholder="" value="{{$product->discount9}}" type="text">
                            </div>
                            <div class="form-group  col-md-12">
                                <label for="name">от 10</label>
                                <input class="form-control" name="discount10" placeholder="" value="{{$product->discount10}}" type="text">
                            </div>




                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@stop


