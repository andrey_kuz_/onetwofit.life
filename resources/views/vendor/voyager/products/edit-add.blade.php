@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = $row->details;
                                    $display_options = isset($options->display) ? $options->display : NULL;

                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif


                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else

                                    <div class="form-group col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>

                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')

                                            @include('vendor.voyager.products.relationship')
                                        @else
                                            @if (isset($display_options->disabled) && $display_options->disabled)
                                                <fieldset disabled>
                                            @endif

                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @if (isset($display_options->disabled) && $display_options->disabled)
                                                </fieldset>
                                            @endif
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach

                                    </div>
                                @endif


                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>
                    <form id="photo_form" action="{{ route('voyager.product.saveImages', $dataTypeContent->getKey()) }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        {{--<input name="image" id="upload_file" type="file" accept="image/*">--}}
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

        var params = {}
        var $image

        $('document').ready(function () {

            let arr = window.location.pathname.split('/');

            if(arr[2] == 'products' && arr[4] == 'edit'){

                let img_upload = $('.form-edit-add').find('input[type=file]');
                img_upload.attr('id', 'uploads_images');
                img_upload.css('clear', 'both');
                $('#uploads_images').parent().attr('id', 'sort_images');

                let cleardiv = $('#sort_images').find('.clearfix');
                cleardiv.css('display', 'none');

                $(function () {
                    $("#sort_images").sortable({
                        items: 'div',
                        stop: function() {
                            setTimeout(
                                function(){
                                    let product_id = '{{$dataTypeContent->getKey()}}';
                                    let images = $("#sort_images").find('.img_settings_container img');
                                    let images_value = [];
                                    for (let i = 0; i < images.length; i++) {
                                        images_value[i] = images[i].getAttribute('data-image');
                                    }
                                    $.ajax({
                                        url: window.location.origin+'/admin/products/sort_images',
                                        type: 'POST',
                                        data : {id: product_id, images: images_value}
                                    });
                                },
                                200
                            )
                        }
                    });
                });

               $('body').on('change', '#uploads_images', function (e) {
                   e.preventDefault();

                   let clearfix = $(this).prev();
                   clearfix.before($('<div class="img_settings_container" data-field-name="image"><img id="file_input" src="" width="200"><a href="#" class="voyager-x remove-multi-image"></a></div>'));
                   $('#file_input').css({'clear':'both', 'display':'block', 'padding':'2px', 'border':'1px solid #ddd', 'margin-bottom':'5px'});
                   $('#file_input').parent().css({'float':'left', 'padding-right':'15px'});
                   let fileInput = this;

                   if (fileInput && fileInput.files[0]) {
                       let reader = new FileReader();
                       reader.onload = function(e){
                           let new_id = $('#file_input').attr('id', "file_input"+fileInput.files[0].lastModified);
                           $(new_id).attr('src', e.target.result);
                       }
                       reader.readAsDataURL(fileInput.files[0]);
                   }

                   $('#photo_form').append(fileInput);
                   clearfix.after('<input type="file" name="image[]" multiple="multiple" accept="image/*" id="uploads_images" style="clear: both;">');

                   let form_data = new FormData();
                   let file_data = fileInput.files[0];
                   form_data.append("image[]", file_data);
                   let slug = $('#photo_form').find('#type_slug').val();
                   form_data.append("type_slug", slug);
                   let token = $('#photo_form').find('input[name=_token]').val();
                   form_data.append("_token", token);

                   $.ajax({
                       url: $('#photo_form').attr('action'),
                       cache: false,
                       contentType: false,
                       processData: false,
                       type: 'POST',
                       data : form_data,
                       success: function(response){
                           let new_img = clearfix.prev('.img_settings_container').find('img');
                           new_img.removeAttr('id');
                           new_img.attr('src', response.src);
                           new_img.attr('data-image', response.image);
                           new_img.attr('data-id', response.product_id);

                           $('#photo_form').find('input[type=file]').remove();
                       }
                   });
               });
            }

            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {

                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
