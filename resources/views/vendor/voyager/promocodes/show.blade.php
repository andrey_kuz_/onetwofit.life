<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <tbody>
            <tr class="">
                <td>Создан</td>
                <td>Покупатель</td>
                <td>E-mail</td>
                <td>Город</td>
                <td>Валюта</td>
                <td>Курс</td>
                <td>Статус заказа</td>
                <td>Статус доставки</td>
                <td>Статус оплаты</td>
                <td>Товаров</td>
                <td>К оплате (без промокода)</td>
                <td>К оплате (с учетом промокода)</td>
            </tr>
            @foreach($orders as $order)
                <tr class="">
                    <td>{{ $order->created_at }}</td>
                    <td>{{ $order->shipping_name }}</td>
                    <td>{{ $order->shipping_email }}</td>
                    <td>{{ $order->shipping_city }}</td>
                    <td>{{ $order->currency }}</td>
                    <td>{{ $order->currency_rate }}</td>
                    <td><span class="badge
                                                    @if ($order->status == 'completed') badge-success @endif
                        @if ($order->status == 'active') badge-primary @endif
                        @if ($order->status == 'pending') badge-warning @endif
                        @if ($order->status == 'canceled') badge-danger @endif">{{ __('messages.order_'.$order->status) }}</span>
                    </td>
                    <td><span class="badge
                                                    @if ($order->shipping_status == 'completed') badge-success @endif
                        @if ($order->shipping_status == 'active') badge-primary @endif
                        @if ($order->shipping_status == 'pending') badge-warning @endif">{{ __('messages.shipping_'.$order->shipping_status) }}</span>
                    </td>
                    <td>@if ($order->payment)
                            <span class="badge
                                                    @if ($order->payment->status == 'success') badge-success
                                                    @elseif ($order->payment->status == 'error' || $order->payment->status == 'failure') badge-danger
                                                    @elseif ($order->payment->status == 'pending') badge-warning
                                                    @elseif ($order->payment->status == 'processing') badge-primary
                                                    @else badge-warning @endif">{{ __('messages.payment_'.$order->payment->status) }}</span>
                        @else
                            <span class="badge badge-danger">{{ __('messages.payment_notpayed') }}</span>&nbsp;
                        @endif
                    </td>
                    <td>{{ $order->item_count }}</td>
                    <td>{{to_currency($order->total_price_discounted_without_promo, true)}}$ ({{ to_currency($order->total_price_discounted_without_promo, true) }})</td>
                    <td>{{to_currency($order->total_price_discounted, true)}}$ ({{ to_currency($order->total_price_discounted, true) }})</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>