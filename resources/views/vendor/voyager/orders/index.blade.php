@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        @can('add',app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete',app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit',app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')

<div class="account-subscribe__content">
    @foreach($orders as $order)
        <div class="account-subscribe__row account-subscribe__row-open">
            <div class="account-subscribe__icon">

            </div>
            <div class="account-subscribe__order-info">
                <span>Номер заказа: <br> {{ $order->id }}</span>
                <span>Дата заказа: <br> {{ $order->created_at }}</span>
                <p class="account-subscribe__product-status">
                    <span class="circle orange"></span><span>Товар в пути</span>
                </p>
            </div>
            <div class="account-subscribe-wrapper account-subscribe-wrapper__open">

                <div class="account-subscribe__viewport">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            @foreach($order->orderItems as $orderItem)
                                <tr class="table-image__wrap">
                                    <td class="table-image">
                                        @if($image=$orderItem->product->getMainImg())
                                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:30px">

                                        @else
                                            <img src="{{ asset( 'images/bitmap.png')}}" alt="product" style="width:30px">
                                        @endif
                                    </td>
                                    <td>
                                        {{ str_limit($orderItem->product->getTranslatedAttribute('name'), 50) }}
                                    </td>
                                    <td>{{ $orderItem->quantity }}</td>
                                    <td>{{to_currency( $order->toCurrencyFormat($orderItem->total_price_discounted), true) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td class="field-total" colspan="3">
                                    Доставка {{ $order->shippingType ? $order->shippingType->name : '' }}, {{ $order->shippingMethod ? $order->shippingMethod->name  : '' }}</br>
                                    {{$order->shipping_city}},
                                    @if ($order->shippingMethod->method == 'warehouse')
                                        {{ $order->shipping_department }}
                                    @endif
                                    @if ($order->shippingMethod->method == 'postman')
                                        {{$order->shipping_address}}
                                    @endif
                                </td>
                                <td></td>
                            </tr>
                            @if ($order->discount_promo > 0)
                                <tr>
                                    <td class="field-total" colspan="3"><b>Сумма</b></td>
                                    <td><b>{{ to_currency($order->toCurrencyFormat($order->total_price_discounted_without_promo), true) }}</b></td>
                                </tr>
                                <tr>
                                    <td class="field-total" colspan="3"><b>С учетом промокода (-{{ to_currency($order->discount_promo, true) }}%)</b></td>
                                    <td><b>{{ to_currency($order->toCurrencyFormat($order->total_price_discounted), true) }}</b></td>
                                </tr>
                            @else
                                <tr>
                                    <td class="field-total" colspan="3"><b>Итого к оплате</b></td>
                                    <td><b>{{ to_currency($order->toCurrencyFormat($order->total_price_discounted), true) }}</b></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="account-subscribe__status">
                    <div class="table__product">
                        <div class="table__product-info">
                            <p class="text-center">Статус заказа</p>
                            <p class="account-subscribe__product-status">
                                                <span class="label
                                                    @if ($order->status == 'completed') label-success @endif
                                                @if ($order->status == 'active') label-primary @endif
                                                @if ($order->status == 'pending') label-primary @endif
                                                @if ($order->status == 'canceled') label-danger @endif
                                                        ">&nbsp;</span>&nbsp;
                                <span>{{ __('messages.order_'.$order->status) }}</span>
                            </p>
                            <p class="account-subscribe__product-status">
                                                <span class="label
                                                    @if ($order->shipping_status == 'completed') label-success @endif
                                                @if ($order->shipping_status == 'active') label-primary @endif
                                                @if ($order->shipping_status == 'pending') label-primary @endif
                                                        ">&nbsp;</span>&nbsp;
                                <span>{{ __('messages.shipping_'.$order->shipping_status) }}</span>
                            </p>
                            <p class="telephone">Уточнить детали вы можете по телефону:<p class="text-center">0 800 888 99 22</p></p>
                            {{--<p class="auto-buy">Делать автоматическую покупку<img class="info-img" src="assets/images/r-pinfo-icon.svg" alt="info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Новинки"></p>--}}
                            {{--<p class="count-days text-center">--}}
                            {{--<select class="count-days__select">--}}
                            {{--<option value="0">через 30 дней</option>--}}
                            {{--<option value="1">через 12 дней</option>--}}
                            {{--</select>--}}
                            {{--</p>--}}
                            {{--<div class="checkbox">--}}
                            {{--<label class="checkbox-wrap">Подписаться на товар--}}
                            {{--<input type="checkbox" class="checkbox__input">--}}
                            {{--<span class="checkbox__text checkmark"></span>--}}
                            {{--</label>--}}
                            {{--</div>--}}
                            <a href="{{ route('account.order.renew', ['order' => $order->id]) }}" class="account-subscribe__repeat-link">Повторить заказ</a>
                            <a href="#" class="account-subscribe__lost-review">Оставить заказ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@stop