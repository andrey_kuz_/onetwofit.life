<div class="account-subscribe__row account-subscribe__row-open">
    <div class="row">
        <div class="col-lg-12">
            <span class="text-success"><strong>Источник</strong>: {{ $order->is_1click ? 'Заказ в 1 клик' : 'Корзина' }}</span></br>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <span><strong>Номер заказа: </strong> {{ $order->id }}</span></br>
            <span><strong>Создан</strong>: {{ $order->created_at }}</span></br>
            <span><strong>Редактирован</strong>: {{ $order->updated_at }}</span></br>
            <span><strong>Оформлен</strong>: {{ $order->completed_at }}</span>
        </div>
        <div class="col-lg-4">
            <span><strong>Имя: {{ $order->shipping_name }}</span></br>
            <span><strong>Фамилия</strong></strong>: {{ $order->shipping_lastname }}</span></br>
            <span><strong>Email</strong>: {{ $order->shipping_email }}</span></br>
            <span><strong>Телефон</strong>: {{ $order->shipping_phone }}</span>
        </div>
        <div class="col-lg-4">
            <span><strong>Валюта</strong>: {{ $order->currency }}</span></br>
            <span><strong>Курс на момент заказа</strong>: {{ $order->currency_rate }}</span></br>
            <span><strong>К-во товаров</strong>: {{ $order->item_count }}</span></br>
            <span><strong>Промокод</strong>: {{ isset($order->promocode) ? $order->promocode->code : ''}}</span></br>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            Статус заказа: <span class="badge
                                                    @if ($order->status == 'completed') badge-success @endif
                @if ($order->status == 'active') badge-primary @endif
                @if ($order->status == 'pending') badge-warning @endif
                @if ($order->status == 'canceled') badge-danger @endif">{{ __('messages.order_'.$order->status) }}</span>
            <br/>


            Статус доставки: <span class="badge
                                                    @if ($order->shipping_status == 'completed') badge-success @endif
                                                @if ($order->shipping_status == 'active') badge-primary @endif
                                                @if ($order->shipping_status == 'pending') badge-warning @endif">{{ __('messages.shipping_'.$order->shipping_status) }}</span>&nbsp;

            <br/>
            Статус оплаты: @if ($order->payment)
                    <span class="badge
                                                    @if ($order->payment->status == 'success') badge-success
                                                    @elseif ($order->payment->status == 'error' || $order->payment->status == 'failure') badge-danger
                                                    @elseif ($order->payment->status == 'pending') badge-warning
                                                    @elseif ($order->payment->status == 'processing') badge-primary
                                                    @else badge-warning @endif">{{ __('messages.payment_'.$order->payment->status) }}</span>
                @else
                    <span class="badge badge-danger">{{ __('messages.payment_notpayed') }}</span>&nbsp;
                @endif


        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <span>Комментарий: {{ $order->note }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table">
                <tbody>
                @foreach($order->orderItems as $orderItem)
                    <tr class="">
                        <td class="">
                            @if($image=$orderItem->product->getMainImg())
                                <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:30px">
                            @else
                                <img src="{{ asset( 'images/bitmap.png')}}" alt="product" style="width: 30px;">
                            @endif
                        </td>
                        <td style="width:600px;">
                            {{ $orderItem->product->getTranslatedAttribute('name') }}
                        </td>
                        <td>{{ $orderItem->quantity }}</td>
                        <td>{{ to_currency($orderItem->total_price, true) }}</td>
                        <td>
                            <p>Количество -{{ to_currency($orderItem->discount_by_qnt, true) }}</p>
                            <p>Специально предложение -{{ to_currency($orderItem->discount_by_combo, true) }}</p>
                        </td>
                        <td>{{ to_currency($orderItem->total_price_discounted, true) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td class="field-total" colspan="5"><b>Без скидок</b></td>
                    <td><b>{{ to_currency($order->total_price, true) }}</b></td>
                </tr>
                <tr>
                    <td class="field-total" colspan="5"><b>Общая сумма скидки по количеству и комбо-предложениям</b></td>
                    <td><b>{{ to_currency($order->discount_by_qnt_combo, true) }}</b></td>
                </tr>
                @if ($order->discount_promo > 0)
                    <tr>
                        <td class="field-total" colspan="5"><b>Сумма</b></td>
                        <td><b>{{ to_currency($order->total_price_discounted_without_promo, true) }}</b></td>
                    </tr>
                    <tr>
                        <td class="field-total" colspan="5"><b>С учетом промокода (-{{ to_currency($order->discount_promo, true) }}%)</b></td>
                        <td><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                    </tr>
                @else
                    <tr>
                        <td class="field-total" colspan="5"><b>Итого</b></td>
                        <td><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                    </tr>
                @endif
                <tr>
                    <td class="field-total" colspan="5">
                        @if (isset($order->shippingType) && $order->shippingType)
                            <b>Доставка</b> {{ isset($order->shippingType) ? $order->shippingType->name : '' }}, {{ isset($order->shippingMethod) ? $order->shippingMethod->name  : '' }}</br>
                            {{$order->shipping_city}},
                        @endif
                        @if (isset($order->shippingMethod) && $order->shippingMethod)
                            @if ($order->shippingMethod->method == 'warehouse')
                                {{ $order->shipping_department }}
                            @endif
                            @if ($order->shippingMethod->method == 'postman')
                                {{$order->shipping_address}}
                            @endif
                        @endif
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="field-total" colspan="5">
                        @if (isset($order->paymentType) && $order->paymentType)
                            <b>Оплата</b> {{ isset($order->paymentType) ? $order->paymentType->name : '' }}
                        @endif
                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>