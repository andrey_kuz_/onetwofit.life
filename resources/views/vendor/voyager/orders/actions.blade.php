@php $action = new $action($dataType, $data); @endphp
@if ($action->getPolicy() != 'read' && $data->status!='published' || ($action->getPolicy() == 'delete'))
@if ($action->shouldActionDisplayOnDataType())
    @can($action->getPolicy(), $data)
        <a href="{{ $action->getRoute($dataType->name) }}" title="{{ $action->getTitle() }}" {!! $action->convertAttributesToHtml() !!}>
            <i class="{{ $action->getIcon() }}"></i>
        </a>
    @endcan
@endif
@endif
