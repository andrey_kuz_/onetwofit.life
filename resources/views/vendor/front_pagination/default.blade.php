@if ($paginator->hasPages())
    <ul class="blog-pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="blog-pagination__item disabled"><span>&laquo;</span></li>
        @else
            <li class="blog-pagination__item"><a class="blog-pagination__link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="blog-pagination__item disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="blog-pagination__item"><a class="blog-pagination__link active">{{ $page }}</a></li>
                    @else
                        <li class="blog-pagination__item"><a class="blog-pagination__link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="blog-pagination__item"><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
            <li class="blog-pagination__item disabled"><span>&raquo;</span></li>
        @endif
    </ul>
@endif
