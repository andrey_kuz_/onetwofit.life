@if ($paginator->hasPages())
    <ul class="blog-pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="blog-pagination__item disabled"><a class="blog-pagination__link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @else
            <li class="blog-pagination__item"><a class="blog-pagination__link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        @if($paginator->currentPage() > 3)
            <li class="blog-pagination__item hidden-xs"><a class="blog-pagination__link" href="{{ $paginator->url(1) }}">1</a></li>
        @endif
        @if($paginator->currentPage() > 4)
            <li class="blog-pagination__item disabled hidden-xs"><span>...</span></li>
        @endif
        @foreach(range(1, $paginator->lastPage()) as $i)
            @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                @if ($i == $paginator->currentPage())
                    <li class="blog-pagination__item"><a class="blog-pagination__link active">{{ $i }}</a></li>
                @else
                    <li class="blog-pagination__item"><a class="blog-pagination__link" href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
                @endif
            @endif
        @endforeach
        @if($paginator->currentPage() < $paginator->lastPage() - 3)
            <li class="blog-pagination__item disabled hidden-xs"><span>...</span></li>
        @endif
        @if($paginator->currentPage() < $paginator->lastPage() - 2)
            <li class=" blog-pagination__item hidden-xs"><a class="blog-pagination__link" href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="blog-pagination__item"><a class="blog-pagination__link" href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
            {{--<li class="blog-pagination__item disabled"><span>&raquo;</span></li>--}}
        @endif
    </ul>
@endif
