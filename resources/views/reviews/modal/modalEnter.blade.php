<div id="enterModal" class="enter-modal modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <span class="close-circle"></span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">

                        <form class="registration-form" id="login-form" action="{{ route('login') }}" method="post">

                            <div class="registration-page__registration registration active">
                                <div class="registration__title"> @lang('messages.Вход') </div>
                                <div class="registration-wrap">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    @if (session('warning'))
                                        <div class="alert alert-warning">
                                            {{ session('warning') }}
                                        </div>
                                    @endif

                                    @include('auth.social')

                                    <div class="registration-page__row">
                                        {{ csrf_field() }}
                                        <input type="text" name="email" id="email1" class="registration__input" value="{{ old('email') }}">
                                        <label for="email1" class="registration__label"> @lang('messages.email') </label>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                        @endif
                                    </div>
                                    <div class="registration-page__row">
                                        <input type="password" name="password" id="password" class="registration__input" value="">
                                        <label for="password" class="registration__label"> @lang('messages.Пароль') </label>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                        @endif
                                    </div>
                                </div>
                                <a href="#" onclick="event.preventDefault();
                             document.getElementById('login-form').submit();" class="registration-page__link login__link"> @lang('messages.Вход в личный кабинет') </a>
                                <a href="{{ url('/password/reset') }}" class="registration-page__forgot-pass login__forgot-pass"> @lang('messages.Забыли пароль?') </a>
                            </div>
                        </form>

                {{--<button type="button" data-dismiss="modal" class="btn-ok">--}}
                    {{--ok--}}
                {{--</button>--}}

            </div>
        </div>
    </div>
</div>