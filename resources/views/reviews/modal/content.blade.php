<!--<div class="modal-header">-->
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"> @lang('messages.Отзыв') </h4>
<!--</div>-->
<form id="feedbbackform" class="modal-body modal-feedback">
    <span> @lang('messages.Насколько сильно вам понравился товар') </span>
    <div class="radio-toolbar">
        <input type="radio" id="one_star" name="rating" value="1">
        <label for="one_star" class="tablinks"></label>

        <input type="radio" id="two_star" name="rating" value="2">
        <label for="two_star" class="tablinks"></label>

        <input type="radio" id="three_star" name="rating" value="3">
        <label for="three_star"></label>

        <input type="radio" id="four_star" name="rating" value="4" >
        <label for="four_star"></label>

        <input type="radio" id="five_star" name="rating" value="5" checked>
        <label for="five_star"></label>

    </div>
    <span> @lang('messages.Поделитесь своим мнением о товаре') </span>
    <textarea name="comment" id="feedbackform__textarea" placeholder="Отзыв"></textarea>

    <div class="modal-feedback__row">
        <div class="registration-page__row userdata-edit__rowСу">
            <input type="text" name="name" id="user-house" class="registration__input" value="{{ ( Auth::user() ) ? Auth::user()->name : ''}}">
            <label for="user-house" class="registration__label"> @lang('messages.Имя') </label>
        </div>
    </div>
    {{ csrf_field() }}
    <input type="hidden" id="reviewUrl" value="{{ route('review.store') }}" />
    <input type="hidden" name="product_id" id="reviewProductId" value="{{$product->id}}" />

    <div class="alert alert-danger reviewErrors" style="display:none"></div>
    <input type="button" value="Оставить отзыв" id="feedbackform__submit" class="default-btn" onclick="reviewProcess();">
</form>
