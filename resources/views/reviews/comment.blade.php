<div class="comment">
    <div class="comment__row comment__row-first">
        <div class="comment__rating">
            @include('reviews.stars', ['ratingValue' => $review->rating])
        </div>
        <div class="comment__date">{{$review->created_at}}</div>
    </div>
    <div class="comment__text text-short">
        <p class="comment__text-data">{{$review->comment}}</p>
        <span class="read-more">Читать полностью</span>
    </div>
    <div class="comment__row">
        <div class="comment__action">
            {{--Комментировать--}}
        </div>
    </div>
    <div class="comment__bottom">
        <div class="comment__author">{{$review->name}}</div>
        {{--<div class="comment__status">--}}
            {{--<div class="comment__likes">--}}
                {{--<img src="{{ asset('images/like.png') }}" alt="like">--}}
                {{--<span class="comment__count">6</span>--}}
            {{--</div>--}}
            {{--<div class="comment__unlikes">--}}
                {{--<img src="{{ asset('images/unlike.png') }}" alt="unlike">--}}
                {{--<span class="comment__count">6</span>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>

<div id="comment-modal" class="start-modal modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <span class="close-circle"></span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body" style="display: block;">
                <div class="modal-upper">
                    <div class="modal-upper-left">
                        <div class="comment__rating">
                            @include('reviews.stars', ['ratingValue' => $review->rating])
                        </div>
                        <div class="comment__author">{{$review->name}}</div>
                    </div>
                    <div class="modal-upper-right">
                        <div class="comment__date">{{$review->created_at}}</div>
                    </div>
                </div>
                <div class="modal-lower">
                    <p class="comment__text-data">{{$review->comment}}</p>
                </div>
            </div>
        </div>
    </div>
</div>