<div class="estimate" id="reviewsContent">
    <h3 class="estimate__title" id="reviews" > @lang('messages.Отзывы') </h3>
    <div class="estimate__rating">
        <div class="estimate__left">
            <div class="estimate__row">
                @for ($stars = 1; $stars <= 5; $stars++)
                    <div class="some-product__rating">
                        @include('reviews.stars', ['ratingValue' => $stars])
                        <span class="rating-count">{{isset($ratingCountByStars->$stars) ? $ratingCountByStars->$stars->cnt : 0}}</span>
                    </div>
                @endfor
            </div>
        </div>
        <div class="estimate__right">
            <div class="estimate__row">
                <div class="some-product__rating">
                    @for ($stars = 1; $stars <= 5; $stars++)
                        @if ($product->rating >= $stars)
                            <img src="{{ asset('images/star-big-fill.png') }}" alt="star">
                        @else
                            <img src="{{ asset('images/star-big.png') }}" alt="star">
                        @endif

                    @endfor
                    <span class="rating-count">{{$product->rating_count}}</span>
                </div>
            </div>
            <div class="estimate__row">
                <input type="hidden" id="reviewAuthError" value="@lang('messages.Комментарии доступны только для зарегестрированных')">
                <input type="hidden" id="reviewAuthUrl" value="{{ route('login') }}">
                <a href="#" class="estimate__link" data-toggle="modal" data-target="#feedback-window" onclick="reviewInit('{{ route('review', ['product' => $product->id]) }}'); return false;">Оставить отзыв</a>
            </div>
        </div>
    </div>
    <div class="estimate__comments">
        @foreach ($reviews as $review)
            @include('reviews.comment')
        @endforeach
    </div>
    {{ $reviews->links('vendor.front_pagination.default') }}

</div>