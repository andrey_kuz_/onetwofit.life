@if($not_ajax)
<head>
    <meta name="robots" content="noindex" />
</head>
@endif
<div class="modal-body__product">
    <div class="modal-body__image">
        @if($mainImage=$product->getMainImg())
            <img src="@if( !filter_var($mainImage, FILTER_VALIDATE_URL)){{ Voyager::image( $mainImage ) }}@else{{ $mainImage }}@endif">
        @else
            <img src="{{ asset( 'images/bitmap.png')}}">
        @endif
    </div>
    <div class="modal-body__description">
        <div class="some-product__characters">
            <div class="some-product__row">
                <h4 class="some-product__row-header">{{ str_limit($product->getTranslatedAttribute('name'), 50) }}</h4>
                <span class="some-product__row-itemquantity">
                    {{--1 {{isset($product->issueForm) ? '('.$product->issueForm->getTranslatedAttribute('name').')' : '' }}--}}
                </span>
                <span class="some-product__row-itemprice">{{ to_currency($product->price_discounted, true) }}</span>
            </div>
        </div>
    </div>
</div>
<div class="modal-body__action">
    <h2> @lang('messages.Купить в один клик') </h2>
    <div class="alert alert-danger oneClickErrors" style="display:none"></div>
    <form id="oneCLickForm">
        {{ csrf_field() }}
        <input type="hidden" name="oneClickUrl" id="oneClickUrl" value="{{ route('oneclick.store') }}" />
        <input type="hidden" name="product_id" id="oneClickProductId" value="{{$product->id}}" />
        <input type="text" name="name" placeholder="@lang('messages.Имя')" class="name" />
        <input type="text" name="phone" id="oneClick-phone" placeholder="+380..." class="phone" />
        <a href="#" class="modal-body__link" onclick="oneClick(); return false;"> @lang('messages.Купить в 1 клик') </a>
    </form>
</div>