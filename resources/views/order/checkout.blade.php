@extends('layouts.appMin')

@section('content')
    @include('order.step1')
    @include('order.step2')
@endsection
