<input type="hidden" value="{{ $order->shipping_department }}" id="shipping_department_default">
<select name="shipping_city" class="select_autocomplete" id="shipping_city_id" onchange="loadShippingNP('shipping_department', '{{route('shipping')}}', 2, this.value);">
    <option value=""> @lang('messages.Выберите город доставки') </option>
    @foreach($cities as $city)
        <option value="{{ $city['id'] }}"
                @if ($order->shipping_city == $city['id']) selected @endif
        >{{ $city['name'] }}</option>
    @endforeach
</select>
<input type="hidden" name="shipping_type_id" value="{{$shippingType->id}}" />

@foreach($shippingMethods as $shippingMethod)
    @include('order.nova_poshta.'.$shippingMethod->method)
@endforeach



