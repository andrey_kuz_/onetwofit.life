<label class="radio-container">
    <div class="payment__content">
        <div class="payment__text">
            <span class="pull-left">{{$shippingMethod->name}}</span>
            <span class="pull-right"><b></b></span>
        </div>
        {{--<img src="{{ asset('images/placeholder.svg') }}" alt="placeholder" class="payment__img">--}}
        {{--<a href="#" class="payment__link">Cм. на карте</a>--}}
        {{--<div class="">--}}
        {{--<a href="#" class="payment__link">График работы отделений</a>--}}
        {{--</div>--}}
    </div>
    <input type="radio" name="shipping_method_id" value="{{$shippingMethod->id}}"
           @if (($order->shipping_method_id == $shippingMethod->id) || !$order->shipping_method_id) checked @endif
           >
    <span class="checkmark"></span>
</label>
<select name="shipping_department" class="payment__select select_autocomplete" id="shipping_department">
</select>