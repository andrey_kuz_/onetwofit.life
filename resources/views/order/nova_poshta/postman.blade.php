<label class="radio-container">
    <div class="payment__content">
        <div class="payment__text">
            <span class="pull-left">{{$shippingMethod->name}}</span>
            <span class="pull-right"><b></b></span>
        </div>

    </div>
    <input type="radio" name="shipping_method_id" value="{{$shippingMethod->id}}"
           @if ($order->shipping_method_id == $shippingMethod->id) checked @endif
    >
    <span class="checkmark"></span>

</label>
<input type="text" placeholder="{{ __('messages.input_address') }}" name="shipping_address" value="{{ $order->shipping_address ?: ((isset($user) ) ? ($user->shipping_street . ( $user->shipping_house ? '/'. $user->shipping_house.($user->shipping_room ? '/'.$user->shipping_room : '') : '')) : '' )}}" />