@extends('layouts.appMin')

@section('content')
    <form action="{{ route('checkout.post') }}" method="post">
    <section class="order-page__wrapper step__first">
        <div class="order-page">
            <div class="order-page__title"> @lang('messages.Оформление заказа') </div>
            <div class="order-page__order-processing">
                <div class="order-page__structure">
                    <span class="order-page__step">1</span>
                    <span class="order-page__name"> @lang('messages.Контактные данные') </span>
                </div>
                <div class="order-page__tabs">
                    <div id="tabs-container r-checkout">
                        @if (Auth::guest())
                        <ul class="tabs-menu">
                            <li class="current" rel="tab1">
                                {{--<a href="{{route('login', ['is_checkout' => 1])}}"> @lang('messages.Войти в систему')--}}
                                {{--</a>                                --}}
                                <a class="enter-modal login-modal" data-toggle="modal" data-target="#enterModal" >
                                    @lang('messages.Войти в систему')
                                </a>

                            </li>
{{--                            <li rel="tab2"><a href="{{route('checkout', ['hasAccount' => 1])}}">Я новый покупатель</a></li>--}}
                        </ul>
                        @endif
                        <div class="tab">
                            <div id="tab1" class="tab-content">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                {{ csrf_field() }}
                                <div class="tab-content__row">
                                    <label class="tab-content__label"> @lang('messages.Имя') </label>
                                    <input type="text" placeholder="@lang('messages.Имя')" name="shipping_name" id="shipping_name" value="{{ old('shipping_name') ?: (isset($user) ? ($user->firstname?:$user->name) : '')}}" class="tab-content__input">
                                </div>
                                <div class="tab-content__row">
                                    <label class="tab-content__label"> @lang('messages.Фамилия') </label>
                                    <input type="text" placeholder="@lang('messages.Фамилия')" name="shipping_lastname" id="shipping_lastname" value="{{ old('shipping_lastname') ?: (isset($user) ? ($user->lastname) : '')}}" class="tab-content__input">
                                </div>
                                <div class="tab-content__row">
                                    <label class="tab-content__label"> @lang('messages.Мобильный') </label>
                                    <input type="text" placeholder="+380..."  id="order-phone" name="shipping_phone" value="{{ old('shipping_phone') ?: (isset($user) ? $user->phone : '') }}" class="tab-content__input">
                                </div>
                                <div class="tab-content__row">
                                    <label class="tab-content__label"> @lang('messages.email') </label>
                                    <input type="text" placeholder="Email" name="shipping_email" value="{{ old('shipping_email') ?: (isset($user) ? $user->email : '')}}" class="tab-content__input">
                                </div>
                                    <span id="email-check" class="label hidden">Пользователь с таким Email уже существует. <p><a class="enter-modal login-modal" data-toggle="modal" data-target="#enterModal" >@lang('messages.Войти в систему')</a></p></span>
                                    <div class="tab-content__row">
                                    <label class="tab-content__label"></label>
                                    <input type="submit" class="registration-page__link" value="@lang('messages.Далее')">
                                </div>
                            </div>
                            <div id="tab2" class="tab-content">
                                @if (Auth::guest())
                                    <a href="{{ route('checkout', ['hasAccount' => 1]) }}"> @lang('messages.Зарегистрироваться') </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="order-page__structure empty">
                <span class="order-page__step">2</span>
                <span class="order-page__name"> @lang('messages.Выбор способов доставки и оплаты') </span>
            </div>
        </div>
        @include('order.items')
    </section>

    </form>
    @include('reviews.modal.modalEnter')
@endsection
@section('scripts')
    <script>
        fbq('track', 'InitiateCheckout');
    </script>
@endsection






