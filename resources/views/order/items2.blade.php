<div class="order-page__cart" id="cartSummary">
    <div class="table-responsive table-cart order-top">
        <table class="table">
            <tbody>
            @foreach($cartItems as $cartItem)
                <tr>
                    <td>
                        @if($image=$cartItem->product->getMainImg())
                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif">
                        @else
                            <img src="{{ asset( 'images/bitmap.png')}}" alt="product">
                        @endif
                    </td>
                    <td>{{str_limit($cartItem->product->getTranslatedAttribute('name'), 30)}}</td>
                    <td>{{$cartItem->quantity}}</td>
                    <td><b>{{to_currency($cart->info->cartItemsInfo[$cartItem->id]->getFinalDiscountedPrice(), true)}}</b></td>
                </tr>
            @endforeach

            @if ($promocodeStatusInfo['success'])
                <tr class="total">
                    <td><b> @lang('messages.К оплате') </b></td>
                    <td></td>
                    <td></td>
                    <td><b>{{ to_currency($cart->info->discountedWithoutPromo, true) }}</b></td>
                </tr>
                <tr class="total">
                    <td> @lang('messages.С учетом промокода') (-{{$cart->info->discountPromo}}%)</td>
                    <td></td>
                    <td></td>
                    <td><b>{{ to_currency($cart->info->discounted, true) }}</b></td>
                </tr>
            @else
                <tr class="total">
                    <td><b> @lang('messages.К оплате') </b></td>
                    <td></td>
                    <td></td>
                    <td><b>{{ to_currency($cart->info->discounted, true) }}</b></td>
                </tr>
            @endif

            </tbody>
        </table>
        <input type="button" class="table-cart__confirm default-btn" value="Подтверждаю заказ" onclick="$('#formStep2').submit(); return false;">
        <p class="table-cart__text text-center">
            @lang('messages.Подтверждая заказ, я принимаю условия')
            <br> <a href="{{ route('page', ['slug' => 'agree']) }}" class="table-cart__link"> @lang('messages.пользовательского соглашения') </a>
        </p>
        <div class="table-cart__footer">
            <div class="alert addPromoForm alert-success" style="display:none"></div>
            <div class="alert addPromoForm alert-danger" @if ($promocodeStatusInfo['success'] || !$promocodeStatusInfo['mess']) style="display:none" @endif>{{(!$promocodeStatusInfo['success'])?$promocodeStatusInfo['mess']:''}}</div>
            <form class="table-cart__form" id="addPromoForm">
                {{ csrf_field() }}
                <input type="text" placeholder="@lang('messages.Промокод')" name="promocode" class="table-cart__promocode" value="{{$cart->promocode}}" />
                <a href="#" onclick="addPromo('{{ route('order.addpromo') }}'); return false;" class="table-cart__promobtn"> @lang('messages.Применить') </a>
            </form>
            <a href="{{ route('cart.view') }}" class="table-cart__link"> @lang('messages.Редактировать заказ') </a>
        </div>
    </div>
</div>