@extends('layouts.appMin')

@section('content')
    <div class="order-page order-page-last order-page-triangle">
        <div class="order-page-last__left">
            <img src="{{asset('images/man.png')}}" alt="director" class="director-photo">
        </div>
        <div class="order-page-last__right">
            <div class="order-page-last__title">
                <img src="{{asset('images/yes.svg')}}" class="thanks__image" alt="thx">Спасибо, Ваш заказ принят!
            </div>
            <div class="order-page-last__number"> @lang('messages.Заказ ') № {{$order->id}}</div>
            <div class="attributes">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        @foreach($order->orderItems()->get() as $orderItem)
                            <tr>
                                <td>
                                    @if($image=$orderItem->product->getMainImg())
                                        <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif">

                                    @else
                                        <img src="{{ asset( 'images/bitmap.png')}}" alt="product">
                                    @endif
                                </td>
                                <td>{{str_limit($orderItem->product->getTranslatedAttribute('name'), 30)}}</td>
                                <td><b>{{ to_currency($orderItem->total_price_discounted,true)}}</b></td>
                            </tr>
                        @endforeach

                        @if (!is_null($order->promocodeUsed))
                            <tr class="total">
                                <td><b> @lang('messages.К оплате') </b></td>
                                <td></td>
                                <td><b>{{ to_currency($order->total_price_discounted_without_promo) }}</b></td>
                            </tr>
                            <tr class="total">
                                <td> @lang('messages.С учетом промокода') (-{{$order->discount_promo}}%)</td>
                                <td></td>
                                <td><b>{{ to_currency($order->total_price_discounted,true) }}</b></td>
                            </tr>
                        @else
                            <tr class="total">
                                <td><b> @lang('messages.К оплате') </b></td>
                                <td></td>
                                <td><b>{{ to_currency($order->total_price_discounted,true) }}</b></td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div class="table-responsive attributes-info">
                <table class="table">
                    <tbody>
                    <tr>
                        <td> @lang('messages.Способ оплаты'): </td>
                        <td class="text-right">{{isset($order->paymentType) ? $order->paymentType->name : ''}}</td>
                    </tr>
                    <tr>
                        <td> @lang('messages.Способ доставки'): </td>
                        <td class="text-right">{{isset($order->shippingType) ? $order->shippingType->name : ''}}, {{!is_null($order->shippingMethod) ? $order->shippingMethod->name : ''}}</td>
                    </tr>
                    <tr class="total totalblock">
                        <td><b> @lang('messages.Сумма'): </b></td>
                        <td class="text-right"><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>


            <div class="sms">
                @lang('messages.we_send_you_sms_with_delivery').
            </div>

            <a href="{{ route('home') }}" class="order-page-last__btn"> @lang('messages.Вернуться к покупкам') </a>
        </div>
    </div>
    <div class="order-page-last_full-width">
        <div class="order-page-last__information">
            <p class="topdescr">Здравствуйте! Меня зовут <b>Андрей Карабак.</b> <br>
                Я основатель онлайн-супермаркета <b>Onetwofit</b>.<br><br>
                <b><span class="congratulations"> Благодарю, что сделали заказ в нашем магазине. Вы на правильном пути!</span></b></p>

            <p>Покупка в Onetwofit — выбор здоровья, красоты и нового качества жизни. Я сам веду здоровый образ жизни, занимаюсь спортом и выбираю правильные продукты для себя и семьи. Я знаю, я не один такой. Поэтому я создаю Onetwofit Hub для людей объединённых одними мыслями и целями. Присоединяйтесь к нам в блоге и социальных сетях. Будем вместе участвовать в челленджах, проводить эксперименты, делиться опытом и учиться у экспертов.</p>

            <p>Не знаете как выбрать витамины и добавки? <a href="{{url('blog/')}}" class="order-page-last__information-link" target="_blank">Читайте в блоге Onetwofit</a><br>
                Не знаете, какое правильное блюдо приготовить из купленных продуктов? <a href="{{url('blog/category/recepty/')}}" class="order-page-last__information-link" target="_blank">Находите в рецептах Onetwofit</a><br>
                Любите интервью с экспертами и эксперименты с блоггерами? <a href="{{setting('site.socialYoutube')}}" class="order-page-last__information-link" target="_blank">Смотрите наш канал на YouTubе</a></p>
            <p>Подписывайтесь на нас в FB и Instagram — участвуйте в спортивных челленджах, опросах, узнавайте новости Onetwofit Hub первыми.</p>
        </div>
        <div class="order-page-last__social">
            <a href="{{setting('site.socialFacebook')}}" target="_blank"><img src="{{ asset('images/fb-icon-order.svg')}}" alt="facebook"></a>
            <a href="{{setting('site.socialInstagram')}}" target="_blank"><img src="{{ asset('images/insticon-order.svg') }}" alt="instagram"></a>
            <a href="{{setting('site.socialYoutube')}}" target="_blank"><img src="{{ asset('images/youtube-thanks.png') }}" alt="youtube" class="social-png"></a>
        </div>
    </div>
@endsection
@section('scripts')
<script>
    fbq('track', 'Purchase', {
        content_type: 'product',
        content_ids: '[{{ $items }}]',
        value: '{{ $order->total_price_discounted }}',
        currency: ' UAH ',
    });
</script>
@endsection
