@extends('layouts.appMin')

@section('content')

    <section class="order-page__wrapper step__second" style="display: flex;">
        <form action="{{ route('checkout.process') }}" method="post" id="formStep2">

            {{ csrf_field() }}
            <input type="hidden" value="{{$order_id}}" name="order_id">
        <div class="order-page">
            <div class="order-page__title"> @lang('messages.Оформление заказа') </div>
            <div class="order-page__order-processing">
                <div class="order-page__structure">
                    <span class="order-page__step">2</span>
                    <span class="order-page__name"> @lang('messages.Выбор способов доставки и оплаты') </span>
                </div>
                <div class="order-page__payment payment">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="payment__cat">
                        <span class="payment__text"><b> @lang('messages.Доставка в') </b></span>
                        {{--<a href="#" class="payment__link">Изменить город</a>--}}
                    </div>
                    <div class="payment__row" id="shipping_block">
                        @include('order.nova_poshta.index')
                    </div>

                    <div class="payment__cat">
                        <span class="payment__text"><b> @lang('messages.Оплата') </b></span>
                    </div>
                    <div class="payment__row">
                        @foreach($paymentTypes as $key=>$paymentType)
                            <label class="radio-container">
                                <div class="payment__content">
                                    <div class="payment__text">
                                        <span class="pull-left">{{$paymentType->name}}</span>
                                    </div>
                                </div>
                                <input type="radio" name="payment_type_id"
                                       value="{{$paymentType->id}}"
                                       @if ($order->payment_type_id == $paymentType->id) checked @endif
                                       @if ($loop->first && !$order->payment_type_id)checked @endif>
                                <span class="checkmark"></span>
                            </label>
                        @endforeach
                    </div>
                    <div class="">
                        <div class="cart-page__name"> @lang('messages.Оставить комментарий к заказу') </div>
                        <textarea cols="20" rows="10" class="cart-page__textarea" name="note">@if(isset($comment)) {{ $comment }}@endif</textarea>
                    </div>
                    {{--<div class="payment__cat">--}}
                        {{--<span class="payment__text"><b>Делать автоматическую покупку</b></span>--}}
                        {{--<img class="info-img" src="{{ asset('images/r-pinfo-icon.svg') }}" alt="info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Подписаться">--}}
                    {{--</div>--}}
                    {{--<div class="payment__row checkbox" id="checkbox">--}}
                        {{--<label class="checkbox-wrap">Подписаться на товар--}}
                            {{--<input type="checkbox" class="checkbox__input">--}}
                            {{--<span class="checkbox__text checkmark"></span>--}}
                        {{--</label>--}}
                        {{--<select name="" class="payment__select">--}}
                            {{--<option value="1">через 30 дней</option>--}}
                            {{--<option value="2">через 12 дней</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        </form>
        @include('order.items2')
    </section>
@endsection
@section('scripts')
    <script>
        fbq('track', 'InitiateCheckout');
    </script>
@endsection
