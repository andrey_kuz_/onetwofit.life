<div class="order-page__cart">
    <div class="table-responsive table-cart order-top">
        <table class="table">
            <tbody>
            @foreach($cartItems as $cartItem)
            <tr>
                <td rowspan="2">
                    @if($image=$cartItem->product->getMainImg())
                        <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:190px" class="center-block">

                    @else
                        <img src="{{ asset( 'images/bitmap.png')}}" class="center-block" alt="product">
                    @endif
                </td>
                <td>
                    <h4>{{str_limit($cartItem->product->getTranslatedAttribute('name'), 30)}}</h4>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="table__itemcount">{{$cartItem->quantity}}</span>
                    <span class="table__itemprice">{{to_currency($cart->info->cartItemsInfo[$cartItem->id]->getFinalDiscountedPrice(), true)}}</span>
                </td>
            </tr>
            @endforeach

            @if ($promocodeStatusInfo['success'])
                <tr class="total">
                    <td><b> @lang('messages.К оплате') </b></td>
                    <td></td>
                    <td><b>{{ to_currency($cart->info->discountedWithoutPromo, true) }}</b></td>
                </tr>
                <tr class="total">
                    <td> @lang('messages.С учетом промокода') (-{{$cart->info->discountPromo}}%)</td>
                    <td></td>
                    <td><b>{{ to_currency($cart->info->discounted, true) }}</b></td>
                </tr>
            @else
                <tr class="total">
                    <td><b> @lang('messages.К оплате') </b></td>
                    <td></td>
                    <td><b>{{ to_currency($cart->info->discounted, true) }}</b></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="table-cart__footer">
            <a href="{{ route('cart.view') }}" class="table-cart__link">{{__('messages.edit_text')}}</a>
        </div>
    </div>
    {{--<div class="alert alert-warning" role="alert">
        <p><b>Подарки всем клиентам на первый заказ в новом году!</b></p>
        <p><b>При заказе от 500 грн вы получаете подарок - упаковку чиа или киноа 250 г на выбор.</b></p>
        <p><b>Вкусные и полезные подарки клиентам Onetwofit.</b></p>
        <p><b>Акция действует с 7 по 31 января.</b></p>
    </div>--}}
</div>