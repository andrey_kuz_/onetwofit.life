{{--<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/"> @lang('messages.Главная') </a><span class="breadcrumbs__icon">/</span> </li>--}}
<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#"> Поиск товара </a><span class="breadcrumbs__icon">/</span> </li>
<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#"> Поиск по тегу </a><span class="breadcrumbs__icon">/</span> </li>
<li class="breadcrumbs__item"><a class="breadcrumbs__link @if (!$section) active @endif" href="{{ route('tag.list') }}"> Все теги </a>@if ($section) <span class="breadcrumbs__icon">/</span> @endif</li>
@if(isset($section))
<li class="breadcrumbs__item"><a class="breadcrumbs__link active" href="{{ route('tag.index', ['slug' => $section->slug]) }} }}"> {{ $section->name }} </a></li>
@endif