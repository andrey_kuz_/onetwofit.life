
@foreach($categories as $rootCategory)
    <a class="bestsellers__link" href="#cat{{$rootCategory->id}}" data-value="{{ $rootCategory->id }}">{{ $rootCategory->getTranslatedAttribute('name') }}</a>
@endforeach