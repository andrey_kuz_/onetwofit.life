<ul class="navigation-list">
    <li class="navigation-list__item">
        <a href="#" class="navigation-list__link">Поиск товара</a>
        <div class="sub-navigation-list__wrapper">
            <ul class="megamenu">
                <li class="onelevel tag_list">
                    <a href="#" class="sub-sub-navigation-list__link">Поиск по тегу</a>
                    <ul class="twolevwel tag_list_item">
                        @foreach($tags as $tag)
                            <li><a href="{{ route('tag.index', ['slug' => $tag->slug]) }}">{{ $tag->name }}</a></li>
                        @endforeach
                    </ul>
                    <a href="{{ route('tag.list') }}" class="sub-sub-navigation-list__all show_all">Смотреть все</a>
                </li>
                <li class="onelevel brand_list">
                    <a href="#" class="sub-sub-navigation-list__link">Поиск по бренду</a>
                    <ul class="twolevwel brand_list_item">
                        @foreach($brands as $brand)
                            <li><a href="{{ route('brand.index', ['slug' => $brand->slug]) }}">{{ $brand->name }}</a></li>
                        @endforeach
                    </ul>
                    <a href="{{ route('brand.list') }}" class="sub-sub-navigation-list__all">Смотреть все</a>
                </li>
            </ul>
        </div>
    </li>

    @foreach($categories as $category)
    <li class="navigation-list__item">
        <a href="{{ route('category.index', $category->slug) }}" class="navigation-list__link">{{ $category->getTranslatedAttribute('name') }}</a>
        @if($category->subcategories->count() > 0)
        <div class="sub-navigation-list__wrapper">
            <ul class="megamenu">
                @foreach($category->subcategories as $subcategory)
                <li class="onelevel">
                    <a href="{{ route('category.index', $subcategory->slug) }}" class="sub-sub-navigation-list__link">{{ $subcategory->getTranslatedAttribute('name') }}</a>
                    @if($subcategory->subcategories->count() > 0)
                        <ul class="twolevwel">
                            @foreach($subcategory->subcategories as $subcategory)
                            <li>
                                <a href="{{ route('category.index', $subcategory->slug) }}">{{ $subcategory->getTranslatedAttribute('name') }}</a>
                                @if($subcategory->subcategories->count() > 0)
                                <ul class="fourth-level-menu">
                                    <li class="fourth-level-menu__item">
                                        @foreach($subcategory->subcategories as $subcategory)
                                        <a href="{{ route('category.index', $subcategory->slug) }}" class="fourth-level-menu__link">{{ $subcategory->getTranslatedAttribute('name') }}</a>
                                        @endforeach
                                    </li>
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>

                    @endif
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </li>
    @endforeach

    <li class="navigation-list__item">
        <a href="{{ route('special', $type='all') }}" class="navigation-list__link"> @lang('messages.Специальное предложение') </a>
        <div class="sub-navigation-list__wrapper lastChild">
            <ul class="megamenu">
                {{--onelevl--}}
                <li class="onelevel">
                    <ul class="twolevwel">



                        <li ><a href="{{ route('special', $type='discount') }}" class="sub-sub-navigation-list__link">@lang('messages.is_super')</a></li>
                        <li ><a href="{{ route('special', $type='new') }}" class="sub-sub-navigation-list__link">@lang('messages.is_new')</a></li>
                        <li><a href="{{ route('special', $type='hit') }}" class="sub-sub-navigation-list__link">@lang('messages.is_hit')</a></li>
                        <li ><a href="{{ route('special', $type='bestseller') }}" class="sub-sub-navigation-list__link">@lang('messages.is_bestseller')</a></li>
                    </ul>
                </li>
                <li class="onelevel">
                    <ul class="twolevwel">
                        @foreach($presents as $present)
                            <li> <a href="{{ route('category.index', $present->slug) }}" class="sub-sub-navigation-list__link">{{ $present->getTranslatedAttribute('name') }}</a>
                                @if($present->subcategories->count() > 0)
                                    <ul class="threelevwel">
                                        @foreach($present->subcategories->take(7) as $subcategory)
                                            <li>
                                                <a href="{{ route('category.index', $subcategory->slug) }}" >{{ $subcategory->getTranslatedAttribute('name') }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif

                            </li>
                            @if($present->subcategories->count() > 7)
                                <a href="{{ route('category.index', $present->slug) }}" class="sub-sub-navigation-list__all">Смотреть все</a>
                            @endif
                        @endforeach
                    </ul>
                </li>







            </ul>
        </div>
          </ul>
    </li>
</ul>

