<div class="adaptive-menu">
    <ul class="adaptive-menu__list">
        <li class="adaptive-menu__item">
            <a href="#" class="adaptive-menu__link">Поиск товара</a>
            <div class="sub-navigation-list__wrapper">
                <ul class="sub-navigation-list">
                    <li class="sub-navigation-list__item">
                        <a href="#" class="sub-navigation-list__link">Поиск по тегу</a>
                        <div class="sub-sub-navigation-list__wrapper">
                            <ul class="sub-sub-navigation-list__wrapper tag_list_item">
                                @foreach($tags as $tag)
                                    <li class="sub-sub-navigation-list__item">
                                        <a class="sub-sub-navigation-list__link" href="{{ route('tag.index', ['slug' => $tag->slug]) }}">{{ $tag->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <a href="{{ route('tag.list') }}" class="sub-sub-navigation-list__all">Смотреть все</a>
                    </li>
                    <li class="sub-navigation-list__item">
                        <a href="#" class="sub-navigation-list__link">Поиск по бренду</a>
                        <div class="sub-sub-navigation-list__wrapper">
                            <ul class="sub-sub-navigation-list brand_list_item">
                                @foreach($brands as $brand)
                                    <li class="sub-sub-navigation-list__item">
                                        <a class="sub-sub-navigation-list__link" href="{{ route('brand.index', ['slug' => $brand->slug]) }}">{{ $brand->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <a href="{{ route('brand.list') }}" class="sub-sub-navigation-list__all">Смотреть все</a>
                    </li>
                </ul>
            </div>
            <a href="#" class="menu-carret"></a>
        </li>
        @foreach($categories as $category)
        <li class="adaptive-menu__item">
            <a href="{{ route('category.index', $category->slug) }}" class="adaptive-menu__link">{{ $category->getTranslatedAttribute('name') }}</a>
            @if($category->subcategories->count() > 0)
            <div class="sub-navigation-list__wrapper">
                <ul class="sub-navigation-list">
                    @foreach($category->subcategories as $subcategory)
                    <li class="sub-navigation-list__item">
                        <a href="{{ route('category.index', $subcategory->slug) }}" class="sub-navigation-list__link">{{ $subcategory->getTranslatedAttribute('name') }}</a>
                        @if($subcategory->subcategories->count() > 0)
                        <div class="sub-sub-navigation-list__wrapper">
                            <ul class="sub-sub-navigation-list">
                                @foreach($subcategory->subcategories as $subcategory)
                                <li class="sub-sub-navigation-list__item">
                                    <a href="{{ route('category.index', $subcategory->slug) }}" class="sub-sub-navigation-list__link">{{ $subcategory->getTranslatedAttribute('name') }}</a>
                                    @if($subcategory->subcategories->count() > 0)
                                    <div class="third-level-menu__wrapper">
                                        <ul class="third-level-menu">
                                            @foreach($subcategory->subcategories as $subcategory)
                                            <li class="third-level-menu__item">
                                                <a href="{{ route('category.index', $subcategory->slug) }}" class="third-level-menu__link">{{ $subcategory->getTranslatedAttribute('name') }}</a>
                                                @if($subcategory->subcategories->count() > 0)
                                                <ul class="fourth-level-menu">
                                                    <li class="fourth-level-menu__item">
                                                        @foreach($subcategory->subcategories as $subcategory)
                                                        <a href="{{ route('category.index', $subcategory->slug) }}" class="fourth-level-menu__link">{{ $subcategory->getTranslatedAttribute('name') }}</a>
                                                        @endforeach
                                                    </li>
                                                </ul>
                                                @endif
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="menu-carret"></a>
            @endif

        </li>
        @endforeach
        <li class="adaptive-menu__item">
            <a href="{{ route('special', $type='all') }}" class="adaptive-menu__link"> @lang('messages.Специальное предложение') </a>
            <div class="sub-navigation-list__wrapper">
                <ul class="sub-navigation-list">
                    <li class="sub-navigation-list__item"><a href="{{ route('special', $type='bestseller') }}" class="sub-navigation-list__link">@lang('messages.is_bestseller')</a></li>
                    <li class="sub-navigation-list__item"><a href="{{ route('special', $type='hit') }}" class="sub-navigation-list__link">@lang('messages.is_hit')</a></li>
                    <li class="sub-navigation-list__item"><a href="{{ route('special', $type='new') }}" class="sub-navigation-list__link">@lang('messages.is_new')</a></li>
                    <li class="sub-navigation-list__item"><a href="{{ route('special', $type='discount') }}" class="sub-navigation-list__link">@lang('messages.is_super')</a></li>
                    @foreach($presents as $present)
                        <li class="sub-sub-navigation-list__item"> <a href="{{ route('category.index', $present->slug) }}" class="sub-navigation-list__link">{{ $present->getTranslatedAttribute('name') }}</a>
                            @if($present->subcategories->count() > 0)
                                <div class="sub-sub-navigation-list__wrapper">
                                <ul class="sub-sub-navigation-list">
                                    @foreach($present->subcategories as $subcategory)

                                        <li class="sub-sub-navigation-list__item">
                                            <a href="{{ route('category.index', $subcategory->slug) }}" class="sub-sub-navigation-list__link">{{ $subcategory->getTranslatedAttribute('name') }}</a>
                                        </li>

                                    @endforeach
                                </ul>
                                </div>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="menu-carret"></a>
        </li>
    </ul>
</div>