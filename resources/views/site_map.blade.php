<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ route('home') }}</loc>
        <lastmod>{{$date}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>1.00</priority>
    </url>
    @foreach($tags as $tag)
        <url>
            <loc>{{ route('tag.index', ['slug' => $tag->slug]) }}</loc>
            <lastmod>{{$date}}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.60</priority>
        </url>
    @endforeach
    @foreach($brands as $brand)
        <url>
            <loc>{{ route('brand.index', ['slug' => $brand->slug]) }}</loc>
            <lastmod>{{$date}}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.60</priority>
        </url>
    @endforeach
    <url>
        <loc>{{ route('special', $type='discount') }}</loc>
        <lastmod>{{$date}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>{{ route('special', $type='new') }}</loc>
        <lastmod>{{$date}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>{{ route('special', $type='hit') }}</loc>
        <lastmod>{{$date}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>{{ route('special', $type='bestseller') }}</loc>
        <lastmod>{{$date}}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.80</priority>
    </url>
    @foreach($categories as $category)
        <url>
            <loc>{{ route('category.index', $category->slug) }}</loc>
            <lastmod>{{$date}}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.80</priority>
        </url>
    @endforeach
    @foreach($products as $product)
        <url>
            <loc>{{ route('product.index', ['id' => $product->id, 'slug' => $product->slug])  }}</loc>
            <lastmod>{{$date}}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.70</priority>
        </url>
    @endforeach
    @foreach($pages as $page)
        <url>
            <loc>{{ route('page', ['slug' => $page->slug ])  }}</loc>
            <lastmod>{{$date}}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.50</priority>
        </url>
    @endforeach
</urlset>