@if(count($hitProducts) > 4)
<div class="slider-category hits">
    <div class="slider-category__title">
        <span> @lang('messages.Хиты продаж') </span>
        <img class="info-img" src="{{ asset('images/i.svg')}}" alt="info" data-toggle="tooltip" data-placement="top" title="@lang('messages.hit_descr')">
    </div>
    <div class="hits__arrow slider-arrow">
        <span class="hits__arrow-left slider-arrow__left">
            <span class='arrow-wrap'>
                <i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>
            </span>
        </span>
        <span class="hits__arrow-right slider-arrow__right">
            <span class='arrow-wrap'>
                <i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>
            </span>
        </span>
    </div>
    <div class="slider-category__content">
        <div class="owl-carousel owl-theme customflex">
            @foreach($hitProducts as $hitProduct)
                @include('products.itemBlock', ['itemProduct'=>$hitProduct])
            @endforeach
        </div>
    </div>
</div>
@elseif(count($hitProducts) > 0)
    <div class="slider-category hits">
        <div class="slider-category__title">
            <span> @lang('messages.Хиты продаж') </span>
            <img class="info-img" src="{{ asset('images/i.svg')}}" alt="info" data-toggle="tooltip" data-placement="top" title="@lang('messages.hit_descr')">
        </div>
        <div class="noslider-container">
            @foreach($hitProducts as $hitProduct)
                @include('products.noslider', ['itemProduct'=>$hitProduct])
            @endforeach
        </div>
    </div>
@endif