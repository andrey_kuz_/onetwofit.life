@if(count($newProducts) > 5)
<div class="slider-category news">
    <div class="slider-category__title">
        <span> @lang('messages.Новинки') </span>
        <img class="info-img" src="{{ asset('images/i.svg')}}" alt="info" data-toggle="tooltip" data-placement="top" title="@lang('messages.new_descr')">
    </div>
    <div class="news__arrow slider-arrow">
        <span class="news__arrow-left slider-arrow__left">
            <span class='arrow-wrap'>
                <i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>
            </span>
        </span>
        <span class="news__arrow-right slider-arrow__right">
            <span class='arrow-wrap'>
               <i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>
            </span>
        </span>
    </div>
    <div class="slider-category__content">
        <div class="owl-carousel owl-theme customflex">
            @foreach($newProducts as $newProduct)
                @include('products.itemBlock', ['itemProduct'=>$newProduct])
            @endforeach
        </div>
    </div>
</div>
    @elseif(count($newProducts) > 0)
    <div class="slider-category news">
    <div class="slider-category__title">
        <span> @lang('messages.Новинки') </span>
        <img class="info-img" src="{{ asset('images/i.svg')}}" alt="info" data-toggle="tooltip" data-placement="top" title="@lang('messages.new_descr')">
    </div>
            <div class="noslider-container">
            @foreach($newProducts as $newProduct)
                @include('products.noslider', ['itemProduct'=>$newProduct])
            @endforeach
     </div>
@endif