<?php
if (!isset($id)){
    $id = 0;
}
?>
@if(count($bestsellerProducts) > 4)
<div class="slider-category bestsellers">
    <div class="slider-category__title">
        <span> @lang('messages.Бестселлеры') </span>
        <img class="info-img" src="{{ asset('images/i.svg')}}" alt="info" data-toggle="tooltip" data-placement="top" title=" @lang('messages.bestseller_descr') ">
    </div>
    <div class="bestsellers__url">
        @include('menu.links')
    </div>
    <div class="bestsellers__arrow slider-arrow">
        <span class="bestsellers__arrow-left slider-arrow__left">
            <span class='arrow-wrap'>
                <i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>
            </span>
        </span>
        <span class="bestsellers__arrow-right slider-arrow__right">
            <span class='arrow-wrap'>
                <i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>
            </span>
        </span>
    </div>
    <div class="slider-category__content">
        <div class="owl-carousel owl-theme customflex">
            @foreach($bestsellerProducts as $bestsellerProduct)
                @include('products.itemBlockBestseller', ['itemProduct'=>$bestsellerProduct])
            @endforeach
        </div>
    </div>
</div>
@elseif(count($bestsellerProducts) > 0)
    <div class="slider-category bestsellers">
        <div class="slider-category__title">
            <span> @lang('messages.Бестселлеры') </span>
            <img class="info-img" src="{{ asset('images/i.svg')}}" alt="info" data-toggle="tooltip" data-placement="top" title="@lang('messages.bestseller_descr')">
        </div>
        <div class="bestsellers__url">
            @include('menu.links')
        </div>
        <div class="noslider-container">
            @foreach($bestsellerProducts as $bestsellerProduct)
                @include('products.noslider', ['itemProduct'=>$bestsellerProduct])
            @endforeach
        </div>
    </div>
@endif