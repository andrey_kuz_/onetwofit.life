<div class="blog">
    <div class="blog-wrapper">
        <div class="blog__title"> @lang('messages.Блог') </div>
        <div class="blog__content">
            @if ( $blogPosts[0] )
                @if($blogPosts[0]->thumbnail)
                    <div class="blog__image">
                        <img src="{{ $blogPosts[0]->thumbnail }}" alt="eat" class="blog-img-eat">
                    </div>
                @endif
                <div class="blog__description">
                    @if ( $blogPosts[0]->taxonomies()->where('taxonomy','category')->orderBy('count','desc')->first() )
                        <div class="tags blog__tags">
                            <div class="tags-block">
                                <a href="/blog/category/{{ $blogPosts[0]->taxonomies()->where('taxonomy','category')->orderBy('count','desc')->first()->slug }}" target="_blank" class="tags__text">{{ $blogPosts[0]->taxonomies()->where('taxonomy','category')->orderBy('count','desc')->first()->name }}</a>
                            </div>
                        </div>
                    @endif
                    <div class="blog__text">
                        <a href="/blog{{ ($blogPosts[0]->post_name) ? '/' . $blogPosts[0]->post_name : '' }}" target="_blank" >{{ ($blogPosts[0]->title) ? $blogPosts[0]->title : '' }}</a>
                    </div>
                    <div class="blog__auth auth">
                        <div class="blog__date auth-date">{{ Carbon\Carbon::parse($blogPosts[0]->post_date)->format('d.m.Y') }}</div>
                        <div class="blog__author auth-author">{{ $users::find($blogPosts[0]->post_author)->display_name }}</div>
                    </div>
                    <a href="/blog {{ ($blogPosts[0]->post_name) ?  '/' . $blogPosts[0]->post_name : '' }}" class="blog__read-more" target="_blank" ><span> @lang('messages.читать статью') </span>
                        <!--<i class="fa fa-long-arrow-right" aria-hidden="true"></i>-->
                        <svg width="23" height="16" viewBox="0 0 23 16" fill="none" xmlns="http://www.w3.org/2000/svg" class="blogarrow">
                            <path d="M0 7.76562L1.16534 8.93096L7.7656 15.5312L10.1067 13.2005L6.3218 9.41568H22.1418V6.11554H6.3218L10.1067 2.33068L7.7656 0L1.16534 6.60026L0 7.76562Z" transform="translate(23) scale(-1 1)" fill="#6004BA"/>
                        </svg>
                    </a>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="blog-slider">
    <div class="blog-slider__arrow slider-arrow">
        <div class="blog-slider__arrow slider-arrow">
            <span class="blog-slider__arrow-left slider-arrow__left gray">
                <span class='arrow-wrap'>
                    <i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>
                </span>
            </span>
            <span class="blog-slider__arrow-right slider-arrow__right gray">
                <span class='arrow-wrap'>
                    <i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>
                </span>
            </span>
        </div>
    </div>
    <div class="blog-slider__content">
        <div class="owl-carousel owl-theme">
            @foreach($blogPosts as $blogPost)
                @if ($loop->first) @continue @endif
                @if ( $blogPost )
                    <div class="blog-slider__item">
                        <div class="blog-slider__image">
                            <a href="/blog{{ ($blogPost->post_name) ? '/' . $blogPost->post_name : ''}} " target="_blank" >
                                @if ($blogPost->thumbnail) <img src="{{ $blogPost->thumbnail }}" alt="Блог"> @endif
                            </a>
                            @if ( $blogPost->taxonomies()->where('taxonomy','category')->orderBy('count','desc')->first() )
                                <div class="tags blog-slider__tags">
                                    <div class="tags-block">
                                        <a href="/blog/category/{{ $blogPost->taxonomies()->where('taxonomy','category')->orderBy('count','desc')->first()->slug }}" class="tags__text" target="_blank" >{{ $blogPost->taxonomies()->where('taxonomy','category')->orderBy('count','desc')->first()->name }}</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="blog-slider__description">
                            <a href="/blog{{ ($blogPost->post_name) ? '/' . $blogPost->post_name : ''}} " target="_blank" >{{ ($blogPost->title) ?  $blogPost->title : ''}}</a>
                        </div>
                        <div class="blog-slider__auth auth">
                            @if($blogPost->post_date) <div class="blog-slider__date auth-date">{{ Carbon\Carbon::parse($blogPost->post_date)->format('d.m.Y') }}</div> @endif
                            @if($blogPost->post_author) <div class="blog-slider__author auth-author">{{ $users::find($blogPost->post_author)->display_name }}</div> @endif
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>