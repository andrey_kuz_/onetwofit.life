<div class="exclusive">
    <div class="exclusive__title text-center"> @lang('messages.Мы - эксклюзивные представители') </div>
    <div class="exclusive__images text-center">
        <div class="exclusive__row">
            <img src="{{ asset('images/1.jpg') }}" alt="exclusive product" class="exclusive__image">
        </div>
        <div class="exclusive__row">
            <img src="{{ asset('images/2.jpg') }}" alt="exclusive product" class="exclusive__image">
        </div>
        <div class="exclusive__row">
            <img src="{{ asset('images/3.jpg') }}" alt="exclusive product" class="exclusive__image">
        </div>
        <div class="exclusive__row">
            <img src="{{ asset('images/4.jpg') }}" alt="exclusive product" class="exclusive__image">
        </div>
        <div class="exclusive__row">
            <img src="{{ asset('images/5.jpg') }}" alt="exclusive product" class="exclusive__image">
        </div>
        <div class="exclusive__row">
            <img src="{{ asset('images/6.jpg') }}" alt="exclusive product" class="exclusive__image">
        </div>
    </div>
</div>