<div class="micromarking">
    <div itemscope itemtype="http://schema.org/Organization">
        <span itemprop="name">Onetwofit</span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="streetAddress">Проспект Соборный 186а, кв. 3</span>
            <span itemprop="postalCode">69035</span>
            <span itemprop="addressLocality">Запорожье, Zaporiz'ka oblast</span>
        </div>
        <span itemprop="telephone">0 800 339 525</span>
        <span itemprop="email">Info@onetwofit.org</span>
    </div>
</div>
