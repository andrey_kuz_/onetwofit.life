<div class="full-width-slider">
    <div class="owl-carousel owl-theme ">
        @foreach($banners as $banner)
            <div class="item">
                <picture>
                    <a href="{{ $banner->url }}">
                        <img class="desktop" src="@if( !filter_var($banner->image, FILTER_VALIDATE_URL)){{ Voyager::image( $banner->image ) }}@else{{ $banner->image }}@endif">
                        <img class="mobile" src="@if( !filter_var($banner->image_mobile, FILTER_VALIDATE_URL)){{ Voyager::image( $banner->image_mobile ) }}@else{{ $banner->image_mobile }}@endif">
                    </a>
                </picture>
            </div>
        @endforeach
    </div>
</div>
