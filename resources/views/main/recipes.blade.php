<div class="receipts">
    <div class="receipts-wrapper">
        <div class="receipts__title"><a href="#"> @lang('messages.Рецепты') </a></div>
        <div class="receipts__arrow slider-arrow">
            <span class="receipts__arrow-left slider-arrow__left gray">
                <span class='arrow-wrap'>
                    <i class="arr-topsslider arr-topslider__left arr-topslider-violet" aria-hidden='true'></i>
                </span>
            </span>
            <span class="receipts__arrow-right slider-arrow__right gray">
                <span class='arrow-wrap'>
                    <i class="arr-topsslider arr-topslider__right arr-topslider-violet" aria-hidden='true'></i>
                </span>
            </span>
        </div>
        <div class="receipts__content">
            <div class="owl-carousel owl-theme">
                @foreach($blogRecipesPosts as $blogRecipePost)
                <div class="receipts__item">
                    <div class="receipts__image">
                        <img src="{{ $blogRecipePost->thumbnail }}" alt="Рецепты">
                    </div>
                    <div class="receipts__description">
                        <a href="/blog/{{ $blogRecipePost->post_name }}">{{ $blogRecipePost->title }}</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>