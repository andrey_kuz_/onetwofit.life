@extends('layouts.app')

@section('content')
<div class="full-width-slider">
    <div class="owl-carousel owl-theme">
        <div class="item">
            <div class="item-help-wrap">
                <div class="item__text">
                    <div class="item__title">Товары по <br>специальной цене</div>
                    <div class="item__image"><img src="{{ asset('images/bitmap.png') }}" alt="special price"></div>
                    <div class="item__btn">
                        <a href="#" class="item__link">Подробнее</a>
                    </div>
                    <div class="item__pager">
                        <span class="active"></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="item__pictures">
                    <img src="{{ asset('images/bitmap.png')}}" alt="банка" class="item__picture">
                </div>
            </div>
        </div>
        <div class="item">
            <div class="item-help-wrap">
                <div class="item__text">
                    <div class="item__title">Товары по <br>специальной цене</div>
                    <div class="item__image"><img src="{{ asset('images/bitmap.png')}}" alt="special price"></div>
                    <div class="item__btn">
                        <a href="#" class="item__link">Подробнее</a>
                    </div>
                    <div class="item__pager">
                        <span class="active"></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="item__pictures">
                    <img src="{{ asset('images/bitmap.png')}}" alt="банка" class="item__picture">
                </div>
            </div>
        </div>
        <div class="item">
            <div class="item-help-wrap">
                <div class="item__text">
                    <div class="item__title">Товары по <br>специальной цене</div>
                    <div class="item__image"><img src="{{ asset('images/bitmap.png')}}" alt="special price"></div>
                    <div class="item__btn">
                        <a href="#" class="item__link">Подробнее</a>
                    </div>
                    <div class="item__pager">
                        <span class="active"></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="item__pictures">
                    <img src="{{ asset('images/bitmap.png')}}" alt="банка" class="item__picture">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
