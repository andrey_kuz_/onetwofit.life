@extends('layouts.app')

@section('content')
<div class="registration-page">
    <div class="registration-page__form">
        <form class="registration-form" id="registration-form" action="{{ route('register') }}" method="post">
            <div class="registration-page__registration registration active">
                <div class="registration__title"> @lang('messages.Регистрация') </div>
                <div class="registration-wrap">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @include('auth.social',['register' => true])

                    <div class="registration-page__row">

                        <input type="text" name="email" id="email" class="registration__input" autocomplete="off" value="{{ old('email') }}">
                        {{ csrf_field() }}
                        <label for="email" class="registration__label"> @lang('messages.email') </label>
                    </div>
                    <div class="registration-page__row">
                        <input type="password" name="password" id="password" class="registration__input" value="" autocomplete="off">
                        <label for="password" class="registration__label"> @lang('messages.Пароль') </label>
                    </div>
                    <div class="registration-page__row">
                        <input type="password" name="password_confirmation" id="password-confirm" class="registration__input" value="" autocomplete="off">
                        <label for="password-confirm" class="registration__label"> @lang('messages.Подтвердить пароль') </label>
                    </div>
                    <div class="checkbox checkbox-registration">
                        <label class="checkbox-wrap">
                            <input type="checkbox" class="checkbox__input">
                            <span class="checkbox__text checkmark"></span>
                        </label>
                        <div class="agree-anchor">
                            @lang('messages.Создавая учетную запись, вы соглашаетесь с')
                            <a href="{{ route('page', ['slug' => 'agree']) }}" class="account-list__link {{ Request::is('page/agree') ? 'account-list__link-active' : '' }}" target="_blank">
                                @lang('messages.пользовательским соглашением')
                            </a>
                        </div>
                    </div>
                </div>


                <a href="#" onclick="event.preventDefault();
                             document.getElementById('registration-form').submit();" class="registration-page__link registration__link"> @lang('messages.Зарегистрироваться') </a>
            </div>

        </form>
        <form class="registration-form">

            <div class="registration-page__login login">
                <div class="login__title"> @lang('messages.Вход') </div>

                <a href="{{ route('login') }}" class="registration-page__link login__link"> @lang('messages.Вход в личный кабинет') </a>
                <a href="{{ route('password.request') }}" class="registration-page__forgot-pass login__forgot-pass"> @lang('messages.Забыли пароль?') </a>
            </div>
        </form>

    </div>
</div>
@endsection
@section('scripts')
    <script>
        fbq('track', 'CompleteRegistration');
    </script>
@endsection
