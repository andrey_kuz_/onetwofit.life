@extends('layouts.app')

<!-- Main Content -->
@section('content')
    <div class="forgot-password">
        <div class="forgot-password__form">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="registration-form" id="resetForm" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="forgot-password__left">
                    <div class="forgot-password__title"> @lang('messages.Забыли пароль?') </div>
                    <div class="forgot-password__row">
                        <input id="email" type="email" class="forgot-password__input" name="email" value="{{ old('email') }}">


                        <label for="email" class="forgot-password__label"> @lang('messages.email') </label>
                    </div>
                    <input type="submit" value="@lang('messages.Сбросить пароль')" class="forgot-password__link">
                </div>
                <div class="forgot-password__right">
                    <a href="{{ route('login') }}" class="forgot-password__link"> @lang('messages.Войти') </a>
                    <a href="{{ route('register') }}" class="forgot-password__link"> @lang('messages.Зарегистрироваться') </a>
                </div>
            </form>
        </div>
    </div>
@endsection
