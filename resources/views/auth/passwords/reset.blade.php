@extends('layouts.app')

@section('content')
    <div class="forgot-password">
        <div class="forgot-password__form">
            <form class="registration-form" role="form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="forgot-password__left">
                    <div class="forgot-password__title">@lang('messages.new_pass')</div>
                    <div class="forgot-password__row">
                        <input id="email" type="email" class="forgot-password__input" name="email" value="{{ $email or old('email') }}" >

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <label for="email" class="forgot-password__label">@lang('messages.email')</label>
                    </div>
                    <div class="forgot-password__row">
                        <input id="password" type="password" class="forgot-password__input" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <label for="password" class="forgot-password__label">@lang('messages.pass')</label>
                    </div>
                    <div class="forgot-password__row">
                        <input id="password-confirm" type="password" class="forgot-password__input" name="password_confirmation" required>

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                        <label for="password-confirm" class="forgot-password__label">@lang('messages.pass_confirm')</label>
                    </div>
                    <input type="submit" value="@lang('messages.Подтвердить пароль')" class="forgot-password__link">
                </div>
            </form>
        </div>
    </div>
@endsection
