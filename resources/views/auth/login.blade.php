@extends('layouts.app')

@section('content')
    <div class="registration-page">
        <div class="registration-page__form">
            <form class="registration-form" id="login-form" action="{{ route('login') }}" method="post">

                <div class="registration-page__registration registration active">
                    <div class="registration__title"> @lang('messages.Вход') </div>
                    <div class="registration-wrap">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (session('warning'))
                            <div class="alert alert-warning">
                                {{ session('warning') }}
                            </div>
                        @endif

                        @include('auth.social')

                        <div class="registration-page__row">

                            <input type="text" name="email" id="email1" class="registration__input" autocomplete="new-password" value="{{ old('email') }}">
                            <label for="email1" class="registration__label"> @lang('messages.email') </label>
                            {{ csrf_field() }}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="registration-page__row">
                            <input type="password" name="password" id="password" class="registration__input" autocomplete="new-password" value="">
                            <label for="password" class="registration__label"> @lang('messages.Пароль') </label>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <a href="#" onclick="event.preventDefault();
                             document.getElementById('login-form').submit();" class="registration-page__link login__link"> @lang('messages.Вход в личный кабинет') </a>
                    <a href="{{ url('/password/reset') }}" class="registration-page__forgot-pass login__forgot-pass"> @lang('messages.Забыли пароль?') </a>
                </div>
            </form>

            <form class="registration-form">
                <div class="login-page__login login">
                    <div class="login__title"> @lang('messages.Регистрация') </div>
                    <div class="login-wrap"></div>
                    <a href="{{ route('register') }}" class="registration-page__link registration__link"> @lang('messages.Зарегистрироваться') </a>
                    @if ($is_checkout == 1)
                        <br />
                        <a href="{{ route('checkout', ['continueWithoutAuth' => 1]) }}" class="registration-page__link"> @lang('messages.Продолжить оформление как гость') </a>
                    @endif
                </div>
            </form>

        </div>
    </div>
@endsection



