<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Произошла ошибка</title>
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet"></head>
<body class="body-letter-page">
<div class="letter-page">
    <div class="letter-page__logo">
        <a href="index.html"><img src="{{asset('images/group-copy.svg')}}" alt="logo"></a>
    </div>
    <div class="letter-page__image">
        <img src="{{asset('images/letter.png')}}" alt="letter">
    </div>
    <div class="letter-page__wrap-white">
        <div class="letter-page__title">Данная страница недоступна</div>
        <a href="{{route('home')}}" class="letter-page__link">Перейти к покупкам</a>
    </div>
    <div class="letter-page__info">
        <div class="letter-page__telephone">
            @include('layouts.phones.phone')
            <a href="#">info@onetwofit.org</a>
        </div>
    </div>
</div>
</body>
</html>