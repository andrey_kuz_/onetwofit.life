<div class="some-product__interact interact-cart">
    <div class="interact-cart__minimize"><i class="fa fa-window-minimize" aria-hidden="true"></i></div>
    <div class="interact-cart__icon"><i class="fa fa-close"></i></div>
    <div class="interact-cart__image">
        <img src="{{ asset( 'images/car.png') }}" alt="car" class="car">
    </div>
    <div class="interact-cart__text text-center summ">
    </div>
    <div class="interact-cart__text text-center"> @lang('messages.Осталось') <span class="interact-cart__balance"></span> @lang('messages.до бесплатной доставки')</div>
    <div class="interact-cart__progress">
        <img src="{{ asset( 'images/filled-line.png') }}" alt="progress">
    </div>
    <div class="interact-cart__product product">
    </div>
    <div class="alert cartPopupErrors alert-danger" style="display:none"></div>
    <a href="{{ route('cart.view') }}" class="interactcart-toorder"> @lang('messages.Оформить заказ') </a>
</div>
<div class="interact-cart-minimize">
    <div class="interact-cart__minimize"><i class="fa fa-window-maximize" aria-hidden="true"></i></div>
    <div class="interact-cart__icon"><i class="fa fa-close"></i></div>
</div>