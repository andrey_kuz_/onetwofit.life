@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="cart-page">
                    <div class="cart-page__content">
                        @if ($cart->item_count == 0)
                            <div class="col-sm-12">
                                <div class="cart-page__title"> @lang('messages.Корзина пуста') </div>
                            </div>
                        @else
                            <div class="col-sm-12">
                                <div class="cart-page__title"> @lang('messages.Корзина') ({{$cart->info->itemsCount}})
                                </div>

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table cart-page__cart">
                                        <tr class="table-caption">
                                            <td></td>
                                            <td></td>
                                            <td> @lang('messages.Товары') </td>
                                            <td> @lang('messages.Цена') </td>
                                            <td> @lang('messages.Количество') </td>
                                            <td> @lang('messages.Скидка') </td>
                                            <td class="all"> @lang('messages.Всего') </td>
                                        </tr>
                                        @foreach($cartItems as $cartItem)
                                            <tr>
                                                <td class="icon"><a href="{{ route('cart.remove', ['product' => $cartItem->product_id, 'isAjax' => false]) }}">X</a></td>
                                                <td>
                                                    @if($image=$cartItem->product->getMainImg())
                                                        <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:190px">
                                                    @else
                                                        <img src="{{ asset( 'images/bitmap.png')}}" alt="product">
                                                    @endif
                                                </td>
                                                @php $item = $cart->info->cartItemsInfo[$cartItem->id]; @endphp
                                                <td><a class="cart-itemlink" href="{{ route('product.index',['id'=>$cartItem->product_id, 'slug'=> $products[$cartItem->product_id]]) }}">{{ $cartItem->product->getTranslatedAttribute('name') }}</a></td>
                                                <td class="cart-summ">
                                                    <span class="cartpage-price">
                                                        <span class="price_value">{{ $item->toCurrency($cartItem->product->price_discounted)['value'] }}</span> <span class="price_currency">{{ $item->toCurrency($cartItem->product->price_discounted)['currency'] }}</span>
{{--                                                        {{to_currency($cartItem->product->price_discounted, true)}}--}}
                                                    </span>
                                                </td>
                                                <td>
                                                    <a href="{{ route('cart.sub', ['product' => $cartItem->product_id, 'isAjax' => false]) }}"><span class="decrease">-</span></a>
                                                    <span class="cart-count">{{$cartItem->quantity}}</span>
                                                    <a href="{{ route('cart.add', ['product' => $cartItem->product_id, 'deltaQuantity' => 1, 'isAjax' => false]) }}"><span class="increase">+</span></a>
                                                </td>

                                                <td class="cart-itemstyle">
                                                    <p>
                                                        @if ($item->getComboDiscountAbs()  > 0)
                                                            <a href="#"> @lang('messages.Специально предложение') -{{ to_currency($item->getComboDiscountAbs(), true) }}</a>
                                                        @endif
                                                    </p>
                                                    <p>
                                                        @if ($item->getQtyDiscountAbs()->total > 0)
                                                            <a href="#"> @lang('messages.Количество') -{{$item->getDiscount()}}%({{ to_currency($item->getQtyDiscountAbs()->total, true) }})</a>
                                                        @endif
                                                    </p>
                                                </td>
                                                <td class="cart-summ">
                                                    <span class="cartpage-price"><span class="price_value">{{ $item->toCurrency($item->getFinalDiscountedPrice())['value'] }}</span> <span class="price_currency">{{ $item->toCurrency($item->getFinalDiscountedPrice())['currency'] }}</span>
{{--                                                        {{ to_currency($cart->info->cartItemsInfo[$cartItem->id]->getFinalDiscountedPrice(), true) }}--}}
                                                    </span>
                                                    @if ($item->getBothDiscountAbs() > 0)
                                                        <span class="cartpage-price-sale">{{ to_currency($item->getPrice()->total, true) }}</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <div class="cart-page__row">
                                    <div class="cart-newwrap">
                                        <form class="table-cart__form" id="addPromoForm">
                                            {{ csrf_field() }}
                                            <label class="table-cart__form-head">
                                                <input type="hidden" name="checkoutUrl" value="{{$checkoutUrl}}" />
                                                <input type="text" placeholder="@lang('messages.Промокод')" name="promocode" class="table-cart__promocode" value="{{$cart->promocode}}" />
                                                <a href="#" onclick="addPromo('{{ route('cart.addpromo') }}'); return false;" class="table-cart__promobtn"> @lang('messages.Применить') </a>
                                                <a href="{{ route('home') }}" class="cart-page__back pull-right"> @lang('messages.Вернуться к покупкам') </a>
                                            </label>
                                            <label>
                                                <p class="text_area_header">@lang('messages.Оставить комментарий к заказу')</p>
                                                <textarea id="comment" name="note"></textarea>
                                            </label>
                                        </form>

                                    </div>

                                </div>
                            </div>
                            <h3></h3>

                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 cart__table-buy">
                                @include('cart.viewsummary')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        fbq('track', 'AddToCart', {
            content_type: 'product',
            currency: ' UAH '
        });
    </script>
@endsection



