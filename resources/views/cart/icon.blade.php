<a href="{{ route('cart.view') }}" class="header-main__cart">
    <span class="header-main__cart-count">{{$cartCount?:0}}</span>
</a>