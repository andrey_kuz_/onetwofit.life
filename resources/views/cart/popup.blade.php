@foreach($cartItems as $cartItem)
    <div class="interact-cart__row">
        <div class="product__counter">
            <span class="increase glyphicon glyphicon-triangle-top" onclick="addProductToCart('{{ route('cart.addAjax', ['product' => $cartItem->product_id, 'deltaQuantity' => 1, 'isAjax' => true]) }}');"></span>
            {{$cartItem->quantity}}
            <span class="descrease glyphicon glyphicon-triangle-bottom" onclick="addProductToCart('{{ route('cart.sub', ['product' => $cartItem->product_id, 'isAjax' => true]) }}');"></span>
        </div>
        <div class="product__image">
            @if($mainImage=$cartItem->product->getMainImg())
                <img src="@if( !filter_var($mainImage, FILTER_VALIDATE_URL)){{ Voyager::image( $mainImage ) }}@else{{ $mainImage }}@endif" class="item__image item__image-first">
            @endif
        </div>
        <div class="product__price">
            <div class="some-product__price-current">{{ to_currency($cartInfo->cartItemsInfo[$cartItem->id]->getFinalDiscountedPrice(), true) }}</div>
        </div>
        <span class="product__icon" onclick="addProductToCart('{{ route('cart.remove', ['product' => $cartItem->product_id, 'isAjax' => true]) }}');">X</span>
    </div>
@endforeach


<script>
    fbq('track', 'AddToCart', {
        content_type: 'product',
        currency: ' UAH ',
    });
</script>

