<div class="order-page__cart" id="cartSummary">
    <div class="table-responsive table-cart">
        <div class="interact-cart__text text-center">
            @if ($cart->info->freeShippingLeft > 0)
                @lang('messages.Осталось') <span class="interact-cart__balance">{{to_currency($cart->info->freeShippingLeft, true)}}</span> @lang('messages.до бесплатной доставки')
            @else
                @lang('messages.Доставка бесплатно')
            @endif
        </div>
        <div class="interact-cart__progress">
            <img src="{{ asset( 'images/filled-line.png') }}" alt="progress" style="left: -{{$cart->info->freeShippingLeft * 100 / $cart->info->freeShipping}}%">
        </div>
        <table class="table">
            <tbody>
            <tr>
                <td> @lang('messages.Сумма') </td>
                <td>{{to_currency($cart->info->total, true)}}</td>
            </tr>
            <tr>
                <td> @lang('messages.Скидка') </td>
                <td>{{to_currency($cart->info->bothDiscountAbs, true)}}</td>
            </tr>
            <!--<tr>
                <td>Налог</td>
                <td></td>
            </tr>-->
            <tr>
                <td> @lang('messages.Бонус') </td>
                <td></td>
            </tr>

            @if ($promocodeStatusInfo['success'])
                <tr class="total">
                    <td> @lang('messages.К оплате') </td>
                    <td>{{to_currency($cart->info->discountedWithoutPromo, true)}}</td>
                </tr>
                <tr class="total">
                    <td> @lang('messages.С учетом промокода') (-{{$cart->info->discountPromo}}%)</td>
                    <td>{{to_currency($cart->info->discounted, true)}}</td>
                </tr>
            @else
                <tr class="total">
                    <td> @lang('messages.К оплате') </td>
                    <td>{{to_currency($cart->info->discounted, true)}}</td>
                </tr>
            @endif
            </tbody>
        </table>
        <a href="{{ is_null($checkoutUrl) ? route('checkout',['continueWithoutAuth' => 1]) : $checkoutUrl }}" class="table-cart__confirm"> @if (is_null($checkoutUrl)) @lang('messages.Перейти к оформлению заказа') @else @lang('messages.Продолжить оформление заказа') @endif</a>
        <div class="table-cart__footer">
            <div class="alert addPromoForm alert-success" style="display:none"></div>
            <div class="alert addPromoForm alert-danger" @if ($promocodeStatusInfo['success'] || !$promocodeStatusInfo['mess']) style="display:none" @endif>{{(!$promocodeStatusInfo['success'])?$promocodeStatusInfo['mess']:''}}</div>
            <div class="table-cart__link_block"><a href="#" class="table-cart__link"> @lang('messages.Редактировать заказ') </a></div>
        </div>
    </div>
</div>