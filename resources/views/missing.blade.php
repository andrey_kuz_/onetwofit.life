@extends('layouts.app')
@section('content')
  <div class="page404">
    <h1 class="title-404">404</h1>
    <span >Страница не найдена</span>
    <a href="{{ route('home') }}" class="add-to-cart" >На главную страницу</a>
  </div>
@endsection