<!DOCTYPE html>
<html>
<head>
    <title>{{__('emails.subject_order_success')}} ID:{{$order->id}}</title>
</head>

<body>
<p> @lang('messages.thank_for_order') </p>
<p> @lang('messages.important_details'):</p>
<br/>
<div class="table-responsive attributes-info">
    <table class="table">
        <tbody>
        <tr>
            <td> @lang('messages.Способ оплаты'): </td>
            <td class="text-right">{{isset($order->paymentType) ? $order->paymentType->name : ''}}</td>
        </tr>
        <tr>
            <td> @lang('messages.Способ доставки'): </td>
            <td class="text-right">{{isset($order->shippingType) ? $order->shippingType->name : ''}}, {{!is_null($order->shippingMethod) ? $order->shippingMethod->name : ''}}</td>
        </tr>
        <tr class="total totalblock">
            <td><b> @lang('messages.Сумма'):</b></td>
            <td class="text-right"><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
        </tr>
        </tbody>
    </table>
</div>
<br/>
<div class="order-page-last__number"> @lang('messages.Заказ ') № {{$order->id}} @lang('messages.Успешно оплачен') </div>
<div class="attributes">
    <div class="table-responsive">
        <table class="table">
            <tbody>
            @foreach($order->orderItems()->get() as $orderItem)
                <tr>
                    <td>{{$orderItem->product->getTranslatedAttribute('name')}}</td>
                    <td><b>{{to_currency($orderItem->total_price_discounted, true)}}</b></td>
                </tr>
            @endforeach

            @if (!is_null($order->promocodeUsed))
                <tr class="total">
                    <td><b> @lang('messages.К оплате') </b></td>
                    <td><b>{{ to_currency($order->total_price_discounted_without_promo, true) }}</b></td>
                </tr>
                <tr class="total">
                    <td> @lang('messages.С учетом промокода') (-{{to_currency($order->discount_promo, true)}}%)</td>
                    <td><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                </tr>
            @else
                <tr class="total">
                    <td><b> @lang('messages.К оплате') </b></td>
                    <td></td>
                    <td><b>{{ to_currency($order->total_price_discounted, true) }}</b></td>
                </tr>
            @endif
        </table>
    </div>
</div>


<div class="sms">
    <p> @lang('messages.want_to_change') :</p>
    @include('layouts.phones.list')
<br/>
<p> @lang('messages.С уважением'), <br/>
@lang('messages.команда Onetwofit').</p>
</div>

</body>

</html>