<!DOCTYPE html>
<html>
<head>
    <title>Reset Email</title>
</head>

<body>

{{__('emails.reset_pass_text')}}
<a href="{{url( "/password/reset/" . $token )}}">{{__('emails.reset_pass_butt')}}</a>
<br/>
<br/>
@lang('messages.С уважением'), <br/>
@lang('messages.команда Onetwofit').
</body>

</html>