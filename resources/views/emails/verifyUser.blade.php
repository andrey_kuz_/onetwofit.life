<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Вы почти достигли цели!</h2>
<br/>
Осталось только перейти по ссылке для завершения регистрации в нашем магазине -
<a href="{{ url( 'user/verify', [$user->verifyUser->token, $cart] ) }}">[подтвердить email]</a>
<br/>
<br/>
@lang('messages.С уважением'), <br/>
@lang('messages.команда Onetwofit').
</body>

</html>