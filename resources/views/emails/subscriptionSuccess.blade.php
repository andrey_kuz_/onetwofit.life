<!DOCTYPE html>
<html>
<head>
    <title>{{__('emails.subject_subscription_success')}}</title>
</head>

<body>
Товар <a href="http://one2fit.demo.ukrohost.com/product/{{$product->id}}"> {{$product->name}} </a> появился на складе.
<br/>
<br/>
@lang('messages.С уважением'), <br/>
@lang('messages.команда Onetwofit').
</body>

</html>