<div class="micromarking">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="#">
            <span itemprop="name">Поиск товара</span></a>
            <meta itemprop="position" content="1" />
        </li>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="#">
                <span itemprop="name">Поиск по бренду</span></a>
            <meta itemprop="position" content="2" />
        </li>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="{{ route('brand.list') }}">
                <span itemprop="name"> Все бренды</span></a>
            <meta itemprop="position" content="3" />
        </li>
        @if(isset($section))
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{{ route('brand.index', ['slug' => $section->slug]) }}">
                    <span itemprop="name"> {{ $section->name }}</span></a>
                <meta itemprop="position" content="4" />
            </li>
        @endif

     </ol>
</div>