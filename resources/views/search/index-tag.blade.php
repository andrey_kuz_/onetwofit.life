@extends('layouts.app')
@section('content')
    @include('tag.micromarking_bread')
    <ul class="tags-breadcrumbs breadcrumbs">
        @include('tag.breadcrumb', ['section' => null])
    </ul>
    <div class="tags-page__name">Все теги</div>

    <div class="search-tag">
        <div class="search-tag__wrap">
            <div class="search-tag__result">
                <div class="result__content">
                    <div class="result__content-wrap">
                        <ul>
                            @foreach($list as $tag)
                                <li> <a href="{{ route('tag.index', ['slug' => $tag->slug]) }}">{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @include('cart.modal')
    @include('oneclick.modal')

@endsection
