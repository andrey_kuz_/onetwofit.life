@extends('layouts.app')
@section('content')
    <div class="search-page">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{route('search')}}" method="get" id="searchForm">
        <div class="search-page__row">
                @if($searchString)
                <span class="search-icon"></span>
                <input type="text" placeholder="Поиск..." class="input-search" name="search_string" value="{{isset($searchString)?$searchString:''}}">
                    @elseif($searchBrand)
                <span class="search-icon"></span>
                <input type="text" placeholder="Поиск..." class="input-search" name="search_brand" value="{{isset($searchBrand)?$searchBrand:''}}">
                    @elseif($searchFlavors)
                <span class="search-icon"></span>
                <input type="text" placeholder="Поиск..." class="input-search" name="search_flavors" value="{{isset($searchFlavors)?$searchFlavors:''}}">
            @elseif($searchCode)
                <span class="search-icon"></span>
                <input type="text" placeholder="Поиск..." class="input-search" name="search_code" value="{{isset($searchCode)?$searchCode:''}}">
                @endif
        </div>
        <div class="search-page__row">
            <label for="price">Сортировать по</label>
            <select class="select-search" name="sort_by" id="price" onchange="$('#searchForm').submit();">
                <option value="price_asc" @if ($sort_by == 'price_asc') selected @endif>возрастанию цены</option>
                <option value="price_desc" @if ($sort_by == 'price_desc') selected @endif>убыванию цены</option>
                <option value="rating_desc" @if ($sort_by == 'rating_desc') selected @endif>рейтингу</option>
            </select>
        </div>
        </form>
        <div class="gridcontainer cols-five bottom_pricepadding">
            @foreach($products as $product)
                @include('products.itemBlockGrid', ['itemProduct'=>$product])
            @endforeach
        </div>
    </div>
    @if($searchString)
        {{$products->appends(['search_string'=>$searchString])->links('vendor.front_pagination.default')}}
    @elseif($searchBrand)
        {{$products->appends(['search_brand'=>$searchBrand])->links('vendor.front_pagination.default')}}
    @elseif($searchFlavors)
        {{$products->appends(['search_flavors'=>$searchFlavors])->links('vendor.front_pagination.default')}}
    @elseif($searchCode)
        {{$products->appends(['search_code'=>$searchCode])->links('vendor.front_pagination.default')}}
    @endif


    @include('cart.modal')
    @include('oneclick.modal')
    @include('products.history')
@endsection

@section('scripts')
    <script>
        fbq('track', 'Search');
    </script>
@endsection
