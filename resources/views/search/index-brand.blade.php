@extends('layouts.app')
@section('content')
    @include('brand.micromarking_bread')
    <ul class="brands-breadcrumbs breadcrumbs">
        @include('brand.breadcrumb', ['section' => null])
    </ul>
    <div class="brands-page__name">Все бренды</div>

    <div class="search-brand">

        <div class="search-brand__wrap">

            <div class="search-brand__letter">
                <div class="letter">
                    @foreach($num_list as $k => $v)
                        <a href="#" class="letter__link">{{ $k }}</a>
                    @endforeach
                    @foreach($en_list as $k => $v)
                        <a href="#{{ $k }}" class="letter__link">{{ $k }}</a>
                    @endforeach
                </div>
                <div class="letter">
                    @foreach($ru_list as $k => $v)
                        <a href="#{{ $k }}" class="letter__link">{{ $k }}</a>
                    @endforeach
                </div>
            </div>

            <div class="search-brand__result">

                @foreach($num_list as $k => $v)
                    <div class="result__content" id="{{ $k }}">
                        <div class="result__content-head">
                            <div class="result__content-head-title">
                                {{ $k }}
                            </div>

                            <a href="#" class="result__content-head-btn">
                                Вернуться к началу
                            </a>
                        </div>

                        <div class="result__content-wrap">
                            <ul>
                                @foreach($v as $brand)
                                    <li><a href="{{ route('brand.index', ['slug' => $brand->slug]) }}">{{ $brand->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach

                @foreach($en_list as $k => $v)
                    <div class="result__content" id="{{ $k }}">
                        <div class="result__content-head">
                            <div class="result__content-head-title">
                                    {{ $k }}
                            </div>

                            <a href="#" class="result__content-head-btn">
                                Вернуться к началу
                            </a>
                        </div>

                        <div class="result__content-wrap">
                            <ul>
                                @foreach($v as $brand)
                                    <li><a href="{{ route('brand.index', ['slug' => $brand->slug]) }}">{{ $brand->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach

                @foreach($ru_list as $k => $v)
                    <div class="result__content" id="{{ $k }}">
                        <div class="result__content-head">
                            <div class="result__content-head-title">
                                {{ $k }}
                            </div>

                            <a href="#" class="result__content-head-btn">
                                Вернуться к началу
                            </a>
                        </div>

                        <div class="result__content-wrap">
                            <ul>
                                @foreach($v as $brand)
                                    <li><a href="{{ route('brand.index', ['slug' => $brand->slug]) }}">{{ $brand->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>


    </div>


    @include('cart.modal')
    @include('oneclick.modal')

@endsection
