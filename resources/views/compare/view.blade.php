@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="account account-featured">
                <div class="col-sm-12 account__page account-featured__page">
                    <div class="search-page">

                        <div class="table-responsive table-compare">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td>
                                        <h1 class="account__title account-featured__title compare-title"> @lang('messages.Сравнение товаров') </h1>
                                        <p class="compare-lined"> @lang('messages.Добавлено товаров') {{ count($compareProductsAssoc) }}/5</p>

                                    </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                    <td class="compare-product">
                                        <a href="{{route('compare.delete', ['id' => $compareProductAssoc->id])}}" class="compare-close">X</a>
                                        <div class="slider-category__item product">
                                            <div class="product-help-block">
                                                <div class="product__image">
                                                    @if($compareProductAssoc->mainImg)
                                                        <img src="@if( !filter_var($compareProductAssoc->mainImg, FILTER_VALIDATE_URL)){{ Voyager::image( $compareProductAssoc->mainImg ) }}@else{{ $compareProductAssoc->mainImg }}@endif" class="img-responsive center-block">
                                                    @else
                                                        <img src="{{ asset( 'images/bitmap.png')}}" alt="product" class="img-responsive center-block">
                                                    @endif
                                                </div>
                                                <div class="product__description">
                                                    {{--<a href="{{ route('product.index', $compareProductAssoc->id) }}">--}}
                                                        {{$compareProductAssoc->name}}
                                                    {{--</a>--}}
                                                </div>
                                                <div class="product__rating">
                                                     @include('reviews.stars', ['ratingValue' => $compareProductAssoc->rating])
                                                    <span class="rating-count">{{$compareProductAssoc->rating_count}}</span>
                                                </div>
                                                <div class="product__price">
                                                    <div class="product__price-old">{{ ($compareProductAssoc->discount > 0) ? (to_currency($compareProductAssoc->price, true)) : '' }}</div>
                                                    <div class="product__price-current">{{ to_currency($compareProductAssoc->price_discounted, true) }}</div>
                                                </div>
                                                <a href="#" class="product__btn" onclick="addToCartEvent('{{ route('cart.addAjax', ['product' => $compareProductAssoc->id, 'deltaQuantity' => 1, 'isAjax' => true]) }}'); return false;"> @lang('messages.Добавить в корзину') </a>
                                            </div>
                                        </div>
                                    </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td> @lang('messages.Артикул') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                    <td>{{ $compareProductAssoc->article }}</td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td> @lang('messages.brand') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td>{{  $compareProductAssoc->params->brand }}</td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td> @lang('messages.country') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td>{{ $compareProductAssoc->params->country }}</td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td> @lang('messages.exp_date') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td>{{ __('messages.exp_date') }} {{ $compareProductAssoc->exp_date }}</td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td> @lang('messages.volume') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td>{{ $compareProductAssoc->params->volume }}</td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td> @lang('messages.weight') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td>{{ $compareProductAssoc->params->weight }}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    <td> @lang('messages.pack_pieces') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td>{{ $compareProductAssoc->params->pack_pieces }}</td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td> @lang('messages.issue_form') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td>{{ $compareProductAssoc->params->issue_form }}</td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td> @lang('messages.flavor') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td>{{ $compareProductAssoc->params->flavor }}</td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td> @lang('messages.Теги') </td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td></td>
                                    @endforeach
                                </tr>

                                <tr>
                                    <td></td>
                                    @foreach($compareProductsAssoc as $compareProductAssoc)
                                        <td><a href="{{route('compare.delete', ['id' => $compareProductAssoc->id])}}"> @lang('messages.Удалить из сравнения') </a></td>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('cart.modal')
    @include('oneclick.modal')
    @include('oneclick.modal')
@endsection
