@if (isset($section))
    @include('categories.breadcrumb', ['section' => $section->parent, 'is_leaf' => 0])

<li class="breadcrumbs__item"><a class="breadcrumbs__link @if ($is_leaf) active @endif" href="{{ route($sectionRoute, ['slug' => $section->slug]) }}">{{ $section->getTranslatedAttribute('name') }}</a>@if (!$is_leaf) <span class="breadcrumbs__icon">/</span> @endif</li>
@else
    <li class="breadcrumbs__item"><a class="breadcrumbs__link @if ($is_leaf) active @endif" href="/"> @lang('messages.Главная') </a>@if (!$is_leaf) <span class="breadcrumbs__icon">/</span> @endif</li>
@endif
