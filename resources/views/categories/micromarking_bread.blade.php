<div class="micromarking">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="{{ route('home') }}">
            <span itemprop="name">@lang('messages.Главная')</span></a>
            <meta itemprop="position" content="1" />
        </li>
        @foreach($breadcrumb_array as $item)
            @if($item != null)
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{{ route($sectionRoute, ['slug' => $item->slug]) }}">
                    <span itemprop="name">{{ $item->getTranslatedAttribute('name') }}</span></a>
                <meta itemprop="position" content="{{ $loop->iteration + 1 }}" />
            </li>
            @endif
        @endforeach
    </ol>
</div>
