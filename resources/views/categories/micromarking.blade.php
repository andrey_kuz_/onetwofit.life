@if($category != false)
<div class="micromarking">
    <div itemscope itemtype="http://schema.org/Product">
        <p itemprop="Name">{{$category->name}}</p>
        <div itemtype="http://schema.org/AggregateOffer" itemscope="" itemprop="offers">
            <meta content="{{ $category->subProducts()->count() }}" itemprop="offerCount">
            <meta content="{{ $category->subProducts()->max('price') }}" itemprop="highPrice">
            <meta content="{{ $category->subProducts()->min('price') }}" itemprop="lowPrice">
            <meta content="UAH" itemprop="priceCurrency">
        </div>
    </div>
</div>
@endif
