@extends('layouts.app')
@section('content')
@include('categories.micromarking')
@include('categories.micromarking_bread')
    <ul class="breadcrumbs">
        @include('categories.breadcrumb', ['section' => $section, 'is_leaf' => 1])
    </ul>

    <div class="categories-page">
        @if (isset($section) && $section)
            <h1 class="categories-page__name">{{ $section->name }}</h1>
        @else
            @if (isset($filters['is_bestseller']) && $filters['is_bestseller'])
                <h1 class="categories-page__name">@lang('messages.is_bestseller')</h1>
            @endif
            @if (isset($filters['is_hit']) && $filters['is_hit'])
                <h1 class="categories-page__name">@lang('messages.is_hit')</h1>
            @endif
            @if (isset($filters['is_special']) && $filters['is_special'])
                <h1 class="categories-page__name">@lang('messages.is_special')</h1>
            @endif
            @if (isset($filters['is_new']) && $filters['is_new'])
                <h1 class="categories-page__name">@lang('messages.is_new')</h1>
            @endif
        @endif
        @include('categories.filters')

        <div class="search-page__row col-md-9 col-sm-8 col-xs-12">
            <form id="sortForm">
            <label for="price"> @lang('messages.Сортировать по') </label>
            <select class="select-search" name="sort_by" id="price" onchange="filterProducts();">
                <option value="rating_desc"> @lang('messages.рейтингу') </option>
                <option value="price_asc"> @lang('messages.возрастанию цены') </option>
                <option value="price_desc"> @lang('messages.убыванию цены') </option>
                <option value="newest"> @lang('messages.по новизне') </option>
            </select>
            </form>

            @include('categories.content')

        </div>
    </div>

    @include('cart.modal')
    @include('oneclick.modal')
    @include('compare.modal')


    @include('products.history')
@endsection
