<form id="filtersForm" action="{{ $ajaxContentUrl }}">
<div class="categories-page__sidebar sidebar col-md-3 col-sm-4 col-xs-12">
    <label class="sidebar-action"> @lang('messages.Фильтровать по') </label>
    @if($sub_categories_exist === true)
    @if($sub_categories->count())
            @if($category != false)
    <div class="form-group categor">
        <label class="form-group__text"> @lang('messages.Категория') </label>
        @foreach($category->subcategories as $subcategory)
        <div class="form-group__result checkbox">
            <label class="checkbox-wrap">   {{ $subcategory->name }}
               {{-- {{var_dump($filters['categories'])}}
                                                       {{var_dump($subcategory->id)}}
                                                       {{dd(stripos($filters['categories'],strval($subcategory->id)))}}--}}
                @if(isset($filters['categories']) && (stripos($filters['categories'],strval($subcategory->id))!== false))
                    <input type="checkbox" class="checkbox__input" value="{{ $subcategory->id }}" data-name="{{ $subcategory->slug }}" name="categories[]" checked onclick="filterProducts();" >
                @else
                    <input type="checkbox" class="checkbox__input" value="{{ $subcategory->id }}" data-name="{{ $subcategory->slug }}" name="categories[]" onclick="filterProducts();">
                @endif
                <span class="checkbox__text checkmark"></span>
            </label>
                @if($subcategory->count())
                    <div class="form-group categor-sub none">
                        @foreach($subcategory->subcategories as $sub_subcategory)
                            <div class="form-group__result checkbox">
                                <label class="checkbox-wrap">   {{ $sub_subcategory->name }}
                                    @if(isset($filters['categories']) && (stripos($filters['categories'],strval($sub_subcategory->id))!== false))
                                        <input type="checkbox" class="checkbox__input" value="{{ $sub_subcategory->id }}" data-name="{{ $sub_subcategory->slug }}" name="categories[]" checked onclick="filterProducts();" >
                                    @else
                                        <input type="checkbox" class="checkbox__input" value="{{ $sub_subcategory->id }}" data-name="{{ $sub_subcategory->slug }}" name="categories[]" onclick="filterProducts();">
                                    @endif
                                    <span class="checkbox__text checkmark"></span>
                                </label>
                                @foreach($sub_subcategory->subcategories as $sub_sub_subcategory)
                                    @if($sub_sub_subcategory->count())
                                        <div class="form-group categor-sub-sub none">
                                                <div class="form-group__result checkbox">
                                                    <label class="checkbox-wrap">   {{ $sub_sub_subcategory->name }}
                                                        @if(isset($filters['categories']) && (stripos($filters['categories'],strval($sub_sub_subcategory->id))!== false))
                                                            <input type="checkbox" class="checkbox__input" value="{{ $sub_sub_subcategory->id }}" data-name="{{ $sub_sub_subcategory->slug }}" name="categories[]" checked onclick="filterProducts();" >
                                                        @else
                                                            <input type="checkbox" class="checkbox__input" value="{{ $sub_sub_subcategory->id }}" data-name="{{ $sub_sub_subcategory->slug }}" name="categories[]" onclick="filterProducts();">
                                                        @endif
                                                        <span class="checkbox__text checkmark"></span>
                                                    </label>
                                                </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                @endif
        </div>
        @endforeach

    </div>
    @endif
    @endif
    @endif


    @if( $sub_categories_exist === "special")
        @if($categories->count())
            <div class="form-group categor">
                <label class="form-group__text"> @lang('messages.Категория') </label>
                @foreach($categories as $category )
                    <div class="form-group__result checkbox">
                        <label class="checkbox-wrap">   {{ $category->name }}
                            @if(isset($filters['categories']) && (stripos($filters['categories'],strval($category->id))!== false))
                                <input type="checkbox" class="checkbox__input" value="{{ $category->id }}" data-name="{{ $category->slug }}" name="categories[]" checked onclick="filterProducts();" >
                            @else
                                <input type="checkbox" class="checkbox__input" value="{{ $category->id }}" data-name="{{ $category->slug }}" name="categories[]" onclick="filterProducts();">
                            @endif
                            <span class="checkbox__text checkmark"></span>
                        </label>
                    </div>
                @endforeach
            </div>
        @endif
    @endif



    @if(isset($brands))
        <div class="form-group brand">
            <label for="brand" class="form-group__text"> @lang('messages.Бренд') </label>
            <input type="text" placeholder="Поиск по бренду" id="brand" onkeyup="filterCheckboxes();" class="form-group__search">
            <div class="form-group__result checkbox list-minimized" id="BrandsList">
                @foreach($brands as $brand)
                <label class="checkbox-wrap brand_label" id="brand{{ $brand->id }}">
                    <span class="brand_text">{{$brand->name}}</span>
                    <input type="checkbox"
                    @if(isset($filters['brands']))
                        @if (in_array($brand->slug, $filters['brands']))
                            checked="checked";
                        @endif
                    @endif
                    class="checkbox__input" name="brands[]" value="{{ $brand->id }}" data-name="{{ $brand->slug }}" onclick="filterProducts(); ">
                    <span class="checkbox__text checkmark"></span>
                </label>
                @endforeach
                <span class="form-group__link-all" id="all-list-maximize"> @lang('messages.Полный список') </span>
            </div>
        </div>
    @endif
    <div class="form-group price-block">
        <label for="price-block" class="form-group__text"> @lang('messages.Цена') ({{ __('messages.currency_' . config('app.currency')) }})</label>
        <div class="form-group__limits">
            <span> @lang('messages.от') </span>
            <input type="text" placeholder="" id="price-block" name="price_from" class="form-group__search"
             @if(isset($filters['price_from']))
                   value="{{$filters['price_from']}}"
             @endif
            onblur="ignorePriceCheckboxes(); filterProducts();">
            <span> @lang('messages.до') </span>
            <input type="text" placeholder="" id="price-block2" name="price_to" class="form-group__search"
            @if(isset($filters['price_to']))
                   value="{{$filters['price_to']}}"
            @endif
            onblur="ignorePriceCheckboxes(); filterProducts();">
        </div>
        <div class="form-group__result checkbox">
            <label class="checkbox-wrap">0 - 150 {{ __('messages.currency_' . config('app.currency')) }}

                <input type="checkbox"
                @if(isset($filters['price_ranges']))
                    @if (in_array('0-150', $filters['price_ranges']))
                       checked="checked"
                    @endif
                @endif
                class="checkbox__input price_checkbox" name="price_ranges[]" value="0-150" onclick="filterProducts();">
                <span class="checkbox__text checkmark"></span>
            </label>
            <label class="checkbox-wrap">150 - 300 {{ __('messages.currency_' . config('app.currency')) }}
                <input type="checkbox"
                @if(isset($filters['price_ranges']))
                    @if (in_array('150-300', $filters['price_ranges']))
                       checked="checked"
                    @endif
                @endif
                class="checkbox__input price_checkbox" name="price_ranges[]" value="150-300" onclick="filterProducts();">
                <span class="checkbox__text checkmark"></span>
            </label>
            <label class="checkbox-wrap">300 - 500 {{ __('messages.currency_' . config('app.currency')) }}
                <input type="checkbox"
                @if(isset($filters['price_ranges']))
                    @if (in_array('300-500', $filters['price_ranges']))
                       checked="checked"
                    @endif
                @endif
                class="checkbox__input price_checkbox" name="price_ranges[]" value="300-500" onclick="filterProducts();">
                <span class="checkbox__text checkmark"></span>
            </label>
            <label class="checkbox-wrap">500+ {{ __('messages.currency_' . config('app.currency')) }}
                <input type="checkbox"
                @if(isset($filters['price_ranges']))
                    @if (in_array('500-', $filters['price_ranges']))
                       checked="checked"
                    @endif
                @endif
                       class="checkbox__input price_checkbox" name="price_ranges[]" value="500-" onclick="filterProducts();">
                <span class="checkbox__text checkmark"></span>
            </label>
        </div>
    </div>
    <div class="form-group spec">
        <label class="form-group__text"> @lang('messages.Специальное предложение') </label>
        <div class="form-group__result checkbox">
            <label class="checkbox-wrap">@lang('messages.is_hit')
                @if(isset($filters['is_hit']) && $filters['is_hit'])
                    <input type="checkbox" class="checkbox__input" name="is_hit" checked onclick="filterProducts();" >
                    <input type="hidden" name="is_hit" value="1" >
                @else
                    <input type="checkbox" class="checkbox__input" name="is_hit" onclick="filterProducts();">
                @endif
                <span class="checkbox__text checkmark"></span>
            </label>
            {{--<label class="checkbox-wrap">@lang('messages.is_super')
                @if(isset($filters['is_super']) && $filters['is_super'])
                    <input type="checkbox" class="checkbox__input" name="is_super" checked onclick="filterProducts();" >
                    --}}{{--<input type="hidden" name="is_hit" value="1" >--}}{{--
                @else
                    <input type="checkbox" class="checkbox__input" name="is_super" onclick="filterProducts();">
                @endif
                <span class="checkbox__text checkmark"></span>
            </label>--}}
            <label class="checkbox-wrap">@lang('messages.is_bestseller')
                @if(isset($filters['is_bestseller']) && $filters['is_bestseller'])
                    <input type="checkbox" class="checkbox__input" name="is_bestseller" checked onclick="filterProducts();">
                @else
                    <input type="checkbox" class="checkbox__input" name="is_bestseller" onclick="filterProducts();">
                @endif
                <span class="checkbox__text checkmark"></span>
            </label>
            <label class="checkbox-wrap">@lang('messages.is_super')
                @if(isset($filters['is_special']) && $filters['is_special'])
                    <input type="checkbox" class="checkbox__input" name="is_special" checked onclick="filterProducts();">
                @else
                    <input type="checkbox" class="checkbox__input" name="is_special" onclick="filterProducts();">
                @endif
                <span class="checkbox__text checkmark"></span>
            </label>
<!--
            <label class="checkbox-wrap">Распродажа
                <input type="checkbox" class="checkbox__input" name="is_discounted" onclick="filterProducts();">
                <span class="checkbox__text checkmark"></span>
            </label>
-->
            <label class="checkbox-wrap">@lang('messages.is_new')
                @if(isset($filters['is_new']) && $filters['is_new'])
                    <input type="checkbox" class="checkbox__input" name="is_new" checked onclick="filterProducts();" >
                @else
                    <input type="checkbox" class="checkbox__input" name="is_new" onclick="filterProducts();">
                @endif
                <span class="checkbox__text checkmark"></span>
            </label>
        </div>
    </div>
    @if($characteristics != '')
        @foreach($characteristics as  $characteristic)
        <div class="form-group charact">
                @if(count($characteristics_values) > 0)
                <label class="form-group__text"> {{ $characteristic->name }}  </label>
                <div class="form-group__result checkbox">
                    @foreach($characteristics_values[$characteristic->id] as  $characteristic_values)
                    <label class="checkbox-wrap">{{ $characteristic_values }} {{ $characteristic->unit }}
                        @if (in_array("$characteristic->id:-:$characteristic_values", $filters['character']))
                            <input type="checkbox" class="checkbox__input" value="{{ $characteristic->id }}:-:{{ $characteristic_values }}" checked  name="character[]" onclick="filterProducts();">
                        @else
                            <input type="checkbox" class="checkbox__input" value="{{ $characteristic->id }}:-:{{ $characteristic_values }}"  name="character[]" onclick="filterProducts();">
                        @endif
                            <span class="checkbox__text checkmark"></span>
                    </label>
                    @endforeach
                </div>
                @endif
                @if(count($characteristics_values_interval))
                   <label class="form-group__text"> {{ $characteristic->name }}  </label>
                     <div class="form-group__result checkbox">
                    @foreach($characteristics_values_interval[1] as  $characteristic_values_interval)
                        <label class="checkbox-wrap">{{ $characteristic_values_interval }} {{ $characteristic->unit }}
                            @if (in_array("$characteristic->id-$characteristic_values_interval", $filters['character']))
                                <input type="checkbox" class="checkbox__input" value="{{ $characteristic->id }}-{{ $characteristic_values_interval }}" checked  name="character[]" onclick="filterProducts();">
                            @else
                                <input type="checkbox" class="checkbox__input" value="{{ $characteristic->id }}-{{ $characteristic_values_interval }}"  name="character[]" onclick="filterProducts();">
                            @endif
                            <span class="checkbox__text checkmark"></span>
                        </label>
                    @endforeach
                     </div>
                @endif
        </div>
        @endforeach
    @endif
</div>
</form>
