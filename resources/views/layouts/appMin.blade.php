<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    {{--<link href="{{ asset('css/styles.css') }}" rel="stylesheet">--}}
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    @include('layouts.scripts')
</head>

<body class="body-order">
@php ($i = 0)
@if(isset($script))
    <script>
        dataLayer = [{
            'transactionId': '{{$order->id}}',
            'transactionTotal': {{ $order->total_price_discounted }},
            'transactionProducts': [
              @foreach($order->orderItems()->get() as $orderItem)
@if($i == 0)@php ($i++)@else,@endif{
                    'sku': '{{$orderItem->id}}',
                    'name': '{{str_replace("'", "", $orderItem->product->getTranslatedAttribute('name'))}}',
                    @if($orderItem->product->categories()->first() == null)'category': 'Нет категории',
                    @else'category': '{{str_replace("\'", "", $orderItem->product->categories()->first()->name)}}',
                    @endif'price': {{$orderItem->total_price_discounted}},
                    'quantity': {{$orderItem->quantity}}
              }@endforeach],
            'event': 'trackTrans'
        }];
    </script>

@endif
@include('layouts.google-script-body')
@include('layouts.headerMin')
@yield('content')
@include('layouts.footerMin')
@yield('scripts')
</body>

</html>
