<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if (isset($product->meta_title))
        <meta name="title" content="{{ $product->meta_title }}">
    @endif
    @if (isset($product->meta_description))
        <meta name="description" content="{{ $product->meta_description }}">
    @endif
    @if (isset($product->meta_keywords))
        <meta name="keywords" content="{{ $product->meta_keywords }}">
    @endif
    @if (isset($category->meta_title))
        <meta name="title" content="{{ $category->meta_title }}">
    @endif
    @if (isset($category->meta_description))
        <meta name="description" content="{{ $category->meta_description }}">
    @endif
    @if (isset($category->meta_keywords))
        <meta name="keywords" content="{{ $category->meta_keywords }}">
    @endif

    @if (isset($page->meta_description))
        <meta name="description" content="{{ $page->meta_description }}">
    @endif
    @if (isset($page->meta_keywords))
        <meta name="keywords" content="{{ $page->meta_keywords }}">
    @endif
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="icon" type="image/png" href="{{ asset('storage/' . setting('site.logo'))  }}" />
{{--    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">--}}
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <!-- Scripts -->
    @include('layouts.scripts')
</head>

<body class="{{isset($bodyClass)?$bodyClass:''}}">
@include('layouts.google-script-body')
@include('layouts.messenger.facebook')
@include('layouts.header')
@yield('content')
@include('layouts.consult')
@include('layouts.footer')
@include('layouts.messenger.jivosite')
@yield('scripts')
</body>

</html>
