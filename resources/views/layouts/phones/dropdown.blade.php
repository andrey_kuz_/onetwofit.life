<div class="header_dropdown_phone dropdown">
    <div class="header-main__telephone" data-toggle="dropdown"><span class="phone_number">{!! setting('site.phoneNumber') !!}</span>
        <span class="caret"></span></div>
    <ul class="dropdown-menu">
        @foreach($phones as $phone)
        <li class="dropdown_phone_item" value="{{ $phone }}">{{ $phone }}</li>
        @endforeach
    </ul>
</div>