<div class="header-top__language">
    <div class="header-top__country" data-toggle="modal" data-target="#start-modal"><img src="{{ asset( 'images/' . strtolower(config('app.country') . '.svg')) }}"> {{ __('messages.country_' . config('app.country')) }} </div>
    <div class="header-top__lang" data-toggle="modal" data-target="#start-modal"> {{ __('messages.lang_' . config('app.locale')) }} </div>
    <div class="header-top__currency">
        <form class="modal-start-screen" id="settingsFormCurrency" method="post" action="{{ url('/savesettings') }}">
            {{ csrf_field() }}
            <div class="header_dropdown_currency dropdown">
               <input type="text" readonly="readonly" class="header-top__currency currency" name="userCurrency" onchange="$('#settingsFormCurrency').submit();" data-toggle="dropdown" value="{{ trans('messages.currency_UAH') }}"><span class="caret"></span>
                <ul class="dropdown-menu">
                    <li class="dropdown_currency_item" value="UAH">@lang('messages.currency_UAH')</li>
                </ul>
            </div>
            {{--<select class="currency" name="userCurrency" onchange="$('#settingsFormCurrency').submit();">--}}
                {{--<option value="UAH" @if (config('app.currency') == "UAH") selected @endif> @lang('messages.currency_UAH') </option>--}}
                {{--<!--<option value="RUB" @if (config('app.currency') == "RUB") selected @endif> @lang('messages.currency_RUB') </option>-->--}}
            {{--</select>--}}
        </form>
    </div>
</div>


