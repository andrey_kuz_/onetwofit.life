<div class="footer">
    <ul class="footer__list">
        <li class="footer__item">
            <span class="footer__link"> @lang('messages.компания') </span>
            <a href="{{ route('page', ['slug' => 'company']) }}" class="footer__link"> @lang('messages.история компании') </a>
            <a href="{{ route('page', ['slug' => 'whyonetwofit']) }}" class="footer__link"> @lang('messages.почему onetwofit') </a>
            <a href="https://onetwofit.life/blog" class="footer__link">блог</a>
<!--        <a href="{{ url('blog/category/recepty/') }}" class="footer__link">рецепты</a>-->
            <a href="{{ route('page', ['slug' => 'benefits']) }}" class="footer__link"> @lang('messages.зарабатывай на покупках') </a>
            <a href="{{ route('page', ['slug' => 'reviews']) }}" class="footer__link"> @lang('messages.отзывы о магазине') </a>
        </li>
        {{--@include('menu.footer.company')--}}
        <li class="footer__item">
            <span class="footer__link"> @lang('messages.поддержка') </span>
            <a href="{{ route('page', ['slug' => 'faq']) }}" class="footer__link"> @lang('messages.faq') </a>
            <a href="{{ route('page', ['slug' => 'return']) }}" class="footer__link"> @lang('messages.возврат') </a>
            <a href="{{ route('page', ['slug' => 'delivery']) }}" class="footer__link"> @lang('messages.доставка')  </a>
            <a href="{{ route('page', ['slug' => 'confident']) }}" class="footer__link"> @lang('messages.политика конфиденциальности') </a>
            <a href="{{ route('page', ['slug' => 'agree']) }}" class="footer__link"> @lang('messages.пользовательское соглашение') </a>
        </li>
        <li class="footer__item">
            @include('layouts.phones.phone')
            <a href="mailto: info@onetowit.org" class="footer__link footer-email">info@onetwofit.org</a>
            <a href="{{setting('site.socialFacebook')}}" class="footer__link footer__social" target="_blank"><img src="{{ asset('images/facebook.svg') }}" alt="facebook"></a>
            <a href="{{setting('site.socialInstagram')}}" class="footer__link footer__social" target="_blank"><img src="{{ asset('images/instagram-icon.svg') }}" alt="instagram"></a>
            <a href="{{setting('site.socialYoutube')}}" class="footer__link footer__social" target="_blank"><img src="{{ asset('images/youtube-icon.svg') }}" alt="youtube"></a>
        </li>
        <li class="footer__item">
            <img src="{{ asset('images/master-card-icon.svg') }}" alt="mastercard">
            <img src="{{ asset('images/visa.svg') }}" alt="VISA">
        </li>
    </ul>
    <div class="footer-bot text-center">
        © OneTwoFit 2018
    </div>
</div>

@include('layouts.popup')

{!! setting('site.googleAnaliticsCode') !!}