<header class="header">
    <div class="header-top">
        @include('layouts.locale')
        @include('layouts.topmenu')
    </div>
    <div class="header-main">
        <div class="header-main__logo">
            <a href="{{ route('home') }}"><img src="{{ asset('images/combined-shape.svg') }}" alt="logo"></a>
        </div>
        <div class="header-main__content">
            <div class="header-main__content-wrap">
                <div class="header-main__telephones telephones">
                    <img src="{{ asset('images/info.svg') }}" alt="information" class="info-img telephones__icon" data-toggle="tooltip" data-placement="bottom" title="@lang('messages.Бесплатные звонки для всех абонентов Украины')">
                    @include('layouts.phones.dropdown')
                    <span class="schedule">
                            <span class="schedule__text">{{ setting('site.workTime', '5/7 9:00 - 18:00') }}</span>
                        </span>
                </div>

                <div class="header-main__search search">
                    <form action="{{route('search')}}" method="get">
                        <input class="search-element" type="text" placeholder="поиск..." name="search_string" />

                        <i class="fa fa-caret-down" aria-hidden="true"></i>

                        <i class="fa fa-search" aria-hidden="true"></i>

                        <div class="header-main__search-wrapper dropdown">

                            <ul class="megamenu">
                                <p>Поиск по:</p>
                                <li class="onelevel">
                                    <a href="#" class="sub-sub-navigation-list__link" data-search="onelevel-code">
                                        Артикулу
                                    </a>
                                </li>

                                <li class="onelevel">
                                    <a href="#" class="sub-sub-navigation-list__link" data-search="onelevel-brand">
                                        Бренду
                                    </a>
                                </li>

                                <li class="onelevel">
                                    <a href="#" class="sub-sub-navigation-list__link" data-search="onelevel-flavor">
                                        Вкусу
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="header-main__information">
            <div class="header-main__autorization">
                @if( Auth::guest() )
                    <a href="{{ route('login') }}" class="login">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                        <span class="login_more">Вход <span class="and">Регистрация</span> </span>
                    </a>
                @else
                    <a href="{{ route('account') }}" class="login">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <span class="login_more">Выход <span class="and">Личный кабинет</span></span>
                    </a>
                @endif
            </div>
            <a href="{{ route('cart.view') }}" class="header-main__cart">
                @if(empty($cartCount))
                    <span class="header-main__cart-count">0</span>
                @else
                    <span class="header-main__cart-count">{{$cartCount?:0}}</span>
                @endif
            </a>
        </div>
    </div>
    <div class="header-bottom navigation">
        <span class="schedule">
            <span class="schedule__text">{{ setting('site.workTime', '5/7 9:00 - 18:00') }}</span>
        </span>
        <div class="burger">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        @include('menu.adaptive')

        <img src="{{ asset('images/info.svg')}}" alt="information" class="info-img telephones__icon" data-toggle="tooltip" data-placement="top" title="telephones icon">
        @include('layouts.phones.dropdown')
        @include('menu.main')

        <div class="header-bottom__mobile">

            <div class="header-bottom__mobile-title">
                <p>каталог товаров</p>
            </div>

            <form action="{{route('search')}}" method="get" >
                <label>
                    <input class="search-element" type="text" placeholder="поиск..." name="search_string" />
                </label>

                <span class="btn-caret">
                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                </span>

                <button class="btn-search" type="submit">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>

                <div class="header-main__search-wrapper dropdown">

                    <ul class="megamenu">
                        <p>Поиск по:</p>
                        <li class="onelevel">
                            <a href="#" class="sub-sub-navigation-list__link" data-search="onelevel-code">
                                Артикулу
                            </a>
                        </li>

                        <li class="onelevel">
                            <a href="#" class="sub-sub-navigation-list__link" data-search="onelevel-brand">
                                Бренду
                            </a>
                        </li>

                        <li class="onelevel">
                            <a href="#" class="sub-sub-navigation-list__link" data-search="onelevel-flavor">
                                Вкусу
                            </a>
                        </li>
                    </ul>
                </div>

            </form>
        </div>
    </div>
</header>