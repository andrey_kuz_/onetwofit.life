<ul class="header-top__actions">
    <li class="header-top__item"><a href="{{ url('blog/')}}" class="header-top__link" target="_blank"> @lang('messages.Блог') </a></li>
    <li class="header-top__item"><a href="{{ route('compare.view') }}" class="header-top__link"> @lang('messages.Сравнить') </a></li>
    <li class="header-top__item {{ ($favorite_count) ? 'fullheart' : '' }}">
        <a href="{{ route('favorites')}}" class="header-top__link">
            <span class="like {{ ($favorite_count) ? 'show_like' : '' }} ">
                <span class="like__number">{{ $favorite_count }}</span>
            </span>
            @lang('messages.Избранное')
        </a>
    </li>
    {{--<li class="header-top__item"><a href="#" class="header-top__link">Поделиться</a></li>--}}
</ul>