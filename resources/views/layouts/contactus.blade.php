<div class="contact-form">

  <div class="contact-form__title"> @lang('messages.На связи с вами') </div>
  <div class="contact-form__text">
      @lang('messages.know_first')!
  </div>

  <div id="mlb2-1024506" class="ml-form-embedContainer ml-subscribe-form ml-subscribe-form-1024506">
    <div class="ml-form-align-center ">
      <div class="ml-form-embedWrapper embedForm">
        <div class="ml-form-embedBody ml-form-embedBodyHorizontal row-form">
          <div class="ml-form-embedContent">
            <h4></h4>
          </div>
          <form class="ml-block-form" action="https://app.mailerlite.com/webforms/submit/k8w4r6" data-code="k8w4r6" method="post" target="_blank">
            <div class="ml-form-formContent horozintalForm">
              <div class="ml-form-horizontalRow">
                <div class="ml-input-horizontal">
                  <div style="width: 100%;" class="horizontal-fields">
                    <div class="ml-field-group ml-field-email ml-validate-email ml-validate-required">
                      <input style="width: 100%;" type="email" class="form-control" data-inputmask="" name="fields[email]" value="" placeholder="Email">
                    </div>
                  </div>
                </div>
                <div class="ml-button-horizontal">
                  <button type="submit" class="primary "> @lang('messages.отправить') </button>
                  <button disabled="disabled" style="display: none;" type="button" class="loading btn btn-block">
                        <div class="ml-form-embedSubmitLoad"><div></div><div></div><div></div><div></div></div>
                      </button>
                </div>
              </div>
            </div>
            <input type="hidden" name="ml-submit" value="1">
          </form>
        </div>
        <div class="ml-form-successBody row-success" style="display: none">
          <div class="ml-form-successContent">
            <h4> @lang('messages.Спасибо')!</h4>
            <p style="text-align: left;"> @lang('messages.Ваше письмо уже на почте') :)</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <img src="https://track.mailerlite.com/webforms/o/1024506/k8w4r6?v03ee2804ef2f008535475a5d62f02f94" width="1" height="1" style="max-width: 1px; max-height: 1px; visibility: hidden; padding: 0; margin: 0; display: block;" border="0">

</div>