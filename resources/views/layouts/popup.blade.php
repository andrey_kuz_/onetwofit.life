<div id="start-modal" class="start-modal modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <span class="close-circle"></span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <form class="modal-start-screen" id="settingsForm" method="post" action="{{ url('/savesettings') }}">
                    {{ csrf_field() }}
                    <div class="start-modal__title"> @lang('messages.Настройте для себя магазин') </div>
                    <div class="start-modal__row settings-top">
                        <div class="form-group">
                            <label class="start-modal__label"> @lang('messages.страна') </label>
                            <select name="userCountry" id="userCountry" class="start-modal__select">
                                <option value="UA" @if (config('app.country') == "UA") selected="selected" @endif>@lang('messages.country_UA')</option>
                                <!--<option value="RU" @if (config('app.country') == "RU") selected="selected" @endif>@lang('messages.country_RU')</option>-->
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="start-modal__label">язык</label>
                            <select name="userLang" class="start-modal__select" id="userLang">
                                <option value="ua" forCountry="UA" @if (config('app.locale') == "ua") selected="selected" @endif>@lang('messages.lang_ua')</option>
                                <option value="ru" forCountry="RU" @if (config('app.locale') == "ru") selected="selected" @endif>@lang('messages.lang_ru')</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="start-modal__label">валюта</label>
                            <select name="userCurrency" id="userCurrency" class="start-modal__select">
                                <option value="UAH" forCountry="UA" @if (config('app.currency') == "UAH") selected="selected" @endif>@lang('messages.currency_UAH')</option>
                                <!--<option value="RUB" forCountry="RU" @if (config('app.currency') == "RUB") selected="selected" @endif>@lang('messages.currency_RUB')</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="start-modal__row settings-bottom">
                        <div class="form-group">
                            <label class="start-modal__label"> @lang('messages.доступные методы доставки') </label>
                            <div class="settings-bottom__images">
                                <img src="{{ asset('images/nova-poshta.png') }}" alt="Nova poshta">
                                <!--<img src="{{ asset('images/ykrposhta.png') }}" alt="Ykr poshta">
                                <img src="{{ asset('images/intime.svg') }}" alt="Intime">-->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="start-modal__label"> @lang('messages.доступные способы оплаты') </label>
                            <div class="settings-bottom__images">
                                <img src="{{ asset('images/mastercard.svg')}}" alt="mastercard">
                                <img src="{{ asset('images/visa.svg')}}" alt="VISA">
								<img src="{{ asset('images/liqpay.png')}}" alt="liqpay">
                            </div>
                        </div>
                    </div>
                    <a href="#" id="settingsFormSubmit" class="start-modal__link-save"> @lang('messages.сохранить настройки') </a>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="attention-modal" class="start-modal modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <span class="close-circle"></span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body" style="display: block;">
                <div>Вы не можете войти через Facebook - на вашей странице не указана почта.</div>

                <div>Добавьте почту на свою страницу в Facebook или воспользуйтесь Google для входа.</div>

                <div class="registration__social">
                    <a href="{{ url('/auth/google') }}" class="reg-social-icon reg-google">
                        <svg width="35px" height="22px" viewBox="0 0 35 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id="site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="Регистрация" transform="translate(-650.000000, -380.000000)" fill="#FFFFFF">
                                    <g id="Group-6" transform="translate(632.000000, 366.000000)">
                                        <g id="google-plus" transform="translate(18.000000, 14.000000)">
                                            <path d="M12.0655611,13.2694117 L17.4385933,13.2694117 C16.4954492,15.9351271 13.9409676,17.8464849 10.9508424,17.824538 C7.32244855,17.7978076 4.32200447,14.8957401 4.18129269,11.2705473 C4.03035586,7.38480426 7.14834118,4.17510363 11.00178,4.17510363 C12.7639605,4.17510363 14.3719209,4.84664406 15.5841059,5.94643219 C15.8713455,6.20698237 16.3082087,6.2087644 16.5906641,5.94277436 L18.5642875,4.0860026 C18.8729153,3.79553323 18.8739472,3.3049148 18.5661636,3.0136951 C16.6438532,1.19246996 14.0648878,0.0574165755 11.2209152,0.00208014378 C5.1552996,-0.116564918 0.0423965398,4.85574174 0.000276815129,10.921365 C-0.0425933723,17.032758 4.89948559,22 11.00178,22 C16.8706806,22 21.6647307,17.4057631 21.9857412,11.618604 C21.9941839,11.5460101 22,9.09394967 22,9.09394967 L12.0655611,9.09394967 C11.658435,9.09394967 11.3283252,9.42399866 11.3283252,9.8310497 L11.3283252,12.5323117 C11.3283252,12.9393627 11.658435,13.2694117 12.0655611,13.2694117 Z" id="Fill-1"></path>
                                            <path d="M31.7419451,10.2580831 L31.7419451,7.64051047 C31.7419451,7.28667634 31.4552794,7 31.1014584,7 L28.8985416,7 C28.5446276,7 28.2579619,7.28667634 28.2579619,7.64051047 L28.2579619,10.2580831 L25.6409517,10.2580831 C25.2870377,10.2580831 25,10.5447595 25,10.8986866 L25,13.1015924 C25,13.4551475 25.2870377,13.7421959 25.6409517,13.7421959 L28.2579619,13.7421959 L28.2579619,16.3593965 C28.2579619,16.7132306 28.5446276,17 28.8985416,17 L31.1014584,17 C31.4552794,17 31.7419451,16.7132306 31.7419451,16.3593965 L31.7419451,13.7421959 L34.3594203,13.7421959 C34.7129623,13.7421959 35,13.4551475 35,13.1015924 L35,10.8986866 C35,10.5447595 34.7129623,10.2580831 34.3594203,10.2580831 L31.7419451,10.2580831 Z" id="Fill-3"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <span>Google</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

