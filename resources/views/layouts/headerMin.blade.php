<div class="order-header">
    <div class="order-header__container">
        <div class="order-header__logo">
            <a href="{{ route('home') }}" class="logo__link">
                <img src="{{ asset('images/combined-shape.svg') }}" alt="logo">
            </a>
        </div>
        <div class="header-main__telephones telephones">
            <img src="{{ asset('images/info.svg') }}" alt="information" class="info-img telephones__icon" data-toggle="tooltip" data-placement="top" title="@lang('messages.Бесплатные звонки для любых абонентов Украины')">
            @include('layouts.phones.dropdown')
            <span class="schedule">
                <span class="schedule__text">{{ setting('site.workTime', '5/7 9:00 - 18:00') }}</span>
            </span>
            <a href="{{ route('cart.view') }}" class="header-main__cart">
                @if(empty($cartCount))
                    <span class="header-main__cart-count">0</span>
                @else
                    <span class="header-main__cart-count">{{$cartCount?:0}}</span>
                @endif
            </a>
        </div>
    </div>
</div>