<?php

return array (
  'order_success_subject' => 'Ваш заказ принят',
  'reset_pass_butt' => 'Скидання паролю',
  'reset_pass_text' => 'Для скидання паролю натисніть кнопку',
  'subject_order_accepted' => 'Ваш заказ успешно оформлен! И это круто!',
  'subject_order_success' => 'Ваш заказ успешно оплачен! И это круто!',
  'subject_subscription_success' => 'У нас появился товар который вы искали! Спешите успеть!',
  'subject_reset_pass' => 'Скинути пароль',
  'subject_verify_email' => 'Подтвердите свой email и завершите регистрацию',
  'subject_welcome' => 'Вы успешно зарегистрировались! Ура!',
  'thank_you' => 'Дякуємо',
);
