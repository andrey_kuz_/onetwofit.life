<?php

return array (
  'account' => 'Account',
  'code_sent' => 'We sent you an activation code. Check your email and click on the link to verify.',
  'code_sent_email_changed' => 'You changed your email. We sent you an activation code. Check your email and click on the link to verify.',
  'confirm_acc_notif' => 'You need to confirm your account. We have sent you an activation code, please check your email.',
  'email' => 'Email',
  'email_not_ident' => 'Sorry your email cannot be identified.',
  'email_verified' => 'Your e-mail is verified. You can now login.',
  'email_verified_already' => 'Your e-mail is already verified. You can now login.',
  'logout' => 'Logout',
  'new_pass' => 'Reset your password',
  'pass' => 'Password',
  'pass_confirm' => 'Password confirm',
  'regards' => 'Regards',
  'reset_pass' => 'Reset your password TR',
  'reset_pass_butt' => 'Reset password TR',
  'reset_pass_text' => 'To reset password click the button',
  'thank_you' => 'Thank you TR',
);
