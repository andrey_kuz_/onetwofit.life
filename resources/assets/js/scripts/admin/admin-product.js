$(document).ready(function() {
    if($("input[name='slug']").val() == '') {
        update_slug();
    }

    var blockedTile = new Array("is_new", "is_hit", "is_bestseller");
    var mainPage = 'is_main';
    var active_name = [];
    function change_main() {
        $("input[name='is_main']").parent().prop('className', 'toggle btn btn-default off');
        $("input[name='is_main']").prop('checked', false);
    }

    $('.toggle.btn').click(function (event) {

        var class_this = $(this).prop('className');
        var name_input = $(this).find('input').attr('name');
        var discount_exists = false;
        if($("input[name='discount']").val() != "0") {
            discount_exists = true;
        }
        var count_active = 0;
        var reset = false;
        if (blockedTile.indexOf(name_input) != -1)
        {
            for (var i = 0; i < blockedTile.length; i++) {
                if ((class_this == 'toggle btn btn-default off') && ($("input[name='"+blockedTile[i]+"']").parent().prop('className') == 'toggle btn btn-primary')) {
                    $("input[name='"+blockedTile[i]+"']").parent().prop('className','toggle btn btn-default off');
                    $("input[name='"+blockedTile[i]+"']").prop('checked', false);
                    count_active++;
                }
            }
            if (class_this == 'toggle btn btn-primary') {
                $("input[name='is_main']").parent().prop('className', 'toggle btn btn-default off');
            }
        }
        if (count_active && !discount_exists){
            alert('Товар должен быть только в одной категории');
            reset = true;
        }

        if (name_input == mainPage) {
            for (var i = 0; i < blockedTile.length; i++) {
                if ($("input[name='"+blockedTile[i]+"']").parent().prop('className') == 'toggle btn btn-primary') {
                    active_name[count_active] = $("input[name='"+blockedTile[i]+"']").attr('name');
                    count_active++;
                }
            }
            if ((count_active == 0 || count_active > 1) && !discount_exists) {
                alert('Товар должен быть только в одной категории');
                setTimeout(change_main, 400);
                return;
            }
            if ($("input[name='is_active']").parent().prop('className') != 'toggle btn btn-primary') {
                alert('Товар должен быть активным чтобы выводиться на главной');
                setTimeout(change_main, 400);
                return;
            }

            if (class_this == 'toggle btn btn-default off') {
                $.ajax({
                    type: "GET",
                    url: "/mainCheck",
                    data: "type="+active_name[0],
                    success: function (data) {
                        if ((parseInt(data) > 14) && (active_name[0] != "is_bestseller")) {
                            alert('Товаров на главной с такой категорией более 15');
                            setTimeout(change_main, 400);
                        } else if((parseInt(data) > 39) && (active_name[0] == "is_bestseller")){
                            alert('Товаров на главной с такой категорией более 40');
                            setTimeout(change_main, 400);
                        }
                    }
                });
            }

        }
        if (reset){
            event.stopPropagation();
        }

    });
});
$("input[name='name']").keyup(function (e) {
    update_slug();
});

$("input[name='value']").keyup(function (e) {
    var value = $("input[name='value']").val();
    if(value.search('.') > -1){
        value = value.replace(',','.');
    }
    $("input[name='value']").val(value);
});
function update_slug() {
    $.ajax({
        type: "GET",
        url: "/admin/update-slug",
        data: "name="+$("input[name='name']").val(),
        success: function (data) {
            $("input[name='slug']").val(data);
        }
    });
}