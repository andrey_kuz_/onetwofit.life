let $rows;
let row;

$(document).ready(function () {

    $rows = document.querySelectorAll('.account-subscribe__row');


    for (let i = 0; i < $rows.length; i++) {
        $rows[i].querySelector('.account-subscribe__icon').addEventListener('click', handler);
        $rows[i].querySelector('.account-subscribe__order-info').addEventListener('click', handler);


    }

    function handler() {
        row = this.parentNode.children; //МАССИВ ВСЕХ ЭЛЕМЕНТОВ, row[row.length-1] <- wrapper

        let wrapper = row[row.length-1];

        if (wrapper.classList.contains('account-subscribe-wrapper__open')) {
            wrapper.classList.remove('account-subscribe-wrapper__open');
            wrapper.parentNode.classList.remove('account-subscribe__row-open');
        } else {
            wrapper.classList.add('account-subscribe-wrapper__open');
            wrapper.parentNode.classList.add('account-subscribe__row-open');
        }
    }
});

