$(document).ready(function () {
window.loadShippingNP = function(loadElemId, url, step, value) {
    $('#' + loadElemId).load(url + '?step=' + step + '&value=' + value, function () {
        //$('#buy-one-click').modal({show:true});
        $('#' + loadElemId).val($('#' + loadElemId + '_default').val());
    });
};

if ($('#shipping_city_id').val()) {
    $('#shipping_city_id').change();
}



    //tabs
    $(".tab-content").hide();
    $(".tab-content:first").show();

    $('.select_autocomplete').select2();

    function myFunction(x) {
        if (x.matches) { // If media query matches
            $('*[data-select2-id="1"]').removeAttr('style');
        } else {
        }
    }

    var x = window.matchMedia("(max-width: 395px)");
    myFunction(x); // Call listener function at run time

    $('#order-phone').mask("+38(999) 999-99-99", {autoclear: false});


    /* if in tab mode */
    // $("ul.tabs-menu li").click(function(e) {
    //     e.preventDefault();
    //
    //     $(".tab-content").hide();
    //     var activeTab = $(this).attr("rel");
    //     $("#"+activeTab).fadeIn();
    //
    //     $("ul.tabs-menu li").removeClass("current");
    //     $(this).addClass("current");
    //
    //     $(".tab_drawer_heading").removeClass("d_active");
    //     $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
    //
    // });

    /* if in drawer mode */
    // $(".tab_drawer_heading").click(function(e) {
    //     e.preventDefault();
    //
    //     $(".tab-content").hide();
    //     var d_activeTab = $(this).attr("rel");
    //     $("#"+d_activeTab).fadeIn();
    //
    //     $(".tab_drawer_heading").removeClass("d_active");
    //     $(this).addClass("d_active");
    //
    //     $("ul.tabs-menu li").removeClass("active");
    //     $("ul.tabs-menu li[rel^='"+d_activeTab+"']").addClass("active");
    // });

    /* Extra class "tab_last"
     to add border to right side
     of last tab */
    // $('ul.tabs li').last().addClass("tab_last");


    // const nextStep = document.querySelector('.tab-content__btn');
    //
    // nextStep.addEventListener('click', () => {
    //     nextStep.closest('.step__first').style.display = 'none';
    //     console.log(nextStep.closest('.step__first'));
    //     nextStep.closest('.step__first').nextElementSibling.style.display = 'flex';
    // });
    $("#tab1 input[name='shipping_email']").keyup(function () {
        if ($(this).val()) {
            $.ajax({
                type: "POST",
                url: '/check-email',
                data: {email: $(this).val()},
                success: function (data) {
                    if( data == '0'){
                        $('#email-check').addClass('hidden');
                    } else {
                        $('#email-check').removeClass('hidden');
                    }
                }
            });
        }
    });

    $("#tab1 .registration-page__link").click(function (e) {
        if (!$('#email-check').hasClass('hidden')) {
            e.preventDefault();
        }
    });
});

