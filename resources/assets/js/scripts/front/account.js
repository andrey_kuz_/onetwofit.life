let loginBtn;
let registrationBtn;

let loginPage;
let registrationPage;

let formReg;
let input;
let label;

let allRow;
let allRow2;


$(document).ready(function () {

    let arr = window.location.pathname.split('/');
    if(arr[1] == 'login'){
        if (arr[2] == 'not_email') {
            $('#attention-modal').modal('show');
        }
    }

    $('.read-more').click(function () {
        var myModal = $('#comment-modal');
        myModal.find('.comment__author').html($(this).closest('.comment').find('.comment__author').text());
        myModal.find('.comment__text-data').html($(this).parent().find('.comment__text-data').text());
        myModal.modal('show');
    });


    loginBtn = document.querySelector('.login__link');
    registrationBtn = document.querySelector('.registration__link');

    if (loginBtn) {
        loginPage = loginBtn.parentElement;
    }
    if (registrationBtn) {
        registrationPage = registrationBtn.parentElement;
    }

    formReg = document.querySelector('.registration-form');

    allRow = document.querySelectorAll('.registration-page__row');

    for (let i = 0; i < allRow.length; i++) {

        input = allRow[i].firstElementChild;
        label = allRow[i].querySelector('label');

        if (input.value !== '') {
            allRow[i].querySelector('label').style.color = '#6004ba';
            allRow[i].querySelector('label').style.top = '8px';
            allRow[i].querySelector('label').style.fontSize = '14px';
            allRow[i].querySelector('label').style.transition = 'all 0.5s linear';
        } else {
            allRow[i].querySelector('label').style.color = '#000';
            allRow[i].querySelector('label').style.top = '35%';
            allRow[i].querySelector('label').style.fontSize = '18px';
            allRow[i].querySelector('label').style.transition = 'all 0.5s linear';
        }

        allRow[i].addEventListener('click', function() {
            if (input.value === '') {
                allRow[i].querySelector('label').style.color = '#6004ba';
                allRow[i].querySelector('label').style.top = '8px';
                allRow[i].querySelector('label').style.fontSize = '14px';
                allRow[i].querySelector('label').style.transition = 'all 0.5s linear';
            }
        });
    }

    function loginAddActive() {
        if (!loginPage.classList.contains('.active')) {
            loginPage.classList.add('active');
        }
    }

    function registrationAddActive() {
        if (!registrationPage.classList.contains('.active')) {
            registrationPage.classList.add('active');
        }
    }

    if (loginPage) {
        loginAddActive();
    }
    if(registrationPage) {
        registrationAddActive();
    }

    $('.usereditform .registration-page__row.userdata-edit__row #userphone-edit').click(function () {
        let label = $(this).next();
        label.css({'color' : '#6004ba', 'top' : '8px', 'font-size' : '14px', 'transition' : 'all 0.5s linear'});

    });
    $('#userphone-edit').mask("+38(999) 999-99-99", { autoclear: false });

    if ($('#userphone-edit').val() !== '') {
        let label = $('#userphone-edit').next();
        label.css({'color' : '#6004ba', 'top' : '8px', 'font-size' : '14px', 'transition' : 'all 0.5s linear'});
    }

    $('body').on('click', '#feedbbackform #user-house', function() {
        $(this).next().css({'color' : '#6004ba', 'top' : '8px', 'font-size' : '14px', 'transition' : 'all 0.5s linear'});
    });


    allRow2 = document.querySelectorAll('.forgot-password__row');

    for (let i = 0; i < allRow2.length; i++) {

        input = allRow2[i].firstElementChild;
        label = allRow2[i].lastElementChild;

        if (input.value !== '') {
            label.style.color = '#6004ba';
            label.style.top = '8px';
            label.style.fontSize = '14px';
            label.style.transition = 'all 0.5s linear';
        } else {
            label.style.color = '#000';
            label.style.top = '35%';
            label.style.fontSize = '18px';
            label.style.transition = 'all 0.5s linear';
        }

        allRow2[i].addEventListener('click', function() {
            if (allRow2[i].firstElementChild.value === '') {
                allRow2[i].lastElementChild.style.color = '#6004ba';
                allRow2[i].lastElementChild.style.top = '8px';
                allRow2[i].lastElementChild.style.fontSize = '14px';
                allRow2[i].lastElementChild.style.transition = 'all 0.5s linear';
            }
        });
    }


    $('.login-modal').on('click', function(){
        $.ajax({
            type: 'POST',
            url: '/login_modal_form',
            success: function() {

            }
        });
    });

});


