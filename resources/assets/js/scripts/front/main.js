$( document ).ready(function() {

    $('#userCountry').change(function() {
        $('#userCurrency').val($('#userCurrency option[forCountry="' + $(this).val() + '"]').val());
        $('#userLang').val($('#userLang option[forCountry="' + $(this).val() + '"]').val());
    });
    $('#settingsFormSubmit').click(function() {
        $('#settingsForm').submit();
    });

    $( "#all-tr-maximize" ).click(function() {
        $( "#TableTr" ).removeClass( "tr-minimized" );
    });

    $('.dropdown_phone_item').click(function (e) {
        e.preventDefault();
        let current_phone = $(this).text();
        $('.header-main__telephone .phone_number').html(current_phone);
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
});