$(document).ready(function () {
    window.toggleFavorite = function (url, className) {

        $.ajax({
            type: "GET",
            url: url,
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.statusText == 'Unauthorized') {
                    window.location.href = '/login';
                }
                $('.' + className).html(data.html);
                $('.header .like__number').text(data.count);
                let like = $('.header-top__actions li:nth-child(3)');
                if (data.count > 0) {
                    like.addClass('fullheart');
                    $('.like').addClass('show_like');
                } else {
                    like.removeClass('fullheart');
                    $('.like').removeClass('show_like');
                }
            }
        });
    };
});
