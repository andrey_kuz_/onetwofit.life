//const decreaseBtn = document.querySelector('.descrease');
//const increaseBtn = document.querySelector('.increase');


let interactCart;
let car;
let closeInteractCart;
let minimizeInteractCart;
let maximizeInteractCart;
let progressLine;
let countProd;

$(document).ready(function() {
    interactCart = document.querySelector('.interact-cart');
    car = document.querySelector('.interact-cart .car');
    closeInteractCart = document.querySelector('.interact-cart__icon');
    minimizeInteractCart = document.querySelector('.interact-cart__minimize');
    if ( minimizeInteractCart !== null) {
        maximizeInteractCart = document.querySelector('.interact-cart-minimize').firstElementChild;
    }
    progressLine = document.querySelector('.interact-cart__progress img');
    countProd = 0;

    window.cartList = function () {
        const cartList = document.querySelectorAll('.interact-cart__row');
        let iconCart;

        for (let i = 0; i < cartList.length; i++) {
            iconCart = cartList[i].lastElementChild;
        }
    };

    init();

    function init() {
        handleEvents();
        cartList();
    }

    function handleEvents() {
        //добавление в корзину

        minimizeInteractCart = document.querySelector('.interact-cart__minimize');
        if ( minimizeInteractCart !== null) {
            //закрыть корзину
            closeInteractCart.addEventListener('click', function () {
                this.parentNode.classList.remove('interact-cart__active');
            });


        //свернуть
        minimizeInteractCart.addEventListener('click', function () {
            //alert(3);
            this.parentNode.classList.remove('interact-cart__active');
            this.parentNode.nextElementSibling.classList.add('interact-cart-minimize__active');
        });

        //развернуть
        maximizeInteractCart.addEventListener('click', function () {
            this.parentNode.classList.remove('interact-cart-minimize__active');
            this.parentNode.previousElementSibling.classList.add('interact-cart__active');
        });

        //закрыть корзину свернутую
        document.querySelector('.interact-cart-minimize').lastElementChild.addEventListener('click', function () {
            this.parentNode.classList.remove('interact-cart-minimize__active');
        });
        }
    }

    let table = $('.table.table-striped tr').length;
    if (table < 15) {
        $('.product-info-full .show-more').hide();
    }
    $('.product-info-full .show-more').click(function () {
        $('.table.table-striped').addClass('show-table');
        $('.product-info-full .show-more').hide();
    });

    $('#instock-modal .modal-body button').click(function () {
        $("#instock-modal").addClass("fade");
    });
    $('#instock-modal .close').click(function () {
        $("#instock-modal").addClass("fade");
    });



window.addToCartEvent = function(addCartUrl) {

    document.querySelector('.interact-cart-minimize').classList.remove('interact-cart-minimize__active');
    interactCart.classList.add('interact-cart__active');

    addProductToCart(addCartUrl);
};

window.addProductToCart = function (addCartUrl)  {

    $.ajax({
        type: "GET",
        url: addCartUrl,
        success: function (data, textStatus) {
            if (data.status == 1)
            {
                $('.alert-danger.cartPopupErrors').html('');
                $('.alert-danger.cartPopupErrors').hide();
            }
            else {
                $('.alert-danger.cartPopupErrors').show();
                $('.alert-danger.cartPopupErrors').html(data.mess);
            }

            document.querySelector('.interact-cart__product').innerHTML = data.result.html;
            document.querySelector('.header-main__cart-count').innerHTML = data.result.cartInfo.itemsCount;
            document.querySelector('.interact-cart__balance').innerText = data.result.cartInfo.freeShippingLeft;
            document.querySelector('.interact-cart__text.text-center.summ').innerHTML = data.result.cartSum;

            freeBalance = data.result.cartInfo.freeShippingLeft;
            freeBalance_start = data.result.cartInfo.freeShipping;


            if (freeBalance > 0) {
                car.style.transition = 'all 0.3s linear';
                car.style.left = '-70px';
            } else {
                car.style.transition = 'all 0.5s linear';
                car.style.left = '0';
            }

            //подсчет progressBar
            countProd++;
            let imgRatioPercent = parseInt(freeBalance) * 100 / freeBalance_start;
            progressLine.style.left = '-' + imgRatioPercent + '%';

            /*
             if (parseInt(getComputedStyle(progressLine).left) <= -80) {
             progressLine.style.left = parseInt(getComputedStyle(progressLine).left) + imgRatioPercent + 'px';
             } else {
             progressLine.style.left = '0';
             }
             */
            //подсчет progressBar

            //увеличиваем область для добавления
            interactCart.style.height = interactCart.offsetHeight + 42 + "px";
            cartSum = data.result.cartSum.substring(48);

            //добавление события для ФБ пикселя
                fbq('track', 'AddToCart', {
                content_type: 'product',
                content_ids: Object.keys(data.result.cartInfo.cartItemsInfo),
                value: cartSum.substring(0,cartSum.length - 8).replace(/ /g, ''),
                currency: ' UAH '
            });
        },
        error: function (xhr, status, data) {

            $('.alert-danger.cartPopupErrors').html('');

            $('.alert-danger.cartPopupErrors').show();
            $('.alert-danger.cartPopupErrors').append('<p>' + xhr.responseJSON.message + '</p>');

        }
    });
};

window.oneClickInit = function (oneClickUrl) {
    $('#buy-one-click .modal-body').html();
    $('#buy-one-click .modal-body').load(oneClickUrl, function () {
        $('#oneClick-phone').mask("+38(999) 999-99-99", { autoclear: false });
    });
};

window.oneClickСompareInit = function (addCompareUrl) {
    $('#add-compare-click .modal-body').html();
    $('#add-compare-click .modal-body').load(addCompareUrl, function () {
        update_count();
        $('#add-compare-click').modal({show: true});

    });
};

function update_count() {
    $.ajax({
        type: "GET",
        url: '/compare/count',
        success: function (data) {
            if(parseInt(data) > 0) {
                $('.some-product-thumbnails-slider .some-product__compare a .number').text(data);
            }
        }
    });
}

window.oneClick = function() {

    $.ajax({
        type: "POST",
        url: $('#oneClickUrl').val(),
        data: $('#oneCLickForm').serialize(),
        success: function (data) {
            var info = jQuery.parseJSON(data['data']);
            $('#buy-one-click .modal-body').html(data.success);
            var script_fb =  `<script> 
            fbq('track', 'Purchase', {
                content_type: 'product',
                content_ids:['`+info['product']['id']+`'],
                value:`+(info['product']['price']*((100 -info['product']['discount'])/100))+`,
                currency:' UAH ',
            });</script>`;

            var script = `<script>
            dataLayer = [{
                'transactionId': '`+info['order_id']+`_click',
                'transactionTotal':`+(info['product']['price']*((100 -info['product']['discount'])/100))+`,
                'transactionProducts': [{
                    'sku':'`+info['product']['article']+`',
                    'name':'`+info['product']['name']+`',
                    'category': '`+info['category']+`',
                    'price':`+(info['product']['price']*((100 -info['product']['discount'])/100))+`,
                    'quantity': 1
                }],
                'event': 'trackTrans'
            }];</script>`;
            if ($('body').children().first().prop("tagName") == 'SCRIPT'){
                $('body').children().first().remove();
            }
            $('body').prepend(script);
            $('body').prepend(script_fb);
        },
        error: function (xhr, status, data) {
            $('.alert-danger.oneClickErrors').html('');
            $.each(xhr.responseJSON.errors, function (key, value) {
                $('.alert-danger.oneClickErrors').show();
                $('.alert-danger.oneClickErrors').append('<p>' + value + '</p>');
            });
        }
    });
};

function freeBalanceCount() {

    return freeBalance;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

window.subcribe = function (url) {
    var email = $('.some-product__buttons input').val();
    if( email != "") {
        if(validateEmail(email)) {

             $.ajax({
             type: "GET",
             url: url+'/'+email,
             success: function (data) {
                 if(data){
                    $('.some-product__status-individual-order.subcribe').text(data); 
                 } else {
                     $('.some-product__status-individual-order.subcribe').text('');
                     $('.add-to-cart.addToCart.subcribe').attr('onclick', '');
                     $('.add-to-cart.addToCart.subcribe').attr('data-target', '#instock-modal').click();
                     $('.add-to-cart.addToCart.subcribe').attr('data-target', '');
                 }
             }
             });
        } else {
            $('.some-product__status-individual-order.subcribe').removeClass('success');
            $('.some-product__status-individual-order.subcribe').text('Email введен неверно');
        }
    } else {
        $('.some-product__status-individual-order.subcribe').removeClass('success');
        $('.some-product__status-individual-order.subcribe').text('Сначала введите email');
    }
};

$(document).ready(function () {


    $('.table-cart__confirm').click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();

        let comment = $('.table-cart__form').find('textarea').val();
        let url = $(this).attr('href');
        $.ajax({
            type: "POST",
            url: url,
            data: {comment: comment},
            success: function () {
                window.location = url;
            }
        });
    });

    let promocode = $('.order-page__cart').find('.table-cart__promocode');
    if (promocode.val() !== '') {
        promocode.hide();
        promocode.next().hide();
    }
});

$(document).ready(function () {
    $('.comment__text-data').each(function () {
        if ($(this).text().length > 300) {
            $(this).parent().find('.read-more').addClass('visible');
        }
    });
});

});
