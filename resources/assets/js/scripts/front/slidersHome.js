$(document).ready(function () {

    let options = {
        items: 1,
        loop: false,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 5300,
        smartSpeed: 1500,
        navText: [
            "<span class='arrow-wrap'><i class='arr-topsslider arr-topslider__left' aria-hidden='true'></i></span>",
            "<span class='arrow-wrap'><i class='arr-topsslider arr-topslider__right' aria-hidden='true'></i></span>"
        ]
    };


    if ($('.full-width-slider .item').length > 1) {
        options.loop = true;
        options.autoplay = true;
    }

    $(".full-width-slider .owl-carousel").owlCarousel(options);
    $(".slider-category .owl-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        smartSpeed: 600,
        navText: [
            "<i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>",
            "<i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>"
        ],
        items: 5,
        responsive: {
            1300: {
                items: 5
            },
            1200: {
                items: 4
            },
            992: {
                items: 3
            },
            768: {
                items: 2
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });

    $(".super_product .owl-carousel").owlCarousel({
        loop: true,
        nav: true,
        smartSpeed: 600,
        URLhashListener: true,
        startPosition: 'one',
        navText: [
            "<i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>",
            "<i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>"
        ],
        items: 5,
        responsive: {
            1300: {
                items: 5
            },
            1200: {
                items: 4
            },
            992: {
                items: 3
            },
            768: {
                items: 2
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });

    $(".bestsellers .owl-carousel").owlCarousel({
        loop: true,
        nav: true,
        smartSpeed: 600,
        URLhashListener: true,
        startPosition: 'one',
        navText: [
            "<i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>",
            "<i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>"
        ],
        items: 5,
        responsive: {
            1300: {
                items: 5
            },
            1200: {
                items: 4
            },
            992: {
                items: 3
            },
            768: {
                items: 2
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });
    $(".blog-slider .owl-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        smartSpeed: 600,
        navText: [
            "<i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>",
            "<i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>"
        ],
        items: 5,
        responsive: {
            1300: {
                items: 5
            },
            1200: {
                items: 4
            },
            992: {
                items: 3
            },
            768: {
                items: 2
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });
    $(".receipts .owl-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        smartSpeed: 600,
        navText: [
            "<i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>",
            "<i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>"
        ],
        items: 5,
        responsive: {
            1300: {
                items: 5
            },
            1200: {
                items: 4
            },
            992: {
                items: 3
            },
            768: {
                items: 2
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });


    let owl1 = $('.hits .owl-carousel');
    owl1.owlCarousel();
    // Go to the next item
    $('.hits .slider-arrow__left').click(function () {
        owl1.trigger('prev.owl.carousel');
    });
    $('.hits .slider-arrow__right').click(function () {
        owl1.trigger('next.owl.carousel');
    });
    let owl2 = $('.news .owl-carousel');
    owl2.owlCarousel();
    $('.news .slider-arrow__left').click(function () {
        owl2.trigger('prev.owl.carousel');
    });
    $('.news .slider-arrow__right').click(function () {
        owl2.trigger('next.owl.carousel');
    });
    let owl3 = $('.receipts .owl-carousel');
    owl3.owlCarousel();
    $('.receipts .slider-arrow__left').click(function () {
        owl3.trigger('prev.owl.carousel');
    });
    $('.receipts .slider-arrow__right').click(function () {
        owl3.trigger('next.owl.carousel');
    });
    let owl4 = $('.bestsellers .owl-carousel');
    owl4.owlCarousel();
    $('.bestsellers .slider-arrow__left').click(function () {
        owl4.trigger('prev.owl.carousel');
    });
    $('.bestsellers .slider-arrow__right').click(function () {
        owl4.trigger('next.owl.carousel');
    });
    let owl5 = $('.blog-slider .owl-carousel');
    owl5.owlCarousel();
    $('.blog-slider .slider-arrow__left').click(function () {
        owl5.trigger('prev.owl.carousel');
    });
    $('.blog-slider .slider-arrow__right').click(function () {
        owl5.trigger('next.owl.carousel');
    });
    let owl6 = $('.super_product .owl-carousel');
    $('.super_product .slider-arrow__left').click(function () {
        owl6.trigger('prev.owl.carousel');
    });
    $('.super_product .slider-arrow__right').click(function () {
        owl6.trigger('next.owl.carousel');
    });

});