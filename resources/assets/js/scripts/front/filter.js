$(document).ready(function () {
    window.filterProducts = function(url) {
        url = url || $('#filtersForm').attr('action');
        var update_url = '';
        var url_full_array = new Array();
        var update_urlArray = new Array();
        var brands = new Array();
        var price_ranges = new Array();
        var categories = new Array();
        var characterisics = new Array();
        var character="";
        var value ="";
        var price_from = '';
        var price_to = '';
        var is_hit = '';
        var is_bestseller = '';
        var is_special = '';
        var is_new = '';
        var sort_by = '';


        params = $('#filtersForm,#sortForm').serialize();

        params_update = $('#filtersForm,#sortForm').serialize();
        var params_array = params_update.split('&');
        params_array.forEach(function (element) {
            if(element.search('brands%5B%5D=') > -1){
                id = element.substring(element.indexOf("=")+1);
                brands.push($('#filtersForm #brand'+id+' input').attr('data-name'));
            } else if(element.search('categories%5B%5D=') > -1) {
                character = element.substring(element.indexOf("=")+1);
                categories.push(character);
            } else if(element.search('character%5B%5D=') > -1) {
                character = element.substring(element.indexOf("=")+1);
                characterisics.push(character);
            } else if(element.search('price_from=') > -1) {
                value = element.substring(element.indexOf("=")+1);
                if (value.length > 0) {
                    price_from = parseInt(value);
                }
            } else if(element.search('price_to=') > -1) {
                value = element.substring(element.indexOf("=")+1);
                if (value.length > 0) {
                    price_to = parseInt(value);
                }
            } else if(element.search('price_ranges%5B%5D=') > -1) {
                price_range = element.substring(element.indexOf("=")+1);
                price_ranges.push(price_range);
            }
            else if(element.search('is_hit=') > -1) {
                value = element.substring(element.indexOf("=")+1);
                if (value.length > 0) {
                    is_hit = value;
                }
            }
            else if(element.search('is_bestseller=') > -1) {
                value = element.substring(element.indexOf("=")+1);
                if (value.length > 0) {
                    is_bestseller = value;
                }
            }
            else if(element.search('is_special=') > -1) {
                value = element.substring(element.indexOf("=")+1);
                if (value.length > 0) {
                    is_special = value;
                }
            }
            else if(element.search('is_new=') > -1) {
                value = element.substring(element.indexOf("=")+1);
                if (value.length > 0) {
                    is_new = value;
                }
            }
            else if(element.search('sort_by=') > -1) {
                value = element.substring(element.indexOf("=")+1);
                if (value.length > 0) {
                    sort_by = value;
                }
            }


        });
        params_update = params_update.substring(params_update.indexOf("price_to=")+(10+price_to.toString().length));
        if (price_ranges.length > 0) {
            params_update = params_update.substring(params_update.indexOf("price_ranges%5B%5D=" + price_ranges[price_ranges.length - 1]) + (20 + price_ranges[price_ranges.length - 1].length));
        }
        var url_full = window.location.href;
        if(url_full.indexOf('filters/') > -1) {
            url_full = url_full.substr(0,url_full.indexOf('filters/')+8);
        } else {
            update_urlArray = url_full.split('/');
            for(var i = 0; i < 5; i++) {
                url_full_array.push(update_urlArray[i]);
            }
            if(update_urlArray.length > 4) {
                url_full= url_full_array.join('/')+'/filters/';
            } else {
                url_full= url_full_array.join('/')+'filters/';
            }

        }
        var update_url_first = url_full;

        params_update = params_update.replace(/&/gm,";");
        var array_params_update = params_update.split(';');
        array_params_update.sort();

        if (brands.length > 0) {
            update_url += 'brands=' + brands.join(',') ;
        }

        if (update_url.length > 0 && categories.length) {
            update_url +=';'+'categories=' + categories.join(',') ;
        } else if(categories.length) {
            update_url += 'categories=' + categories.join(',') ;
        }
        if (update_url.length > 0 && characterisics.length) {
            update_url +=';'+'character=' + characterisics.join(',') ;
        } else if(characterisics.length) {
            update_url += 'character=' + characterisics.join(',') ;
        }
        if (update_url.length > 0 && is_bestseller) {
            update_url +=';'+ 'is_bestseller=' + is_bestseller ;
        } else if(is_bestseller) {
            update_url +='is_bestseller=' + is_bestseller ;
        }
        if (update_url.length > 0 && is_hit) {
            update_url +=';'+ 'is_hit=' + is_hit ;
        } else if(is_hit) {
            update_url +='is_hit=' + is_hit ;
        }
        if (update_url.length > 0 && is_new) {
            update_url +=';'+ 'is_new=' + is_new ;
        } else if(is_new) {
            update_url +='is_new=' + is_new ;
        }
        if (update_url.length > 0 && is_special) {
            update_url +=';'+ 'is_special=' + is_special ;
        } else if(is_special) {
            update_url +='is_special=' + is_special ;
        }


        if (update_url.length > 0 && price_from) {
            update_url +=';'+ 'price_from=' + price_from ;
        } else if(price_from ) {
            update_url +='price_from=' + price_from ;
        }

        if (update_url.length > 0 && price_ranges.length) {
            update_url +=';'+'price_ranges=' + price_ranges.join(',') ;
        } else if(price_ranges.length) {
            update_url += 'price_ranges=' + price_ranges.join(',') ;
        }

        if (update_url.length > 0 && price_to) {
            update_url += ';'+'price_to=' + price_to ;
        } else if(price_to) {
            update_url += 'price_to=' + price_to ;
        }

        if (update_url.length > 0 && sort_by) {
            update_url +=';'+ 'sort_by=' + sort_by ;
        } else if(sort_by) {
            update_url +='sort_by=' + sort_by ;
        }

        history.pushState(null, null, (update_url_first+update_url));

        $.ajax({
            type: "GET",
            url: url,
            data: params,
            success: function (data) {
                if (data.success) {

                    $('#searchContent').html(data.data.html);
                }
                else {
                    $('#searchContent').html(data.data.html);
                }
                $('.blog-pagination li a').each(function (element) {
                    var href = $( this ).prop('href');
                    var href_part = href.substr(href.indexOf('?'));
                    $( this ).attr('href',update_url_first+update_url+href_part);
                });
            },
            error: function (xhr, status, data) {
                $('.alert-danger').html('');
                $.each(xhr.responseJSON.errors, function (key, value) {
                    $('.addPromoForm.alert-danger').show();
                    $('.addPromoForm.alert-danger').append('<p>' + value + '</p>');
                });
            }
        });

    };
    function setLocation(curLoc){
        try {
            history.pushState(null, null, curLoc);
            return;
        } catch(e) {}
        location.hash = '#' + curLoc;
    }

    window.ignorePriceCheckboxes = function() {
        $('.price_checkbox').prop('checked', false);
    };

    window.filterCheckboxes = function () {
        var input, filter, ul, li, a, i;
        input = document.querySelector("#brand");
        filter = input.value.toUpperCase();
        ul = document.getElementById("BrandsList");
        li = ul.getElementsByClassName('checkbox-wrap');

        for (i = 0; i < li.length; i++) {
            text = li[i].getElementsByClassName("brand_text")[0];
            if (text.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    };

    (function($) {
        $.fn.clickToggle = function(func1, func2) {
            var funcs = [func1, func2];
            this.data('toggleclicked', 0);
            this.click(function() {
                var data = $(this).data();
                var tc = data.toggleclicked;
                $.proxy(funcs[tc], this)();
                data.toggleclicked = (tc + 1) % 2;
            });
            return this;
        };
    }(jQuery));


        let items = $('.categories-page .list-minimized').children('label');

        let hide_items = items.filter(function () {
            return $(this).index() > 6
        });

        hide_items.hide();

        $( "#all-list-maximize" ).clickToggle(function() {
            $( "#BrandsList" ).removeClass( "list-minimized" );
            $( "#all-list-maximize" ).text('Краткий список');
            hide_items.show();
        }, function() {
            $( "#BrandsList" ).addClass( "list-minimized" );
            $( "#all-list-maximize" ).text('Полный список');
            hide_items.hide();
        });

        let arr = window.location.pathname.split('/');

        if(arr[1] == 'special-offer'){
            $('.form-group.spec').hide();
            $('.categories-page #filtersForm').attr('action', $('.categories-page #filtersForm').attr('action') + '/' + arr[2]);
        }

        if(arr[1] == 'tag'){
            $('.form-group.tag').hide();
        }
        if(arr[1] == 'brand'){
            $('.form-group.price-block').css('margin-top', 0);
            // $('.form-group.brand').hide();
        }
        if ((arr[1] == 'category')&&((arr[2] == 'gotovye-nabory-fitbox'))) {
            $('.form-group.price-block').css('margin-top', 0);
            $('.form-group.brand').hide();
        }

        $('.form-group.categor .form-group__result.checkbox .checkbox__input').change(function () {
            if($(this).prop( "checked" )) {
                $(this).closest(".form-group__result.checkbox").find('.form-group.categor-sub').removeClass('none');
            }   else {
                if($('.form-group.categor-sub .form-group__result.checkbox .checkbox__input').prop( "checked" ) === false) {
                    $(this).closest(".form-group__result.checkbox").find('.form-group.categor-sub').addClass('none');
                }
            }

            group_categor();
            group_categor_sub();
            group_categor_sub_sub();
        });
        $('.form-group.categor-sub .form-group__result.checkbox .checkbox__input').on('change', function () {
            if($(this).prop( "checked" )) {

                let parent = $(this).closest(".form-group__result").parent().closest(".form-group__result").find("> .checkbox-wrap .checkbox__input");
                if (parent.prop('checked') === true) {
                    parent.click();
                }
                // if($(this).closest(".form-group__result").parent().closest(".form-group__result").find("> .checkbox-wrap .checkbox__input").prop("checked") === true) {
                //     $(this).closest(".form-group__result").parent().closest(".form-group__result").find("> .checkbox-wrap").click();
                // }
                $(this).closest('.form-group.categor').removeClass('none');
                $(this).closest('.form-group.categor-sub').removeClass('none');
                $(this).closest(".form-group__result.checkbox").find('.categor-sub-sub').removeClass('none');
            } else {
                if($('.form-group.categor-sub-sub .form-group__result.checkbox .checkbox__input').prop( "checked" ) === false) {
                    $(this).closest(".form-group__result.checkbox").find('.categor-sub-sub').addClass('none');
                }
            }
            group_categor();
            group_categor_sub();
            group_categor_sub_sub();
        });
        $('.form-group.categor-sub-sub .form-group__result.checkbox .checkbox__input').change(function () {
            if($(this).prop( "checked" )) {
                let parent = $(this).closest(".form-group").parent().closest(".form-group").find("> .checkbox-wrap .checkbox__input");
                if (parent.prop('checked') === true) {
                    parent.click();
                }
                // if($(this).closest(".form-group").parent().closest(".form-group").find("> .checkbox-wrap .checkbox__input").prop("checked")) {
                //     $(this).closest(".form-group").parent().closest(".form-group").find("> .checkbox-wrap").click();
                // }
                $(this).closest('.form-group.categor-sub-sub').removeClass('none');
                $(this).closest('.form-group.categor-sub').removeClass('none');
            }
            group_categor();
            group_categor_sub();
            group_categor_sub_sub();
        });
        function group_categor () {
            $('.form-group.categor .form-group__result.checkbox .checkbox__input').each(function () {
                if($(this).prop( "checked" )) {
                    $(this).closest(".form-group__result.checkbox").find('.form-group.categor-sub').removeClass('none');
                }   else {
                    $(this).closest(".form-group__result.checkbox").find('.form-group.categor-sub').addClass('none');
                }
            });

        }

        function group_categor_sub () {
            $('.form-group.categor-sub .form-group__result.checkbox .checkbox__input').each(function () {

                if($(this).prop( "checked" )) {
                    $(this).closest('.form-group.categor-sub').removeClass('none');
                    $(this).closest(".form-group__result.checkbox").find('.form-group.categor-sub-sub').removeClass('none');
                }   else {
                    $(this).closest(".form-group__result.checkbox").find('.form-group.categor-sub-sub').addClass('none');
                }
            });
        }

        function group_categor_sub_sub () {
            $('.form-group.categor-sub-sub .form-group__result.checkbox .checkbox__input').each(function () {
                if ($(this).prop("checked")) {
                    $(this).closest('.form-group.categor-sub-sub').parent().find('.form-group__result.checkbox .checkbox__input').each(function () {
                        $(this).closest('.form-group.categor-sub-sub').removeClass('none');
                    });
                    //$(this).closest('.form-group.categor-sub').removeClass('none');
                }
            });
        }
        group_categor();
        group_categor_sub();
        group_categor_sub_sub();


});

