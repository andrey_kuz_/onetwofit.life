$(document).ready(function () {
    window.isCheck = function(name) {
        return document.querySelector('input[name="' + name + '"]:checked');
    };

    window.reviewInit = function (oneClickUrl) {
        $('#feedback-window .modal-content').html();
        $('#feedback-window .modal-content').load(oneClickUrl, function (responseText, textStatus, jqXHR ) {

            // if ((jqXHR.statusText == 'Unauthorized')||(jqXHR.status == 401))
            // {
            //     $(this).html($('#reviewAuthError').val());
            //     window.location.href = $('#reviewAuthUrl').val();
            // }

            if($('#user-house').val() !== ''){
                $('#user-house').next().css({'color' : '#6004ba', 'top' : '8px', 'font-size' : '14px', 'transition' : 'all 0.5s linear'});
            }

            starcount = isCheck('rating').value;
            allLabels = $('.radio-toolbar label');

            for (var i = 0; i < allLabels.length; i++) {
                if ( i < starcount ) {

                    allLabels[i].className = '';
                    allLabels[i].className += "fullstar";
                }
            }

            $('.radio-toolbar input:radio').click(function(){

                starcount = isCheck('rating').value;

                for (var i = 0; i < allLabels.length; i++) {
                    if ( i < starcount ) {
                        allLabels[i].className = '';
                        allLabels[i].className += "fullstar";
                    }
                    else {
                        allLabels[i].className = '';
                        allLabels[i].className += "emptystar";
                    }
                }
            });
        });

    };

    window.reviewProcess = function() {

        $.ajax({
            type: "POST",
            url: $('#reviewUrl').val(),
            data: $('#feedbbackform').serialize(),
            success: function (data) {
                $('#feedback-window .modal-content').html(data.html);
                if (data.success)
                {
                    //$('#reviewsContent').replaceWith(data.block)
                }

            },
            error: function (xhr, status, data) {
                $('.alert-danger.oneClickErrors').html('');
                $.each(xhr.responseJSON.errors, function (key, value) {
                    $('.alert-danger.reviewErrors').show();
                    $('.alert-danger.reviewErrors').append('<p>' + value + '</p>');
                });
            }
        });
    };
});