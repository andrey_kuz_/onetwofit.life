$(document).ready(function () {
    $('.some-product-thumbnails-slider .owl-carousel').owlCarousel({
        items: 1,
        autoHeight: true,
        loop: false,
        nav: false,
        dots: false,
        smartSpeed: 1200,
        thumbs: true,

        // When only using images in your slide (like the demo) use this option to dynamicly create thumbnails without using the attribute data-thumb.
        thumbImage: false,

        // Enable this if you have pre-rendered thumbnails in your html instead of letting this plugin generate them. This is recommended as it will prevent FOUC
        thumbsPrerendered: true,

        // Class that will be used on the thumbnail container
        thumbContainerClass: 'owl-thumbs',

        // Class that will be used on the thumbnail item's
        thumbItemClass: 'owl-thumb-item'
    });

    $(".with-product-slider .owl-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        smartSpeed: 600,
        navText: [
            "<i class='fa fa-bug'></i>",
            "<i class='fa fa-bug'></i>"
        ],
        items: 8,
        responsive: {
            1500: {
                items: 6
            },
            1200: {
                items: 5
            },
            1024: {
                items: 4
            },
            992: {
                items: 3
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });

    var owl1 = $('.with-product-slider .owl-carousel');
    owl1.owlCarousel();
    // Go to the next item
    $('.with-product-slider .slider-arrow__left').click(function () {
        owl1.trigger('prev.owl.carousel');
    });
    $('.with-product-slider .slider-arrow__right').click(function () {
        owl1.trigger('next.owl.carousel');
    });
    $(".maybe-like-slider .owl-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        smartSpeed: 600,
        navText: [
            "<i class='fa fa-bug'></i>",
            "<i class='fa fa-bug'></i>"
        ],

        items: 8,
        responsive: {
            1500: {
                items: 6
            },
            1200: {
                items: 5
            },
            1024: {
                items: 4
            },
            992: {
                items: 3
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });

    var owl2 = $('.maybe-like-slider .owl-carousel');
    owl2.owlCarousel();
    // Go to the next item
    $('.maybe-like-slider .slider-arrow__left').click(function () {
        owl2.trigger('prev.owl.carousel');
    });
    $('.maybe-like-slider .slider-arrow__right').click(function () {
        owl2.trigger('next.owl.carousel');
    });
    $(".product-bought .owl-carousel").owlCarousel({
        loop: false,
        dots: false,
        nav: false,
        smartSpeed: 600,
        navText: [
            "<i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i>",
            "<i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i>"
        ],
        items: 3,
        responsive: {
            1920: {
                items: 3
            },
            1800: {
                items: 3
            },
            1400: {
                items: 3
            },
            1199: {
                items: 3
            },
            991: {
                items: 3
            },
            767: {
                items: 3
            },
            550: {
                items: 2
            },
            425: {
                items: 1
            },
            375: {
                items: 1
            },
            360: {
                items: 1
            },
            325: {
                items: 1
            },
            320: {
                items: 1
            }
        }
    });

    var owlProduct = $('.product-bought .owl-carousel');
    owlProduct.owlCarousel();
    $('.product-bought .product-bought__arrow-left').click(function () {
        owlProduct.trigger('prev.owl.carousel');
    });
    $('.product-bought .product-bought__arrow-right').click(function () {
        owlProduct.trigger('next.owl.carousel');
    });

    $(".interested-good .owl-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        smartSpeed: 600,
        navText: [
            "<i class='fa fa-bug'></i>",
            "<i class='fa fa-bug'></i>"
        ],
        items: 5,
        responsive: {
            1300: {
                items: 5
            },
            1200: {
                items: 4
            },
            992: {
                items: 3
            },
            768: {
                items: 2
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });
    var owlinterested = $('.interested-good .owl-carousel');
    owlinterested.owlCarousel();
    $('.interested-good .slider-arrow__left').click(function () {
        owlinterested.trigger('prev.owl.carousel');
    });
    $('.interested-good .slider-arrow__right').click(function () {
        owlinterested.trigger('next.owl.carousel');
    });

    $(".history-product-slider .owl-carousel").owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        smartSpeed: 600,
        navText: [
            "<i class='fa fa-bug'></i>",
            "<i class='fa fa-bug'></i>"
        ],
        items: 8,
        responsive: {
            1500: {
                items: 6
            },
            1200: {
                items: 5
            },
            1024: {
                items: 4
            },
            992: {
                items: 3
            },
            600: {
                items: 2
            },
            550: {
                items: 1
            },
            0: {
                items: 1
            }
        }
    });

    var owlhistory = $('.history-product-slider .owl-carousel');
    owlhistory.owlCarousel();
    // Go to the next item
    $('.history-product-slider .slider-arrow__left').click(function () {
        owlhistory.trigger('prev.owl.carousel');
    });
    $('.history-product-slider .slider-arrow__right').click(function () {
        owlhistory.trigger('next.owl.carousel');
    });
});
