$( document ).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let burger = document.querySelector('.burger');
    let menu = document.querySelectorAll('.adaptive-menu__item');

    burger.addEventListener('click', () => {
        if (burger.classList.contains('open')) {
        burger.classList.remove('open');
        burger.nextElementSibling.classList.remove('adaptive-menu__open');
        burger.firstElementChild.classList.remove('fa-close');
        burger.firstElementChild.classList.add('fa-bars');
        document.body.style.overflowY = 'scroll';
        $('jdiv').show();
        $('#fb-root').show();


        } else {
        $('#fb-root').hide();
        $('jdiv').hide();
        burger.classList.add('open');
        burger.nextElementSibling.classList.add('adaptive-menu__open');
        burger.firstElementChild.classList.add('fa-close');
        burger.firstElementChild.classList.remove('fa-bars');
        document.body.style.overflowY = 'hidden';
    }
    });
    for (let i = 0; i < menu.length; i++) {
        menu[i].addEventListener('click', () => {
            if (menu[i].classList.contains('sub__open')) {
            menu[i].classList.remove('sub__open');
        } else {
            menu[i].classList.add('sub__open');
        }
    });
    }
    $('.burger .fa.fa-bars').click(function () {
        $('.header-bottom__mobile').removeClass('active');
        $('.header-main__search-wrapper').removeClass('active');
    });

    $(".header-main__search.search .fa.fa-search").click(function () {
        $(this).closest('form').submit();
    });

    $(".search-page .search-icon").click(function () {
        $(this).closest('form').submit();
    });



    /*

    $('.navigation-list__link').hover(function(){

        if ($(this).closest("li").find("div").length > 0) {
            $('html, body').css('overflow', 'hidden');
        }

    },function(){
        $('html, body').css('overflow', 'auto');
    });

    $('.sub-sub-navigation-list__wrapper') && $('.sub-navigation-list__wrapper') && $('.searchgoods-wrap').hover(function(){

        $('html, body').css('overflow', 'hidden');

    },function(){

        $('html, body').css('overflow', 'auto');

    });
    */
    $('.header-main__search-wrapper.dropdown .megamenu .onelevel a').click(function (e) {
        e.preventDefault();
        var choose =  $(this).attr('data-search');
        if(choose == "onelevel-code") {
            $('.search-element').prop('name','search_code');
            $('.search-element').prop('placeholder', 'поиск по артикулу...');
        }
        if(choose == "onelevel-brand"){
            $('.search-element').prop('name','search_brand');
            $('.search-element').prop('placeholder', 'поиск по бренду...');
        }
        if(choose == "onelevel-flavor"){
            $('.search-element').prop('name','search_flavors');
            $('.search-element').prop('placeholder', 'поиск по вкусу...');
        }
    });
    $('.header-bottom.navigation .header-bottom__mobile .btn-search').click(function (e) {
        if($('.header-bottom.navigation .header-bottom__mobile input').val() == '') {
            e.preventDefault();
            if($('.burger').hasClass('open')) {
                $('.burger i').click();
            }
            if ($('.header-bottom__mobile').hasClass('active')) {
                $('.header-bottom__mobile').removeClass('active');
                $('.header-main__search-wrapper').removeClass('active');
            } else {
                $('.header-bottom__mobile').addClass('active');
            }
        }
    });
    $('.header-bottom.navigation .header-bottom__mobile .btn-caret').click(function (e) {
        e.preventDefault();
        if ($('.header-main__search-wrapper').hasClass('active')) {
            $('.header-main__search-wrapper').removeClass('active');
        } else {
            $('.header-main__search-wrapper').addClass('active');
        }
    });
    $('.header-bottom.navigation .header-bottom__mobile .megamenu .onelevel a').click(function (e) {
        e.preventDefault();
        var choose =  $(this).attr('data-search');
        if(choose == "onelevel-code") {
            $('.search-element').prop('name','search_code');
            $('.search-element').prop('placeholder', 'поиск по артикулу...');
        }
        if(choose == "onelevel-brand"){
            $('.search-element').prop('name','search_brand');
            $('.search-element').prop('placeholder', 'поиск по бренду...');
        }
        if(choose == "onelevel-flavor"){
            $('.search-element').prop('name','search_flavors');
            $('.search-element').prop('placeholder', 'поиск по вкусу...');
        }
        $('.header-main__search-wrapper').removeClass('active');
    });


    function owlBestsellers() {

        $(".bestsellers .owl-carousel").owlCarousel({
            loop: true,
            nav: false,
            dots:false,
            smartSpeed: 600,
            URLhashListener: true,
            startPosition: 'one',
            navText: [
                "<i class='arr-topsslider arr-topslider__left arr-topslider-violet' aria-hidden='true'></i>",
                "<i class='arr-topsslider arr-topslider__right arr-topslider-violet' aria-hidden='true'></i>"
            ],
            items: 5,
            responsive: {
                1300: {
                    items: 5
                },
                1200: {
                    items: 4
                },
                992: {
                    items: 3
                },
                768: {
                    items: 2
                },
                600: {
                    items: 2
                },
                550: {
                    items: 1
                },
                0: {
                    items: 1
                }
            }
        });
        let owl = $('.bestsellers .owl-carousel');
        owl.owlCarousel();
        $('.bestsellers .slider-arrow__left').click(function () {
            owl.trigger('prev.owl.carousel');
        });
        $('.bestsellers .slider-arrow__right').click(function () {
            owl.trigger('next.owl.carousel');
        });
    }

    let default_category_id = $('.slider-category.bestsellers .bestsellers__url a:first-child').attr('data-value');

    $.ajax({
        type: "POST",
        url: "/",
        data: {id: default_category_id},
        success: function (data) {
            if (data) {

                $('.slider-category.bestsellers').remove();
                $('.blog').before(data);
                owlBestsellers();
                $('.slider-category.bestsellers .bestsellers__url a:first-child').addClass('active');
                $('[data-toggle="tooltip"]').tooltip();
            }
        }
    });


    $('body').on("click", ".bestsellers__url .bestsellers__link", function (e) {

        e.preventDefault();

        let id = $(this).attr('data-value');
        let current_link = $(this);

        $('.slider-category.bestsellers .bestsellers__url a').removeClass('active');
        current_link.addClass('active');

        let bestsellers = $('.slider-category.bestsellers');
        let slider_content = $('.slider-category.bestsellers .slider-category__content');
        let noslider_content = $('.slider-category.bestsellers .noslider-container');
        let not_result = $('.slider-category.bestsellers h1');

        $.ajax({
            type: "POST",
            url: "/bestsellersFindCategory",
            data: {id: id},
            success: function (data) {
                if (data) {
                    bestsellers.remove();
                    $('.blog').before(data);
                    owlBestsellers();
                    $('[data-toggle="tooltip"]').tooltip();

                    $('.slider-category.bestsellers .bestsellers__url a').each(function () {
                        let link_id = $( this ).attr('data-value');
                        if (id == link_id) {
                            $( this ).addClass('active');
                        }
                    });

                } else {

                    if (slider_content.length > 0) {
                        slider_content.remove();
                    }

                    if (noslider_content.length > 0) {
                        noslider_content.remove();
                    }

                    if (not_result.length > 0) {
                        not_result.remove();
                    }

                    bestsellers.append('<h1>Извините, для вашего запроса нет результатов</h1>');
                }
            }
        });
    });

    let tag_items = $('.header-bottom.navigation .navigation-list .tag_list_item').children('li');
    let tag_items_hide = tag_items.filter(function () {
        return $(this).index() > 10
    });
    tag_items_hide.hide();

    let adaptiv_tag_items = $('.adaptive-menu .tag_list_item').children('li');
    let adaptiv_tag_items_hide = adaptiv_tag_items.filter(function () {
        return $(this).index() > 10
    });
    adaptiv_tag_items_hide.hide();



    let brand_items = $('.header-bottom.navigation .navigation-list .brand_list_item').children('li');
    let brand_items_hide = brand_items.filter(function () {
        return $(this).index() > 17
    });
    brand_items_hide.hide();

    let adaptiv_brand_items = $('.adaptive-menu .brand_list_item').children('li');
    let adaptiv_brand_items_hide = adaptiv_brand_items.filter(function () {
        return $(this).index() > 10
    });
    adaptiv_brand_items_hide.hide();


    if($(window.location.hash).length > 0){
        $('html, body').animate({ scrollTop: $(window.location.hash).offset().top - 150}, 1000);
    }
    $(".letter__link").click(function (){
        let content = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(content).offset().top - 150
        }, 1000);
    });



});

