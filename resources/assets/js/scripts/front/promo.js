$(document).ready(function () {
    window.addPromo = function(url) {

        $.ajax({
            type: "POST",
            url: url,
            data: $('#addPromoForm').serialize(),
            success: function (data) {
                if (data.success) {
                    $('#cartSummary').replaceWith(data.data.html);
                    //$('.addPromoForm.alert-success').html('');
                    //$('.addPromoForm.alert-success').show();
                    //$('.addPromoForm.alert-success').append('<p>' + data.mess + '</p>');
                }
                else {
                    $('#cartSummary').html(data.data.html);
                    //$('.addPromoForm.alert-danger').html('');
                    //$('.addPromoForm.alert-danger').show();
                    //$('.addPromoForm.alert-danger').append('<p>' + data.mess + '</p>');
                }
            },
            error: function (xhr, status, data) {
                $('.alert-danger').html('');
                $.each(xhr.responseJSON.errors, function (key, value) {
                    $('.addPromoForm.alert-danger').show();
                    $('.addPromoForm.alert-danger').append('<p>' + value + '</p>');
                });
            }
        });
    };
});
