/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



require('./bootstrap');
require("jquery.maskedinput/src/jquery.maskedinput.js");
require("select2");
require('owl.carousel2');
window.lightBox = require("lightbox2");
require('./scripts/front/cart');
require('./scripts/front/account');
require('./scripts/front/favorites');
require('./scripts/front/filter');
require('./scripts/front/menu');
require('./scripts/front/myorders');
require('./scripts/front/order');
require('./scripts/front/pagination');
require('./scripts/front/promo');
require('./scripts/front/review');
require('./scripts/front/slidersHome');
require('./scripts/front/slidersProduct');
require('./scripts/front/main');
