<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;
use Location;

class Language
{
    public function handle($request, Closure $next)
    {
        $this->setLocale();
        $this->setCurrency();
        $this->setCountry();

        return $next($request);
    }

    private function setLocale()
    {
        if (Session::has('applocale') AND in_array(Session::get('applocale'), Config::get('app.languages')))
        {
            App::setLocale(Session::get('applocale'));
        }
        else
        {
            App::setLocale(Config::get('app.locale'));
        }
    }

    private function setCurrency()
    {
        if (Session::has('appcurrency') AND in_array(Session::get('appcurrency'), Config::get('app.currencies'))) {
            config(['app.currency' => Session::get('appcurrency')]);
        }
    }

    private function setCountry()
    {
        if (Session::has('appcountry'))
        {
            config(['app.country' => Session::get('appcountry')]);
        }
        else
        {
            // if country is not set previously - get it from ip address
            $position = Location::get();

            if (isset($position->countryCode) AND in_array($position->countryCode, Config::get('app.countries'))) {
                config(['app.country' => $position->countryCode]);
            }
        }
    }
}
