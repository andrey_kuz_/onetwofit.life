<?php

namespace App\Http\Controllers;

use App\Http\Requests\PromocodeRequest;
use App\Exceptions\PromocodeExeption;
use App\Models\ComboOffer;
use App\Models\Promocode;
use Illuminate\Http\Request;
use \App\Models\Cart;
use App\Models\Product;
use \App\Models\CartLine;
use App\Services\CartService;
use Illuminate\Routing\Route;
use App\Exceptions\ProductUnavailableExeption;
use App\Exceptions\ProductUnavailableQuantityExeption;
use App\Services\SubscriptionService;
use App\Models\Subscription;

class CartController extends Controller
{
    protected $redirectTo = '/cart';

    protected $cartService;

    public function __construct()
    {


    }

    /**
     * add combo products top cart
     *
     * @param Request $request
     * @param ComboOffer $combo
     * @param bool $isAjax
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addCombo(Request $request, ComboOffer $combo, $isAjax = true)
    {
        $this->cartService = new CartService();
        $mess = '';
        try {
            $this->cartService->addProduct($combo->product_id, 1);
            //$mess = __('messages.added');
            $status = 1;

            // if first product is added - try to add the second one
            try {
                $this->cartService->addProduct($combo->related_product_id, 1);
                //$mess = __('messages.added');
                $status = $status && 1;
            } catch (ProductUnavailableExeption $e) {
                // if second product add failed - remove the first one
                $this->cartService->subProduct($combo->product_id);
                $mess = $e->getMessage();
                $status = 0;
            }
        } catch (ProductUnavailableExeption $e) {
            $mess = $e->getMessage();
            $status = 0;
        }

        if ($isAjax) {
            return response()->json([
                'status' => $status,
                'mess' => $mess,
                'result' => $this->getCartPopup()
            ]);
        } else {
            return redirect($this->redirectTo);
        }
    }

    /**
     * add product
     * increase product amount in cart
     *
     * @param  Request $request
     * @param  int $product product id
     * @param  mixed $deltaQuantity
     * @param  bool $isAjax if true or == 1 - return ajax response
     *                              if false - redirect to cart page
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request, $product, $deltaQuantity = 1, $isAjax = false)
    {

        try {
            $this->cartService = new CartService();
            $this->cartService->addProduct($product, $deltaQuantity);
            $mess = __('messages.added');
            $status = 1;
        } catch (ProductUnavailableExeption $e) {

            $mess = $e->getMessage();
            $status = 0;
        }

        if ($status) {
            return redirect($this->redirectTo);
        } else {
            return redirect()->back()->withErrors($mess);
        }
    }

    /**
     * add product
     * increase product amount in cart
     *
     * @param  Request $request
     * @param  int $product product id
     * @param  mixed $deltaQuantity
     * @param  bool $isAjax if true or == 1 - return ajax response
     *                              if false - redirect to cart page
     *
     * @return \Illuminate\Http\Response
     */
    public function addAjax(Request $request, $product, $deltaQuantity = 1, $isAjax = true)
    {
        $this->cartService = new CartService();

        try {
            $this->cartService->addProduct($product, $deltaQuantity);
            $mess = __('messages.added');
            $status = 1;
        } catch (ProductUnavailableExeption $e) {
            $mess = $e->getMessage();
            $status = 0;
        }


        return response()->json([
            'status' => $status,
            'mess' => $mess,
            'result' => $this->getCartPopup()
        ]);
    }


    /**
     * decrease products count from cart
     * increase product amount in cart
     *
     * @param  Request $request
     * @param  int $product product id
     * @param  bool $isAjax if true or == 1 - return ajax response
     *                              if false - redirect to cart page
     *
     * @return \Illuminate\Http\Response
     */
    public function sub(Request $request, $product, $isAjax = false)
    {
        $this->cartService = new CartService();
        $this->cartService->subProduct($product);

        if ($isAjax) {
            return response()->json([
                'status' => 1,
                'mess' => '',
                'result' => $this->getCartPopup()
            ]);
        } else {
            return redirect($this->redirectTo);
        }
    }

    /**
     * remove product item from cart
     *
     * @param  Request $request
     * @param  Product $product
     * @param  bool $isAjax if true or == 1 - return ajax response
     *                              if false - redirect to cart page
     *
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $product, $isAjax = false)
    {
        $this->cartService = new CartService();
        $this->cartService->removeProduct($product);



        if ($isAjax) {
            return response()->json([
                'status' => 1,
                'mess' => '',
                'result' => $this->getCartPopup()
            ]);
        } else {
            return redirect($this->redirectTo);
        }
    }


    /**
     * get the cart entity and count of cart items (for ajax request)
     *
     * @return \Illuminate\Http\Response
     */
    public function getCartPopup($cart = null)
    {
        $this->cartService = new CartService();

        $cart = $this->cartService->cart;
//        print_r($cart);die;
        \Log::info('item_count after - '.$cart->item_count);
        $cart->refreshCartInfo();
        $cartItems = $cart->items()->with('product.translations')->get();
        $cartInfo = $cart->info;
        $cartInfo->freeShippingLeft = to_currency($cartInfo->freeShippingLeft, true);
        $cartSum =  trans('messages.Сумма Заказа').'<span class="interact-cart__summ">'.to_currency($cartInfo->total, true).'</span>';

        $html = view('cart.popup')
            ->with(compact(
                'cartInfo',
                'cartItems'
            ))->render();

        return [
            'cartInfo' => $cartInfo,
            'html' => $html,
            'cartSum' => $cartSum,
        ];
    }

    /**
     * Show the cart entity
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Request $request)
    {
        $prevRoute = app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName();
        $checkoutUrl = null;
        if ($prevRoute == 'checkout2') {
            $checkoutUrl = url()->previous();
            session()->put('checkoutUrl', $checkoutUrl);
        } elseif ($prevRoute == 'cart.view') {
            if (session()->has('checkoutUrl')) {
                $checkoutUrl = session()->get('checkoutUrl');
            }
        } else {
            session()->forget('checkoutUrl');
        }


        try {
            $this->cartService = new CartService();
            $cart = $this->cartService->cart;
            $cart->refreshCartInfo();
            //Cart::all();
            $cartItems = $cart->items; //()->with('product.translations')->get();
            $products_id = $cartItems->pluck('product_id')->toArray();
            $productsInfo = Product::Select('id','slug')->whereIN('id',$products_id)->get();
            $products = array();
            foreach ($productsInfo as $productInfo){
                $products[$productInfo->id] = $productInfo->slug;
            }
            $promocodeStatusInfo = Promocode::statusInfo($cart->promocode);

            return view('cart.view')
                ->with(compact(
                    'promocodeStatusInfo',
                    'cart',
                    'cartItems',
                    'checkoutUrl',
                    'products'
                ));
        } catch (ProductUnavailableExeption $e) {
            $mess = $e->getMessage();
            $status = 0;
        }


    }

    public function addPromocode(PromocodeRequest $request)
    {
        $this->cartService = new CartService();
        $cart = $this->cartService->updateCartWithPromocode($request->promocode);
        $promocodeStatusInfo = Promocode::statusInfo($request->promocode);

        $checkoutUrl = $request->checkoutUrl;

        $html = view('cart.viewsummary')
            ->with(compact(
                'checkoutUrl',
                'promocodeStatusInfo',
                'cart'
            ))->render();

        return response()->json([
            'data' => [
                'cartInfo' => $cart->info,
                'html' => $html
            ]
        ]);
    }

}
