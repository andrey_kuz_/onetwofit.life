<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Models\Product;

class FileController extends Controller
{
    public function export()
    {
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="products.csv"',
        );

        $products = Product::whereNotIn('id',[1310,824,1741,1332,1162,1291,819,1283,695,1045,1071,800,1301,1329,729,930,1174,1284,931,1773,1179,697,1060,1517,1037,698,768,808,1285,659,1282,782,927,662,848,218,1518,1579,1036,935,936,694,1074,934,702,700,841,692,1328,730,693,699,1333,701])->get();
        $columns = array('id', 'title', 'link', 'image_link ', 'price', 'description', 'sale_​price', 'availability', 'condition', 'brand');

        $callback = function() use ($columns, $products)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            $currency = \Config::get('app.currency');

            foreach($products as $product) {

                if ($product->slug) {
                    $final_url = route('product.index', ['id'=>$product->id, 'slug'=> $product->slug]);
                } else {
                    $final_url = '';
                }

                if($product->image) {
                    $images = json_decode($product->image);
                }

                if($images) {
                    $image_url = \Storage::disk('public')->url($images[0]);
                } else {
                    $image_url = '';
                }
                $images = '';

                if ($product->discount > 0) {
                    $sale_price = number_format($product->price_discounted, 2, '.', '') . ' ' . $currency;
                } else {
                    $sale_price = '';
                }

                $pattern = "~(?<=\d),(?=\d)~";
                $replacement = ".";

                $item_description = mb_substr(strip_tags($product->description), 0, 5000);
                $item_description = html_entity_decode($item_description);
                $item_description = preg_replace($pattern , $replacement , $item_description);
                $replace_description = str_replace(',', '  ',$item_description);

                $item_title = mb_substr($product->name, 0, 150);
                $item_title = preg_replace($pattern , $replacement , $item_title);
                $replace_title = str_replace(',', '  ',$item_title);

                $price = $product->price . ' ' . $currency;

                if ($product->is_indiv) {
                    $availability = 'preorder';
                } else {
                    if ($product->is_finished) {
                        $availability = 'out of stock';
                    } else {
                        $availability = 'in stock';
                    }
                }

                $condition = 'new';

                $product_brand = Brand::find($product->brand_id);
                if($product_brand) {
                    $brand = mb_substr($product_brand->name, 0, 70);
                } else {
                    $brand = '';
                }

                fputcsv($file,
                    array(
                        $product->id,
                        $replace_title,
                        $final_url,
                        $image_url,
                        $price,
                        $replace_description,
                        $sale_price,
                        $availability,
                        $condition,
                        $brand
                    )
                );
            }
            fclose($file);
        };
        return \Response::stream($callback, 200, $headers);
    }
}
