<?php

namespace App\Http\Controllers\Buffer;

use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Services\Buffer\ImportCategoriesProductsService;
use App\Services\Buffer\ImportCountriesService;
use App\Services\Buffer\ImportService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Import;
use App\Services\Buffer\ImportFlavorsService;
use App\Services\Buffer\ImportBrandsService;
use App\Services\Buffer\ImportIssueFormsService;
use App\Services\Buffer\ImportCategoriesService;
use App\Services\Buffer\ImportProductsService;


class ImportController extends Controller
{
    public function __construct()
    {
    }

    public function full()
    {
        $deletes = DB::connection('mysql_buffer')->table('deletes')->get();
        $lastImport = Import::success()->orderBy('created_at', 'desc')->first();
        $currentImport = Import::create();
        $currentImport->type = 'full';

        try{
            $importFlavourService = new ImportFlavorsService($lastImport);
            $importFlavourService->exportImportFull();
            $importFlavourService->processDeletes();

            $importBrandsService = new ImportBrandsService($lastImport);
            $importBrandsService->processDeletes();
            $importBrandsService->exportImportFull();

            $importIssueFormsService = new ImportIssueFormsService($lastImport);
            $importIssueFormsService->processDeletes();
            $importIssueFormsService->exportImportFull();

            $importCountriesService = new ImportCountriesService($lastImport);
            $importCountriesService->processDeletes();
            $importCountriesService->exportImportFull();

            $importCategoriesService = new ImportCategoriesService($lastImport);
            $importCategoriesService->processDeletes();
            $importCategoriesService->exportImportFull();

            $importProductsService = new ImportProductsService($lastImport);
            $importProductsService->processDeletes();
            $importProductsService->exportImportFull();

            $importCategoriesProductsService = new ImportCategoriesProductsService($lastImport);
            $importCategoriesProductsService->processDeletes();
            $importCategoriesProductsService->exportImportFull();

            ImportService::$importInfo['initial_deletes'] = $deletes;
            DB::connection('mysql_buffer')->table('deletes')->delete();


            $importInfo = ImportService::$importInfo;
            $currentImport->info = json_encode($importInfo);
            $currentImport->success = true;
            $currentImport->save();
            /*Update all null slug (Brand, Categories, Products) */
            $brand = new BrandController();
            $brand->updateSlug();
            $categories = new CategoryController();
            $categories->updateSlug();
            $products = new ProductController();
            $products->updateSlug();
        }
        catch (\Exception $e)
        {
            $importInfo = ImportService::$importInfo;

            $currentImport->info = json_encode($importInfo);
            $currentImport->error = json_encode($e);
            $currentImport->save();
            return json_encode(['success' => false, 'error' => json_encode($e)]);
        }
    }

    public function changes()
    {
        $deletes = DB::connection('mysql_buffer')->table('deletes')->get();
        \Log::info('initial deletes - '.json_encode($deletes));

        $lastImport = Import::success()->orderBy('created_at', 'desc')->first();
        $currentImport = Import::create();

        $currentImport->type = 'changes';

        try{
            $importFlavourService = new ImportFlavorsService($lastImport);
            $importFlavourService->processDeletes();
            $importFlavourService->exportImportChanges();

            $importBrandsService = new ImportBrandsService($lastImport);
            $importBrandsService->processDeletes();
            $importBrandsService->exportImportChanges();

            $importIssueFormsService = new ImportIssueFormsService($lastImport);
            $importIssueFormsService->processDeletes();
            $importIssueFormsService->exportImportChanges();

            $importCountriesService = new ImportCountriesService($lastImport);
            $importCountriesService->processDeletes();
            $importCountriesService->exportImportChanges();

            $importCategoriesService = new ImportCategoriesService($lastImport);
            $importCategoriesService->processDeletes();
            $importCategoriesService->exportImportChanges();

            $importProductsService = new ImportProductsService($lastImport);
            $importProductsService->processDeletes();
            $importProductsService->exportImportChanges();

            $importCategoriesProductsService = new ImportCategoriesProductsService($lastImport);
            $importCategoriesProductsService->processDeletes();
            $importCategoriesProductsService->exportImportChanges();

            ImportService::$importInfo['initial_deletes'] = $deletes;
            DB::connection('mysql_buffer')->table('deletes')->delete();

            $importInfo = ImportService::$importInfo;

            $currentImport->info = json_encode($importInfo);
            $currentImport->success = true;
            $currentImport->save();

            return json_encode(['success' => true]);
        }
        catch (\Exception $e)
        {
            $importInfo = ImportService::$importInfo;

            $currentImport->info = json_encode($importInfo);
            $currentImport->error = json_encode($e);
            $currentImport->save();

            return json_encode(['success' => false, 'error' => json_encode($e)]);
        }

        /*
        $issue_forms = $this->importService->getBufferData('issue_forms');
        $flavors = $this->importService->getBufferData('flavors');
        $categories = $this->importService->getBufferData('categories');
        $countries = $this->importService->getBufferData('countries');
        $products = $this->importService->getBufferData('products');
        $categories_products = $this->importService->getBufferData('categories_products');

        dd($brands);
        */
    }
}
