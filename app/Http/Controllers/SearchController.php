<?php

namespace App\Http\Controllers;

//use Re;
use App\Http\Requests\SearchRequest;
use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Product;
use App\Models\Viewed;
use App\Models\Brand;
use App\Models\Flavor;
use Validator;
use DB;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $dataSet = [];
        $delete_table = false;
        $tag_id = $request->tag_id ?: '';
        $searchString = $request->search_string ?: '';
        $searchBrand = $request->search_brand ?: '';
        $searchFlavors = $request->search_flavors ?: '';
        $searchCode = $request->search_code ?: '';
        $sort_by = $request->sort_by ?: 'price_asc';

        $validator = Validator::make(array('str' => $searchString, 'str_brand' => $searchBrand, 'str_flavor' => $searchFlavors, 'str_flavor' => $searchFlavors,'str_code' => $searchCode ), [
            'str' => 'string',
            'str_brand' => 'string',
            'str_flфavor' => 'string',
            'str_code' => 'string',
        ]);

        if ($validator->fails()) {
            return redirect('search/')
                ->withErrors($validator)
                ->withInput();
        }

        //DB::connection()->enableQueryLog();

        $productsScope = Product::query();
        if ($tag_id)
        {
            $tag = Tag::findOrFail($tag_id);
            $productsScope = $tag->products()->where('is_active', true);
        }

        if (!empty($searchString))
        {

            $productscheck = Product::byString($searchString)->active()->where('is_finished',false);
            $searchArrays =  explode(" ", $searchString);
            $products = array();
            $i = 0;
            if (!$productscheck->count()) {
                foreach ($searchArrays as $searchArray) {
                    $arrayFind = Product::select('id')->byString($searchArray)->active()->where('is_finished',false)->get();
                    foreach ($arrayFind as $find){
                        $products[] = $find->id;
                    }
                    $i++;
                }
                $products = array_count_values($products);
                if(count($products)) {

                    $productList = DB::insert(DB::raw("CREATE TEMPORARY TABLE tempproducts( `id_products` INT NOT NULL , `count` INT NOT NULL )"));
                    foreach ($products as $id_products => $count) {
                        $dataSet[] = [
                            'id_products' => $id_products,
                            'count' => $count,
                        ];
                    }
                    DB::table('tempproducts')->insert($dataSet);
                    $productsScope = $productsScope->join('tempproducts', 'products.id', '=', 'tempproducts.id_products')->orderBy('tempproducts.count', 'desc');
                    $delete_table = true;
                }
                else {
                    $productsScope = $productsScope->where('id','<', 0);
                }
            } else {
                $productsScope = $productsScope->byString($searchString)->active()->where('is_finished',false);
            }
        }


        if (!empty($searchBrand))
        {
            $brand = Brand::where('name',$searchBrand)->first();
            if($brand) {
                $productsScope = $productsScope->where('brand_id', $brand->id)->active();
            } else {
                $productsScope = $productsScope->where('brand_id','<', 0 )->active();
            }
        }
        if (!empty($searchFlavors))
        {
            $flavor = Flavor::where('name',$searchFlavors)->first();
            if($flavor) {
                $productsScope = $productsScope->where('flavor_id', $flavor->id)->active();
            } else {
                $productsScope = $productsScope->where('flavor_id','<', 0 )->active();
            }
        }
        if (!empty($searchCode))
        {
            $productsScope = $productsScope->where('article', $searchCode )->active();
        }
     /*   if (!empty($sort_by))
        {
            $productsScope = $productsScope->sortBy($sort_by)->active();
        }*/
        $products = $productsScope->with(
            'myFavorite',
            'translations'
        )->paginate(setting('site.paginationCount', 12));// get();

        //dd($products->count());
        $historyProducts = Viewed::where('session', session()->getId())->orderBy('updated_at', 'desc')->limit(10)->get();
        $bodyClass = 'body-search-page';
        if($delete_table) {
            $dropTable = DB::unprepared(DB::raw("DROP TEMPORARY TABLE tempproducts"));
        }
        return view('search.index')
            ->with(compact(
                'sort_by',
                'searchString',
                'searchBrand',
                'searchFlavors',
                'searchCode',
                'products',
                'historyProducts',
                'bodyClass'
            ));
    }


}
