<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SubscriptionService;
use App\Models\Subscription;
use App\Models\Product;
use Validator;

class SubscribeController extends Controller
{
    public function subscription($id,$isAjax = false, $email = null)
    {
        $email_array = ['email' => $email];
        $validator = Validator::make($email_array, [
            //how to validate only the test value?
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            echo "Email введен неверно";
        }

        if($isAjax && !is_null($email)) {
            $product_subscription = Product::find($id)->active()->finished();
            $check_duplicate = Subscription::where('product_id',$id)->where('email',$email)->count();
            if ($product_subscription && !$check_duplicate) {
                $add_subscription = new Subscription();
                $add_subscription->email = $email;
                $add_subscription->product_id = $id;
                $add_subscription->save();
            }
            else {
                echo 'Вы уже подписаны на этот товар';
            }
        }
    }
    public function check()
    {
        SubscriptionService::check();
    }
}
