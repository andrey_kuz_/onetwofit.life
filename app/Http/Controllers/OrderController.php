<?php

namespace App\Http\Controllers;

use App\Mail\OrderMail;
use App\Mail\OrderSuccessMail;
use App\Models\Manager;
use App\Models\Order;
use App\Models\OrderManager;
use App\Models\Payment;
use App\Models\PaymentType;
use App\Models\Product;
use App\Notifications\OrderSuccessNotification;
use App\Notifications\OrderAcceptedNotification;
use App\Services\CartService;
use App\Services\OrderService;
use App\Http\Requests\OneClickStoreRequest;
use App\Http\Requests\Checkout1StoreRequest;
use App\Http\Requests\Checkout2StoreRequest;
use App\Services\Payment\PaymentService;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\VerifyUser;
use Auth;
use App\User;
use App\Models\ShippingType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\PromocodeRequest;
use App\Models\Promocode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use TCG\Voyager\Models\Role;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Hash;


class OrderController extends Controller
{

    private $orderService;
    private $client;


    /**
     * OrderController constructor.
     * @param OrderService $OrderService
     */
    public function __construct(OrderService $OrderService)
    {

        $this->orderService = $OrderService;

        $this->client = new \RetailCrm\ApiClient(
            'https://onetwofit-life.retailcrm.ru',
            'UiMJnfj2G0z6XjHuW13JjolvJVu3uZ0i',
            \RetailCrm\ApiClient::V3
        );
    }


    /**
     * one click payment popup content
     *
     * @param Product $product
     * @return string
     */
    public function oneClick(Request $request, Product $product)
    {
        $not_ajax = true;
        if( $request->ajax() ) {
            $not_ajax = false;
        }
        $html = view('oneclick.content')
            ->with(compact(
                'product','not_ajax'
            ))->render();

        return $html;
    }

    /**
     * one click payment processing
     * @param OneClickStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function oneClickStore(OneClickStoreRequest $request)
    {
        if ($this->orderService->createOneClickOrder($request->name, $request->phone, $request->product_id)) {

            $order_id = $this->integrationOrderOneClick($request->product_id, $request->name, $request->phone);
            $product_array = Product::find($request->product_id)->toArray();
            $product_array['name'] = str_replace("'", "", $product_array['name']);
            $product = Product::find($request->product_id);
            if ($product->categories()->first() == null) {
                $category = 'Нет категории';
            } else {
                $category = $product->categories()->first()->name;
                $category = str_replace("'", "", $category);
            }
            $data = ['order_id' => $order_id,'product' => $product_array, 'category' => $category];
            return response()->json([
                'success' => [__('messages.oneclick_thanks')],
                'data' => json_encode($data),
            ]);
        }
    }


    /**
     * Show the cart entity
     *
     * @return \Illuminate\Http\Response
     */
    public function checkoutStep1(Request $request, $continueWithoutAuth = false)
    {
        if($request->comment) {
            $comment = trim($request->comment);
            session(['cart_comment' =>  $comment]);
        }
        if (!Auth::check() && !$continueWithoutAuth) {
            return redirect()->route('login', ['is_checkout' => 1]);
        }
        $this->cartService = new CartService();
        $cart = $this->cartService->cart;
        $cart->refreshCartInfo();
        $cartItems = $cart->items; //()->with('product.translations')->get();

        $promocodeStatusInfo = Promocode::statusInfo($cart->promocode);

        $bodyClass = 'body-order';

        $user = Auth::user();

        return view('order.step1')
            ->with(compact(
                'bodyClass',
                'promocodeStatusInfo',
                'cartItems',
                'cart',
                'user'
            ));
    }

    /**
     * Create new user if not exists
     * Show the cart entity
     *
     * @return \Illuminate\Http\Response
     */
    public function checkoutStep1Post(Checkout1StoreRequest $request)
    {
        if(!Auth::check()) {
            $data['password'] = str_random(12);
            $data['email'] = $request->shipping_email;
            $role_user = Role::where('name', 'user')->first();
            $user = User::create([
                'name' => $request->shipping_name,
                'lastname' => $request->shipping_lastname,
                'email' => $request->shipping_email,
                'password' => Hash::make(str_random(12)),
                'verified' => 1,
                'role_id' => $role_user->id
            ]);
            Mail::to($user->email)->send(new WelcomeMail($user));
            $cart_default =  $request->session()->get('cart_default');
            $cart = Cart::where('session',$cart_default)->first();
            $cart->user_id = $user->id;
            $cart->save();
            Auth::loginUsingId($user->id);
        }
        $order = $this->orderService->initOrder($request->all());
        return redirect()->route('checkout2', ['order' => $order->id]);
    }

    /**
     * Show the cart entity
     *
     * @return \Illuminate\Http\Response
     */
    public function checkoutStep2(Order $order)
    {
        $comment = '';
        if( Session::has('cart_comment') ) {
            $comment = Session::get('cart_comment');
        }
        $order_id = $order->id;

        $cart = Cart::current();
        $cart->refreshCartInfo();
        $cartItems = $cart->items; //()->with('product.translations')->get();

        $shippingType = ShippingType::where('api', 'nova_poshta')->firstOrFail();
        $data = $shippingType->apiStep(1, Input::all());
        $cities = $data['data'];

        $shippingMethods = $shippingType->shippingMethods()->get();
        $paymentTypes = $shippingType->paymentTypes()->active()->get();

        $promocodeStatusInfo = Promocode::statusInfo($cart->promocode);
        $bodyClass = 'body-order';
        $user = Auth::user();

        return view('order.step2')
            ->with(compact(
                'shippingType',
                'shippingMethods',
                'bodyClass',
                'paymentTypes',
                'order_id',
                'cities',
                'order',
                'user',
                'promocodeStatusInfo',
                'cartItems',
                'cart',
                'comment'
            ));
    }

    /**
     * process 2nd step of order, save data and redirect to payment
     *
     * @param Checkout2StoreRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function checkoutProcess(Checkout2StoreRequest $request)
    {
        $request->session()->forget('cart_comment');
        $order = $this->orderService->processOrder($request->order_id, $request->all());
        $paymentService = new PaymentService($order->paymentType);

        $html = $paymentService->init($order);

        $this->integrationOrderFromCart($order);

        if ($order->user) {
            $order->user->notify(new OrderAcceptedNotification($order));
        } else {
            Mail::to($order->shipping_email)->send(new OrderMail($order));
        }
        $script = true;
        if ($html) {
            return view('order.payment')
                ->with(compact(
                    'html','script'
                ));
        } else {
            return redirect()->route('order.thankyou', ['order' => $order->id,'script' => $script]);
        }
    }

    /**
     * thank you page when order finished
     *
     * @param Order $order
     * @return $this
     */
    public function thankyou(Order $order)
    {
        $paymentService = new PaymentService($order->paymentType);
        $successGateway = $paymentService->complete($order);

        // if successfull payment through gateway - send email of success payment
        if ($successGateway)
        {
            if ($order->user) {
                $order->user->notify(new OrderSuccessNotification($order));
            } else {
                Mail::to($order->shipping_email)->send(new OrderSuccessMail($order));
            }
        }

        $items = $order->orderItems()->get();
        $items = implode(",", $items->pluck('id')->toArray());

        $bodyClass = 'body-order';
        $script = true;
        return view('order.thankyou')->with(compact(
            'bodyClass',
            'order',
            'script',
            'items'
        ));
    }

    public function returnUrl(Order $order)
    {
        if ($order->payment->status === 'success') {
            return redirect(route('order.thankyou', ['order' => $order->id]));
        } else {
            return redirect(route('account.orders'));
        }
    }

    /**
     * action for catching payment callbacks from payment systems
     *
     * @param Request $request
     * @param $ptid
     */
    public function paymentCallback(Request $request, $ptid)
    {
        Log::info('payment:' . json_encode($request->all()));
        $paymentType = PaymentType::findOrFail($ptid);
        $paymentService = new PaymentService($paymentType);
        $paymentService->callback($request->all());

        $response = $request['data'];

        if ($response) {

            Log::info('request: Its data');

            $data = json_decode(base64_decode($response), true);

            if ($data['status'] === 'success') {

                Log::info('status: success');

                $this->updatePaymentStatus($data['order_id']);
            }
        }
    }

    /**
     * shipping methods area
     *
     * @param Request $request
     * @return $this
     */
    public function shipping(Request $request)
    {
        $shippingType = ShippingType::where('api', 'nova_poshta')->firstOrFail();
        $data = ($shippingType->apiStep($request->step, $request->all()))['data'];
        return view('order.options')
            ->with(compact(
                'data'
            ));
    }


    /**
     * add or change promocode
     *
     * @param PromocodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPromocode(PromocodeRequest $request)
    {
        $this->cartService = new CartService();
        $cart = $this->cartService->updateCartWithPromocode($request->promocode);
        $cartItems = $cart->items()->get();

        $promocodeStatusInfo = Promocode::statusInfo($request->promocode);

        $html = view('order.items2')
            ->with(compact(
                'promocodeStatusInfo',
                'cart',
                'cartItems'
            ))->render();

        return response()->json([
            'data' => [
                'cartInfo' => $cart->info,
                'html' => $html
            ]
        ]);
    }

    private function integrationOrderOneClick($product_id, $name, $phone)
    {
        $order = $this->orderService->order;

        $manager = $this->getManager();
        $customer=  $this->getCustomer($order->shipping_phone);
        $user = $this->orderService->order->user;

        $product = Product::find($product_id);

        $item = array(
            'quantity' => 1,
            'productName' => $product->name,
            'initialPrice' => $product->price,
            'productId' => $product->id
        );

        if (env('APP_ENV') === 'production') {
            $order_id = $order->id;
        } else {
            $order_id = $order->id . '_dev';
        }

        $orderCrm = array(
            'status' => 'call',
            'call' => true,
            'firstName' => $name,
            'phone' => $phone,
            'orderMethod' => 'one-click',
            'externalId' => $order_id,
            'items' => [$item]
        );

        if ($manager) {
            $orderCrm['managerId'] = $manager->manager_id;
        } else {
            $orderCrm['managerId'] = 8;
        }

        if($user) {
            $orderCrm['email'] = $user->email;
        }

        if($customer) {
            $orderCrm['customer']['id'] = $customer['id'];
        }

        try {

            $response = $this->client->request->ordersCreate($orderCrm);

        } catch (\RetailCrm\Exception\CurlException $e) {

            echo "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful() && 201 === $response->getStatusCode()) {

            if ($manager) {

                OrderManager::create([
                    'manager_id' => $manager->manager_id,
                    'order_id' => $order->id
                ]);
            }
            return $order->id;
        }

    }

    private function integrationOrderFromCart($order)
    {
        $customer =  $this->getCustomer($order->shipping_phone);

        $manager = $this->getManager();

        $userName = explode(" ", $order->shipping_name);
        $firstName = $userName[0];
        $lastName = $userName[1];

        $orderItems = $order->orderItems;

        $items = [];

        foreach ($orderItems as $item) {

            $product = Product::find($item->product_id);

            $productDiscount = $product->price - ($item->total_price / $item->quantity);
            $comboDiscountItem = $item->discount_by_qnt / $item->quantity;

            $totalDiscount = $productDiscount + $comboDiscountItem;

            $items[] = array(
                'initialPrice' => $product->price,
                'quantity' => $item->quantity,
                'productId' => $item->product_id,
                'productName' => $product->name,
                'discount' => $totalDiscount
            );
        }

        $delivery = array(
            'code' => 'nova-poshta',
            'integrationCode' => 'newpost',
            'address' => array(
                'city' => $order->shipping_city,
            )
        );

        if (env('APP_ENV') === 'production') {
            $order_id = $order->id;
        } else {
            $order_id = $order->id . '_dev';
        }

        $orderCrm = array(
            'externalId' => $order_id,
            'lastName' => $lastName,
            'firstName' => $firstName,
            'phone' => $order->shipping_phone,
            'email' => $order->shipping_email,
            'call' => true,
            'customerComment' => $order->note,
            'orderType' => 'eshop-individual',
            'orderMethod' => 'shopping-cart',
            'status' => 'in-work',
            'items' => $items,
            'delivery' => $delivery,
            'discountPercent' => $order->discount_promo
        );

        if ($manager) {
            $orderCrm['managerId'] = $manager->manager_id;
        } else {
            $orderCrm['managerId'] = 8;
        }

        if ($order->payment_type_id == 1) {
            $orderCrm['paymentStatus'] = 'not-paid';
            $orderCrm['paymentType'] = 'bank-transfer';
        } elseif ($order->payment_type_id == 2) {
            $orderCrm['paymentStatus'] = 'afterpayment';
            $orderCrm['paymentType'] = 'cash';
        } elseif ($order->payment_type_id == 3) {
            $orderCrm['paymentStatus'] = 'bank-card';
            $orderCrm['paymentType'] = 'bank-transfer';
        }

        if($customer) {
            $orderCrm['customer']['id'] = $customer['id'];
        }

        try {
            $response = $this->client->request->ordersCreate($orderCrm);
        } catch (\RetailCrm\Exception\CurlException $e) {

            echo "Connection error: " . $e->getMessage();
        }
        if ($response->isSuccessful() && 201 === $response->getStatusCode()) {

            if ($manager) {
                OrderManager::create([
                    'manager_id' => $manager->manager_id,
                    'order_id' => $order->id
                ]);
            }
        }
    }

    private function updatePaymentStatus($order_id)
    {
        $order = Order::find($order_id);

        if ($order) {

            $orderCrm['externalId'] = $order->id;
            $orderCrm['paymentStatus'] = 'paid';

            try {
                $this->client->request->ordersEdit($orderCrm);

            } catch (\RetailCrm\Exception\CurlException $e) {

                echo "Connection error: " . $e->getMessage();
            }
        }
    }

    private function getManager()
    {
        try{
            $this->editManagersData();

            $manager = DB::table('managers')
                ->where('managers.is_active', 1)
                ->leftJoin('orders_managers', 'orders_managers.manager_id', '=', 'managers.manager_id')
                ->select( DB::raw('managers.manager_id, MAX(orders_managers.created_at) as last_date') )
                ->groupBy('managers.manager_id')
                ->orderBy('last_date')
                ->first();

            return $manager;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    private function editManagersData()
    {
        try{
            $client = new \RetailCrm\ApiClient(
                'https://onetwofit-life.retailcrm.ru',
                'UiMJnfj2G0z6XjHuW13JjolvJVu3uZ0i',
                \RetailCrm\ApiClient::V4
            );
            $response = $client->request->usersList();
            $users = collect($response->users);
            $allManagers = $users->where('isManager', true)->values();
            Manager::whereNotIn('manager_id', $allManagers->pluck('id'))->update(['is_active' => 0]);

            foreach ($allManagers->all() as $manager) {

                Manager::updateOrCreate(['manager_id' => $manager['id']], ['is_active' => $manager['active']]);
            }

        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    private function getCustomer($phone)
    {
        $order_phone = $this->validatePhone($phone);

        if($order_phone) {

            try {

                $response = $this->client->request->customersList();

            } catch (\RetailCrm\Exception\CurlException $e) {

                echo "Connection error: " . $e->getMessage();
            }

            if ($response->isSuccessful()) {

                $customers = $response->customers;

                foreach ($customers as $customer) {

                    $phones = $customer['phones'];

                    foreach ($phones as $phone) {

                        $customer_phone = $this->validatePhone($phone['number']);

                        if($customer_phone == $order_phone){
                            return $customer;
                        }
                    }
                }

            }
        }
        return false;
    }

    private function validatePhone($phone)
    {
        $validate_phone = preg_replace("/[^0-9]/", '', $phone);
        $count_numbers = strlen($validate_phone);

        if($count_numbers == 12) {

            return $validate_phone;

        } else{
            return false;
        }
    }

}
