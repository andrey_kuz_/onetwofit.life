<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use Illuminate\Http\Request;

class AjaxController extends VoyagerBaseController
{
    public function updateSlug(Request $request)
    {
        $slug = str_slug($request->name);
        return $slug;
    }
}