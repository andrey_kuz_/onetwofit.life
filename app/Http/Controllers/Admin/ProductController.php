<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\ContentTypes\File;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ProductController  extends VoyagerBaseController
{
    //
    public function editPrices(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        return Voyager::view('vendor.voyager.prices.edit-add', compact('product'));
        //return $this->edit($request, $id);
    }

    public function updateSlug($id = null)
    {
        if ($id) {

            $product = Product::find($id);
            $i = 1;

            $product_slug = $product->slug;
            $new_slug = str_slug($product->name);

            $str = str_replace($new_slug, '', $product_slug);
            $str = preg_replace('/^-[0-9]+$/', '', $str);

            if (!$product_slug || $str !== '') {
                if(Product::where('slug',$new_slug)->where('id','<>',$product->id)->count() == 0) {
                    $product->slug = $new_slug;
                } else {
                    while (Product::where('slug',$new_slug.'-'.$i)->where('id','<>',$product->id)->count()) {
                        $i++;
                    }
                    $product->slug = $new_slug.'-'.$i;
                }
            }
            $product->save();

        } else {
            $products =  Product::where('slug',null)->get();

            foreach ($products as $product) {
                $i = 1;
        	    $new_slug = str_slug($product->name);

                if(Product::where('slug',$new_slug)->where('id','<>',$product->id)->count() == 0) {
                    $product->slug = $new_slug;
                } else {
                    while (Product::where('slug',$new_slug.'-'.$i)->count()) {
                        $i++;
                    }
                    $product->slug = $new_slug.'-'.$i;
                }
                $product->save();
            }
        }
    }

    public function updatePrices(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $this->validate($request, [
            'discount2' => 'nullable|numeric|max:100',
            'discount3' => 'nullable|numeric|max:100',
            'discount4' => 'nullable|numeric|max:100',
            'discount5' => 'nullable|numeric|max:100',
            'discount6' => 'nullable|numeric|max:100',
            'discount7' => 'nullable|numeric|max:100',
            'discount8' => 'nullable|numeric|max:100',
            'discount9' => 'nullable|numeric|max:100',
            'discount10' => 'nullable|numeric|max:100'
        ]);

        $product->discount2 = $request->discount2;
        $product->discount3 = $request->discount3;
        $product->discount4 = $request->discount4;
        $product->discount5 = $request->discount5;
        $product->discount6 = $request->discount6;
        $product->discount7 = $request->discount7;
        $product->discount8 = $request->discount8;
        $product->discount9 = $request->discount9;
        $product->discount10 = $request->discount10;

        $product->save();

        return redirect()
            ->route("voyager.products.index");
    }

    public function saveImages(Request $request, $id)
    {
        $product = Product::find($id);
        $slug = $request->type_slug;
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $rows = $dataType->editRows;
        foreach ($rows as $row) {
            if($row->type == 'multiple_images'){
                $options = $row->details;
                $content = $this->getContentBasedOnType($request, $slug, $row, $options);

                if ($product->image == null) {
                    $product->image = $content;
                } else {
                    $product_images = json_decode($product->image);
                    if (!empty($product_images)) {
                        $ex_files = json_decode($product->image, true);
                        if (!is_null($ex_files)) {
                            $content = json_encode(array_merge($ex_files, json_decode($content)));
                            if (is_null($content)) {
                                $content = $product->image;
                            }
                            $product->image = $content;
                        }
                    } else {
                        $product->image = $content;
                    }
                }
            }
        }
        $product->save();
        $images = json_decode($product->image, true);
        $last_image = array_pop($images);
        $url = Storage::disk(config('voyager.storage.disk'))->url($last_image);
        return response()->json(['image' => $last_image, 'product_id' => $id, 'src' => $url]);
    }

    public function sortImages(Request $request)
    {
        $product = Product::find($request->id);
        $new_sort = json_encode($request->images);
        $product->image = $new_sort;
        $product->save();

    }
}
