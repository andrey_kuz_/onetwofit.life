<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;

class CategoryController  extends VoyagerBaseController
{
    public function updateSlug($id = null)
    {
        if ($id) {
            $category = Category::find($id);
            $i = 1;

            $category_slug = $category->slug;
            $new_slug = str_slug($category->name);

            $str = str_replace($new_slug, '', $category_slug);
            $str = preg_replace('/^-[0-9]+$/', '', $str);

            if (!$category_slug || $str !== '') {
                if(Category::where('slug', $new_slug)->where('id','<>',$category->id)->count() == 0) {
                    $category->slug = $new_slug;
                } else {
                    while (Category::where('slug', $new_slug.'-'.$i)->where('id','<>',$category->id)->count()) {
                        $i++;
                    }
                    $category->slug = $new_slug.'-'.$i;
                }
            }
            $category->save();
        } else {
            $categories =  Category::where('slug',null)->get();

            foreach ($categories as $category) {
                $i = 1;
                $new_slug = str_slug($category->name);

                if(Category::where('slug', $new_slug)->where('id', '<>', $category->id)->count() == 0) {
                    $category->slug = $new_slug;
                } else {
                    while (Category::where('slug', $new_slug.'-'.$i)->count()) {
                        $i++;
                    }
                    $category->slug = $new_slug.'-'.$i;
                }
                $category->save();
            }
        }

    }
}
