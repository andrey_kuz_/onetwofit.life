<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends VoyagerBaseController
{
    public function updateSlug($id = null)
    {
        if ($id) {
            $brand = Brand::find($id);
            $i = 1;

            $brand_slug = $brand->slug;
            $new_slug = str_slug($brand->name);

            $str = str_replace($new_slug, '', $brand_slug);
            $str = preg_replace('/^-[0-9]+$/', '', $str);

            if (!$brand_slug || $str !== '') {
                if(Brand::where('slug', $new_slug)->where('id','<>',$brand->id)->count() == 0) {
                    $brand->slug = $new_slug;
                } else {
                    while (Brand::where('slug', $new_slug.'-'.$i)->where('id','<>',$brand->id)->count()) {
                        $i++;
                    }
                    $brand->slug = $new_slug.'-'.$i;
                }
            }
            $brand->save();
        }else {
            $brands = Brand::where('slug', null)->get();

            foreach ($brands as $brand) {
                $i = 1;
                $new_slug = str_slug($brand->name);

                if(Brand::where('slug', $new_slug)->where('id', '<>', $brand->id)->count() == 0) {
                    $brand->slug = $new_slug;
                } else {
                    while (Brand::where('slug', $new_slug.'-'.$i)->count()) {
                        $i++;
                    }
                    $brand->slug = $new_slug.'-'.$i;
                }
                $brand->save();
            }
        }
    }
}
