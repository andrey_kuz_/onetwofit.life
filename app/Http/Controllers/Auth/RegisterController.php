<?php

namespace App\Http\Controllers\Auth;

use App\Models\Cart;
use App\Notifications\WelcomeNotification;
use App\User;
use App\VerifyUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Auth;
use TCG\Voyager\Models\Role;
use Socialite;

use Illuminate\Http\Request;
use App\Notifications\VerifyEmailNotification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirectTo = url()->previous();
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {

        $request = app('request');
        session(['session_cart' =>  $request->session()->get('cart_default')]);
        return view('auth.register');
    }
    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        \Log::info('oauth redirect - ' . $provider . ' - ' . \Carbon\Carbon::now());

        try {
            $user = Socialite::driver($provider)->user();

            $authUser = $this->findOrCreateUser($user, $provider);
            if ($authUser == 'not_email') {
                return redirect()->route('login', $authUser);
            }
            $session_cart_id = \Request::session()->get('cart_default');
            Cart::where('session', $session_cart_id)->update(['user_id' => $user->id]);

            Auth::login($authUser);

            \Log::info('oauth redirect - ' . json_encode($authUser));

            return redirect()->intended($this->redirectPath());

        } catch (\Exception $e) {
            return redirect()->route('login');
        }


    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }


    /**
     * action for verification user email by token value
     *
     * @param  string $token
     * @return Response
     */
    public function verifyUser($token, $session_cart_id)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if (!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $verifyUser->user->notify(new WelcomeNotification($user));
                //Mail::to($user->email)->send(new WelcomeMail($user));
                $status = __('messages.email_verified');
            } else {
                $status = __('messages.email_verified_already');
            }
        } else {
            return redirect('/login')->with('warning', __('messages.email_not_ident'));
        }
        Cart::where('session', $session_cart_id)->update(['user_id' => $user->id]);

        return redirect('/login')->with('status', $status);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $role_user = Role::where('name', 'user')->first();

        $user = User::create([
            'email' => $data['email'],
            'role_id' => $role_user->id,
            'password' => bcrypt($data['password']),
        ]);

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        $user->notify(new VerifyEmailNotification($user));

        return $user;
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $role_user = Role::where('name', 'user')->first();

        $authUser = User::where('provider_id', $user->id)->orWhere('email', $user->email)->first();

        if ($authUser) {
            return $authUser;
        }

        if (!$user->email){

            return 'not_email';

        } else {
            return User::create([
                'name' => $user->name,
                'email' => $user->email,
                'verified' => 1,
                'role_id' => $role_user->id,
                'provider' => $provider,
                'provider_id' => $user->id
            ]);
        }
    }

    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('status', __('messages.code_sent'));
    }

    public function checkEmail(Request $request){
        $email = User::where('email',$request->email)->count();
        return $email;
    }

}
