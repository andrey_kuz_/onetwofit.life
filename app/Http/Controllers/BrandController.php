<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Services\FilterService;
use App\Models\Viewed;

class BrandController extends Controller
{
    public function index(Request $request, $slug, $filters = null)
    {
        $brand = Brand::where('slug', $slug)->firstOrFail();
        $characteristics = '';
        // get all products from currend and children categories
        $productsScope = $products = $brand->products();

        $filterService = new FilterService();
        $productsScope = $filterService->filter($productsScope, $filters);

        if(isset($filters['price_ranges'])) {
            $filters['price_ranges'] = explode(",", $filters['price_ranges']);
        } else {
            $filters['price_ranges'] = array();
        }

        $products = $productsScope->with(
            'myFavorite',
            'translations'
        )->paginate(setting('site.paginationCount', 12));

        if ($request->ajax()) {
            $html = view('categories.content')
                ->with(compact(
                    'products'
                ))->render();

            return response()->json([
                'success' => 1,
                'mess' => '',
                'data' => [
                    'html'=> $html
                ]
            ]);
        }

        $sectionRoute = 'brand.index';
        $ajaxContentUrl = route('brand.ajaxcontent', ['id' => $brand->id]);
        $section = $brand;
        $historyProducts = Viewed::where('session', session()->getId())->orderBy('updated_at', 'desc')->limit(10)->get();
        $sub_categories_exist = false;
        $bodyClass = 'body-categories-page';

        return view('brand.index')
            ->with(compact(
                'bodyClass',
                'section',
                'sectionRoute',
                'ajaxContentUrl',
                'sub_categories_exist',
                'products',
                'historyProducts',
                'filters',
                'characteristics'
            ));
    }

    function index_old($id)
    {
        $brand = Brand::findOrFail($id);
        return redirect()->route('brand.index', ['slug' => $brand->slug ])->setStatusCode(301);
    }

    public function ajaxContent(Request $request, $id)
    {
        $filters = $request->all();
        $brand = Brand::findOrFail($id);

        // get all products from currend and children categories
        $productsScope = $products = $brand->products();

        $filterService = new FilterService();
        $productsScope = $filterService->filter($productsScope, $filters);

        $products = $productsScope->with(
            'myFavorite',
            'translations'
        )->paginate(setting('site.paginationCount', 12));

        if ($request->ajax()) {
            $html = view('categories.content')
                ->with(compact(
                    'products'
                ))->render();

            return response()->json([
                'success' => 1,
                'mess' => '',
                'data' => [
                    'html'=> $html
                ]
            ]);
        }
    }

    public function filters(Request $request,$slug = null, $filters = null)
    {

        $request_array = array();
        $filters_array = array();
        $update_request_array = array();
        $updatelink = false;
        if(strripos($slug,'=')) {
            $filters = $slug;
            $slug = null;
        }

        $filters_array = explode(';',$filters);
        foreach ($filters_array as $filter_item)
        {
            $key = substr($filter_item, 0, stripos($filter_item,'='));
            $value = substr($filter_item, stripos($filter_item,'=')+1);
            $request_array[$key] = $value;

        }
        $beforeSort = array_keys($request_array);
        ksort($request_array);
        $afterSort = array_keys($request_array);
        for($i = 0; $i < count($beforeSort); $i ++) {
            if($afterSort[$i] != $beforeSort[$i]) {
                $updatelink = true;
            }
        }

        if ($updatelink) {
            foreach ($request_array as $key => $val) {
                $update_request_array[] = $key.'='.$val;
            }
            $filters = implode(";", $update_request_array);
            if($request->page) {
                $filters = implode(";", $update_request_array).'?page='.$request->page;
            }
            return redirect()->route('brand.filters', array('slug' => $slug,'filters' => $filters));
        }
        if (!isset($request_array['price_from'])) {
            $request_array['price_from'] ='';
        }
        if (!isset($request_array['price_to'])) {
            $request_array['price_to'] ='';
        }
        return $this->index($request,$slug,$request_array);

    }

    public function getList()
    {
        $brands = Brand::orderBy('name')->get();

        $en_list = [];
        $ru_list = [];
        $num_list = [];

        foreach ($brands as $brand){

            if (preg_match('/^[0-9]+$/',$brand->name[0])){

                $num_list['#'][] = $brand;

            } elseif (preg_match('/^[A-z]+$|i/',$brand->name[0])){

                if (!array_key_exists($brand->name{0}, $en_list)) {

                    $en_list[$brand->name[0]][] = $brand;

                } else {

                    $en_list[$brand->name[0]][] = $brand;

                }
            } elseif (preg_match('/^[а-я]+$/',$brand->name[0])){

                $key = substr($brand->name, 0, 2);

                if (!array_key_exists($key, $ru_list)) {

                    $ru_list[$key][] = $brand;

                } else {

                    $ru_list[$key][] = $brand;

                }

            }

        }

        return view('search.index-brand', compact('en_list', 'ru_list', 'num_list'));
    }

}
