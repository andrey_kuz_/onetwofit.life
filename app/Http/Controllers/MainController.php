<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class MainController extends Controller
{
    function index(Request $request)
    {
        $count_main = 0;
        $types = array("is_new", "is_hit", "is_bestseller");
        $type = $request->type;
        if (in_array($type, $types)) {
            $count_main =  Product::where($type,true)->active()->main()->notFinished()->count();
        }

        return $count_main;
    }
}
