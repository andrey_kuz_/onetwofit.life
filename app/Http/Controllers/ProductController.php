<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Requests\AddReviewRequest;
use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Characteristic;
use App\Models\CharacteristicProduct;
use App\Models\Review;
use App\Models\Product;
use App\Services\StatService;
use App\Models\Subscription;
//use http\Env\Request;
use ChristianKuri\LaravelFavorite\Models\Favorite;
use Illuminate\Support\Facades\Auth;
use App\Models\Brand;
use App\Models\Viewed;
use App\Services\FilterService;
use Illuminate\Http\Request;
use App\Services\BreadcrumbService;

class ProductController extends Controller
{
    /**
     * product page action
     *
     * @param $id
     * @return $this
     */
    public function index($id,$slug)
    {
        $buyTogethers_id = StatService::buyTogether($id)->pluck('product_id');
        $buyTogethers = Product::whereIN('id',$buyTogethers_id)->get();
        $product = Product::active()->with('translations', 'reviews')->findOrFail($id);
        $characteristicsName = Array();
        $tags = $product->tags()->get()->translate();
        $characteristics = CharacteristicProduct::getCharacteristics($id)->get();
        foreach ($characteristics as $characteristic) {
            $key = $characteristic->characteristic_id;
            $characteristicsName[$key] = Characteristic::where('id',$key)->first();
        }
        $comboOffer = $product->getRandomComboOffer();
        $specialPricesInfo = $product->getAvailableSpecialPricesInfo();
        $withProducts = [];
        $analogProducts = $product->analogProducts(setting('site.analogProductsCount'));
        $params = $product->getParameters();

        $product->addToViewed(null, null, setting('site.viewedProductsCount') ?: 10);
        $ratingCountByStars = json_decode($product->rating_json);
        $reviews = $product->reviewsApproved()->paginate(setting('site.paginationReviewsCount', 4));

        $bodyClass = 'body-product';
        $sectionRoute = 'category.index';
        $historyProducts = Viewed::recent()->with('product')->get();
        $breadcrumb_array = array();
        $section = $product->getFirstCategory();
        if($section != null) {
            if ($section->parent) {
                $breadcrumb_array[] = $section;
                $breadcrumb_array = BreadcrumbService::generate($breadcrumb_array, $section);
                $breadcrumb_array = array_reverse($breadcrumb_array);
            }
        }

        return view('products.index')
            ->with(compact(
                'product',
                'reviews',
                'ratingCountByStars',
                'bodyClass',
                'params',
                'specialPricesInfo',
                'analogProducts',
                'buyTogethers',
                'historyProducts',
                'withProducts',
                'comboOffer',
                'tags',
                'characteristics',
                'characteristicsName',
                'breadcrumb_array',
                'sectionRoute'
            ));
    }
    public function indexOld($id =null)
    {
        $product = Product::findOrFail($id);
        return redirect()->route('product.index', ['id' => $id , 'slug' => $product->slug ])->setStatusCode(301);

    }

    /**
     * review popup content
     *
     * @param Product $product
     * @return string
     */
    public function reviewBlock(Product $product)
    {
        $ratingCountByStars = json_decode($product->rating_json);

        $html = view('reviews.main')
            ->with(compact(
                'ratingCountByStars',
                'product'
            ))->render();

        return $html;
    }

    /**
     * review popup content
     *
     * @param Product $product
     * @return string
     */
    public function review(Product $product)
    {
//        if (!Auth::check())
//        {
//            return redirect()->route('login');
//        }
        $html = view('reviews.modal.content')
            ->with(compact(
                'product'
            ))->render();

        return $html;
    }

    /**
     * review storage processing
     * @param AddReviewRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reviewStore(AddReviewRequest $request)
    {
        $review = new Review();

        $product = Product::find($request->product_id);

        if ($product) {
            if ($review->storeReviewForProduct($request->product_id, $request->comment, $request->rating, $request->name)) {
                return response()->json([
                    'success' => 1,
                    'html' => view('reviews.modal.thankyou')->render(),
                    //'block' => $this->reviewBlock($product)
                ]);
            } else {
                return response()->json([
                    'success' => 0,
                    'html' => __('messages.review_error')
                ]);
            }
        }
    }

    public function specialProducts(Request $request, $type=null, $filters=null)
    {
        $characteristics = '';
        if ($type == null){
            return redirect(route('special', $type='all') );
        }
        $sub_categories_exist = 'special';
        $productsScope = Product::query();
        $ajaxContentUrl = route('special.ajaxcontent');

        $filterService = new FilterService();
        if ($type == 'bestseller') {
            $specialProducts = $productsScope->bestseller()->with('myFavorite', 'translations');
            $section = trans(('messages.is_bestseller'));
        } elseif($type == 'hit') {
            $specialProducts = $productsScope->hit()->with('myFavorite', 'translations');
            $section = trans(('messages.is_hit'));
        } elseif($type == 'new') {
            $specialProducts = $productsScope->new()->with('myFavorite', 'translations');
            $section = trans(('messages.is_new'));
        } elseif($type == 'discount') {
            $specialProducts = $productsScope->discounted()->with('myFavorite', 'translations');
            $section = trans(('messages.is_super'));
        } else{
            $specialProducts = $productsScope->where(function ($query){
                $query->where('is_bestseller', true);
                $query->orWhere('is_hit', true);
                $query->orWhere('is_new', true);
                $query->orWhere('discount', '>', 0);
                $query->with('myFavorite', 'translations');
            });
            $section = trans('messages.Специальное предложение');
        }
        $brands = Brand::assignedToProducts($productsScope->pluck('id')->toArray())->get();
        $product_ids = $productsScope->pluck('id')->toArray();
        $categories_id = CategoryProduct::whereIN('product_id',$product_ids)->select('category_id')->distinct()->pluck('category_id');
        $categories = Category::whereIN('id',$categories_id)->get();
        $productsScope = $filterService->filter($productsScope, $filters);

        if(isset($filters['brands'])) {
            $filters['brands'] = explode(",", $filters['brands']);
        } else {
            $filters['brands'] = array();
        }
        if(isset($filters['price_ranges'])) {
            $filters['price_ranges'] = explode(",", $filters['price_ranges']);
        } else {
            $filters['price_ranges'] = array();
        }


        $specialProducts = $productsScope->paginate(setting('site.paginationCount', 6));

        if ($request->ajax()) {
            $html = view('special.content')
                ->with(compact(
                    'specialProducts'
                ))->render();

            return response()->json([
                'success' => 1,
                'mess' => '',
                'data' => [
                    'html'=> $html
                ]
            ]);
        }

        $sectionRoute = 'special';
        $historyProducts = Viewed::recent()->with('product')->get();
        $bodyClass = 'body-categories-page';


        return view('special.index')
            ->with(compact(
                'bodyClass',
                'section',
                'sectionRoute',
                'sub_categories_exist',
                'categories',
                'ajaxContentUrl',
                'brands',
                'specialProducts',
                'historyProducts',
                'filters',
                'type',
                'characteristics'
            ));
    }

    public function filters(Request $request, $type=null, $filters = null)
    {
        $request_array = array();
        $filters_array = array();
        $update_request_array = array();
        $updatelink = false;

        $filters_array = explode(';',$filters);
        foreach ($filters_array as $filter_item)
        {
            $key = substr($filter_item, 0, stripos($filter_item,'='));
            $value = substr($filter_item, stripos($filter_item,'=')+1);
            $request_array[$key] = $value;

        }
        $beforeSort = array_keys($request_array);
        ksort($request_array);
        $afterSort = array_keys($request_array);
        for($i = 0; $i < count($beforeSort); $i ++) {
            if($afterSort[$i] != $beforeSort[$i]) {
                $updatelink = true;
            }
        }

        if ($updatelink) {
            foreach ($request_array as $key => $val) {
                $update_request_array[] = $key.'='.$val;
            }
            $filters = implode(";", $update_request_array);
            if($request->page) {
                $filters = implode(";", $update_request_array).'?page='.$request->page;
            }
            return redirect()->route('special.filters', array('filters' => $filters));
        }
        if (!isset($request_array['price_from'])) {
            $request_array['price_from'] ='';
        }
        if (!isset($request_array['price_to'])) {
            $request_array['price_to'] ='';
        }

        return $this->specialProducts($request, $type, $request_array);
    }


    public function ajaxContent(Request $request, $type=null)
    {
        $filters = $request->all();

        $productsScope = Product::query();

        $filterService = new FilterService();
        $productsScope = $filterService->filter($productsScope, $filters);

        if ($type == 'bestseller') {
            $specialProducts = $productsScope->bestseller()->with('myFavorite', 'translations')
                ->paginate(setting('site.paginationCount', 6)
                );
        } elseif($type == 'hit') {
            $specialProducts = $productsScope->hit()->with('myFavorite', 'translations')
                ->paginate(setting('site.paginationCount', 6)
                );
        } elseif($type == 'new') {
            $specialProducts = $productsScope->new()->with('myFavorite', 'translations')
                ->paginate(setting('site.paginationCount', 6)
                );
        } elseif($type == 'discount') {
            $specialProducts = $productsScope->discounted()->with('myFavorite', 'translations')
                ->paginate(setting('site.paginationCount', 6)
                );
        } else{
            $specialProducts = $productsScope->where(function ($query){
                $query->where('is_bestseller', true);
                $query->orWhere('is_hit', true);
                $query->orWhere('is_new', true);
                $query->orWhere('discount', '>', 0);
                $query->with('myFavorite', 'translations');
            })->paginate(setting('site.paginationCount', 6));
        }

        if ($request->ajax()) {
            $html = view('special.content')
                ->with(compact(
                    'specialProducts'
                ))->render();

            return response()->json([
                'success' => 1,
                'mess' => '',
                'data' => [
                    'html'=> $html
                ]
            ]);
        }
    }

}
