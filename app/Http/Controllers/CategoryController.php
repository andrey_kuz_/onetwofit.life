<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\CharacteristicProduct;
use App\Models\Product;
use App\Models\Viewed;
use Illuminate\Http\Request;
use App\Services\FilterService;
use DB;
use Illuminate\Support\Arr;
use phpDocumentor\Reflection\Types\Array_;
use App\Services\BreadcrumbService;


class CategoryController extends Controller
{
    public static $breadcrumb_array = array();
    /**
     * category page
     *
     * @param $slug category slug
     * @return $this
     */
    public function index(Request $request, $slug = null, $filters = null)
    {
        $characteristics_values = array();
        $characteristics_interval = array();
        $characteristics_values_interval = array();
        $characteristics = '';
        $category = false;
        $sub_categories ='';
        $sub_categories_exist = true;
        if (isset($slug) && (!empty($slug))) {

            $category = Category::where('slug', $slug)->firstOrFail();
            $sub_categories = $category->subcategories;
            $characteristics = $category->characteristic;

            // get all products from current and children categories
            $productsScope = $category->subProducts();
            $products_ids = $productsScope->pluck('id');
            foreach ($characteristics as $characteristic)
            {
                $characteristic_info = CharacteristicProduct::where('characteristic_id',$characteristic->id)->whereIN('product_id',$products_ids)->distinct('value')->pluck('value');
                if (count($characteristic_info) < 24 ) {
                    $characteristics_values[$characteristic->id] = $characteristic_info;
                }
                else {
                    $characteristics_interval[$characteristic->id] = $characteristic_info;
                }
            }
            //$brands = $category->subBrands()->get();
            $ajaxContentUrl = route('category.ajaxcontent', ['id' => $category->id]);
            $section = $category;

        } else {
            $productsScope = Product::query()->active();
            $sub_categories_exist = false;
            $ajaxContentUrl = route('category.ajaxcontent');
            $section = null;
        }
        if (count($characteristics_interval)) {

            foreach ($characteristics_interval as $characteristic_id => $array) {
                $max = max($array->toArray());
                $min = min($array->toArray());
                $interval = ceil((floatval($max) - $min)/8);
                $value = 0;
                while ($value < $max) {
                    $characteristics_values_interval[$characteristic_id][] = $value.'-'.($value + $interval);
                    $value+= $interval;
                }
            }
        }
        $filterService = new FilterService();
        $brands = Brand::assignedToProducts($productsScope->pluck('id')->toArray())->get();
        $productsScope = $filterService->filter($productsScope, $filters);
        if (isset($filters['brands'])) {
            $filters['brands'] = explode(",", $filters['brands']);
        } else {
            $filters['brands'] = array();
        }
        if (isset($filters['category'])) {
            $filters['category'] = explode(",", $filters['category']);
        } else {
            $filters['category'] = array();
        }
        if (isset($filters['price_ranges'])) {
            $filters['price_ranges'] = explode(",", $filters['price_ranges']);
        } else {
            $filters['price_ranges'] = array();
        }
        if (isset($filters['character'])) {
            $filters['character'] = explode(",", $filters['character']);
        } else {
            $filters['character'] = array();
        }

        $products = $productsScope->with(
            'myFavorite',
            'translations'
        )->paginate(setting('site.paginationCount', 6));




        if ($request->ajax()) {
            $html = view('categories.content')
                ->with(compact(
                    'products'
                ))->render();

            return response()->json([
                'success' => 1,
                'mess' => '',
                'data' => [
                    'html' => $html
                ]
            ]);
        }

        $sectionRoute = 'category.index';
        $historyProducts = Viewed::recent()->with('product')->get();
        $bodyClass = 'body-categories-page';
        $breadcrumb_array = array();
        $breadcrumb_array[] = $section;
        if($section != null) {
            if ($section->parent != null) {
                $breadcrumb_array = BreadcrumbService::generate($breadcrumb_array, $section);
                $breadcrumb_array = array_reverse($breadcrumb_array);
            }
        }
        return view('categories.index')
            ->with(compact(
                'bodyClass',
                'section',
                'sectionRoute',
                'ajaxContentUrl',
                'sub_categories_exist',
                'sub_categories',
                'brands',
                'category',
                'products',
                'historyProducts',
                'filters',
                'characteristics',
                'characteristics_values',
                'characteristics_values_interval',
                'breadcrumb_array'
            ));
    }


    public function indexOld($id)
    {
        $category = Category::findOrFail($id);
        return redirect()->route('category.index', ['slug' => $category->slug])->setStatusCode(301);
    }

    /**
     * @param null $slug
     * @param null $brands
     */
    public function filters(Request $request, $slug = null, $filters = null)
    {

        $request_array = array();
        $filters_array = array();
        $update_request_array = array();
        $updatelink = false;
        if (strripos($slug, '=')) {
            $filters = $slug;
            $slug = null;
        }

        $filters_array = explode(';', $filters);
        foreach ($filters_array as $filter_item) {
            $key = substr($filter_item, 0, stripos($filter_item, '='));
            $value = substr($filter_item, stripos($filter_item, '=') + 1);
            $request_array[$key] = $value;

        }
        $beforeSort = array_keys($request_array);
        ksort($request_array);
        $afterSort = array_keys($request_array);
        for ($i = 0; $i < count($beforeSort); $i++) {
            if ($afterSort[$i] != $beforeSort[$i]) {
                $updatelink = true;
            }
        }

        if ($updatelink) {
            foreach ($request_array as $key => $val) {
                $update_request_array[] = $key . '=' . $val;
            }
            $filters = implode(";", $update_request_array);
            if ($request->page) {
                $filters = implode(";", $update_request_array) . '?page=' . $request->page;
            }
            return redirect()->route('category.filters', array('slug' => $slug, 'filters' => $filters));
        }
        if (!isset($request_array['price_from'])) {
            $request_array['price_from'] = '';
        }
        if (!isset($request_array['price_to'])) {
            $request_array['price_to'] = '';
        }
        return $this->index($request, $slug, $request_array);

    }


    /**
     * get content of filtered products
     *
     * @param Request $request
     * @param $id  category id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxContent(Request $request, $id = null)
    {
        $filters = $request->all();

        if (isset($id) && (!empty($id))) {
            $category = Category::findOrFail($id);

            // get all products from current and children categories
            $productsScope = $category->subProducts();
        } else {
            $productsScope = Product::query();
        }


        $filterService = new FilterService();
        $productsScope = $filterService->filter($productsScope, $filters);

        //$brandsIds = Brand::assignedToProducts($productsScope->pluck('id')->toArray())->pluck('id')->toArray();


        $products = $productsScope->with(
            'myFavorite',
            'translations'
        )->paginate(setting('site.paginationCount', 6));

        if ($request->ajax()) {
            $html = view('categories.content')
                ->with(compact(
                    'products'
                ))->render();

            return response()->json([
                'success' => 1,
                'mess' => '',
                'data' => [
                    'html' => $html
                    //'brandsIds'=> $brandsIds
                ]
            ]);
        }
    }
}
