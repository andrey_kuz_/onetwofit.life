<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;
use Auth;
use App\Models\Viewed;
use App\Services\FilterService;
use App\Models\Brand;
use Illuminate\Support\Facades\Log;

class TagController extends Controller
{
    /**
     * tag page
     *
     * @param $id tag id
     * @return $this
     */
    public function index(Request $request, $slug, $filters = null)
    {
        $tag = Tag::where('slug', $slug)->firstOrFail();
        $characteristics = '';
        // get all products from currend and children categories
        $productsScope = $products = $tag->products();

        $brands = Brand::assignedToProducts($productsScope->pluck('id')->toArray())->get();
        $filterService = new FilterService();
        $productsScope = $filterService->filter($productsScope, $filters);

        if(isset($filters['brands'])) {
            $filters['brands'] = explode(",", $filters['brands']);
        } else {
            $filters['brands'] = array();
        }
        if(isset($filters['price_ranges'])) {
            $filters['price_ranges'] = explode(",", $filters['price_ranges']);
        } else {
            $filters['price_ranges'] = array();
        }

        $products = $productsScope->with(
            'myFavorite',
            'translations'
        )->paginate(setting('site.paginationCount', 12));

        if ($request->ajax()) {
            $html = view('categories.content')
                ->with(compact(
                    'products'
                ))->render();

            return response()->json([
                'success' => 1,
                'mess' => '',
                'data' => [
                    'html'=> $html
                ]
            ]);
        }

        $sectionRoute = 'tag.index';
        $ajaxContentUrl = route('tag.ajaxcontent', ['id' => $tag->id]);
        $section = $tag;
        $sub_categories_exist = false;

        $historyProducts = Viewed::where('session', session()->getId())->orderBy('updated_at', 'desc')->limit(10)->get();

        $bodyClass = 'body-categories-page';

        return view('tag.index')
            ->with(compact(
                'bodyClass',
                'brands',
                'section',
                'sectionRoute',
                'sub_categories_exist',
                'ajaxContentUrl',
                'products',
                'characteristics',
                'historyProducts',
                'filters'
            ));
    }


    /**
     * get content of filtered products
     *
     * @param Request $request
     * @param $id  category id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxContent(Request $request, $id)
    {
        $filters = $request->all();
        $tag = Tag::findOrFail($id);

        // get all products from currend and children categories
        $productsScope = $products = $tag->products();

        $filterService = new FilterService();
        $productsScope = $filterService->filter($productsScope, $filters);

        $products = $productsScope->with(
            'myFavorite',
            'translations'
        )->paginate(setting('site.paginationCount', 12));

        if ($request->ajax()) {
            $html = view('categories.content')
                ->with(compact(
                    'products'
                ))->render();

            return response()->json([
                'success' => 1,
                'mess' => '',
                'data' => [
                    'html'=> $html
                ]
            ]);
        }
    }

    /**
     * @param $id
     * @return $this
     */
    function index_old($id)
    {
        $tag = Tag::findOrFail($id);
        return redirect()->route('tag.index', ['slug' => $tag->slug ])->setStatusCode(301);
    }

    /**
     * @param Request $request
     * @param null $slug
     * @param null $filters
     * @return TagController|\Illuminate\Http\RedirectResponse
     */
    public function filters(Request $request,$slug = null, $filters = null)
    {

        $request_array = array();
        $filters_array = array();
        $update_request_array = array();
        $updatelink = false;
        if(strripos($slug,'=')) {
            $filters = $slug;
            $slug = null;
        }

        $filters_array = explode(';',$filters);
        foreach ($filters_array as $filter_item)
        {
            $key = substr($filter_item, 0, stripos($filter_item,'='));
            $value = substr($filter_item, stripos($filter_item,'=')+1);
            $request_array[$key] = $value;

        }
        $beforeSort = array_keys($request_array);
        ksort($request_array);
        $afterSort = array_keys($request_array);
        for($i = 0; $i < count($beforeSort); $i ++) {
            if($afterSort[$i] != $beforeSort[$i]) {
                $updatelink = true;
            }
        }

        if ($updatelink) {
            foreach ($request_array as $key => $val) {
                $update_request_array[] = $key.'='.$val;
            }
            $filters = implode(";", $update_request_array);
            if($request->page) {
                $filters = implode(";", $update_request_array).'?page='.$request->page;
            }
            return redirect()->route('tag.filters', array('slug' => $slug,'filters' => $filters));
        }
        if (!isset($request_array['price_from'])) {
            $request_array['price_from'] ='';
        }
        if (!isset($request_array['price_to'])) {
            $request_array['price_to'] ='';
        }
        return $this->index($request,$slug,$request_array);

    }

    public function getList()
    {
        $list = Tag::orderBy('name')->get();
        return view('search.index-tag', compact('list'));
    }


}
