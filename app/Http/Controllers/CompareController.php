<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CompareController extends Controller
{
    protected $redirectTo = '/compare';


    /**
     * add product to comparison
     *
     * @param Request $request
     * @param $id  product item id
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request, $id)
    {
        $compareProductsIds = session('compareitems') ? : array();
        if (count($compareProductsIds) >= setting('site.maxCompareProducts')) {
            return __('messages.max_compared');
            /*return response()->json([
                'success' => __('messages.max_compared'),
            ]);*/
        }

        if (!in_array($id, $compareProductsIds)) {
            $compareProductsIds[] = $id;
        }
        $request->session()->put('compareitems', $compareProductsIds);

        return __('messages.added_compared');
        /*return response()->json([
            'success' => __('messages.added'),
        ]);*/
    }


    /**
     * delete product fron comparison
     *
     * @param Request $request
     * @param $id  product item id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $id)
    {
        $compareProductsIds = session('compareitems') ? : array();
        if (($key = array_search($id, $compareProductsIds)) !== false) {
            unset($compareProductsIds[$key]);
        }

        $request->session()->put('compareitems', $compareProductsIds);

        return redirect($this->redirectTo);
        //return __('messages.deleted_compared');
    }


    /**
     * compare products page
     */
    public function view()
    {
        $compareProductsIds = session('compareitems');

        $compareProductsAssoc = array();

        if (!empty($compareProductsIds))
        {
            $compareProducts = Product::whereIn('id', $compareProductsIds)->with(['flavor', 'country', 'issueForm', 'brand', 'translations'])->get();

            if ($compareProducts)
                foreach ($compareProducts as $key => $compareProduct)
                {
                    $tmpProduct = array();
                    $tmpProduct['id'] = $compareProduct->id;
                    $tmpProduct['mainImg'] = $compareProduct->getMainImg();
                    $tmpProduct['price_discounted'] = $compareProduct->price_discounted;
                    $tmpProduct['discount'] = $compareProduct->discount;
                    $tmpProduct['price'] = $compareProduct->price;
                    $tmpProduct['article'] = $compareProduct->article;
                    $tmpProduct['exp_date'] = $compareProduct->exp_date;
                    $tmpProduct['name'] = $compareProduct->getTranslatedAttribute('name');
                    $tmpProduct['params'] = $compareProduct->getParameters();
                    $tmpProduct['rating'] = $compareProduct->rating;
                    $tmpProduct['rating_count'] = $compareProduct->rating_count;
                    $compareProductsAssoc[$key] = (object) $tmpProduct;
                }
        }
        return view('compare.view')
            ->with(compact(
                'compareProductsAssoc'
            ));

    }

    /**
     * Get count poroduct for user
     */
    public function getCount()
    {
        if(Session::has('compareitems')) {
            $compareProductsIds = session('compareitems');
            $compare_count = Product::whereIn('id', $compareProductsIds)->with(['flavor', 'country', 'issueForm', 'brand', 'translations'])->count();
        } else {
            $compare_count = 0;
        }
        return $compare_count;
    }


}
