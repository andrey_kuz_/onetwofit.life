<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Notifications\VerifyEmailNotification;
use App\Services\CartService;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Auth;
use App\Models\Order;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AccountStoreRequest;
use App\Http\Requests\PasswordChangeRequest;
use App\Exceptions\ProductUnavailableExeption;

class AccountController extends Controller
{

    /* Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }


    /**
     * add or remove from favorites
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleFavorite(Request $request, Product $product)
    {
        $product->toggleFavorite();

        $user = Auth::user();
        $favorite_count = $user->favoriteProducts()->count();

        if ($request->ajax()) {
            $html = view('products.heart')
                ->with(['itemProduct' => $product])->render();

            return response()->json([
                'count' => $favorite_count,
                'html' => $html
            ]);
        }
        return redirect()->back();
    }

    /**
     * Display a listing of the favorite products.
     *
     * @return \Illuminate\Http\Response
     */
    public function favorites(Request $request)
    {
        $user = Auth::user();
        $sort_by = $request->sort_by ?: 'price_asc';
        $favorites = $user->favoriteProducts()->sortBy($sort_by)->get();

        return view('account.favorites', [
            'favorites' => $favorites,
            'sort_by' => $sort_by,
        ]);
    }

    /**
     * Display user profile
     *
     * @return \Illuminate\Http\Response
     */
    public function account(Request $request)
    {

        return view('account.account', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * Show the form for editing profile
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('account.edit', [
            'user' => Auth::user(),
        ]);
    }


    /**
     *  Update profile data
     *
     * @param AccountStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(AccountStoreRequest $request)
    {
        $user = Auth::user();
        User::where('id', $user->id)->update($request->except('_token'));

        if ($request['email'] != Auth::user()->email) {
            User::where('id', $user->id)->update([
                'verified' => 0,
            ]);
            $user->email = $request['email'];

            $user->notify(new VerifyEmailNotification($user));
            //Mail::to($request['email'])->send(new VerifyMail($user));

            auth()->logout();
            return redirect('/login')->with('status', __('messages.code_sent_email_changed'));
        }

        return redirect('/account');
    }

    
    public function editPassword()
    {
        return view('account.editpassword', [
            'user' => Auth::user(),
        ]);
    }

    public function updatePassword(PasswordChangeRequest $request)
    {
        if (!(Hash::check($request->password_old, Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", __('messages.old_password_dismatch'));
        }


        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->back()->with("success", __('messages.pass_changed'));
    }
    

    /**
     * view user orders
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orders()
    {
        $user = Auth::user();
        $orders = $user->orders()->updated()->get();

        return view('account.orders', [
            'orders' => $orders,
        ]);
    }


    /**
     * @param Order $order
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function renewOrder(Order $order)
    {
        $orderService = new OrderService($order);
        $orderService->renewOrder();

        $cart = Cart::current();
        $cart->clear();

        $cartService = new CartService($cart);
        $mess = array();
        $status = 1;

        foreach ($order->orderItems()->get() as $orderItem) {
            try {
                $cartService->addProduct($orderItem->product_id, $orderItem->quantity);
            } catch (ProductUnavailableExeption $e) {
                $mess[] = $orderItem->product->getTranslatedAttribute('name'). ': ' .$e->getMessage(). '(' . __('messages.exists') . ': ' . $orderItem->product->quantity . ')';
                $status = 0;
            }
        }

        if (!$status)
        {
            return redirect()->route('checkout2', ['order' => $orderService->order->id])->withErrors($mess);
        }
        return redirect()->route('checkout2', ['order' => $orderService->order->id]);
    }

}
