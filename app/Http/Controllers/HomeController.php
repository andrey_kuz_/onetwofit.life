<?php

namespace App\Http\Controllers;

use Illuminate\Notifications\Notifiable;
use Location;
use Cookie;
use App;
use Session;
use Illuminate\Http\Request;
use Currency;
use Config;
use Auth;
use App\Models\Product;
use App\Models\Banner;
use App\Exceptions\PromocodeExeption;
use App\Models\Category;
use App\Models\CharacteristicProduct;
use Corcel\Model\User;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $statService = new App\Services\StatService();
        $blogService = new App\Services\BlogService();
        $users = new User();
        $banners = Banner::active()->get();
        $newProducts = Product::active()->new()->with('myFavorite', 'translations')->main()->notFinished()->notHit()->notBestseller()->get();
        $hitProducts = $statService->getHits();
        $superPriceProducts = $statService->getDiscount();

        if($request->id) {
            $bestsellerProducts = $this->getDefaultBestsellers($request->id);
            return view('main.bestsellers', compact('bestsellerProducts'));
        }
        else {
            $bestsellerProducts = $statService->getBestsellers();
        }

        $blogPosts = $blogService->getLastPosts(6);
        $blogRecipesPosts = $blogService->getLastRecipesPosts(5);
        $bodyClass = 'body-main';

        return view('home')
            ->with(compact(
                'bodyClass',
                'banners',
                'newProducts',
                'hitProducts',
                'bestsellerProducts',
                'blogPosts',
                'blogRecipesPosts',
                'superPriceProducts',
                'users'
            ));
    }

    public function getDefaultBestsellers($category_id)
    {
        if($category_id) {
            $category = Category::find($category_id);
            $productsScope = $category->subProducts();
            $bestsellerProducts = $productsScope->with(
                'translations'
            )->active()->bestseller()->main()->notFinished()->notNew()->notHit()->limit(10)->get();

            return $bestsellerProducts;
        } else{
            return false;
        }

    }

    public function getBestsellersFindCategory(Request $request)
    {
        $category = Category::find($request->id);
        $productsScope = $category->subProducts();
        $bestsellerProducts = $productsScope->with(
            'translations'
        )->active()->bestseller()->main()->notFinished()->notNew()->notHit()->limit(10)->get();

        return view('main.bestsellers', compact('bestsellerProducts'));
    }
}
