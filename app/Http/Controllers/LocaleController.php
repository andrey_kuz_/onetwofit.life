<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use Illuminate\Support\Facades\Config;
use Hassansin\DBCart\Models\Cart;

class LocaleController extends Controller
{
    /**
     * save user settings - lang currency country
     *
     * @return \Illuminate\Http\Response
     */
    public function savesettings(Request $request)
    {
        if (isset($request->userCountry)) {
            Session::put('appcountry', $request->userCountry);
        }

        if (isset($request->userLang)) {
            if (in_array($request->userLang, Config::get('app.languages')))
            {
                Session::put('applocale', $request->userLang);
            }
        }

        if (isset($request->userCurrency)) {
            if (in_array($request->userCurrency, Config::get('app.currencies'))) {
                Session::put('appcurrency', $request->userCurrency);
            }
        }

        return Redirect::back();
    }


}
