<?php

namespace App\Http\Controllers\XmlMap;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class XmlController extends Controller
{
    public function generate(Request $request) {
        $tags = Tag::all();
        $brands = Brand::all();
        $categories = Category::active()->get();
        $products = Product::active()->get();
        $pages = Page::where('status', 'ACTIVE')->get();
        $date = Carbon::now()->toDateTimeString();

        $content = view('site_map', compact('tags', 'brands', 'categories', 'products', 'pages', 'date'));

        return response($content)->header('Content-Type', 'text/xml');
    }
}
