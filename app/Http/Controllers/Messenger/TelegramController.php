<?php
namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;

use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;

class TelegramController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        DriverManager::loadDriver(\BotMan\Drivers\Telegram\TelegramDriver::class);

        // Create an instance
        $botman = BotManFactory::create(\Config::get('services.botman'));


        //$botman->reply('Hello yourself telegram-----------.');
        // Give the bot something to listen for.
        $botman->hears('hello', function (BotMan $bot) {
            $bot->reply('Hello yourself123.');
        });

        $botman->hears('hi', function (BotMan $bot) {
            $bot->reply('hi. can i help you123?.');
        });

        // Start listening
        $botman->listen();
    }
}