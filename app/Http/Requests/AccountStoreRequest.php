<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class AccountStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name' => __('validation.attributes.flname')
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha|max:30',
            'email' => 'required|string|email|max:190|unique:users,email,' . Auth::user()->id,
            'phone' => [
                'nullable',
                function ($attribute, $value, $fail) {
                    $str = preg_replace("/[^0-9]/", '', $value);
                    if (strlen($str) < 12) {
                        $fail('Некорректный номер телефона');
                    }
                },
            ],
            'shipping_city' => 'nullable|string|max:50',
            'shipping_street' => 'max:50',
            'shipping_house' => 'max:10',
            'shipping_room' => 'max:10',

        ];
    }
}
