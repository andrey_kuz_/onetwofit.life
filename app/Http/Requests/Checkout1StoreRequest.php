<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Checkout1StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipping_name' => 'required|string',
            'shipping_lastname' => 'required|string',
            'shipping_phone' => [
                'required',
                function ($attribute, $value, $fail) {
                    $str = preg_replace("/[^0-9]/", '', $value);
                    if (strlen($str) < 12) {
                        $fail('Некорректный номер телефона');
                    }
                },
            ],
            'shipping_email' => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            'shipping_name.required' => 'Введите имя',
            'shipping_name.string' => 'Введите имя',
            'shipping_lastname.required' => 'Введите фамилию',
            'shipping_lastname.string' => 'Введите фамилию',
            'shipping_phone.required' => 'Введите номер телефона',
            'shipping_email.required' => 'Введите e-mail',
            'shipping_email.digits_between' => 'Введите e-mail',
        ];
    }

}
