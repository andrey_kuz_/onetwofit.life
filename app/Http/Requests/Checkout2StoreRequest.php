<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Checkout2StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipping_type_id' => 'required',
            'payment_type_id' => 'required',
            'shipping_method_id' => 'required',
            'shipping_city' => 'required',
            'shipping_department' => 'required_if:shipping_method_id,==,1',
            'shipping_address' => 'required_if:shipping_method_id,==,2',
        ];
    }
}
