<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OneClickStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'phone' => [
                'required',
                function ($attribute, $value, $fail) {
                    $str = preg_replace("/[^0-9]/", '', $value);
                    if (strlen($str) < 12) {
                        $fail('Некорректный номер телефона');
                    }
                },
            ],
        ];
    }
}
