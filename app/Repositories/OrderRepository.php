<?php
namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderItem;
use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\Repository;

class OrderRepository extends Repository
{
    protected $model;

    public function __construct(Order $orderModel)
    {
        $this->model = $orderModel;
    }
}