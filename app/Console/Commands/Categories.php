<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class Categories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:refresh_active';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $categories = Category::get();
        foreach ($categories as $category)
        {
            $count = $category->subProducts()->count();
            $category->is_active = $count > 0 ? true : false;
            $category->products_cnt = $count;
            $category->save();
            $this->info('after save..');
            $this->info($category->id . ' - ' . $category->products_cnt);
        }
    }
}
