<?php

namespace App\Console\Commands;

use App\Models\Cart;
use Illuminate\Console\Command;
use Carbon\Carbon;

class ClearCarts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:cart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear expired 7 days and completed carts and its items';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('cart clear - '.\Carbon\Carbon::now());

        $cartsQuery = Cart::expired7days();
        $cartsCompletedQuery = Cart::completed();

        \Log::info('cart expired clear - '.json_encode($cartsQuery->get()));
        \Log::info('cart completed clear - '.json_encode($cartsCompletedQuery->get()));

        $cartsQuery->delete();
        $cartsCompletedQuery->delete();
    }
}
