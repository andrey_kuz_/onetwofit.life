<?php
namespace App\Traits;

use Carbon\Carbon;
use App\Models\Promocode;
use App\Exceptions\PromocodeExeption;
trait Promocodable
{
    /**
     * Get the promocodes that are related to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function promocodes()
    {
        return $this->belongsToMany(Promocode::class, 'promocode_used')
            ->withPivot('used_at');
    }


    /**
     * @param $code
     * @param null $callback
     * @return bool|null
     */
    public function applyCode($code, $callback = null)
    {
        if ($promocode = Promocode::check($code)) {

            $promocode->users()->attach($this->id, [
                'used_at' => Carbon::now(),
            ]);

            if (is_callable($callback)) {
                $callback($promocode);
            }
            return $promocode;
        }
        if (is_callable($callback)) {
            $callback(null);
        }
        return null;
    }

    /**
     * @param $code
     * @param null $callback
     * @return bool|null
     */
    public function cancelCode($code, $callback = null)
    {
        if ($promocode = Promocode::check($code)) {

            $promocode->users()->detach($this->id);

            if (is_callable($callback)) {
                $callback($promocode);
            }
            return $promocode;
        }
        if (is_callable($callback)) {
            $callback(null);
        }
        return null;
    }
}