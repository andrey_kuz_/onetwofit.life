<?php

namespace App\Traits;

use Config;
trait Convertable
{
    // output currency units or not
    public $format = true;

    public function convertPrice($price)
    {
        return to_currency($price, true);
    }

    public function roundNumber($price)
    {
        return round($price, 2);
    }
}