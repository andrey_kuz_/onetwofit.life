<?php
namespace App\Traits;

trait CanVote
{
    protected $voteRelation = __CLASS__;
    /**
     * Up vote a item or items.
     *
     * @param int|array|\Illuminate\Database\Eloquent\Model $item
     *
     * @return boolean
     */
    public function upVote($item)
    {
        $this->cancelVote($item);
        $res = $this->vote($item);
        if (isset($item->up_votes))
        {
            $item->up_votes = $item->countVoters('up');
        }


        return $res;
    }
    /**
     * Down vote a item or items.
     *
     * @param int|array|\Illuminate\Database\Eloquent\Model $item
     *
     * @return boolean
     */
    public function downVote($item)
    {
        $this->cancelVote($item);
        $res = $this->vote($item, 'down');
        if (isset($item->down_votes))
        {
            $item->down_votes = $item->countVoters('down');
        }

        return $res;
    }
    /**
     * Vote a item or items.
     *
     * @param  int|array|\Illuminate\Database\Eloquent\Model $item
     * @param  string $type
     * @return boolean
     */
    public function vote($item, $type = 'up')
    {
        $items = array_fill_keys((array) $this->checkVoteItem($item), ['type' => $type]);
        return $this->votedItems()->sync($items, false);
    }
    /**
     * Cancel vote a item or items.
     *
     * @param int|array|\Illuminate\Database\Eloquent\Model $item
     *
     * @return int
     */
    public function cancelVote($item)
    {
        $item = $this->checkVoteItem($item);
        return $this->votedItems()->detach((array)$item);
    }


    /**
     * Check if user has voted item.
     *
     * @param $item
     * @param string $type
     *
     * @return bool
     */
    public function hasVoted($item, $type = null)
    {
        $item = $this->checkVoteItem($item);
        $votedItems = $this->votedItems();
        if(!is_null($type)) $votedItems->wherePivot('type', $type);
        return $votedItems->get()->contains($item);
    }

    /**
     * Return the user what has items.
     *
     * @param string $class
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function votedItems($class = null)
    {
        if (!empty($class)) {
            $this->setVoteRelation($class);
        }
        return $this->morphedByMany($this->voteRelation, 'votable', $this->vote_table ?: 'votes')->withTimestamps();
    }

    /**
     * Determine whether $item is an object of Eloquent\Model
     *
     * @param $item
     *
     * @return int
     */
    protected function checkVoteItem($item)
    {
        if ($item instanceof \Illuminate\Database\Eloquent\Model) {
            $this->setVoteRelation(get_class($item));
            return $item->id;
        };
        return $item;
    }

    /**
     * Set the vote relation class.
     *
     * @param $class
     * @return mixed
     */
    protected function setVoteRelation($class)
    {
        return $this->voteRelation = $class;
    }
}