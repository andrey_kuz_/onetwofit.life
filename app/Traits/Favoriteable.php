<?php
/**
 * Created by PhpStorm.
 * User: Owner
 * Date: 06.08.2018
 * Time: 9:21
 */

namespace App\Traits;

use ChristianKuri\LaravelFavorite\Traits\Favoriteable as BaseFavoriteable;
use Auth;
use ChristianKuri\LaravelFavorite\Models\Favorite;
trait Favoriteable
{
    use BaseFavoriteable;


    public function myFavorite()
    {
        return $this->hasMany(Favorite::class, 'favoriteable_id')->where('user_id', Auth::id());

    }

    /**
     * Check if the user has favorited this Object
     *
     * @param  int $user_id  [if  null its added to the auth user]
     * @return boolean
     */
    public function isMyFavorite()
    {
        return $this->favorites()->where('user_id', Auth::id());
    }

}