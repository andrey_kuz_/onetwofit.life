<?php

namespace App\Traits;

use TCG\Voyager\Traits\Translatable as VoyagerTranslatable;
trait Translatable
{
    //use VoyagerTranslatable;
    use VoyagerTranslatable{
        getTranslatedAttribute as getTranslatedAttributeBase;
    }
    // output currency units or not
    public function translateWithReplace()
    {

        foreach ($this->getTranslatableAttributes() as $attribute) {
            $this->setAttribute($attribute, $this->getTranslatedAttribute($attribute));
        }
    }

    public function getTranslatedAttribute($attribute, $language = null, $fallback = true)
    {
        $value = $this->getTranslatedAttributeBase($attribute, $language, $fallback);
        //var_dump($value);
        if (!$value)
        {
            //var_dump($this->$attribute);
            return $this->$attribute;
        }
        return $value;
    }
}