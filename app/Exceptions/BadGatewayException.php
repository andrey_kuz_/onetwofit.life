<?php
/**
 * Created by PhpStorm.
 * User: urich-dev
 * Date: 23.07.2018
 * Time: 12:04
 */

namespace App\Exceptions;
use Exception;


class BadGatewayException extends Exception
{
    /**
     * @var string
     */
    protected $message = 'Платежная система не найдена';

    /**
     * @var int
     */
    protected $code = 403;
}