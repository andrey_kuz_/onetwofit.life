<?php

namespace App\Exceptions;

use App\Exceptions\PromocodeExeption;

class InvalidPromocodeException extends PromocodeExeption
{
    /**
     * @var string
     */
    protected $message = 'Введен неверный промокод';

    /**
     * @var int
     */
    protected $code = 404;
}
