<?php

namespace App\Exceptions;

use App\Exceptions\PromocodeExeption;

class AlreadyUsedException extends PromocodeExeption
{
    /**
     * @var string
     */
    protected $message = 'Промокод уже использован.';

    /**
     * @var int
     */
    protected $code = 403;
}
