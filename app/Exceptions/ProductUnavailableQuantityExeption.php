<?php
/**
 * Created by PhpStorm.
 * User: urich-dev
 * Date: 16.07.2018
 * Time: 15:04
 */

namespace App\Exceptions;
use Exception;

class ProductUnavailableQuantityExeption extends Exception
{
    protected $message = '';

}