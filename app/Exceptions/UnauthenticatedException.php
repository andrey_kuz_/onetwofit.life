<?php

namespace App\Exceptions;

use App\Exceptions\PromocodeExeption;

class UnauthenticatedException extends PromocodeExeption
{
    /**
     * @var string
     */
    protected $message = 'Неавторизованый пользователь не может использовать промокод';

    /**
     * @var int
     */
    protected $code = 401;
}
