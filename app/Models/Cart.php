<?php

namespace App\Models;

use App\Exceptions\ProductUnavailableExeption;
use App\Observers\CartLineObserver;
use App\Traits\Convertable;
use Hassansin\DBCart\Models\Cart as CartBase;
use Illuminate\Database\Eloquent\Model;
use App\Services\Discount\Discount;
use App\Exceptions\PromocodeExeption;
use Carbon\Carbon;
use DebugBar\DebugBar;

class Cart extends Model
{

    use Convertable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'session', 'name', 'status', 'item_count', 'placed_at', 'completed_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['placed_at', 'completed_at'];

    public $info;
    public $productsCounts;

    /**
     * Get the user that owns the cart.
     */
    public function user()
    {
        return $this->belongsTo(config('cart.user_model'));
    }

    /**
     * Get the items for the cart.
     */
    public function items()
    {
        return $this->hasMany(config('cart.cart_line_model'));
    }

    public function scopePending($query)
    {
        return $query->where('status', 'pending');
    }

    public function scopeCompleted($query)
    {
        return $query->where('status', 'complete');
    }

    public function scopeExpired($query)
    {
        return $query->where('status', 'expired');
    }

    public function scopeExpired7days($query)
    {
        return $query->whereDate('updated_at', '<', Carbon::now()->subDays(7));
    }

    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function scopeInstance($query, $instance_name = 'default')
    {
        return $query->where('name', $instance_name);
    }

    public function scopeUser($query, $user_id = null)
    {
        $user_id = $user_id ?: config('cart.user_id');
        if ($user_id instanceof \Closure)
            $user_id = $user_id();
        return $query->where('user_id', $user_id);
    }

    public function scopeSession($query, $session_id = null)
    {
        $session_id = $session_id ?: app('request')->session()->getId();
        return $query->where('session', $session_id);
    }

    /*
    public function setTotalPriceAttribute($value){
        $this->attributes['total_price'] = $value;
    }
    */


    /**
     * If a change is made to items, then reset lazyloaded relations to reflect new changes
     *
     * @return $this
     */
    public function resetRelations()
    {
        foreach ($this->relations as $key => $value) {
            $this->getRelationshipFromMethod($key);
        }
        return $this;
    }

    /**
     * Add item to a cart
     *
     * @param  array $attributes
     * @return cartItem model
     */
    public function addItem(array $attributes = [])
    {
        return $this->items()->create($attributes);
    }

    /**
     * remove item from a cart
     *
     * @param  array $attributes
     * @return mixed
     */
    public function removeItem(array $attributes = [])
    {
        return $this->items()->where($attributes)->first()->delete();
    }

    /**
     * update item in a cart
     *
     * @param array $where
     * @param array $values
     * @return bool|int
     */
    public function updateItem(array $where, array $values)
    {
        return $this->items()->where($where)->first()->update($values);
    }


    /**
     * Cart checkout.
     *
     * @return bool
     */
    public function checkout()
    {
        return $this->update([
            'status' => 'pending',
            'placed_at' => Carbon::now()
        ]);
    }


    /**
     * Expires a cart
     *
     * @return bool
     */
    public function expire()
    {
        return $this->update(['status' => 'expired']);
    }

    /**
     * Set a cart as complete
     *
     * @return bool
     */
    public function complete()
    {
        return $this->update(['status' => 'complete', 'completed_at' => Carbon::now()]);
    }

    /**
     * Check if cart is empty
     *
     * @return bool
     */
    public function isEmpty()
    {
        return $this->items->count() === 0;
    }


    /**
     * check if has item with conditions
     * @param $where
     * @return bool
     */
    public function hasItem($where)
    {
        return !is_null($this->items()->where($where)->first());
    }


    /**
     * Empties a cart
     *
     * @return bool
     */
    public function clear()
    {
        $this->items()->delete();
        $this->resetRelations()->updateTimestamps();
        $this->item_count = 0;
        return $this->save();
    }


    /**
     * Move Items to another cart instance
     *
     * @param Cart $cartTo
     * @return bool
     */
    public function moveItemsTo(Cart $cartTo)
    {
        $cartItemsFrom = $this->items()->get();

        foreach ($cartItemsFrom as $cartItemFrom) {
            $cartToItem = $cartTo->items()->where('product_id', $cartItemFrom->product_id)->first();

            try{
                if ($cartToItem) {
                    $cartToItem->quantity = $cartToItem->quantity + $cartItemFrom->quantity;
                    $cartToItem->save();
                } else {
                    $cartItemFrom->moveTo($cartTo);
                }
            }
            catch (ProductUnavailableExeption $e)
            {
                $status = 0;
            }

        }
        $cartTo->item_count += $this->item_count;
        $cartTo->save();

        $this->item_count = 0;
        return $this->save();
        //throw new ProductUnavailableExeption(__('messages.product_not_avail_quant'));
    }

    /**
     * get amount left to free shipping
     *
     * @return mixed
     */
    public function getFreeShippingLeft()
    {
        return $this->info->freeShippingLeft;
    }

    /**
     * calc actual cart count
     *
     * @return mixed
     */
    public function getItemsCount()
    {
        return $this->items()->sum('quantity');
    }

    /**
     * products ids array stored in cart
     * @return array
     */
    public function getProductIdsArray()
    {
        return $this->items()->select('product_id')->get()->toArray();
    }

    /**
     * get array of products count in cart where key is id and value is quantity
     *
     * @return array
     */
    public function getProductsQuantityAssocArray()
    {
        return $this->items()->pluck('quantity', 'product_id')->toArray();
    }


    public function checkAvailability()
    {
        $cartItems = $this->items()->with('product')->get();
        //dd($cartItems);
        foreach ($cartItems as $cartItem) {
            //var_dump($cartItem);
            \Debugbar::info($cartItem);
            $res =
                CartLineObserver::check($cartItem);
            //var_dump($res);
            \Debugbar::info($cartItem);
        }
    }


    /**
     * refresh all variable info in cart according actual prices, currency rate, rest quantity
     * set $this->info class property
     *
     */
    public function refreshCartInfo()
    {
        $this->refresh();

        $productsCounts = $this->getProductsQuantityAssocArray();

        $cartItems = $this->items()->with(['product' => function ($qp) use ($productsCounts) {
            return $qp->with(['comboOffers' => function ($qco) use ($productsCounts) {
                return $qco->whereIn('related_product_id', array_keys($productsCounts));
            }]);
        }])->get();
//        var_dump($cartItems);
        $total = 0;
        $discounted = 0;
        $bothDiscountAbs = 0;
        $info = array();
        $cartItemsInfo = array();

        \Illuminate\Support\Facades\Cache::put('cart_products_counts_' . $this->id, $productsCounts, 1);
        foreach ($cartItems as $cartItem) {

            if ($cartItem) {
                $cartItemInfo = $cartItem->getCartItemInfo();
                $cartItemsInfo[$cartItem->id] = $cartItemInfo;
                $total = $total + $cartItemInfo->getPrice()->total;
                $discounted = $discounted + $cartItemInfo->getFinalDiscountedPrice();
                $bothDiscountAbs = $bothDiscountAbs + $cartItemInfo->getQtyDiscountAbs()->total + $cartItemInfo->getComboDiscountAbs();
            }

        }

        \Illuminate\Support\Facades\Cache::forget('cart_products_counts_' . $this->id);

        $discountedWithoutPromo = $discounted;
        $discountAbsPromo = 0;
        $promocodeDiscount = 0;

        if ($this->promocode) {
            try {
                $promocode = Promocode::check($this->promocode);
                $promocodeDiscount = $promocode->discount;
                $discountPromo = new Discount($discountedWithoutPromo, $promocode->discount);
                $discountAbsPromo = $discountPromo->getQtyDiscountAbs()->total;
                $discounted = $discountPromo->getDiscountedPrice()->total;
            } catch (PromocodeExeption $exception) {
            }
        }
        $freeShipping = setting('site.freeShipping_' . config('app.currency'), 1000);
        $freeShippingLeft = round(max($freeShipping - to_currency($discounted), 0), 2);
        $itemsCount = $this->item_count;

        $info = array(
            'cartItemsInfo' => $cartItemsInfo,
            'total' => $total, // without discounts
            'discounted' => $discounted, //with all discounts!
            'discountedWithoutPromo' => $discountedWithoutPromo, // with discounts but without promo discount
            'discountAbsPromo' => $discountAbsPromo,
            'discountPromo' => $promocodeDiscount,
            'bothDiscountAbs' => $bothDiscountAbs,
            'freeShipping' => $freeShipping,
            'freeShippingLeft' => $freeShippingLeft,
            'itemsCount' => $itemsCount,
        );

        $this->info = (object)$info;
    }

    /**
     * Get the current cart instance
     *
     * @param  string $instance_name
     * @return mixed
     */
    public static function current($instance_name = 'default', $save_on_demand = null)
    {

        $save_on_demand = is_null($save_on_demand) ? config('cart.save_on_demand', false) : $save_on_demand;

        return static::init($instance_name, $save_on_demand);
    }

    /**
     * Initialize the cart
     *
     * @param  string $instance_name
     * @return mixed
     */
    public static function init($instance_name, $save_on_demand)
    {

        $request = app('request');
        $session_id = $request->session()->getId();

        return \Cache::store('array')->remember('cart_' . $instance_name . '_' . $session_id, 2, function () use ($instance_name, $request, $save_on_demand, $session_id) {
            $user_id = config('cart.user_id');

            if ($user_id instanceof \Closure)
                $user_id = $user_id();
            //if user logged in
            if ($user_id) {

                $user_cart = static::active()->user()->where('name', $instance_name)->first();

                $session_cart_id = $request->session()->get('session_cart');

                $session_cart = is_null($session_cart_id) ? null : static::active()->with('items')->session($session_cart_id)->where('name', $instance_name)->first();

                switch (true) {

                    case is_null($user_cart) && is_null($session_cart): //no user cart or session cart
                        $attributes = array(
                            'user_id' => $user_id,
                            'name' => $instance_name,
                            'status' => 'active'
                        );
                        if ($save_on_demand)
                            $cart = new static($attributes);
                        else
                            $cart = static::create($attributes);

                        break;

                    case !is_null($user_cart) && is_null($session_cart): //only user cart
                        $cart = $user_cart;
                        break;

                    case is_null($user_cart) && !is_null($session_cart): //only session cart
                        $cart = $session_cart;
                        $cart->user_id = $user_id;
                        $cart->session = null;
                        $cart->save();
                        break;

                    case !is_null($user_cart) && !is_null($session_cart): //both user cart and session cart exists
                        $session_cart->moveItemsTo($user_cart); //move items from session cart to user cart
                        $session_cart->delete(); //delete session cart
                        $cart = $user_cart;
                        break;
                }
                //$cart->refreshCartInfo();

                $request->session()->forget('cart_' . $instance_name); //no longer need it.
                //return $cart;
            } //guest user, create cart with session id
            else {
                $attributes = array(
                    'session' => $session_id,
                    'name' => $instance_name,
                    'status' => 'active'
                );
                $cart = static::firstOrNew($attributes);

                if (!$save_on_demand)
                    $cart->save();

                //save current session id, since upon login session id will be regenerated
                //we will use this id to get back the cart before login
                //$cart->refreshCartInfo();

                $request->session()->put('cart_' . $instance_name, $session_id);
                //return $cart;
            }

            return $cart;
        });


    }
}
