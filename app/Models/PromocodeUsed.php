<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromocodeUsed extends Model
{
    //
    protected $table = 'promocode_used';

    public function promocode()
    {
        return $this->belongsTo('App\Models\Promocode');
    }
}
