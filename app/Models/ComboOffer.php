<?php

namespace App\Models;

use App\Traits\Convertable;
use Illuminate\Database\Eloquent\Model;

class ComboOffer extends Model
{
    //use Convertable;
    protected $table = 'combo_offers';

    public function scopeWithActiveProducts($query)
    {
        return $query->whereHas('relatedProduct')->whereHas('product');
    }


    public function relatedProduct()
    {
        return $this->belongsTo('App\Models\Product', 'related_product_id')->active();
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id')->active();
    }

    public function getComboOfferPrice()
    {
        // calculate total price - the sum of both products prices in USD, subtract discount in USD
        $totalPrice = $this->product->price_discounted + $this->relatedProduct->price_discounted - $this->combo_discount_abs;

        return $totalPrice;
    }

    public function getComboOfferDiscount()
    {
        return $this->combo_discount_abs;
    }
}