<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Country extends Model
{
    use Translatable;

    protected $translatable = ['name'];
    protected $fillable = ['id', 'name', 'country_code'];


    /**
     * get products belongs to this brand
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product')->active();
    }

    /**
     * get products belongs to this brand
     */
    public function cities()
    {
        return $this->hasMany('App\Models\City')->active();
    }
}
