<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translatable;

class ShippingMethod extends Model
{
    use Translatable;
    protected $translatable = ['name'];
    public function shippingTypes()
    {
        return $this->belongsToMany(
            'App\Models\ShippingType',
            'shipping_methods_types'
        );
    }
}
