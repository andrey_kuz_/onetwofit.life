<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    public function scopeSuccess($query)
    {
        return $query->where('success', true);
    }
}
