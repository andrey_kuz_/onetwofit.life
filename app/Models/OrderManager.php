<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderManager extends Model
{
    protected $table = 'orders_managers';

    protected $fillable = [
        'manager_id', 'order_id'
    ];
}
