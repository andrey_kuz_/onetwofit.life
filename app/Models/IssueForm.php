<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translatable;

class IssueForm extends Model
{
    use Translatable;

    protected $translatable = ['name'];
    protected $fillable = ['id', 'name'];

}
