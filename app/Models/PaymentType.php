<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translatable;

class PaymentType extends Model
{
    use Translatable;
    protected $translatable = ['name'];
    protected $table = 'payment_types';

    protected $fillable = ['name', 'gateway'];

    protected $hidden = [ 'created_at', 'updated_at' ];

    public function scopeActive($query)
    {
        return $query;
    }
}
