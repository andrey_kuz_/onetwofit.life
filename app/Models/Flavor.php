<?php

namespace App\Models;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Flavor extends Model
{
    use Translatable;

    protected $translatable = ['name'];

    protected $fillable = ['id', 'name'];
}
