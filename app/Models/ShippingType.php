<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\NovaPoshta;
use App\Traits\Translatable;

class ShippingType extends Model
{
    use Translatable;
    protected $translatable = ['name'];
    protected $table = 'shipping_types';

    protected $fillable = ['name','api_key','api', 'price'];

    protected $hidden = [ 'created_at', 'updated_at'];

    protected $apiService = false;

    private function hasApi(){
        if($this->api && $this->api=='nova_poshta'){
            $this->apiService = new NovaPoshta($this->api_key);
            return true;
        }
    }

    public function apiOptions(){
        if($this->hasApi()){
            return $this->apiService->getOptions();
        }
    }

    public function apiStep($step, $request){
        if($this->hasApi()){
            return $this->apiService->apiStep($step, $request);
        }
    }

    public function paymentTypes()
    {
        return $this->belongsToMany(
            'App\Models\PaymentType',
            'shipping_type_payment_type'
        );
    }

    public function shippingMethods()
    {
        return $this->hasMany(
            'App\Models\ShippingMethod'
        );
    }
}
