<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    //
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    /**
     * Get the product record associated with the item.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
