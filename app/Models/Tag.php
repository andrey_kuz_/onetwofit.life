<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translatable;

class Tag extends Model
{
    use Translatable;

    protected $translatable = ['name'];

    public $timestamps = false;

    public function products()
    {
        return $this->belongsToMany(
            'App\Models\Product',
            'products_tags'
        )->active();
    }

    public function getProductIdsArrayAttribute()
    {
        return $this->products->pluck('id')->toArray();
    }

    public function scopeAssignedToProducts($query, $product_ids)
    {
        return $query->whereHas('products', function($q) use ($product_ids){
            $q->whereIN('id', $product_ids);
        });
    }

    public function subBrands()
    {
        return $brands = Brand::assignedToProducts($this->productIdsArray);
    }
}
