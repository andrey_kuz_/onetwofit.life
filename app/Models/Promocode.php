<?php

namespace App\Models;

use App\Exceptions\AlreadyUsedException;
use App\Exceptions\InvalidPromocodeException;
use App\Exceptions\PromocodeExeption;
use App\Exceptions\ExpiredException;
use App\Exceptions\UnauthenticatedException;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Promocode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'discount', 'is_disposable', 'exp_date'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['exp_date'];


    /**
     * Get  users who used promo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'promocode_used')
            ->withPivot('used_at');
    }

    /**
     * Get  users who used promo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany('App\Models\Order', 'promocode_used')
            ->withPivot('used_at');
    }

    /**
     * Query builder to find promocode using code.
     *
     * @param $query
     * @param $code
     *
     * @return mixed
     */
    public function scopeByCode($query, $code)
    {
        return $query->where('code', $code);
    }

    /**
     * Query builder to get disposable codes.
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsDisposable($query)
    {
        return $query->where('is_disposable', true);
    }

    /**
     * Query builder to get non-disposable codes.
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsNotDisposable($query)
    {
        return $query->where('is_disposable', false);
    }


    /**
     * Query builder to get expired promotion codes.
     *
     * @param $query
     * @return mixed
     */
    public function scopeExpired($query)
    {
        return $query->whereNotNull('exp_date')->whereDate('exp_date', '<=', Carbon::now());
    }


    /**
     * Query builder to get expired promotion codes.
     *
     * @param $query
     * @return mixed
     */
    public function scopeNotExpired($query)
    {
        return $query->whereNotNull('exp_date')->whereDate('exp_date', '>', Carbon::now());
    }

    /**
     * Check if code is disposable (ont-time).
     *
     * @return bool
     */
    public function isDisposable()
    {
        return $this->is_disposable;
    }

    /**
     * Check if code is one-user disposable (ont-time).
     *
     * @return bool
     */
    public function isOneuserDisposable()
    {
        return $this->is_oneuser_disposable;
    }

    /**
     * Check if code is expired.
     *
     * @return bool
     */
    public function isExpired()
    {
        return $this->exp_date ? Carbon::now()->gte($this->exp_date) : false;
    }

    /**
     * check promocode valid, not expired and not used by other users  and current user
     *
     * @param $code
     * @return mixed
     * @throws AlreadyUsedException
     * @throws InvalidPromocodeException
     * @throws UnauthenticatedException
     * @throws ExpiredException
     */
    static public function check($code)
    {
        $promocode = self::byCode($code)->first();



        if ($promocode === null) {
            throw new InvalidPromocodeException;
        }

        if ($promocode->isExpired()) {

            throw new PromocodeExeption;
        }

        if (!auth()->check()) {
            throw new UnauthenticatedException;
        }

        // if promocode is used by current user
        if ($promocode->isDisposable() && $promocode->users()->wherePivot('user_id', auth()->user()->id)->exists()) {
            throw new AlreadyUsedException;
        }

        // if promocode is one user disposable and used by any user
        if ($promocode->isOneuserDisposable() && $promocode->users()->exists()) {
            throw new AlreadyUsedException;
        }
        return $promocode;
    }

    /**
     * check promocode valid, not expired and not used by other users
     *
     * @param $code
     * @return mixed
     * @throws AlreadyUsedException
     * @throws InvalidPromocodeException
     */
    static public function checkUnauthorized($code)
    {
        $promocode = self::byCode($code)->first();
        if ($promocode === null) {
            throw new InvalidPromocodeException;
        }
        if ($promocode->isExpired()) {
            throw new ExpiredException;
        }

        // if promocode is one user disposable and user by any user
        if ($promocode->isOneuserDisposable() && $promocode->users()->exists()) {
            throw new AlreadyUsedException;
        }
        return $promocode;
    }

    /**
     * check promocode valid, not expired and not used by other users
     *
     * @param $code
     * @return mixed
     * @throws AlreadyUsedException
     * @throws InvalidPromocodeException
     */
    static public function checkUnauthorizedByEmail($code, $email)
    {
        $promocode = self::byCode($code)->first();
        if ($promocode === null) {
            throw new InvalidPromocodeException;
        }
        if ($promocode->isExpired()) {
            throw new ExpiredException;
        }

        // if promocode is used by current user
        if ($promocode->isDisposable() && $promocode->users()->wherePivot('user_id', auth()->user()->id)->exists()) {
            throw new AlreadyUsedException;
        }

        // if promocode is one user disposable and used by any user
        if ($promocode->isOneuserDisposable() && $promocode->users()->exists()) {
            throw new AlreadyUsedException;
        }
        return $promocode;
    }

    static public function statusInfo($code)
    {
        if ($code) {
            try {
                $promocode = Promocode::check($code);
                //$cart->promocode = $code;
                $mess = __('messages.promocode_success');
                $success = 1;
            } catch (PromocodeExeption $exception) {
                $mess = $exception->getMessage();
                //$cart->promocode = '';
                $success = 0;
            }
        }
        else{
            $mess = '';
            $success = 0;
        }
        return array(
            'success' => $success,
            'mess' => $mess,
        );
    }

}
