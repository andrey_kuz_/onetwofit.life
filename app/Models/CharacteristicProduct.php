<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CharacteristicProduct extends Model
{
    protected $table = 'characteristic_product';
    protected $guarded = [];
    public $timestamps = false;

    public function scopeGetCharacteristics($query, $product_id)
    {
        return $query->where('product_id', $product_id);
    }

}
