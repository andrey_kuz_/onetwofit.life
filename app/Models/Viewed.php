<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Viewed extends Model
{
    protected $table = 'viewed';


    static function getViewed($user_id, $session, $limit)
    {
        $user_id = ($user_id) ? $user_id : Auth::id();
        $session = ($session) ? $session : session()->getId();

        return self::where('session', $session)->orderBy('updated_at', 'desc')->limit($limit)->get();
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id')->with('translations')->active();
    }

    public function scopeRecent($query)
    {
        $query->where('session', session()->getId())->orderBy('updated_at', 'desc')->limit(setting('site.viewedProductsCount'));
    }

}
