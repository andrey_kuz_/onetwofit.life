<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * Statuses.
     */
    const STATUS_PENDING = 'pending';
    const STATUS_COMPLETED = 'success';

    /**
     * List of statuses.
     *
     * @var array
     */
    public static $statuses = [self::STATUS_PENDING, self::STATUS_COMPLETED];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

}
