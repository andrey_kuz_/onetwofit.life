<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translatable;

class City extends Model
{
    use Translatable;

    protected $translatable = ['name'];

    /**
     * get products belongs to this brand
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
