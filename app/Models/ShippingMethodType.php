<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingMethodType extends Model
{
    protected $table = 'shipping_methods_types';
}
