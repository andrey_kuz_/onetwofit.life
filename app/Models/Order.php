<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Promocode;
use Carbon\Carbon;

class Order extends Model
{
    protected $table = 'orders';
    //protected $dateFormat = 'U';
    protected $dates = ['created_at'];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Statuses.
     */
    const STATUS_PENDING = 'pending';
    const STATUS_ACTIVE = 'active';
    const STATUS_COMPLETED = 'completed';
    const STATUS_PUBLISHED = 'published';
    const STATUS_CANCELED = 'canceled';


    /**
     * List of statuses.
     *
     * @var array
     */
    public static $statuses = [self::STATUS_PENDING, self::STATUS_ACTIVE, self::STATUS_COMPLETED, self::STATUS_CANCELED];


    public function getDefaultStatus()
    {
        return self::STATUS_PENDING;
    }

    public function getDefaultShippingStatus()
    {
        return 'pending';
    }

    public function paymentType()
    {
        return $this->belongsTo('App\Models\PaymentType');
    }

    public function payment()
    {
        return $this->hasOne('App\Models\Payment');
    }

    public function promocodeUsed()
    {
        return $this->hasOne('App\Models\PromocodeUsed');
    }

    public function shippingType()
    {
        return $this->belongsTo('App\Models\ShippingType');
    }

    public function shippingMethod()
    {
        return $this->belongsTo('App\Models\ShippingMethod');
    }

    /**
     * scope pending orders
     * @param $query
     * @return mixed $query
     */
    public function scopePending($query)
    {
        return $query->where('status', SELF::STATUS_PENDING);
    }

    /**
     * scope pending orders
     * @param $query
     * @return mixed $query
     */
    public function scopeActive($query)
    {
        return $query->where('status', SELF::STATUS_ACTIVE);
    }


    /**
     * scope pending orders
     * @param $query
     * @return mixed $query
     */
    public function scopeCompleted($query)
    {
        return $query->where('status', SELF::STATUS_COMPLETED);
    }


    /**
     * scope pending orders
     * @param $query
     * @return mixed $query
     */
    public function scopeCanceled($query)
    {
        return $query->where('status', SELF::STATUS_CANCELED);
    }

    /**
     * scope pending orders
     * @param $query
     * @return mixed $query
     */
    public function scopeCreated($query, $sort = 'DESC')
    {
        return $query->orderBy('created_at', $sort);
    }

    /**
     * scope pending orders
     * @param $query
     * @return mixed $query
     */
    public function scopeUpdated($query, $sort = 'DESC')
    {
        return $query->orderBy('updated_at', $sort);
    }

    /**
     * @param $code
     * @param null $callback
     * @return bool|null
     */
    public function applyCode($code, $callback = null)
    {
        if ($promocode = Promocode::check($code)) {

            $promocode->orders()->attach($this->id, [
                'user_id' => $this->user_id,
                'used_at' => Carbon::now(),
            ]);

            if (is_callable($callback)) {
                $callback($promocode);
            }
            return $promocode;
        }
        if (is_callable($callback)) {
            $callback(null);
        }
        return null;
    }


    /**
     * @param $product_id
     * @param $price
     * @param $quantity
     * @param $total_price
     * @param int $discount_by_qnt
     * @param int $discount_by_combo
     * @return Model
     */
    public function addItem($product_id, $total_price, $quantity, $total_price_discounted, $discount_by_qnt = 0, $discount_by_combo = 0)
    {
        $result = $this->orderItems()->create([
            'product_id' => $product_id,
            'total_price' => $total_price,
            'quantity' => $quantity,
            'total_price_discounted' => $total_price_discounted,
            'discount_by_qnt' => $discount_by_qnt,
            'discount_by_combo' => $discount_by_combo,
        ]);

        return $result;
    }

    public function addItems($orderItemsInfo)
    {
        $result = $this->orderItems()->create($orderItemsInfo);
        //$this->refreshOrderInfo();
        return $result;
    }

    /**
     * Delete an Order
     * @param $item_id
     * @return bool
     */
    public function removeItem($item_id)
    {
        $result = $this->orderItems()->where('id', '=', $item_id)->delete();
        //$this->refreshOrderInfo();
        return $result;
    }

    /**
     * Update an Order Status
     * @param $order_id
     * @param $status
     * @return bool
     */
    public function updateStatus($status)
    {
        if (in_array($status, SELF::$statuses)) {
            if ($status == SELF::STATUS_COMPLETED) {
                $this->completed_at = date("Y-m-d H:i:s");
            }
            $this->status = $status;
            return $this->save();
        }
        return false;
    }


    /**
     * Refresh an Order's values
     * @param $order_id
     * @return bool
     */
    public function refreshOrderInfo()
    {
        $this->total_price = $this->total();
        $this->total_price_discounted = $this->total_discounted();
        $this->item_count = $this->count();
        $this->discount_by_qnt_combo = $this->discount();
        $this->save();
    }

    public function discount()
    {
        $items = $this->orderItems()->get();

        $totalDiscount = 0;
        if ($items) {
            foreach ($items as $item) {
                if ($item->discount_amount) $totalDiscount += $item->discount_by_qnt + $item->discount_by_combo;
            }
        }
        return $totalDiscount;
    }

    /**
     * Calculate an Order total amount
     * @param $order_id
     * @return int
     */
    public function total()
    {
        $items = $this->orderItems()->get();

        $total = 0;
        if ($items) {
            foreach ($items as $item) {
                $total += $item->total_price;
            }
        }
        return $total;
    }

    /**
     * Calculate an Order total amount
     * @param $order_id
     * @return int
     */
    public function total_discounted()
    {
        $items = $this->orderItems()->get();

        $total = 0;
        if ($items) {
            foreach ($items as $item) {
                $total += $item->total_price_discounted;
            }
        }
        return $total;
    }

    /**
     * Calculate an Order total item count
     * @param $order_id
     * @return int
     */
    public function count()
    {
        $items = $this->orderItems()->get();

        $count = 0;
        if ($items) {
            foreach ($items as $item) {
                $count += $item->quantity;
            }
        }
        return $count;
    }

    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function savePromocode($promocode_id)
    {
        $this->promocode_id = $promocode_id;
        $this->save();
    }

    public function toCurrency($price)
    {
        return $price * $this->currency_rate;
    }

    public function toCurrencyFormat($price)
    {
        return to_currency_format($this->toCurrency($price), true);
    }
}
