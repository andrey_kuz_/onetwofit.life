<?php

namespace App\Models;

use App\Traits\Convertable;
use Hassansin\DBCart\Models\CartLine as CartLineBase;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ProductUnavailableExeption;

class CartLine extends Model
{
    use Convertable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart_lines';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cart_id', 'product_id', 'quantity'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * Get the product record associated with the item.
     */
    public function product()
    {
        return $this->belongsTo(config('cart.product_model'), 'product_id');
    }

    /**
     * Get the cart that owns the item.
     */
    public function cart()
    {
        return $this->belongsTo(config('cart.cart_model'), 'cart_id');
    }

    /*
    * Get Item orginal quantity before update
    *
    * @return integer
    */

    public function getOriginalQuantity()
    {
        return $this->original['quantity'];
    }


    /*
    * Get the singleton cart of this line item.
    *
    */
    public function getCartInstance()
    {
        $carts = app('cart_instances');
        foreach ($carts as $name => $cart) {
            if ($cart->id === $this->cart_id) {
                return $cart;
            }
        }
        return null;
    }

    /*
    * Move this item to another cart
    *
    * @param Cart $cart
    */
    public function moveTo(Cart $cart)
    {
        $this->delete(); // delete from own cart
        return $cart->items()->create($this->attributes);
    }

    /*
    * Get Items discounts - discount by quantity | discount by combo offers
    *
    * @return float
    */
    public function getCartItemInfo()
    {
        $discountInfo = $this->product->getDiscountInfo($this->quantity);
        $discountInfo->setComboDiscountAbs($this->getComboDiscount());
        return $discountInfo;
    }


    /**
     *
     */
    public function getComboDiscount()
    {
        // get assoc array with ids as keys and product quantity in cart as value
        $productsCounts = \Illuminate\Support\Facades\Cache::get('cart_products_counts_'.$this->cart_id);

        // get combo offers for product related to other products in cart
        $comboOffers = $this->product->comboOffers(); //->whereIn('related_product_id', array_keys($productsCounts))->get();

        $restQuantity = $this->quantity;
        $discount = 0;
        foreach ($comboOffers as $comboOffer) {
            $tmpQuantity = min($restQuantity, $productsCounts[$comboOffer->related_product_id]);
            $discount += $tmpQuantity * $comboOffer->combo_discount_abs;

            $restQuantity = $restQuantity - $tmpQuantity;

            if ($restQuantity == 0) {
                break;
            }
        }
        return $discount;
    }
}
