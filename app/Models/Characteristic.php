<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Characteristic extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class,'characteristic_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Characteristic::class,'characteristic_id');
    }

}
