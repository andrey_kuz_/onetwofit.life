<?php

namespace App\Models;

use App\Traits\Translatable;
//use TCG\Voyager\Traits\Translatable;
use TCG\Voyager\Traits\HasRelationships;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Database\Eloquent\Model;

use App\Models\Product;
use App\Models\Brand;
use App\Models\CategoryProduct;

class Category extends Model
{
    use Translatable,
        HasRelationships;

    public $products_count;
    protected $translatable = ['name'];
    protected $fillable = ['id', 'name', 'parent_id', 'order', 'products_count', 'is_active'];

    public function parentId()
    {
        return $this->belongsTo(self::class);
    }

    public function products()
    {
        return $this->belongsToMany(
            'App\Models\Product',
            'categories_products'
        )->active();
    }

    public function productsActive()
    {
        return $this->belongsToMany(
            'App\Models\Product',
            'categories_products'
        )->active();
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id', 'id');
    }

    public function parentRoot()
    {

        return $this->parentRecursive($this);
    }

    public function parentRecursive($cat)
    {
        $parent = $cat->parent;
        if (!$parent) {
            return $cat;
        } else {
            return $this->parentRecursive($parent);
        }
    }

    public function subcategories()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id')->with('subcategories', 'translations');

    }

    public function subcategoriesActive()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id')->with('subcategoriesActive', 'translations')->active();

    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function getSubIds()
    {
        return $this->getSubIdsByIds(array($this->id));
    }

    public function getSubIdsById($id, $allIds = null)
    {
        $subIds = array();
        $subIds = DB::table('categories')->where('parent_id', $id)->pluck('id')->toArray();
        if (empty($subIds)) {
            return $allIds;
        } else {
            $allIds = array_merge($allIds, $subIds);
            foreach ($subIds as $subId) {
                $this->getSubIdsById($subId, $allIds);
            }
        }
    }

    public function getSubIdsByIds($ids, $allIds = array())
    {
        $subIds = array();
        $subIds = DB::table('categories')->whereIN('parent_id', $ids)->pluck('id')->toArray();

        if (empty($subIds)) {
            return $subIds;
        } else {

            return array_merge($this->getSubIdsByIds($subIds), $subIds);
        }


        /*
        $ids = DB::select('SELECT GROUP_CONCAT(lv SEPARATOR ",") as children FROM (SELECT @pv:=(SELECT GROUP_CONCAT(id SEPARATOR ",") FROM categories WHERE parent_id IN (@pv)) AS lv FROM categories JOIN (SELECT @pv:=?)tmp WHERE parent_id IN (@pv)) a;', [$this->id]);
        return explode(',', $ids[0]->children);
        */
    }

    public function getSubProducts()
    {
        return $this->subProducts()->get();
    }

    public function subProducts()
    {
        $productIds = $this->getSubProductsIds();

        return Product::whereIn('id', $productIds)->active();
    }

    public function getSubProductsIds()
    {
        //$subCategoryIds = $this->getSubIdsWithOwn();
        $subCategoryIds = $this->getSubIdsWithOwn();
        // get all products including sub|sub sub .. categories
        return CategoryProduct::getProductIds($subCategoryIds);
    }

    public function getSubIdsWithOwn()
    {
        $ids = $this->getSubIds();
        $ids = array_merge($ids, [$this->id]);
        return $ids;
    }

    public function subBrands()
    {
        /*return $brands = Brand::whereHas('products', function($q){
            $q->whereIN('id', $this->getSubProductsIds());
        });*/

        return $brands = Brand::assignedToProducts($this->getSubProductsIds());
    }


    public function scopeRootLevel($query)
    {
        $query->whereNull('parent_id');
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('order');
    }

    /**
     * get all characteristic of categories
     */
    public function characteristic()
    {
        return $this->belongsToMany(Characteristic::class);
    }

}
