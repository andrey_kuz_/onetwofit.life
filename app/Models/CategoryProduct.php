<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{

    protected $table = 'categories_products';
    protected $guarded = [];
    public $timestamps = false;

    public static function getProductIds($categoryIds)
    {

        return self::whereIn('category_id', $categoryIds)->pluck('product_id')->toArray();
    }

}

