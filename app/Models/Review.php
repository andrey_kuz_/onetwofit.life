<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Product;
use Auth;
use App\Traits\CanBeVoted;

class Review extends Model
{
    use CanBeVoted;

    /**
     * owner of review
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * product of review
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * filter approved only
     *
     * @param $query
     * @return mixed
     */
    public function scopeApproved($query)
    {
        return $query->where('is_approved', true);
    }

    /**
     * filter spam
     *
     * @param $query
     * @return mixed
     */
    public function scopeSpam($query)
    {
        return $query->where('is_spam', true);
    }

    /**
     * filter not spam
     * @param $query
     * @return mixed
     */
    public function scopeNotSpam($query)
    {
        return $query->where('is_spam', false);
    }


    public function storeReviewForProduct($productID, $comment, $rating, $name)
    {
        $product = Product::find($productID);
        if (Auth::user()){
            $this->user_id = Auth::user()->id;
        }

        $this->comment = $comment;
        $this->rating = $rating;
        $this->name = $name;
        $product->reviews()->save($this);

        // recalculate ratings for the specified product
        //$product->recalculateRating($rating);

        return true;
    }
}
