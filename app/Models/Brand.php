<?php

namespace App\Models;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use Translatable;
    protected $translatable = ['name'];
    protected $fillable = ['id', 'name'];

    /**
     * get products belongs to this brand
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * get products belongs to this brand
     */
    public function productsActive()
    {
        return $this->hasMany('App\Models\Product')->active();
    }

    public function scopeAssignedToProducts($query, $product_ids)
    {
        return $query->whereHas('products', function($q) use ($product_ids){
            $q->whereIN('id', $product_ids);
        });
    }
}
