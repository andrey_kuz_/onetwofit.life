<?php

namespace App\Models;

use App\Services\Discount\Discount;
use App\Traits\Convertable;
use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\HasRelationships;
use App\Traits\Favoriteable;
use Auth;
use Config;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use Translatable,
        HasRelationships,
        Favoriteable,
        Convertable;

    protected $translatable = ['name'];
    protected $guarded = [];

    /**
     * get product brand
     *
     * @return  Brand
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }

    /**
     * get product issue form
     *
     * @return  Brand
     */
    public function issueForm()
    {
        return $this->belongsTo('App\Models\IssueForm');
    }


    /**
     * get product flavor
     *
     * @return  Brand
     */
    public function flavor()
    {
        return $this->belongsTo('App\Models\Flavor');
    }


    /**
     * get product country
     *
     * @return  Country
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * get all categories current product belongs to
     *
     * @return  Category Collection
     */
    public function categories()
    {
        return $this->belongsToMany(
            'App\Models\Category',
            'categories_products'
        );
    }

    /**
     * get all categories current product belongs to
     *
     * @return  Category Collection
     */
    public function getFirstCategory()
    {
        return $this->categories()->first();
    }

    /**
     * get product category belongs to from root level
     *
     * @return  Category Collection
     */
    public function getRootCategory()
    {
        return \Cache::remember('parentRootCatForProduct_'.$this->id, 5, function() {
            $first = $this->categories()->first();

            if ($first == null) {
                return null;
            } else {
                return $first->parentRoot();
            }
        });

    }

    /**
     * get id of product category belongs to from root level
     *
     * @return  Category Collection
     */
    public function getRootCategoryId()
    {
        if ($this->getRootCategory() == null) {
            return null;
        } else {
            return $this->getRootCategory()->id;
        }
    }

    /**
     * get all combo offer of current product
     *
     * @return  ComboOffer Collection
     */
    public function comboOffers()
    {
        return $this->hasMany(
            'App\Models\ComboOffer',
            'product_id'
        )->withActiveProducts();
    }

    /**
     * get all reviews of current product
     *
     * @return  Review Collection
     */
    public function reviews()
    {
        return $this->hasMany(
            'App\Models\Review'
        );
    }
    /**
     * get all characteristic of product
     */
    public function characteristic()
    {
        return $this->belongsToMany(Characteristic::class);
    }

    public function characteristic_products()
    {
        return $this->belongsToMany(CharacteristicProduct::class);
    }

    /**
     * get all approved reviews of current product
     *
     * @return  Review Collection
     */
    public function reviewsApproved()
    {
        return $this->reviews()->notSpam()->approved();
    }


    /**
     * @param $rating
     */
    public function recalculateRating()
    {
        $reviews = $this->reviewsApproved();
        $avgRating = $reviews->avg('rating');
        $this->rating = ceil($avgRating);
        $this->rating_count = $reviews->count();
        $this->rating_json = json_encode($reviews->select(DB::raw('count(*) as cnt, rating'))->groupBy('rating')->get()->keyBy('rating'));
        $this->save();
    }


    /**
     * get all combo products connected with product
     *
     * @return  Product Collection
     */
    public function comboProducts()
    {
        return $this->belongsToMany(
            'App\Models\Products',
            'combo_offers',
            'related_product_id',
            'product_id'
        )->active();
    }

    /**
     * get all related combo products with current project
     *
     * @return  Product Collection
     */
    public function comboProductsRelated()
    {
        return $this->belongsToMany(
            'App\Models\Products',
            'combo_offers',
            'product_id',
            'related_product_id'
        )->active();
    }

    /**
     * @param $limit    number of analog products
     * @return Product Collection
     */
    public function analogProducts($limit)
    {
        $category = $this->categories()->first();
        //dd($category->id);
        if (isset($category)) {
            return $category->products()->with('translations', 'myFavorite')->notThis($this->id)->orderByNewest()->take($limit)->get(); //->inRandomOrder()
        }
        //->where('brand_id', '<>', $this->brand_id)
    }

    public function scopeByString($query, $str)
    {

        return $query->whereHas('translations', function ($query) use ($str) {
            return $query->where('translations.value', 'like', '%' . $str . '%');
        })->orWhere('name', 'like', '%' . $str . '%');
    }

    public function scopeSortBy($query, $sort_by)
    {
        switch ($sort_by) {
            case 'price_asc':
                return $query->selectRaw('*, price * (100 - discount) / 100 as price_discounted')->orderBy('price_discounted', 'ASC');
                break;
            case 'price_desc':
                return $query->selectRaw('*, price * (100 - discount) / 100 as price_discounted')->orderBy('price_discounted', 'DESC');
                break;
            case 'rating_asc':
                return $query->orderBy('rating', 'ASC');
                break;
            case 'rating_desc':
                return $query->orderBy('rating', 'DESC');
                break;
            case 'newest':
                return $query->orderByNewest('is_new', 'DESC')->orderBy('updated_at', 'DESC');
                break;
        }
    }

    public function scopeNotThis($query, $id = null)
    {
        if (is_null($id)) {
            $id = $this->id;
        }
        return $query->where('id', '<>', $id);
    }

    public function scopeOrderByNewest($query)
    {
        return $query->orderBy('is_new', 'DESC');
    }


    /**
     * get random combo offer for current product
     *
     * @return ComboOffer
     */
    public function getRandomComboOffer()
    {
        return $this->comboOffers()->inRandomOrder()->with('product')->first();
    }


    public function scopeHasBrands($query, $brands)
    {
        return $query->whereIn('brand_id', $brands);
    }

    public function scopePriceTransform($query)
    {
        return $query->select(DB::raw('(price*discount/100) as price_discounted'), 'products.*');
    }

    public function scopePriceRanges($query, $priceRanges)
    {
        return $query->where(function ($query) use ($priceRanges) {
            foreach ($priceRanges as $priceRange) {
                $from = $priceRange['from'];
                $to = $priceRange['to'];
                if (!$from) {
                    $query->orPriceLessThen($to);
                    continue;
                }
                if (!$to) {
                    $query->orPriceMoreThen($from);
                    continue;
                }

                $query->orWhereBetween(DB::raw('ceil((price*(100-discount)/100))'), [$from, $to]);
            }
        });
    }

    public function scopePriceRange($query, $from = 0, $to = 0)
    {
        if (!$from) {
            return $this->scopePriceLessThen($query, $to);
        }
        if (!$to) {
            return $this->scopePriceMoreThen($query, $from);
        }
        return $query->whereBetween(DB::raw('floor((price*(100-discount)/100))'), [$from, $to]);
    }

    public function scopePriceMoreThen($query, $price)
    {
        return $query->where(DB::raw('ceil((price*(100-discount)/100))'), '>=', $price);
    }

    public function scopeOrPriceMoreThen($query, $price)
    {
        return $query->orWhere(DB::raw('(price*(100-discount)/100)'), '>=', $price);
    }

    public function scopePriceLessThen($query, $price)
    {
        return $query->where(DB::raw('(price*(100-discount)/100)'), '<=', $price);
    }

    public function scopeOrPriceLessThen($query, $price)
    {
        return $query->orWhere(DB::raw('(price*(100-discount)/100)'), '<=', $price);
    }

    /**
     * Scope a query, order prodcuts by order field
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('order');
    }

    /**
     * scope active
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    /**
     * scope main
     * @param $query
     * @return mixed
     */

    public function scopeMain($query)
    {
        return $query->where('is_main', true);
    }

    /**
     * scope notFinished
     * @param $query
     * @return mixed
     */
    public function scopeNotFinished($query)
    {
        return $query->where('is_finished', false);
    }

    /**
     * reverse scope enabled by rest quantity, filters products with zero quantity or finished flag
     *
     * @param $query
     * @return mixed
     */
    public function scopeFinished($query)
    {
        return $query->orWhere('is_finished', true);
    }

    /**
     * scope enabled by rest quantity, filter all available products
     *
     * @param $query
     * @return mixed
     */
    public function scopeAvailable($query)
    {
        return $query->where('quantity', '>', 0)->where('is_finished', false);
    }


    /**
     * scope seller hits products
     *
     * @param $query
     * @return mixed
     */
    public function scopeHit($query)
    {
        return $query->where('is_hit', true);
    }

    public function scopeNotHit($query)
    {
        return $query->where('is_hit', false);
    }

    /**
     * scope bestsellers
     *
     * @param $query
     * @return mixed
     */
    public function scopeBestseller($query)
    {
        return $query->where('is_bestseller', true)->active();
    }

    public function scopeNotBestseller($query)
    {
        return $query->where('is_bestseller', false)->active();
    }

    /**
     * scope pruducts manualy discounted bcs of some deffect
     *
     * @param $query
     * @return mixed
     */
    public function scopeSpecial($query)
    {
        return $query->where('is_special', true)->active();
    }

    /**
     * scope pruducts manualy discounted bcs of some deffect
     *
     * @param $query
     * @return mixed
     */
    public function scopeDiscounted($query)
    {
        return $query->where('discount', '>', 0)->active();
    }

    /**
     * scope new products
     *
     * @param $query
     * @return mixed
     */
    public function scopeNew($query)
    {
        return $query->where('is_new', true)->active();
    }

    public function scopeNotNew($query)
    {
        return $query->where('is_new', false)->active();
    }

    public function checkActive()
    {
        return $this->is_active;
    }

    public function checkAvailable($quantity = 1)
    {
        if ($this->checkActive()) { // && ($this->quantity >= $quantity) && (!$this->is_finished)) {
            return true;
        }
        return false;
    }

    /**
     * get main product image
     *
     * @return  string  image path
     */
    public function getMainImg()
    {

        $images = json_decode($this->image);
        if ($images) {
            return array_shift($images);
        }
        return false;
    }

    /*
     * get product images except main
     *
     * @return   array   images paths
     */
    public function getOtherImg()
    {
        $images = json_decode($this->image);
        if ($images) {
            array_shift($images);
            return $images;
        }

        return false;
    }

    /*
     * get tags related to product
     *
     * @return Tag Collection
     */
    public function tags()
    {
        return $this->belongsToMany(
            'App\Models\Tag',
            'products_tags'
        )->with('translations');
    }

    /**
     * accessor for initial price attribute
     * @return string
     */
    public function getInitialPriceAttribute()
    {
        return $this->getOriginal('price');
    }


    /**
     * discounted price attribute
     * @return string
     */
    public function getPriceDiscountedAttribute()
    {
        return ($this->discount > 0) ? $this->calcDiscountedPrice($this->price, $this->discount) : $this->roundNumber($this->price);
    }

    /**
     * accessor for initial price attribute
     * @return string
     */
    public function getPriceDiscountedConvertedAttribute()
    {
        $str = $this->convertPrice($this->price_discounted);
        $number = '';
        $string = '';
        for($i=0; $i<strlen($str); $i++)
        {
            is_numeric($str[$i]) ? $number .= $str[$i] : $string .= $str[$i];
        }
        $price = [
            'value' =>$number,
            'currency' => $string
        ];
        return $price;
    }


    /**
     * accessor for price attribute converted to current currency
     * @return string
     */
    public function getPriceConvertedAttribute()
    {
        return $this->convertPrice($this->price);
    }


    /**
     * get discounted price
     *
     * @param bool $convert convert to current currency or not
     * @return float|int
     */
    public function getPrice($convert = true)
    {
        return $convert ? $this->convertPrice($this->price_discounted) : $this->roundNumber($this->price_discounted);
    }

    /**
     * calculate price according to discount
     *
     * @param $price price need to be calculated
     * @param $discount number of percentages
     * @return float|int
     */
    public function calcDiscountedPrice($price, $discount)
    {
        return $this->roundNumber($price * (100 - $discount) / 100);
    }

    /**
     * calculate discount abs value according to discount
     *
     * @param $price price need to be calculated
     * @param $discount number of percentages
     * @return float|int
     */
    public function calcDiscountAbs($price, $discount)
    {
        return $this->roundNumber($price * ($discount) / 100);
    }


    /**
     * get prices for special offer, when byuing several items
     *
     * @param  bool $convert convert to selected on site currency or not
     * @param  bool $all if  true returns all special prices from 2 to 10, if false - only stored in db f.e ['2' => ..., '5' => ...]
     * @return array special prices where key is product count and value is an object with
     *                  sum         total price for i products
     *                  discount    discount for i products
     *                  item price  one-item price when buying i products
     * / return array of special prices
     */
    public function getAvailableSpecialPricesInfo($convert = true, $all = false)
    {
        $specialPrices = array();

        $discount = 0;
        for ($i = 1; $i <= 10; $i++) {
            $discountField = 'discount' . $i;
            if ($this->$discountField) {
                $discount = $this->$discountField;
            } else {
                if (!$all) {
                    continue;
                }
            }
            $specialPrices[$i] = $this->getSpecialPriceInfo($i, $convert);
        }
        return $specialPrices;
    }

    public function getDiscountInfo($productsCount)
    {
        $discount = $this->getQuantityDiscountValue($productsCount);
        return new Discount($this->price_discounted, $discount, $productsCount);
    }

    /**
     * get price for specioal offer, when byuing several items
     *
     * @param  int $productsCount
     * @param  bool $convert
     * @return object special price according to products count
     */
    public function getSpecialPriceInfo($productsCount, $convert = true)
    {
        $discount = $this->getQuantityDiscountValue($productsCount);
        return new Discount($this->price_discounted, $discount, $productsCount);
    }


    /**
     * get 1 item product price of special offer, when byuing several items
     *
     * @param  int $productsCount
     * @param  bool $convert
     * @return decimal special price according to products count
     */
    public function getSpecialPrice($productsCount = 2, $convert = true)
    {
        return $this->getSpecialPriceInfo($productsCount, $convert)->discountedItemPrice;
    }


    /**
     * get 1 item product price of special offer, when byuing several items
     *
     * @param  int $productsCount
     * @param  bool $convert
     * @return decimal special price according to products count
     */
    public function getSummarySpecialPrice($productsCount = 2, $convert = true)
    {
        return $this->getSpecialPriceInfo($productsCount, $convert)->discountedTotalPrice;
    }

    public function getMaxAvailableSpecialOffer($productsCount)
    {
        $max = 0;
        for ($i = 2; $i <= min($productsCount, 10); $i++) {
            $priceField = 'discount' . $i;
            if ($this->$priceField) {
                $max = $i;
            }
        }
        return $max;
    }

    public function getQuantityDiscountValue($productsCount)
    {
        $discount = 0;
        for ($i = 2; $i <= min($productsCount, 10); $i++) {
            $discountField = 'discount' . $i;
            if ($this->$discountField) {
                $discount = $this->$discountField;
            }
        }
        return $discount;

    }


    /**
     * get product parameters
     *
     * @return object
     */
    public function getParameters()
    {
        $params = array();
        $params['exp_date'] = $this->exp_date;
        $params['brand'] = $this->brand_id ? (isset($this->brand->name) ? $this->brand->name : '') : '';
        $params['country'] = $this->country_id ? $this->country->getTranslatedAttribute('name') : '';
        $params['pack_pieces'] = $this->pack_pieces ?: '';
        $params['volume'] = $this->volume ? $this->formatVolume() : '';
        $params['weight'] = $this->weight ? $this->formatWeight() : '';
        $params['issue_form'] = $this->issue_form_id ? $this->issueForm->getTranslatedAttribute('name') : '';
        $params['flavor'] = $this->flavor_id ? $this->flavor->getTranslatedAttribute('name') : '';
        return (object)$params;
    }

    public function formatVolume()
    {
        return floatval($this->volume) . ' ' . __('messages.dim_' . $this->volume_dim);
    }

    public function formatWeight()
    {
        return floatval($this->weight) . ' ' . __('messages.dim_' . $this->weight_dim);
    }

    public function addToViewed($user_id = null, $session, $limit)
    {
        $session = isset($session) ? $session : session()->getId();
        $item = Viewed::where('session', $session)->where('product_id', $this->id)->get()->first();

        if ($item) {
            $item->touch();
        } else {
            $item = new Viewed();
            $item->user_id = $user_id;
            $item->session = $session;
            $item->product_id = $this->id;
            $item->save();
        }

        try {
            $updated = Viewed::where('session', $session)->orderBy('updated_at', 'desc')->offset($limit)->limit(1)->first()->updated_at;

            // left in db only $limit number of recent records
            Viewed::where('session', $session)->where('updated_at', '<', $updated)->delete();
        } catch (\Exception $e) {
        }
    }

}


