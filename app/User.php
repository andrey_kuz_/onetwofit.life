<?php

namespace App;

use App\Traits\CanVote;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use ChristianKuri\LaravelFavorite\Traits\Favoriteability;
use App\Traits\Promocodable;
use ChristianKuri\LaravelFavorite\Models\Favorite;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable,
        Favoriteability,
        Promocodable,
        CanVote;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'provider', 'provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function favoriteProducts()
    {
        return $this->belongsToMany(
            'App\Models\Product',
            'favorites',
            'user_id',
            'favoriteable_id'
        )->active();
    }

    public function orders()
    {
        return $this->hasMany(
            'App\Models\Order'
        );
    }


}
