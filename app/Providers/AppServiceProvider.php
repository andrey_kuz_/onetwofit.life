<?php

namespace App\Providers;

use App\Http\Controllers\Admin\ComboOfferController;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\Product;
use App\Models\CartLine;
use App\Models\Tag;
use App\Observers\CartObserver;
use App\Observers\CartLineObserver;
use ChristianKuri\LaravelFavorite\Models\Favorite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Category;
use View;
use Validator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        if (env('APP_ENV') === 'production') {
            \URL::forceScheme('https');
        }

        Schema::defaultStringLength(191);

        try {
            config([
                'app.name' => setting('site.title'),
                'services.facebook.client_id' => setting('site.FACEBOOK_ID'),
                'services.facebook.client_secret' => setting('site.FACEBOOK_SECRET'),
                'services.facebook.redirect' => setting('site.FACEBOOK_URL'),
                'services.google.client_id' => setting('site.GOOGLE_APP_ID'),
                'services.google.client_secret' => setting('site.GOOGLE_APP_SECRET'),
                'services.google.redirect' => setting('site.GOOGLE_REDIRECT'),
                'services.vkontakte.client_id' => setting('site.VKONTAKTE_ID'),
                'services.vkontakte.client_secret' => setting('site.VKONTAKTE_SECRET'),
                'services.vkontakte.redirect' => setting('site.VKONTAKTE_URL'),
            ]);


        } catch (\Exception $e) {

        }
        View::composer('menu.*', function ($view) {
            //\Cache::forget('categories_tree');
            $categories = \Cache::remember('categories_tree', 1, function () {
                return get_categories_tree();
            });

            $favorite_count = 0;
            $brands = Brand::all();
            $tags = Tag::all();
            $presents = get_categories_tree_gift_box();
            $view->with(compact('categories', 'brands', 'tags','presents'));
        });

        View::composer('*', function ($view) {
            $view->with('cartCount', Cart::current()->getItemsCount());//
        });

        View::composer('layouts.topmenu', function ($view) {
            $favorite_count = 0;
            $user = Auth::user();
            if($user) {
                $favorite_count = $user->favoriteProducts()->count();
            }
            $view->with(compact('favorite_count'));
        });

        View::composer('products.index', function ($view) {
           // dd(session()->all());
            if(Session::has('compareitems')) {
                $compareProductsIds = session('compareitems');
                $compare_count = Product::whereIn('id', $compareProductsIds)->with(['flavor', 'country', 'issueForm', 'brand', 'translations'])->count();
            } else {
                $compare_count = '';
            }

            $view->with(compact('compare_count'));
        });

        View::composer('layouts.phones.*', function ($view) {
            $view->with('phones', explode(';', setting('site.phoneNumbers')));//
        });

        CartLine::observe(CartLineObserver::class);
        Cart::observe(CartObserver::class);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
