<?php

namespace App\Providers;

use App\Models\ComboOffer;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app['validator']->extend('combo_unique', function($attribute, $value, $parameters)
        {
            $count = ComboOffer::whereRaw('product_id = ? AND related_product_id = ? OR product_id = ? AND related_product_id = ?', [request('product_id'), request('related_product_id'), request('related_product_id'), request('product_id')])->count();

            if ($count > 0) {
                return false;
            }
            else {
                return true;
            }
        });

        $this->app['validator']->extend('combo_unique', function($attribute, $value, $parameters)
        {
            $count = ComboOffer::whereRaw('product_id = ? AND related_product_id = ? OR product_id = ? AND related_product_id = ?', [request('product_id'), request('related_product_id'), request('related_product_id'), request('product_id')])->count();

            if ($count > 0) {
                return false;
            }
            else {
                return true;
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
