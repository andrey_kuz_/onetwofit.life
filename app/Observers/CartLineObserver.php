<?php

namespace App\Observers;
use App\Models\CartLine;
use App\Exceptions\ProductUnavailableExeption;

class CartLineObserver
{
    /*
    public function retrieved(CartLine $line)
    {
        if ($line->product) {
            if (!$line->product->checkAvailable()) {
                $line->delete();
                //return false;
            }
            if (!$line->product->checkAvailable($line->quantity)) {
                //set max available quantity of product
                $line->update(['quantity' => $line->product->quantity]);
                //return false;
            }
        }
        else{
            $line->delete();
            //return false;
        }
        //return true;
    }*/

    public function saving(CartLine $line)
    {
        if (!$line->product->checkAvailable()) {
            $line->delete();
            throw new ProductUnavailableExeption(__('messages.product_not_avail'));
        }
        if (!$line->product->checkAvailable($line->quantity)) {
            $line->update(['quantity' => $line->product->quantity]);
            throw new ProductUnavailableExeption(__('messages.product_not_avail_quant'));
        }
    }

    //when an item is created
    public function created(CartLine $line)
    {
        $cart = $line->getCartInstance() ?: $line->cart;
        $cart->resetRelations();

        $cart->item_count = $cart->getItemsCount();
        \Log::info('item_count - '.$cart->item_count);
        //dd($cart->item_count);

        //$cart->refreshCartInfo();
        $cart->save();
        \Cache::store('array')->put('cart_' . $cart->name . '_' .session()->getId(), $cart, 1);

    }

    //when an item is updated
    public function updated(CartLine $line)
    {
        $cart = $line->getCartInstance() ?: $line->cart;
        $cart->resetRelations();

        $cart->item_count = $cart->getItemsCount();
        \Log::info('item_count update - '.$cart->item_count);
        $cart->save();
        \Cache::store('array')->put('cart_' . $cart->name . '_' .session()->getId(), $cart, 1);
    }

    //when item deleted
    public function deleted(CartLine $line)
    {
        $cart = $line->getCartInstance() ?: $line->cart;
        $cart->resetRelations();

        $cart->item_count = $cart->getItemsCount();
        //$cart->refreshCartInfo();

        $cart->save();
        \Cache::put('cart_' . $cart->name . '_' .session()->getId(), $cart, 1);
    }
}