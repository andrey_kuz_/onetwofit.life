<?php
/**
 * Created by PhpStorm.
 * User: urich-dev
 * Date: 07.08.2018
 * Time: 14:39
 */

namespace App\Observers;

use App\Models\Cart;


class CartObserver
{
    public function retrieved(Cart $cart)
    {
        $cart->load('items.product.translations');
        $items = $cart->items; //()->with('product')->get();
        $changed = false;
        foreach ($items as $line) {
            if ($line->product) {
                if (!$line->product->checkAvailable()) {
                    $line->delete();
                    $changed = true;
                }
                if (!$line->product->checkAvailable($line->quantity)) {
                    //set max available quantity of product
                    $line->update(['quantity' => $line->product->quantity]);
                    $changed = true;
                }
            } else {
                $line->delete();
                $changed = true;
            }
        }
        if ($changed) {
            $cart->load('items.product.translations');
        }

        //return true;
    }
}