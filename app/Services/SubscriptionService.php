<?php
namespace App\Services;

use App\Models\Product;
use App\User;
use App\Models\Subscription;
use App\Notifications\SubscriptionNotification;
use App\Mail\SubscriptionMail;
use Mail;

class SubscriptionService
{
    public static function check()
    {
        $subs_products = Subscription::all()->pluck('product_id');

        $i = 0;
        $products = Product::whereIn('id',$subs_products)->notFinished()->pluck('id')->toArray();

        if(count($products)){
            $subs_users = Subscription::whereIn('product_id',$products)->get();
            foreach ($subs_users as $subs_user){
                    $email = $subs_user->email;
                    $product = Product::find($subs_user->product_id);
                    Mail::to($email)->send(new SubscriptionMail($product));
                    Subscription::where('email',$email)->delete();
                    $i++;
            }
        }
        echo "Send ".$i. " email(s)";
    }
}