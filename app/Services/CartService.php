<?php
namespace App\Services;

use App\Models\Product;
use App\Models\Cart;
use App\Models\CartLine;
use App\Models\Promocode;
use App\Exceptions\PromocodeExeption;
use App\Exceptions\AlreadyUsedException;
use App\Exceptions\InvalidPromocodeException;
use App\Exceptions\ExpiredException;
use App\Exceptions\UnauthenticatedException;
use App\Exceptions\ProductUnavailableExeption;
use App\Exceptions\ProductUnavailableQuantityExeption;

class CartService
{
    public $cart;

    public function __construct($cart = null)
    {
        if (is_null($cart))
        {
            $this->cart = Cart::current();
        }
        else{
            $this->cart = $cart;
        }
        //$this->cart->load('items.product');
    }


    /**
     * add product to cart logic (add or update if exist)
     *
     * @param $product_id
     * @param $deltaQuantity
     * @return bool
     */
    public function addProduct($product_id, $deltaQuantity)
    {
        if (is_null($this->cart))
        {
            $this->cart = Cart::current();
        }

        $cartItem = $this->cart->items()->where('product_id', $product_id)->first();

        if (!$cartItem) {
            // if quantity > 0 : add new item
            if ($deltaQuantity > 0) {
                if ($this->cart->addItem([
                    'product_id' => $product_id,
                    'quantity' => $deltaQuantity
                ])
                ) {
                    //\Log::info('item_count after add - '.$this->cart->item_count);

                    return true;
                }
            }
            return false;
            // if quantity < 0 or 0 : do nothing
        } else {
            $cartItem->quantity += $deltaQuantity;
            $cartItem->save();
            //\Log::info('item_count after update - '.$this->cart->item_count);
            return true;
        }
        return false;
    }

    /**
     * decrement product quantity in cart or delete it from cart if total quantity = 0
     *
     * @param $product_id
     * @return bool
     */
    public function subProduct($product_id)
    {
        if (is_null($this->cart))
        {
            $this->cart = Cart::current();
        }

        //$product = Product::findOrFail($product_id);

        $cartItem = $this->cart->items()->where(
            'product_id', '=', $product_id
        )->first();

        if ($cartItem) {
            $quantity = $cartItem->quantity - 1;
            $quantity = $quantity > 0 ? $quantity : 0;

            // if summary count == 0 - remove product item from cart at all
            if ($quantity == 0) {
                if ($cartItem->delete()) {
                //if ($this->cart->removeItem(['id' => $cartItem->id])) {
                    return true;
                }
            } else {
                $cartItem->quantity = $quantity;
                $cartItem->save();
                return true;
            }
        }
        return false;
    }

    /**
     * remove product from cart
     *
     * @param $product_id
     * @return bool
     */
    public function removeProduct($product_id)
    {
        if (is_null($this->cart))
        {
            $this->cart = Cart::current();
        }

        $product = Product::findOrFail($product_id);

        $cartItem = $this->cart->items()->where([
            ['product_id', '=', $product->id],
            ['cart_id', '=', $this->cart->id],
        ])->first();

        if ($cartItem) {
            if ($this->cart->removeItem([
                'id' => $cartItem->id
            ])
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * update cart with new promocode
     *
     * @param $code
     * @return mixed|null
     */
    public function updateCartWithPromocode($code)
    {
        if (is_null($this->cart))
        {
            $this->cart = Cart::current();
        }
        $this->cart = Cart::current();
        $this->cart->promocode = $code;
        $this->cart->save();
        $this->cart->refreshCartInfo();
        return $this->cart;
    }
}