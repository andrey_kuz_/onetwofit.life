<?php
namespace App\Services;

use App\Exceptions\PromocodeExeption;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use Config;
use Auth;
use App\Models\Cart;
use Illuminate\Support\Facades\DB;
use Corcel\Model\Post;
use Corcel\Model\Taxonomy;


class BlogService
{
    private $blogUrl = '';
    private $blogCategoriesUrl = '/blog//wp-json/wp/v2/categories';

    public function __construct()
    {
        $this->blogUrl = env('APP_BLOG_URL', env('APP_URL'));
    }

    public function getLastPosts($postsCount)
    {
        return Post::published()->where('post_type', 'post')->orderBy('post_date', 'desc')->paginate($postsCount);
    }

    public function getLastRecipesPosts($postsCount)
    {
        return Taxonomy::category()->slug('recepty')->first()->posts->take($postsCount);
    }

    public function getCategories()
    {
        return file_get_contents($this->blogUrl.$this->blogCategoriesUrl);
    }

    public function getCategoriesNames()
    {
        $categories = json_decode($this->getCategories());
        $categoriesByIds = array();
        foreach ($categories as $category)
        {
            $categoriesByIds[$category->id] = $category->name;
        }
        return $categoriesByIds;
    }
}
