<?php
namespace App\Services;

use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Promocode;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StatService
{
    /**
     * get product with max sells during last 24 hours, but prefferably get products manually marked as hits
     *
     * @return mixed
     */
    public function getHits()
    {
        $maxCount = setting('site.hitsProductsCount');
        $hitsManualQuery = Product::active()->hit()->main()->notFinished()->notNew()->notBestseller()->limit($maxCount);
        $hitsManualCount = $hitsManualQuery->count();

       /* if ($hitsManualCount < $maxCount) {
            $hitsActualIds = OrderItem::where('created_at', '>', Carbon::now()->subDay())
                ->select('product_id', DB::raw('count(*) as product_sells'))
                ->whereHas('product', function ($query) {
                    return $query->active()->main();
                })
                ->orderBy('product_sells', 'DESC')
                ->groupBy('product_id')
                ->limit($maxCount)->pluck('product_id')->toArray();

            $hitsQuery = $hitsManualQuery
                ->orWhereIn('id', $hitsActualIds)
                ->orderBy('is_bestseller', 'DESC');
        } else { */
            $hitsQuery = $hitsManualQuery;
     //   }

        $hits = $hitsQuery->with(
            'myFavorite',
            'translations'
        )->get();

        return $hits;
    }
    /**
     * get products with discount
     */
    public function getDiscount()
    {
        $maxCount = setting('site.hitsProductsCount');
        $discountsManualQuery = Product::active()->main()->discounted()->limit($maxCount);
        $discountsQuery = $discountsManualQuery;
        $discounts = $discountsQuery->with(
            'myFavorite',
            'translations'
        )->get();

        return $discounts;
    }

    /**
     * get products with max sells during last year, but prefferably get products manually marked as bestsellers
     *
     * @return mixed
     */
    public function getBestsellers()
    {
        $maxCount = setting('site.hitsProductsCount');
        $bestsellersManualQuery = Product::active()->bestseller()->main()->notFinished()->notNew()->notHit()->limit($maxCount);
        $bestsellersManualCount = $bestsellersManualQuery->count();

      /*  if ($bestsellersManualCount < $maxCount) {
            $bestsellersActualIds = OrderItem::where('created_at', '>', Carbon::now()->subYear())
                ->select('product_id', DB::raw('count(*) as product_sells'))
                ->whereHas('product', function ($query) {
                    return $query->active()->main();
                })
                ->orderBy('product_sells', 'DESC')
                ->groupBy('product_id')
                ->limit($maxCount)->pluck('product_id')->toArray();

            $bestsellersQuery = $bestsellersManualQuery
                ->orWhereIn('id', $bestsellersActualIds)
                ->orderBy('is_bestseller', 'DESC');

        } else { */
            $bestsellersQuery = $bestsellersManualQuery;
       // }

        $bestsellers = $bestsellersQuery->with(
            'myFavorite',
            'translations'
        )->get();

        return $bestsellers;
    }

    public function promocodesStat()
    {
        return  DB::table('promocodes')
            ->join('promocode_used', 'promocode_used.promocode_id', '=', 'promocodes.id')
            ->join('orders', 'promocode_used.order_id', '=', 'orders.id')
            ->select(
                'promocode_used.promocode_id',
                DB::raw('count(*) as orders_count'),
                DB::raw('sum(item_count) as orders_item_count'),
                DB::raw('sum(total_price_discounted) as orders_total_sum'),
                DB::raw('sum(total_price_discounted_without_promo) as  orders_total_sum_without_promo'))
            ->groupBy('promocode_used.promocode_id')
            ->get();
    }

    public static function buyTogether($id)
    {
        $orders = OrderItem::where('product_id',$id)->pluck('order_id')->ToArray();
        $buyTogethers = OrderItem::selectRaw('product_id ,COUNT(product_id) as product_id_count')
        ->whereIN('order_id',$orders)
        ->where('product_id','!=',$id)
        ->groupBy('product_id')->orderBy('product_id_count', 'desc')->paginate(5);

        return $buyTogethers;
    }

}