<?php
/**
 * Created by PhpStorm.
 * User: urich-dev
 * Date: 18.07.2018
 * Time: 10:29
 */

namespace App\Services;

use App\Models\Brand;
use App\Models\CategoryProduct;
use App\Models\CharacteristicProduct;
use DB;
use App\Models\Category;


class FilterService
{

    public function filter($productsScope, $filters )
    {
        $products = array();
        $products_in = array();
        $characters = array();

        try {
            // apply filters
            if (!empty($filters['categories']) && is_array($filters['categories'])) {
                $categories = $filters['categories'];
                $products_categories = array();
                foreach ($categories as $category) {
                    $category = Category::findOrFail($category);
                    $products_categories[] = $category->subProducts()->pluck('id')->toArray();
                }
                $products_categories = array_unique(array_merge(...$products_categories));
                $productsScope = $productsScope->whereIn('id',$products_categories);
            } elseif (!empty($filters['categories'])) {
                $array_categories = explode(",", $filters['categories']);
                $categories = $array_categories;
                $products_categories = array();
                foreach ($categories as $category) {
                    $category = Category::findOrFail($category);
                    $products_categories[] = $category->subProducts()->pluck('id')->toArray();
                }
                $products_categories = array_unique(array_merge(...$products_categories));
                $productsScope = $productsScope->whereIn('id',$products_categories);
            }


            if (!empty($filters['brands']) && is_array($filters['brands'])) {
                $productsScope = $productsScope->hasBrands($filters['brands']);
            } elseif (!empty($filters['brands'])) {
                $array_brands = explode(",", $filters['brands']);
                $brands_find = Brand::whereIN('slug', $array_brands)->pluck('id');
                $productsScope = $productsScope->hasBrands($brands_find);
            }

            $special_types = [];
            if(isset($filters['is_hit'])) {
                $special_types[] = 'is_hit';
            }
            if(isset($filters['is_bestseller'])) {
                $special_types[] = 'is_bestseller';
            }
            if(isset($filters['is_new'])) {
                $special_types[] = 'is_new';
            }
            if(isset($filters['is_special'])) {
                $special_types[] = 'discount';
            }

            if (!empty($special_types) && is_array($special_types)) {

                $productsScope->where(function ($query) use ($special_types){
                    foreach ($special_types as $v){

                        if($v == 'discount') {
                            $query->orWhere('discount', '>', 0);
                        } else {
                            $query->orWhere($v, true);
                        }
                    }
                });
            }

            if (isset($filters['sort_by'])) {
                $productsScope->sortBy($filters['sort_by']);
            } else {
                $productsScope->sortBy('rating_desc');
            }


            if (isset($filters['price_ranges']) && !empty($filters['price_ranges'])) {

                if (!is_array($filters['price_ranges'])) {
                    $priceRanges = explode(",", $filters['price_ranges']);
                } else {
                    $priceRanges = $filters['price_ranges'];
                }

                $priceRangesConverted = array();
                foreach ($priceRanges as $key => $priceRange) {

                    list($from, $to) = explode('-', $priceRange);
                    $priceRangesConverted[$key] = [
                        'from' => $from ? to_currency_reverse($from) : false,
                        'to' => $to ? to_currency_reverse($to) : false
                    ];

                }

                $productsScope->priceRanges($priceRangesConverted);
            }
            if(isset($filters['character'])) {
                if (!is_array($filters['character'])) {
                    $characters = explode(",", $filters['character']);
                } else {
                    $characters = ($filters['character']);
                }
                foreach ($characters as $character) {
                    if (strpos($character, ':-:')) {
                        list($cat, $value) = explode(':-:', $character);
                        $products = CharacteristicProduct::where('characteristic_id', $cat)->where('value', $value)->pluck('product_id');
                    } else {
                        list($cat, $min, $max) = explode('-', $character);
                        //$products = CharacteristicProduct::where('characteristic_id', $cat)->whereBetween('value', [intval($min), intval($max)])->pluck('product_id');
                        $products = CharacteristicProduct::where('characteristic_id', $cat)->where(function ($query) use ($min, $max){
                                    $query->orWhere('value','>', intval($min))->Where('value','<', intval($max));
                         })->pluck('product_id');
                   }
                    foreach ($products as $product) {
                        $products_in[] = $product;
                    }

                }

            }

            if (count($characters)) {
                $productsScope->whereIN('id',$products_in);
            }


            if (!empty($filters['price_from']) || !empty($filters['price_to'])) {
                $productsScope = $productsScope->priceRange($filters['price_from'] ? to_currency_reverse($filters['price_from']) : false, $filters['price_to'] ? to_currency_reverse($filters['price_to']) : false);
            }

            return $productsScope;
        }
        catch (Exception $e){
            return $productsScope;
        }
    }
}