<?php
namespace App\Services;

class BreadcrumbService
{
    public static function generate(&$breadcrumb_array, $section)
    {
        $breadcrumb_array[] = $section->parent;
        if($section->parent->parent != null) {
            self::generate($breadcrumb_array, $section->parent);
        }
        return $breadcrumb_array;
    }

}