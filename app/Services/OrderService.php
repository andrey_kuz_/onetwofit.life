<?php
namespace App\Services;

use App\Exceptions\PromocodeExeption;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use Config;
use Auth;
use App\Models\Cart;

class OrderService
{
    public $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function createOneClickOrder($name, $phone, $product_id)
    {
        $currencyCode = Config::get('app.currency');
        $currency = app('currency')->getCurrency($currencyCode);
        $product = Product::findOrFail($product_id);
        if ($this->order = $this->order->create([
            'user_id' => Auth::check() ? Auth::user()->id : null,
            'item_count' => 1,
            'is_1click' => true,
            'shipping_phone' => $phone,
            'shipping_name' => $name,
            'currency' => Config::get('app.currency'),
            'currency_rate' => $currency['exchange_rate'],
        ])
        ) {
            if ($this->order->addItem($product_id, $product->price_discounted, 1, $product->price_discounted, 0, 0)) {
                $this->order->refreshOrderInfo();
                return true;
            }
        }
        return false;
    }

    public function renewOrder()
    {
        $order = $this->order;
        $currencyCode = Config::get('app.currency');
        $currency = app('currency')->getCurrency($currencyCode);

        $newOrder = $order->replicate(['status', 'shipping_status', 'total_price', 'discount_by_qnt_combo', 'discount_promo', 'total_price_discounted_without_promo', 'total_price_discounted']);
        $newOrder->currency = $currencyCode;
        $newOrder->currency_rate = $currency['exchange_rate'];

        //$newOrder->status = $newOrder->getDefaultStatus();
        //$newOrder->shipping_status = $newOrder->getDefaultShippingStatus();
        $newOrder->save();
        $this->order = $newOrder;
        return $this->order;
    }

    public function initOrder($data)
    {
        $currencyCode = Config::get('app.currency');
        $currency = app('currency')->getCurrency($currencyCode);
        if ($this->order = $this->order->create([
            'user_id' => Auth::check() ? Auth::id() : null,
            'currency' => $currencyCode,
            'currency_rate' => $currency['exchange_rate'],
            'shipping_name' => $data['shipping_name'].' '.$data['shipping_lastname'],
            'shipping_phone' => $data['shipping_phone'],
            'shipping_email' => $data['shipping_email'],
        ])
        ) {
            $cart = Cart::current();
            $cart->refreshCartInfo();

            $this->moveCartToOrder($cart);
            $this->order->save();
            return $this->order;
        }
        return false;
    }

    /**
     * @param $order_id
     * @param $data
     * @return Order|bool
     */
    public function processOrder($order_id, $data)
    {
        if ($order_id) {
            $this->order = Order::findOrFail($order_id);
        }
        if ($this->order) {
            $this->order->note = $data['note'];
            $this->order->shipping_city = $data['shipping_city'];
            $this->order->shipping_type_id = $data['shipping_type_id'];
            $this->order->shipping_method_id = $data['shipping_method_id'];
            $this->order->shipping_department = isset($data['shipping_department']) ? $data['shipping_department'] : '';
            $this->order->shipping_address = isset($data['shipping_address']) ? $data['shipping_address'] : '';
            $this->order->payment_type_id = $data['payment_type_id'];

            $cart = Cart::current();
            $cart->refreshCartInfo();

            $this->order->orderItems()->delete();
            try {
                if ($cart->promocode) {
                    $this->order->applyCode($cart->promocode);
                }
            } catch (PromocodeExeption $e) {

            }

            $this->moveCartToOrder($cart);

            $this->order->save();
            $cart->complete();

            return $this->order;
        }
        return false;
    }


    /**
     * move cart items and total info to order
     * @param $cart
     */
    private function moveCartToOrder($cart)
    {

        $this->order->item_count = $cart->item_count;
        $this->order->discount_by_qnt_combo = $cart->info->bothDiscountAbs;
        $this->order->total_price = $cart->info->total;
        $this->order->discount_promo = $cart->info->discountPromo;
        $this->order->total_price_discounted_without_promo = $cart->info->discountedWithoutPromo;
        $this->order->total_price_discounted = $cart->info->discounted;

        $cartItems = $cart->items()->get();

        if (!empty($cartItems)) {
            foreach ($cartItems as $cartItem) {
                $this->order->addItem(
                    $cartItem->product_id,
                    $cart->info->cartItemsInfo[$cartItem->id]->getPrice()->total, // total price for items without discount
                    $cartItem->quantity,
                    $cart->info->cartItemsInfo[$cartItem->id]->getDiscountedPrice()->total, // final price for items with discount
                    $cart->info->cartItemsInfo[$cartItem->id]->getQtyDiscountAbs()->total,
                    $cart->info->cartItemsInfo[$cartItem->id]->getComboDiscountAbs()
                );
            }
        }
    }

    /**
     * Get an Order
     *
     * @param $order_id
     *
     * @return mixed
     */
    public function getOrder($order_id)
    {
        $this->order->findOrFail($order_id);
    }
}