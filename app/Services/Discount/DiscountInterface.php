<?php
namespace App\Services\Discount;

interface DiscountInterface
{
    public function discounted();
    public function discountAbs();
}