<?php
namespace App\Services\Discount;

use App\Models\Product;

class ProductDiscountBuilder
{
    public $quantity;
    public $price;
    public $discount;

    public function __construct()
    {
    }

    public function setProduct(Product $product, $quantity = 1)
    {
        if ($quantity > 1)
        {
            $this->setMultiProduct($product, $quantity);
        }
        elseif ($quantity == 1)
        {
            $this->setSingleProduct($product);
        }
        return $this;
    }

    public function setSingleProduct(Product $product)
    {
        $this->initialPrice = $product->initial_price;
        $this->initialDiscount = $product->discount;
        $this->discount = $product->getQuantityDiscountValue($this->quantity);
        $this->price = $product->price;
    }

    public function setMultiProduct(Product $product, $quantity)
    {
        $this->quantity = $quantity;
        $this->discount = $product->getQuantityDiscountValue($this->quantity);
        $this->price = $product->price;
    }

    public function build(): ProductDiscount
    {
        return new ProductDiscount($this);

    }
}