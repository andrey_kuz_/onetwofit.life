<?php
namespace App\Services\Discount;

class ProductDiscount extends AbstractDiscount
{

    public function __construct(ProductDiscountBuilder $builder)
    {
        $this->setDiscount($builder->discount);
        $this->setPrice($builder->price);
        $this->setQuantity($builder->quantity);
    }
}