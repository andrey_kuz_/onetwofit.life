<?php
/**
 * Created by PhpStorm.
 * User: Owner
 * Date: 15.07.2018
 * Time: 1:21
 */

namespace App\Services\Discount;


class Total
{
    public $item;
    public $total;

    public function __construct($item = 0, $quantity = 1)
    {
        $this->item = $item;
        $this->total = $item * $quantity;
    }
}