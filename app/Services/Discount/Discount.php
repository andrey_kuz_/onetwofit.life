<?php
namespace App\Services\Discount;

use App\Services\Discount\DiscountInterface;

class Discount implements DiscountInterface
{
    private $quantity = 1;
    /**
     * discount in percents from 0 to 100
     *
     * @var int
     */
    private $discount;
    private $price;
    private $qtyDiscountAbs;
    private $bothDiscountAbs;
    private $comboDiscountAbs = 0;

    // price minus quantity discount abs value
    private $discountedPrice;

    // price minus both quantity and combo discount abs values
    private $finalDiscountedPrice;

    public function __construct($price, $discount, $quantity = 1)
    {
        $this->quantity = $quantity;
        $this->discount = $discount;
        $this->price = new Total($price, $quantity);
        $this->qtyDiscountAbs = new Total($this->discountAbs(), $quantity);
        $this->discountedPrice = new Total($this->discounted(), $quantity);
    }

    public function discounted()
    {
        // TODO: Implement discounted() method.
        return $this->roundNumber($this->price->item * (100 - $this->discount) / 100);
    }

    public function discountAbs()
    {
        // TODO: Implement discounted() method.
        return $this->roundNumber($this->price->item * $this->discount / 100);
    }


    public function roundNumber($price)
    {
        return round($price, 2);
    }

    /**
     * @param mixed $comboDiscountAbs
     * @return Discount
     */
    public function setComboDiscountAbs($comboDiscountAbs)
    {
        $this->comboDiscountAbs = $comboDiscountAbs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComboDiscountAbs()
    {
        return $this->comboDiscountAbs;
    }

    /**
     * @return Total
     */
    public function getDiscountedPrice(): Total
    {
        return $this->discountedPrice;
    }

    public function getFinalDiscountedPrice()
    {
//        dd($this->discountedPrice->total - $this->comboDiscountAbs);
        return $this->discountedPrice->total - $this->comboDiscountAbs;
    }


    /**
     * @return Total
     */
    public function getQtyDiscountAbs(): Total
    {
        return $this->qtyDiscountAbs;
    }

    /**
     * @return Total
     */
    public function getBothDiscountAbs()
    {
        return ($this->qtyDiscountAbs->total + $this->comboDiscountAbs);
    }

    /**
     * @return Total
     */
    public function getPrice(): Total
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getDiscount(): int
    {
        return $this->discount;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function toCurrency($value){

        $str = to_currency($value, true);
        $number = '';
        $string = '';
        for($i=0; $i<strlen($str); $i++)
        {
            is_numeric($str[$i]) ? $number .= $str[$i] : $string .= $str[$i];
        }
        $price = [
            'value' =>$number,
            'currency' => $string
        ];
        return $price;
    }
}