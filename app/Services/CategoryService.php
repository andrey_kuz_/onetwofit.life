<?php
/**
 * Created by PhpStorm.
 * User: urich-dev
 * Date: 15.08.2018
 * Time: 11:56
 */

namespace App\Services;


class CategoryService
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function getCategoriesTree()
    {
        $categories = $this->category
            ->rootLevel()
            ->with('subcategoriesActive', 'translations')
            ->whereHas('productsActive')
            ->get();
    }

}