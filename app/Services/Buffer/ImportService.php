<?php

namespace App\Services\Buffer;

use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;
use App\Models\Import;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\ProductController;

abstract class ImportService
{
    protected $lastImport;
    public $dbBuffer;
    public $db;
    protected $model;

    public static $importInfo = array();

    const TABLE = '';
    const TRANSLATIONS_TABLE = 'translations';
    const DELETES_TABLE = 'deletes';

    protected $langs = array('ru', 'ua');

    public function __construct($lastImport)
    {
        $this->lastImport = $lastImport;
        $this->dbBuffer = DB::connection('mysql_buffer');
        $this->db = DB::connection('mysql');
    }

    public function getBufferData($table = null)
    {
        if (!$table)
        {
            $table = static::TABLE;
        }
        if ($this->lastImport)
        {
            $items = $this->dbBuffer->table($table)->where('updated_at', '>', $this->lastImport->created_at)->get();
        }
        else{
            $items = $this->dbBuffer->table($table)->get();
        }

        //$resultArray = json_decode(json_encode($items), true);
        return $items;
    }

    public function clearBaseTable()
    {
        $this->db->table(static::TABLE)->delete();
    }

    public function clearTranslationsTable()
    {
        $this->db->table(static::TRANSLATIONS_TABLE)->where('table_name', static::TABLE)->delete();
    }

    public function clearTables()
    {
        $this->clearBaseTable();
        $this->clearTranslationsTable();
    }

    public function exportImportChanges()
    {
        $items = $this->getBufferData();
        $this->importFull($items);
    }

    public function exportImportFull()
    {
        //$this->clearTables();
        $this->lastImport = null;
        $items = $this->getBufferData();
        $this->importFull($items);
    }

    public function importFull($items)
    {
        $itemsIds = array();
        foreach ($items as $item)
        {
            $itemsIds[] = $item->id;
            $attrsDefinitions = array();
            foreach ($this->importable as $importableAttr)
            {
                $attrsDefinitions[$importableAttr] = $item->$importableAttr;
            }

            $itemLocal = ($this->model)::find ($item->id);
            if (!$itemLocal)
            {
                $itemLocal = new $this->model;

            }
	    $itemLocal->fill($attrsDefinitions);

            //name, descr
            $translatableAttrs = $itemLocal->getTranslatableAttributes();

            foreach ($translatableAttrs as $translatableAttr) {
                $attrsTransDefinitions = array();
                foreach ($this->langs as $lang) {
                    $bufferAttr = $translatableAttr.'_'.$lang;
                    $attrsTransDefinitions[$lang] = $item->$bufferAttr;
                }
                $itemLocal->setAttributeTranslations($translatableAttr, $attrsTransDefinitions, true);
            }
            $itemLocal->save();
            /*Update slug (Brand, Categories, Products) */

            if ($this->model =='App\Models\Product'){
                $products = new ProductController();
                $products->updateSlug($item->id);
            }
            if ($this->model =='App\Models\Brand'){
                $brand = new BrandController();
                $brand->updateSlug($item->id);
            }
            if ($this->model =='App\Models\Category'){
                $category = new CategoryController();
                $category->updateSlug($item->id);
            }

            self::$importInfo[static::TABLE]['items'] = $itemsIds;
            self::$importInfo[static::TABLE]['count'] = count($itemsIds);

        }
    }

    public function processDeletes()
    {
        $idsToDelete = $this->dbBuffer->table(static::DELETES_TABLE)->where('type', '=', static::TABLE)->pluck('object_id')->toArray();
        /*
        if ($this->lastImport)
        {
            $idsToDelete = $this->dbBuffer->table(static::DELETES_TABLE)->where('updated_at', '>', $this->lastImport->created_at)->where('type', '=', static::TABLE)->pluck('object_id')->toArray();
        }
        else{
            $idsToDelete = $this->dbBuffer->table(static::DELETES_TABLE)->where('type', '=', static::TABLE)->pluck('object_id')->toArray();
        }
        */


        if ($idsToDelete)
        {
            if ($this->dbBuffer->table(static::TABLE)->whereIn('id', $idsToDelete)->delete())
            {
                self::$importInfo[static::TABLE]['deletes_from_buffer'] = $idsToDelete;
            }
            if ($this->db->table(static::TABLE)->whereIn('id', $idsToDelete)->delete())
            {
                self::$importInfo[static::TABLE]['deletes'] = $idsToDelete;
            }
        }

    }
}