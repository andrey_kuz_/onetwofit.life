<?php
namespace App\Services\Buffer;

use App\Models\Country;

class ImportCountriesService extends ImportService
{
    CONST TABLE = 'countries';

    protected $importable = ['id', 'country_code'];
    protected $translatable = ['name'];

    protected $model = Country::class;

    public function importChanges()
    {
        // TODO: Implement importChanges() method.
    }
}