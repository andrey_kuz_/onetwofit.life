<?php
namespace App\Services\Buffer;

use App\Models\IssueForm;

class ImportIssueFormsService extends ImportService
{
    CONST TABLE = 'issue_forms';

    protected $importable = ['id'];
    protected $translatable = ['name'];

    protected $model = IssueForm::class;

    public function importChanges()
    {
        // TODO: Implement importChanges() method.
    }
}