<?php
namespace App\Services\Buffer;

use App\Models\Category;

class ImportCategoriesService extends ImportService
{
    CONST TABLE = 'categories';

    protected $importable = ['id', 'parent_id', 'order'];
    protected $translatable = ['name'];

    protected $model = Category::class;


}