<?php
namespace App\Services\Buffer;

use App\Models\Brand;

class ImportBrandsService extends ImportService
{
    CONST TABLE = 'brands';

    protected $importable = ['id'];
    protected $translatable = ['name'];

    protected $model = Brand::class;

    public function importChanges()
    {
        // TODO: Implement importChanges() method.
    }
}