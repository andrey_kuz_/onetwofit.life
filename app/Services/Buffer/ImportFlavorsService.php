<?php
namespace App\Services\Buffer;

use App\Models\Flavor;

class ImportFlavorsService extends ImportService
{
    CONST TABLE = 'flavors';

    protected $importable = ['id'];
    protected $translatable = ['name'];

    protected $model = Flavor::class;

    public function importChanges()
    {
        // TODO: Implement importChanges() method.
    }
}