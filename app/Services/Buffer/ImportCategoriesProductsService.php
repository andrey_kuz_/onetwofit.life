<?php
namespace App\Services\Buffer;

use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Product;

class ImportCategoriesProductsService extends ImportService
{
    CONST TABLE = 'categories_products';

    protected $importable = ['category_id', 'product_id'];
    protected $translatable = [];

    protected $model = CategoryProduct::class;

    public function exportImportChanges()
    {
        $this->exportImportFull();
    }

    public function exportImportFull()
    {
        $this->lastImport = null;
        $this->clearBaseTable();
        $items = $this->getBufferData();
        $this->importFull($items);
    }

    public function importFull($items)
    {
        $resultArray = json_decode(json_encode($items), true);

        $resultArray = array_filter($resultArray, function ($item) {
            return (bool) Category::find($item['category_id']) && (bool) Product::find($item['product_id']) && (bool) (!CategoryProduct::where('category_id', $item['category_id'])->where('product_id', $item['product_id'])->first());

        });

        $itemsIds = $resultArray;
        $this->db->table(static::TABLE)->insert($resultArray);

        self::$importInfo[static::TABLE]['items'] = $itemsIds;
        self::$importInfo[static::TABLE]['count'] = count($itemsIds);
    }

}