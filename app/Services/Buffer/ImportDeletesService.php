<?php
/**
 * Created by PhpStorm.
 * User: urich-dev
 * Date: 14.08.2018
 * Time: 11:27
 */

namespace App\Services\Buffer;


class ImportDeletesService extends ImportService
{
    CONST TABLE = 'deletes';

    public function __construct($lastImport)
    {
        $this->lastImport = false;
        $this->dbBuffer = DB::connection('mysql_buffer');
        $this->db = DB::connection('mysql');
    }

    public function getBufferData()
    {
        if ($this->lastImport)
        {
            $items = $this->dbBuffer->table(static::TABLE)->where('updated_at', '>', $this->lastImport->created_at)->get();
        }
        else{
            $items = $this->dbBuffer->table(static::TABLE)->get();
        }

        //$resultArray = json_decode(json_encode($items), true);
        return $items;
    }

    public function processDeletes()
    {
        $items = $this->getBufferData();


    }

}