<?php
/**
 * Created by PhpStorm.
 * User: urich-dev
 * Date: 03.08.2018
 * Time: 12:07
 */

namespace App\Services\Buffer;

use Illuminate\Support\Facades\DB;


class ExportService
{

    protected $dbBuffer;
    protected $db;
    protected $model;

    public static $importInfo = array();

    public function __construct()
    {
        $this->dbBuffer = DB::connection('mysql_buffer');
        $this->db = DB::connection('mysql');
    }

    public function exportOrder($id)
    {
        $this->dbBuffer->table('orders')->where('id', $id)->delete();
        $order = $this->db->table('orders')->where('id', $id)->first();
        $orderArray = (array) $order;
        $this->dbBuffer->table('orders')->where('id', $id)->delete();
        $result = $this->dbBuffer->table('orders')->insert($orderArray);

        self::$importInfo['orders']['item'] = $orderArray;

        return $result;
    }

    public function clearBufferTable($table)
    {
        $this->dbBuffer->table($table)->delete();
    }
}