<?php
namespace App\Services\Buffer;

use App\Models\Product;

class ImportProductsService extends ImportService
{
    CONST TABLE = 'products';

    protected $importable = ['id', 'article', 'price', 'brand_id', 'exp_date', 'volume', 'volume_dim', 'weight', 'weight_dim', 'quantity', 'pack_pieces', 'flavor_id', 'issue_form_id', 'country_id'];
    protected $translatable = ['name'];
    protected $model = Product::class;

    public function importChanges()
    {
        // TODO: Implement importChanges() method.
    }
}