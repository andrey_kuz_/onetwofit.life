<?php
namespace App\Services\Buffer;


class ImportFactory
{
    /**
     * factory for creating import object for different tables
     * @param $table
     * @return mixed
     */
    public static function factory($table)
    {
        $className = 'App\Services\Buffer\Import' . ucfirst($table);
        $object = new $className();
        return $object;
    }
}