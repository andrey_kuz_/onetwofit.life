<?php
/**
 * Created by PhpStorm.
 * User: urich-dev
 * Date: 24.07.2018
 * Time: 12:16
 */

namespace App\Services\Payment;


class PaymentAbsctract
{
    protected $returnUrl;
    protected $callbackUrl;

    /**
     * @return mixed
     */
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }

    /**
     * @param mixed $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $this->returnUrl = $returnUrl;
    }

    /**
     * @return mixed
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

    /**
     * @param mixed $callbackUrl
     */
    public function setCallbackUrl($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
    }

}