<?php
namespace App\Services\Payment;

use App\Models\Payment;
use App\Models\PaymentType;
use App\Models\Order;
use Carbon\Carbon;

class PaymentService
{

    private $paymentType;
    private $gateway;

    /**
     * PaymentService constructor.
     * @param PaymentType $paymentType
     */
    public function __construct(PaymentType $paymentType)
    {
        $this->paymentType = $paymentType;

        if ($this->paymentType->gateway) {
            $this->gateway = PaymentFactory::factory($this->paymentType);
        }
    }

    /**
     * init order payment
     *
     * @param $order
     * @return bool|string
     */
    public function init($order)
    {
        if ($this->gateway)
        {
            // create payment in db
            $order->payment()->create();
            $order->status = Order::STATUS_ACTIVE;
            $order->save();

            $this->gateway->setReturnUrl(route('payment.return_url', ['order' => $order->id]));
            $this->gateway->setCallbackUrl(route('payment.callback', ['ptid' => $this->paymentType->id]));

            if (env('APP_ENV') === 'production') {
                $order_id = $order->id;
            } else {
                $order_id = $order->id . '_dev';
            }
            return $this->gateway->getForm($order_id, ($order->total_price_discounted * $order->currency_rate), $order->currency);
        }
        return false;

    }

    /**
     * callback logic
     *
     * @param $requestArr
     * @return bool
     */
    public function callback($requestArr)
    {
        if ($this->gateway)
        {
            $data = $this->gateway->callback($requestArr);

            if ($data) {
                $payment = Payment::where('order_id', '=', $data['order_id'])->first();
                $payment->data = json_encode($data);
                $payment->status = $data['status'];
                $payment->save();

                $success = $this->gateway->checkPaymentStatus($payment);
                if ($success)
                {
                    $payment->order->status = Order::STATUS_COMPLETED;
                    $payment->order->payment_status = Payment::STATUS_COMPLETED;
                    $payment->order->save();
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * complete order payment
     *
     * @param Order $order
     * @return bool  true if successfull payment
     */
    public function complete(Order $order)
    {
        $payment = $order->payment;

        if ($this->gateway) {
            $success = $this->gateway->checkPaymentStatus($payment);
            if ($success)
            {
                $order->status = Order::STATUS_COMPLETED;
                $order->completed_at = Carbon::now();
                $order->save();
                return true;
            }
        }
        else {
            // if cash payment type
            $order->status = Order::STATUS_COMPLETED;
            $order->completed_at = Carbon::now();
            //var_dump(Carbon::now());
            $order->save();
        }

        return false;
    }

}