<?php
namespace App\Services\Payment;

use App\Models\PaymentType;
use App\Services\Payment\Libs\LiqPay;
use Illuminate\Support\Facades\Log;
use App\Models\Payment;

class PaymentLiqPay extends PaymentAbsctract
{
    private $publicKey;
    private $privateKey;

    private $api;


    /**
     * PaymentLiqPay constructor.
     * @param PaymentType $paymentType
     */
    public function __construct(PaymentType $paymentType)
    {
        $this->publicKey = $paymentType->public_key;
        $this->privateKey = $paymentType->private_key;
        $this->api = new LiqPay($this->publicKey, $this->privateKey);
    }

    /**
     * get form for payment
     *
     * @param $order_id
     * @param $amount
     * @param $currency
     * @param string $descr
     * @return string
     */
    public function getForm($order_id, $amount, $currency, $descr = '')
    {
        $html = $this->api->cnb_form(array(
            'action'         => 'pay',
            'amount'         => $amount,
            'currency'       => $currency,
            'description'    => $descr,
            'result_url'    => $this->returnUrl,
            'server_url'    => $this->callbackUrl,
            'order_id'       => $order_id,
            'sandbox'        => 0,
            'version'        => '3'
        ));
        return $html;
    }


    /**
     * callback logic
     *
     * @param $requestArr
     * @return array|bool
     */
    public function callback($requestArr)
    {
        $sign = $this->api->str_to_sign(
            $this->privateKey .
            $requestArr['data'] .
            $this->privateKey
        );
        Log::info('payment sign:'.$sign);
        Log::info('payment signature:'.$requestArr['signature']);

        $data = $this->api->decode_params($requestArr['data']);

        if ($sign === $requestArr['signature'])
        {
            Log::info('payment sign equel:');
            Log::info('payment data:'.json_encode($data));

            return $data;
        }
        return false;
    }

    public function checkPaymentStatus(Payment $payment)
    {
        if (($payment->status === 'success') || ($payment->status === 'sandbox'))
        {
            return true;
        }
        return false;
    }
}