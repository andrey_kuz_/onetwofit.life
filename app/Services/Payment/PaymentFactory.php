<?php
namespace App\Services\Payment;

use App\Exceptions\BadGatewayException;
use App\Models\PaymentType;

class PaymentFactory
{
    const AVAILABLE_GATEWAYS = array('LiqPay', 'Cash');

    /**
     * factory for payment gateway
     * @param $gateway
     * @return PaymentLiqPay
     * @throws BadGatewayException
     */
    public static function factory(PaymentType $gateway)
    {
        if (in_array($gateway->gateway, SELF::AVAILABLE_GATEWAYS)) {
            $className = 'App\Services\Payment\Payment' . $gateway->gateway;
            $object = new $className($gateway);
            return $object;
        } else {
            throw new BadGatewayException;
        }
    }
}