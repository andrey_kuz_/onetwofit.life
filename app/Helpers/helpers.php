<?php

if (!function_exists('to_currency')) {
    /**
     * Convert given number.
     *
     * @param float  $amount
     * @param string $from
     * @param string $to
     * @param bool   $format
     *
     * @return \Torann\Currency\Currency|string
     */
    function to_currency($amount = null, $format = false)
    {
        if (is_null($amount)) {
            return app('currency');
        }

        if ($format) {

            $amount = app('currency')->convert($amount, 'USD', Config::get('app.currency'), $format);

            return preg_replace('/,\d\d/', '', $amount);
        }

        return $amount;
    }
}

if (!function_exists('to_currency_reverse')) {
    /**
     * Convert given number.
     *
     * @param float  $amount
     * @param string $from
     * @param string $to
     * @param bool   $format
     *
     * @return \Torann\Currency\Currency|string
     */
    function to_currency_reverse($amount = null, $format = false)
    {
        if (is_null($amount)) {
            return app('currency');
        }

        return app('currency')->convert($amount,Config::get('app.currency'), 'USD', $format);
    }
}


if (!function_exists('to_currency_format')) {
    /**
     * Format given number.
     *
     * @param float  $amount
     * @param string $currency
     * @param bool   $include_symbol
     *
     * @return string
     */
    function to_currency_format($amount = null, $include_symbol = true)
    {
        return app('currency')->format($amount, Config::get('app.currency'), $include_symbol);
    }
}

if (!function_exists('get_categories_tree')) {
    /**
     * get the tree of categories
     *
     * @return string
     */
    function get_categories_tree()
    {
        return $categories = \App\Models\Category::rootLevel()
            ->where('categories.id','<>',1739)
            ->with('subcategories', 'translations')
            ->get();
    }

    function get_categories_tree_gift_box()
    {
        return $categories = \App\Models\Category::where('categories.id',1739)
            ->with('subcategories', 'translations')
            ->get();
    }
}
