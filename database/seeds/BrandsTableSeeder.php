<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('brands')->delete();
        
        \DB::table('brands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Natrol',
                'slug' => 'Natrol',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Doctor Best',
                'slug' => 'doctorbest',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => ' Mizon',
                'slug' => 'mizon',
                'created_at' => '2018-07-19 06:30:34',
                'updated_at' => '2018-07-19 06:30:34',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Andalou Naturals',
                'slug' => 'andalou-naturals',
                'created_at' => '2018-07-19 06:30:51',
                'updated_at' => '2018-07-19 06:30:51',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => ' Tony Moly',
                'slug' => 'tony-moly',
                'created_at' => '2018-07-19 06:31:05',
                'updated_at' => '2018-07-19 06:31:05',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Surya Henna',
                'slug' => 'surya-henna',
                'created_at' => '2018-07-19 06:31:21',
                'updated_at' => '2018-07-19 06:31:21',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => ' Neutrogena',
                'slug' => 'neutrogena',
                'created_at' => '2018-07-19 06:31:31',
                'updated_at' => '2018-07-19 06:31:31',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => ' Life Extension',
                'slug' => 'life-extension',
                'created_at' => '2018-07-19 06:31:44',
                'updated_at' => '2018-07-19 06:31:44',
            ),
        ));
        
        
    }
}