<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Красота',
                'slug' => 'Beauty',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-22 06:46:18',
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Спорт укр',
                'slug' => 'sport',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-22 06:46:35',
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Category 1',
                'slug' => 'category-1',
                'created_at' => '2018-07-13 11:31:29',
                'updated_at' => '2018-07-13 11:31:29',
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Category 2',
                'slug' => 'category-2',
                'created_at' => '2018-07-13 11:31:29',
                'updated_at' => '2018-07-13 11:31:29',
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Витамины и минералы',
                'slug' => 'vitaminy-i-mineraly',
                'created_at' => '2018-07-13 12:39:39',
                'updated_at' => '2018-07-13 12:39:39',
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Добавки',
                'slug' => 'dobavki',
                'created_at' => '2018-07-13 12:41:04',
                'updated_at' => '2018-07-13 12:41:04',
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Травы и грибы',
                'slug' => 'travy-i-griby',
                'created_at' => '2018-07-13 12:41:23',
                'updated_at' => '2018-07-13 12:41:23',
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Здоровое питание',
                'slug' => 'zdorovoe-pitanie',
                'created_at' => '2018-07-13 12:41:43',
                'updated_at' => '2018-07-13 12:41:43',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => NULL,
                'order' => 1,
                'name' => 'Спорт и фитнес',
                'slug' => 'sport-i-fitnes',
                'created_at' => '2018-07-13 12:42:20',
                'updated_at' => '2018-07-13 12:42:20',
            ),
            9 => 
            array (
                'id' => 11,
                'parent_id' => 5,
                'order' => 1,
                'name' => 'Активные добавки',
                'slug' => 'aktivnye-dobavki',
                'created_at' => '2018-07-13 13:01:48',
                'updated_at' => '2018-07-13 13:01:48',
            ),
            10 => 
            array (
                'id' => 12,
                'parent_id' => 5,
                'order' => 1,
                'name' => 'Добавки по предназначению',
                'slug' => 'dobavki-po-prednaznacheniyu',
                'created_at' => '2018-07-13 13:02:29',
                'updated_at' => '2018-07-13 13:02:29',
            ),
            11 => 
            array (
                'id' => 13,
                'parent_id' => 11,
                'order' => 1,
                'name' => 'Антивозрастные добавки',
                'slug' => 'antivozrastnye-dobavki',
                'created_at' => '2018-07-13 13:03:39',
                'updated_at' => '2018-07-13 13:03:39',
            ),
            12 => 
            array (
                'id' => 14,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Коэнзим Q10',
                'slug' => 'koenzim-q10',
                'created_at' => '2018-07-13 13:05:07',
                'updated_at' => '2018-07-13 13:05:07',
            ),
            13 => 
            array (
                'id' => 16,
                'parent_id' => 14,
                'order' => 1,
                'name' => 'Коэнзим Q10 + рыбий жир',
                'slug' => 'koenzim-q10-rybij-zhir',
                'created_at' => '2018-07-13 13:06:07',
                'updated_at' => '2018-07-13 13:06:07',
            ),
            14 => 
            array (
                'id' => 17,
                'parent_id' => 14,
                'order' => 1,
                'name' => 'Формулы коэнзим Q10',
                'slug' => 'formuly-koenzim-q10',
                'created_at' => '2018-07-13 13:06:57',
                'updated_at' => '2018-07-13 13:06:57',
            ),
            15 => 
            array (
                'id' => 18,
                'parent_id' => 14,
                'order' => 1,
                'name' => 'Убихинол',
                'slug' => 'ubihinol',
                'created_at' => '2018-07-13 13:07:32',
                'updated_at' => '2018-07-13 13:07:32',
            ),
            16 => 
            array (
                'id' => 19,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Куркумин',
                'slug' => 'kurkumin',
                'created_at' => '2018-07-13 13:08:21',
                'updated_at' => '2018-07-13 13:08:21',
            ),
            17 => 
            array (
                'id' => 20,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Лютеин',
                'slug' => 'lyutein',
                'created_at' => '2018-07-13 13:09:03',
                'updated_at' => '2018-07-13 13:09:03',
            ),
            18 => 
            array (
                'id' => 21,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Альфа-липоевая кислота',
                'slug' => 'al-fa-lipoevaya-kislota',
                'created_at' => '2018-07-13 13:11:18',
                'updated_at' => '2018-07-13 13:11:18',
            ),
            19 => 
            array (
                'id' => 22,
                'parent_id' => 21,
                'order' => 1,
                'name' => 'R-липоевая кислота',
                'slug' => 'r-lipoevaya-kislota',
                'created_at' => '2018-07-13 13:11:48',
                'updated_at' => '2018-07-13 13:11:48',
            ),
            20 => 
            array (
                'id' => 23,
                'parent_id' => 21,
                'order' => 1,
                'name' => 'Другие формулы',
                'slug' => 'drugie-formuly',
                'created_at' => '2018-07-13 13:12:47',
                'updated_at' => '2018-07-13 13:12:47',
            ),
            21 => 
            array (
                'id' => 24,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Антиаксантин',
                'slug' => 'antiaksantin',
                'created_at' => '2018-07-13 13:25:18',
                'updated_at' => '2018-07-13 13:25:18',
            ),
            22 => 
            array (
                'id' => 25,
                'parent_id' => 24,
                'order' => 1,
                'name' => 'Биосатин',
                'slug' => 'biosatin',
                'created_at' => '2018-07-13 13:26:43',
                'updated_at' => '2018-07-13 13:26:43',
            ),
            23 => 
            array (
                'id' => 26,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Ликопин',
                'slug' => 'likopin',
                'created_at' => '2018-07-13 13:27:23',
                'updated_at' => '2018-07-13 13:27:23',
            ),
            24 => 
            array (
                'id' => 27,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Пихногенол',
                'slug' => 'pihnogenol',
                'created_at' => '2018-07-13 13:28:19',
                'updated_at' => '2018-07-13 13:28:19',
            ),
            25 => 
            array (
                'id' => 28,
                'parent_id' => 27,
                'order' => 1,
            'name' => 'Сосновая кора (экстракт)',
                'slug' => 'sosnovaya-kora-ekstrakt',
                'created_at' => '2018-07-13 13:28:52',
                'updated_at' => '2018-07-13 13:28:52',
            ),
            26 => 
            array (
                'id' => 29,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Кверцетин',
                'slug' => 'kvercetin',
                'created_at' => '2018-07-13 13:29:58',
                'updated_at' => '2018-07-13 13:29:58',
            ),
            27 => 
            array (
                'id' => 30,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Рутин',
                'slug' => 'rutin',
                'created_at' => '2018-07-13 13:31:03',
                'updated_at' => '2018-07-13 13:31:03',
            ),
            28 => 
            array (
                'id' => 31,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'Зеленый чай',
                'slug' => 'zelenyj-chaj',
                'created_at' => '2018-07-13 13:31:41',
                'updated_at' => '2018-07-13 13:31:41',
            ),
            29 => 
            array (
                'id' => 32,
                'parent_id' => 13,
                'order' => 1,
                'name' => 'L-глупатион',
                'slug' => 'l-glupation',
                'created_at' => '2018-07-13 13:32:06',
                'updated_at' => '2018-07-13 13:32:06',
            ),
        ));
        
        
    }
}