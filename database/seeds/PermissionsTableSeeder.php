<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_categories',
                'table_name' => 'categories',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_categories',
                'table_name' => 'categories',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_categories',
                'table_name' => 'categories',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_categories',
                'table_name' => 'categories',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_categories',
                'table_name' => 'categories',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_posts',
                'table_name' => 'posts',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_posts',
                'table_name' => 'posts',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_posts',
                'table_name' => 'posts',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_posts',
                'table_name' => 'posts',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_posts',
                'table_name' => 'posts',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'browse_pages',
                'table_name' => 'pages',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'read_pages',
                'table_name' => 'pages',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'edit_pages',
                'table_name' => 'pages',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'add_pages',
                'table_name' => 'pages',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'delete_pages',
                'table_name' => 'pages',
                'created_at' => '2018-06-01 06:36:37',
                'updated_at' => '2018-06-01 06:36:37',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'browse_products',
                'table_name' => 'products',
                'created_at' => '2018-06-07 08:12:52',
                'updated_at' => '2018-06-07 08:12:52',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'read_products',
                'table_name' => 'products',
                'created_at' => '2018-06-07 08:12:52',
                'updated_at' => '2018-06-07 08:12:52',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'edit_products',
                'table_name' => 'products',
                'created_at' => '2018-06-07 08:12:52',
                'updated_at' => '2018-06-07 08:12:52',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'add_products',
                'table_name' => 'products',
                'created_at' => '2018-06-07 08:12:52',
                'updated_at' => '2018-06-07 08:12:52',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'delete_products',
                'table_name' => 'products',
                'created_at' => '2018-06-07 08:12:52',
                'updated_at' => '2018-06-07 08:12:52',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'browse_tags',
                'table_name' => 'tags',
                'created_at' => '2018-06-14 09:34:44',
                'updated_at' => '2018-06-14 09:34:44',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'read_tags',
                'table_name' => 'tags',
                'created_at' => '2018-06-14 09:34:44',
                'updated_at' => '2018-06-14 09:34:44',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'edit_tags',
                'table_name' => 'tags',
                'created_at' => '2018-06-14 09:34:44',
                'updated_at' => '2018-06-14 09:34:44',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'add_tags',
                'table_name' => 'tags',
                'created_at' => '2018-06-14 09:34:44',
                'updated_at' => '2018-06-14 09:34:44',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'delete_tags',
                'table_name' => 'tags',
                'created_at' => '2018-06-14 09:34:44',
                'updated_at' => '2018-06-14 09:34:44',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'browse_currencies',
                'table_name' => 'currencies',
                'created_at' => '2018-06-14 12:22:24',
                'updated_at' => '2018-06-14 12:22:24',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'read_currencies',
                'table_name' => 'currencies',
                'created_at' => '2018-06-14 12:22:24',
                'updated_at' => '2018-06-14 12:22:24',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'edit_currencies',
                'table_name' => 'currencies',
                'created_at' => '2018-06-14 12:22:24',
                'updated_at' => '2018-06-14 12:22:24',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'add_currencies',
                'table_name' => 'currencies',
                'created_at' => '2018-06-14 12:22:24',
                'updated_at' => '2018-06-14 12:22:24',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'delete_currencies',
                'table_name' => 'currencies',
                'created_at' => '2018-06-14 12:22:24',
                'updated_at' => '2018-06-14 12:22:24',
            ),
            55 => 
            array (
                'id' => 61,
                'key' => 'browse_brands',
                'table_name' => 'brands',
                'created_at' => '2018-06-22 07:08:05',
                'updated_at' => '2018-06-22 07:08:05',
            ),
            56 => 
            array (
                'id' => 62,
                'key' => 'read_brands',
                'table_name' => 'brands',
                'created_at' => '2018-06-22 07:08:05',
                'updated_at' => '2018-06-22 07:08:05',
            ),
            57 => 
            array (
                'id' => 63,
                'key' => 'edit_brands',
                'table_name' => 'brands',
                'created_at' => '2018-06-22 07:08:05',
                'updated_at' => '2018-06-22 07:08:05',
            ),
            58 => 
            array (
                'id' => 64,
                'key' => 'add_brands',
                'table_name' => 'brands',
                'created_at' => '2018-06-22 07:08:05',
                'updated_at' => '2018-06-22 07:08:05',
            ),
            59 => 
            array (
                'id' => 65,
                'key' => 'delete_brands',
                'table_name' => 'brands',
                'created_at' => '2018-06-22 07:08:05',
                'updated_at' => '2018-06-22 07:08:05',
            ),
            60 => 
            array (
                'id' => 66,
                'key' => 'browse_countries',
                'table_name' => 'countries',
                'created_at' => '2018-06-25 08:12:23',
                'updated_at' => '2018-06-25 08:12:23',
            ),
            61 => 
            array (
                'id' => 67,
                'key' => 'read_countries',
                'table_name' => 'countries',
                'created_at' => '2018-06-25 08:12:23',
                'updated_at' => '2018-06-25 08:12:23',
            ),
            62 => 
            array (
                'id' => 68,
                'key' => 'edit_countries',
                'table_name' => 'countries',
                'created_at' => '2018-06-25 08:12:23',
                'updated_at' => '2018-06-25 08:12:23',
            ),
            63 => 
            array (
                'id' => 69,
                'key' => 'add_countries',
                'table_name' => 'countries',
                'created_at' => '2018-06-25 08:12:23',
                'updated_at' => '2018-06-25 08:12:23',
            ),
            64 => 
            array (
                'id' => 70,
                'key' => 'delete_countries',
                'table_name' => 'countries',
                'created_at' => '2018-06-25 08:12:23',
                'updated_at' => '2018-06-25 08:12:23',
            ),
            65 => 
            array (
                'id' => 71,
                'key' => 'browse_combo_offers',
                'table_name' => 'combo_offers',
                'created_at' => '2018-06-26 11:54:24',
                'updated_at' => '2018-06-26 11:54:24',
            ),
            66 => 
            array (
                'id' => 72,
                'key' => 'read_combo_offers',
                'table_name' => 'combo_offers',
                'created_at' => '2018-06-26 11:54:24',
                'updated_at' => '2018-06-26 11:54:24',
            ),
            67 => 
            array (
                'id' => 73,
                'key' => 'edit_combo_offers',
                'table_name' => 'combo_offers',
                'created_at' => '2018-06-26 11:54:24',
                'updated_at' => '2018-06-26 11:54:24',
            ),
            68 => 
            array (
                'id' => 74,
                'key' => 'add_combo_offers',
                'table_name' => 'combo_offers',
                'created_at' => '2018-06-26 11:54:24',
                'updated_at' => '2018-06-26 11:54:24',
            ),
            69 => 
            array (
                'id' => 75,
                'key' => 'delete_combo_offers',
                'table_name' => 'combo_offers',
                'created_at' => '2018-06-26 11:54:24',
                'updated_at' => '2018-06-26 11:54:24',
            ),
            70 => 
            array (
                'id' => 76,
                'key' => 'browse_promocodes',
                'table_name' => 'promocodes',
                'created_at' => '2018-07-16 11:56:37',
                'updated_at' => '2018-07-16 11:56:37',
            ),
            71 => 
            array (
                'id' => 77,
                'key' => 'read_promocodes',
                'table_name' => 'promocodes',
                'created_at' => '2018-07-16 11:56:37',
                'updated_at' => '2018-07-16 11:56:37',
            ),
            72 => 
            array (
                'id' => 78,
                'key' => 'edit_promocodes',
                'table_name' => 'promocodes',
                'created_at' => '2018-07-16 11:56:37',
                'updated_at' => '2018-07-16 11:56:37',
            ),
            73 => 
            array (
                'id' => 79,
                'key' => 'add_promocodes',
                'table_name' => 'promocodes',
                'created_at' => '2018-07-16 11:56:37',
                'updated_at' => '2018-07-16 11:56:37',
            ),
            74 => 
            array (
                'id' => 80,
                'key' => 'delete_promocodes',
                'table_name' => 'promocodes',
                'created_at' => '2018-07-16 11:56:37',
                'updated_at' => '2018-07-16 11:56:37',
            ),
            75 => 
            array (
                'id' => 81,
                'key' => 'browse_banners',
                'table_name' => 'banners',
                'created_at' => '2018-07-19 07:24:20',
                'updated_at' => '2018-07-19 07:24:20',
            ),
            76 => 
            array (
                'id' => 82,
                'key' => 'read_banners',
                'table_name' => 'banners',
                'created_at' => '2018-07-19 07:24:20',
                'updated_at' => '2018-07-19 07:24:20',
            ),
            77 => 
            array (
                'id' => 83,
                'key' => 'edit_banners',
                'table_name' => 'banners',
                'created_at' => '2018-07-19 07:24:20',
                'updated_at' => '2018-07-19 07:24:20',
            ),
            78 => 
            array (
                'id' => 84,
                'key' => 'add_banners',
                'table_name' => 'banners',
                'created_at' => '2018-07-19 07:24:20',
                'updated_at' => '2018-07-19 07:24:20',
            ),
            79 => 
            array (
                'id' => 85,
                'key' => 'delete_banners',
                'table_name' => 'banners',
                'created_at' => '2018-07-19 07:24:20',
                'updated_at' => '2018-07-19 07:24:20',
            ),
            80 => 
            array (
                'id' => 86,
                'key' => 'browse_cities',
                'table_name' => 'cities',
                'created_at' => '2018-07-19 09:22:07',
                'updated_at' => '2018-07-19 09:22:07',
            ),
            81 => 
            array (
                'id' => 87,
                'key' => 'read_cities',
                'table_name' => 'cities',
                'created_at' => '2018-07-19 09:22:07',
                'updated_at' => '2018-07-19 09:22:07',
            ),
            82 => 
            array (
                'id' => 88,
                'key' => 'edit_cities',
                'table_name' => 'cities',
                'created_at' => '2018-07-19 09:22:07',
                'updated_at' => '2018-07-19 09:22:07',
            ),
            83 => 
            array (
                'id' => 89,
                'key' => 'add_cities',
                'table_name' => 'cities',
                'created_at' => '2018-07-19 09:22:07',
                'updated_at' => '2018-07-19 09:22:07',
            ),
            84 => 
            array (
                'id' => 90,
                'key' => 'delete_cities',
                'table_name' => 'cities',
                'created_at' => '2018-07-19 09:22:07',
                'updated_at' => '2018-07-19 09:22:07',
            ),
            85 => 
            array (
                'id' => 91,
                'key' => 'browse_shipping_types',
                'table_name' => 'shipping_types',
                'created_at' => '2018-07-19 11:02:38',
                'updated_at' => '2018-07-19 11:02:38',
            ),
            86 => 
            array (
                'id' => 92,
                'key' => 'read_shipping_types',
                'table_name' => 'shipping_types',
                'created_at' => '2018-07-19 11:02:38',
                'updated_at' => '2018-07-19 11:02:38',
            ),
            87 => 
            array (
                'id' => 93,
                'key' => 'edit_shipping_types',
                'table_name' => 'shipping_types',
                'created_at' => '2018-07-19 11:02:38',
                'updated_at' => '2018-07-19 11:02:38',
            ),
            88 => 
            array (
                'id' => 94,
                'key' => 'add_shipping_types',
                'table_name' => 'shipping_types',
                'created_at' => '2018-07-19 11:02:38',
                'updated_at' => '2018-07-19 11:02:38',
            ),
            89 => 
            array (
                'id' => 95,
                'key' => 'delete_shipping_types',
                'table_name' => 'shipping_types',
                'created_at' => '2018-07-19 11:02:38',
                'updated_at' => '2018-07-19 11:02:38',
            ),
            90 => 
            array (
                'id' => 96,
                'key' => 'browse_flavors',
                'table_name' => 'flavors',
                'created_at' => '2018-07-20 08:21:07',
                'updated_at' => '2018-07-20 08:21:07',
            ),
            91 => 
            array (
                'id' => 97,
                'key' => 'read_flavors',
                'table_name' => 'flavors',
                'created_at' => '2018-07-20 08:21:07',
                'updated_at' => '2018-07-20 08:21:07',
            ),
            92 => 
            array (
                'id' => 98,
                'key' => 'edit_flavors',
                'table_name' => 'flavors',
                'created_at' => '2018-07-20 08:21:07',
                'updated_at' => '2018-07-20 08:21:07',
            ),
            93 => 
            array (
                'id' => 99,
                'key' => 'add_flavors',
                'table_name' => 'flavors',
                'created_at' => '2018-07-20 08:21:07',
                'updated_at' => '2018-07-20 08:21:07',
            ),
            94 => 
            array (
                'id' => 100,
                'key' => 'delete_flavors',
                'table_name' => 'flavors',
                'created_at' => '2018-07-20 08:21:07',
                'updated_at' => '2018-07-20 08:21:07',
            ),
            95 => 
            array (
                'id' => 101,
                'key' => 'browse_payment_types',
                'table_name' => 'payment_types',
                'created_at' => '2018-07-20 08:39:18',
                'updated_at' => '2018-07-20 08:39:18',
            ),
            96 => 
            array (
                'id' => 102,
                'key' => 'read_payment_types',
                'table_name' => 'payment_types',
                'created_at' => '2018-07-20 08:39:18',
                'updated_at' => '2018-07-20 08:39:18',
            ),
            97 => 
            array (
                'id' => 103,
                'key' => 'edit_payment_types',
                'table_name' => 'payment_types',
                'created_at' => '2018-07-20 08:39:18',
                'updated_at' => '2018-07-20 08:39:18',
            ),
            98 => 
            array (
                'id' => 104,
                'key' => 'add_payment_types',
                'table_name' => 'payment_types',
                'created_at' => '2018-07-20 08:39:18',
                'updated_at' => '2018-07-20 08:39:18',
            ),
            99 => 
            array (
                'id' => 105,
                'key' => 'delete_payment_types',
                'table_name' => 'payment_types',
                'created_at' => '2018-07-20 08:39:18',
                'updated_at' => '2018-07-20 08:39:18',
            ),
            100 => 
            array (
                'id' => 106,
                'key' => 'browse_shipping_methods',
                'table_name' => 'shipping_methods',
                'created_at' => '2018-07-23 12:40:43',
                'updated_at' => '2018-07-23 12:40:43',
            ),
            101 => 
            array (
                'id' => 107,
                'key' => 'read_shipping_methods',
                'table_name' => 'shipping_methods',
                'created_at' => '2018-07-23 12:40:43',
                'updated_at' => '2018-07-23 12:40:43',
            ),
            102 => 
            array (
                'id' => 108,
                'key' => 'edit_shipping_methods',
                'table_name' => 'shipping_methods',
                'created_at' => '2018-07-23 12:40:43',
                'updated_at' => '2018-07-23 12:40:43',
            ),
            103 => 
            array (
                'id' => 109,
                'key' => 'add_shipping_methods',
                'table_name' => 'shipping_methods',
                'created_at' => '2018-07-23 12:40:43',
                'updated_at' => '2018-07-23 12:40:43',
            ),
            104 => 
            array (
                'id' => 110,
                'key' => 'delete_shipping_methods',
                'table_name' => 'shipping_methods',
                'created_at' => '2018-07-23 12:40:43',
                'updated_at' => '2018-07-23 12:40:43',
            ),
            105 => 
            array (
                'id' => 111,
                'key' => 'browse_issue_forms',
                'table_name' => 'issue_forms',
                'created_at' => '2018-07-24 09:44:29',
                'updated_at' => '2018-07-24 09:44:29',
            ),
            106 => 
            array (
                'id' => 112,
                'key' => 'read_issue_forms',
                'table_name' => 'issue_forms',
                'created_at' => '2018-07-24 09:44:29',
                'updated_at' => '2018-07-24 09:44:29',
            ),
            107 => 
            array (
                'id' => 113,
                'key' => 'edit_issue_forms',
                'table_name' => 'issue_forms',
                'created_at' => '2018-07-24 09:44:29',
                'updated_at' => '2018-07-24 09:44:29',
            ),
            108 => 
            array (
                'id' => 114,
                'key' => 'add_issue_forms',
                'table_name' => 'issue_forms',
                'created_at' => '2018-07-24 09:44:29',
                'updated_at' => '2018-07-24 09:44:29',
            ),
            109 => 
            array (
                'id' => 115,
                'key' => 'delete_issue_forms',
                'table_name' => 'issue_forms',
                'created_at' => '2018-07-24 09:44:29',
                'updated_at' => '2018-07-24 09:44:29',
            ),
            110 => 
            array (
                'id' => 116,
                'key' => 'browse_orders',
                'table_name' => 'orders',
                'created_at' => '2018-08-01 12:06:16',
                'updated_at' => '2018-08-01 12:06:16',
            ),
            111 => 
            array (
                'id' => 117,
                'key' => 'read_orders',
                'table_name' => 'orders',
                'created_at' => '2018-08-01 12:06:16',
                'updated_at' => '2018-08-01 12:06:16',
            ),
            112 => 
            array (
                'id' => 118,
                'key' => 'edit_orders',
                'table_name' => 'orders',
                'created_at' => '2018-08-01 12:06:16',
                'updated_at' => '2018-08-01 12:06:16',
            ),
            113 => 
            array (
                'id' => 119,
                'key' => 'add_orders',
                'table_name' => 'orders',
                'created_at' => '2018-08-01 12:06:16',
                'updated_at' => '2018-08-01 12:06:16',
            ),
            114 => 
            array (
                'id' => 120,
                'key' => 'delete_orders',
                'table_name' => 'orders',
                'created_at' => '2018-08-01 12:06:16',
                'updated_at' => '2018-08-01 12:06:16',
            ),
            115 => 
            array (
                'id' => 121,
                'key' => 'browse_reviews',
                'table_name' => 'reviews',
                'created_at' => '2018-08-14 15:22:07',
                'updated_at' => '2018-08-14 15:22:07',
            ),
            116 => 
            array (
                'id' => 122,
                'key' => 'read_reviews',
                'table_name' => 'reviews',
                'created_at' => '2018-08-14 15:22:07',
                'updated_at' => '2018-08-14 15:22:07',
            ),
            117 => 
            array (
                'id' => 123,
                'key' => 'edit_reviews',
                'table_name' => 'reviews',
                'created_at' => '2018-08-14 15:22:07',
                'updated_at' => '2018-08-14 15:22:07',
            ),
            118 => 
            array (
                'id' => 124,
                'key' => 'add_reviews',
                'table_name' => 'reviews',
                'created_at' => '2018-08-14 15:22:08',
                'updated_at' => '2018-08-14 15:22:08',
            ),
            119 => 
            array (
                'id' => 125,
                'key' => 'delete_reviews',
                'table_name' => 'reviews',
                'created_at' => '2018-08-14 15:22:08',
                'updated_at' => '2018-08-14 15:22:08',
            ),
        ));
        
        
    }
}