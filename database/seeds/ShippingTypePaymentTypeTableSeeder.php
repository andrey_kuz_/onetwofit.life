<?php

use Illuminate\Database\Seeder;

class ShippingTypePaymentTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shipping_type_payment_type')->delete();
        
        \DB::table('shipping_type_payment_type')->insert(array (
            0 => 
            array (
                'shipping_type_id' => 1,
                'payment_type_id' => 1,
            ),
            1 => 
            array (
                'shipping_type_id' => 1,
                'payment_type_id' => 2,
            ),
            2 =>
                array (
                    'shipping_type_id' => 1,
                    'payment_type_id' => 3,
                ),
        ));
        
        
    }
}