<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('currencies')->delete();
        
        \DB::table('currencies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ukraine, Hryvnia',
                'code' => 'UAH',
                'symbol' => '₴',
                'format' => '1 0,00 грн',
                'exchange_rate' => '1',
                'active' => 0,
                'created_at' => '2018-06-13 13:58:11',
                'updated_at' => '2018-06-13 13:58:11',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Russian Ruble',
                'code' => 'RUB',
                'symbol' => '₽',
                'format' => '1 0,00 руб',
                'exchange_rate' => '50',
                'active' => 0,
                'created_at' => '2018-06-13 13:58:36',
                'updated_at' => '2018-06-13 13:58:36',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'US Dollar',
                'code' => 'USD',
                'symbol' => '$',
                'format' => '$1,0.00',
                'exchange_rate' => '1',
                'active' => 0,
                'created_at' => '2018-06-13 14:07:43',
                'updated_at' => '2018-06-13 14:07:43',
            ),
        ));
        
        
    }
}