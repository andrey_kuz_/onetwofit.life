<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $tag = Tag::firstOrNew([
            'slug' => 'organicheskie',
        ]);
        if (!$tag->exists) {
            $tag->fill([
                'name' => 'Органические',
            ])->save();
        }

        $tag = Tag::firstOrNew([
            'slug' => 'dlya-diabetikov',
        ]);
        if (!$tag->exists) {
            $tag->fill([
                'name' => 'Для диабетиков',
            ])->save();
        }

        $tag = Tag::firstOrNew([
            'slug' => 'dlya-lyudej',
        ]);
        if (!$tag->exists) {
            $tag->fill([
                'name' => 'Для людей',
            ])->save();
        }

    }

}
