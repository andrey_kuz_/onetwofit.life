<?php

use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_types')->delete();
        
        \DB::table('payment_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2018-07-20 08:45:11',
                'updated_at' => '2018-07-26 13:02:25',
                'name' => 'Visa\\Mastercard',
                'description' => 'Оплата приват 24',
                'gateway' => 'LiqPay',
                'price' => NULL,
                'active' => 1,
                'private_key' => '8Nfu3FFdElysp39AksM0RIeGE0WCXeVgNnpdDiiF',
                'public_key' => 'i18759351302',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2018-07-20 08:46:47',
                'updated_at' => '2018-07-20 08:46:47',
                'name' => 'Наличными',
                'description' => NULL,
                'gateway' => NULL,
                'price' => NULL,
                'active' => 1,
                'private_key' => NULL,
                'public_key' => NULL,
            ),
            array (
                'id' => 3,
                'created_at' => '2018-10-17 14:40:37',
                'updated_at' => '2018-10-17 14:40:37',
                'name' => 'Оплата на карту',
                'description' => NULL,
                'gateway' => NULL,
                'price' => NULL,
                'active' => 1,
                'private_key' => NULL,
                'public_key' => NULL,
            ),
        ));
        
        
    }
}