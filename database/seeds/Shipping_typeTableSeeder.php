<?php
use Illuminate\Database\Seeder;
class Shipping_methodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipping_method')->insert([
            'name' => 'Доставка курьером Новой почты'
        ]);
        DB::table('shipping_method')->insert([
            'name' => 'Доставка в отделение Новой почты'
        ]);
    }
}