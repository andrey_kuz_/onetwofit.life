<?php

use Illuminate\Database\Seeder;

class ShippingTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shipping_types')->delete();
        
        \DB::table('shipping_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2018-07-19 11:03:31',
                'updated_at' => '2018-07-19 11:03:31',
                'name' => 'Новая почта',
                'description' => NULL,
                'api' => 'nova_poshta',
                'api_key' => '90f19a4d6b5888825d7189f507d05392',
                'price' => NULL,
                'active' => 1,
            ),
        ));
        
        
    }
}