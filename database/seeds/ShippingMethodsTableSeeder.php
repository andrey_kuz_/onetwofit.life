<?php

use Illuminate\Database\Seeder;

class ShippingMethodsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shipping_methods')->delete();
        
        \DB::table('shipping_methods')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Отделение Новая Почта',
                'created_at' => '2018-07-23 12:41:00',
                'updated_at' => '2018-08-01 07:23:29',
                'method' => 'warehouse',
                'shipping_type_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Курьер Новая Почта',
                'created_at' => '2018-07-23 12:41:00',
                'updated_at' => '2018-08-01 07:23:14',
                'method' => 'postman',
                'shipping_type_id' => 1,
            ),
        ));
        
        
    }
}