<?php

use Illuminate\Database\Seeder;

use TCG\Voyager\Models\Setting;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        if (!Setting::where('key', 'site.title')->get())
        Setting::firstOrCreate(array (
            'key' => 'site.title',
            'display_name' => 'Site Title',
            'value' => 'Site Title',
            'details' => '',
            'type' => 'text',
            'order' => 1,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.description')->get())
        Setting::firstOrCreate(array (
            'key' => 'site.description',
            'display_name' => 'Site Description',
            'value' => 'Site Description',
            'details' => '',
            'type' => 'text',
            'order' => 2,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.logo')->get())
        Setting::firstOrCreate(array (
            'key' => 'site.logo',
            'display_name' => 'Site Logo',
            'value' => '',
            'details' => '',
            'type' => 'image',
            'order' => 3,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.google_analytics_tracking_id')->get())
        Setting::firstOrCreate(array (
            'key' => 'site.google_analytics_tracking_id',
            'display_name' => 'Google Analytics Tracking ID',
            'value' => NULL,
            'details' => '',
            'type' => 'text',
            'order' => 4,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.bg_image')->get())
        Setting::firstOrCreate(array (
            'key' => 'admin.bg_image',
            'display_name' => 'Admin Background Image',
            'value' => '',
            'details' => '',
            'type' => 'image',
            'order' => 5,
            'group' => 'Admin',
        ));

        if (!Setting::where('key', 'site.title')->get())
        Setting::firstOrCreate(array (
            'key' => 'admin.title',
            'display_name' => 'Admin Title',
            'value' => 'Voyager',
            'details' => '',
            'type' => 'text',
            'order' => 1,
            'group' => 'Admin',
        ));

        if (!Setting::where('key', 'site.description')->get())
        Setting::firstOrCreate(array (
            'key' => 'admin.description',
            'display_name' => 'Admin Description',
            'value' => 'Welcome to Voyager. The Missing Admin for Laravel',
            'details' => '',
            'type' => 'text',
            'order' => 2,
            'group' => 'Admin',
        ));

        if (!Setting::where('key', 'site.loader')->get())
        Setting::firstOrCreate(array (
            'key' => 'admin.loader',
            'display_name' => 'Admin Loader',
            'value' => '',
            'details' => '',
            'type' => 'image',
            'order' => 3,
            'group' => 'Admin',
        ));

        if (!Setting::where('key', 'site.icon_image')->get())
        Setting::firstOrCreate(array (
            'key' => 'admin.icon_image',
            'display_name' => 'Admin Icon Image',
            'value' => '',
            'details' => '',
            'type' => 'image',
            'order' => 4,
            'group' => 'Admin',
        ));

        if (!Setting::where('key', 'site.google_analytics_client_id')->get())
        Setting::firstOrCreate(array (
            'key' => 'admin.google_analytics_client_id',
            'display_name' => 'Google Analytics Client ID (used for admin dashboard)',
            'value' => NULL,
            'details' => '',
            'type' => 'text',
            'order' => 1,
            'group' => 'Admin',
        ));

        if (!Setting::where('key', 'site.FACEBOOK_ID')->get())
        Setting::firstOrCreate(array (
            'key' => 'site.FACEBOOK_ID',
            'display_name' => 'FACEBOOK_ID',
            'value' => '2135342523172497',
            'details' => NULL,
            'type' => 'text',
            'order' => 6,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.FACEBOOK_SECRET')->get())
        Setting::firstOrCreate(array (
            'key' => 'site.FACEBOOK_SECRET',
            'display_name' => 'FACEBOOK_SECRET',
            'value' => '81dbc900d194eab8d74c83343fbf5885',
            'details' => NULL,
            'type' => 'text',
            'order' => 7,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.FACEBOOK_URL')->get())
        Setting::firstOrCreate(array (
            'key' => 'site.FACEBOOK_URL',
            'display_name' => 'FACEBOOK_URL',
            'value' => 'http://localhost:8000/auth/facebook/callback',
            'details' => NULL,
            'type' => 'text',
            'order' => 8,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.GOOGLE_APP_ID')->get())
        Setting::firstOrCreate(array (
            'key' => 'site.GOOGLE_APP_ID',
            'display_name' => 'GOOGLE_APP_ID',
            'value' => '82842844842-q799lq5feqpq37m5o7kmh0o909a2i4nr.apps.googleusercontent.com',
            'details' => NULL,
            'type' => 'text',
            'order' => 9,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.GOOGLE_APP_SECRET')->get())
        Setting::create(array (
            'key' => 'site.GOOGLE_APP_SECRET',
            'display_name' => 'GOOGLE_APP_SECRET',
            'value' => 'yEiKnkJDDEORiYhIDeLzllSk',
            'details' => NULL,
            'type' => 'text',
            'order' => 10,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.GOOGLE_REDIRECT')->get())
        Setting::create(array (
            'key' => 'site.GOOGLE_REDIRECT',
            'display_name' => 'GOOGLE_REDIRECT',
            'value' => 'http://localhost:8000/auth/google/callback',
            'details' => NULL,
            'type' => 'text',
            'order' => 11,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.VKONTAKTE_ID')->get())
        Setting::create(array (
            'key' => 'site.VKONTAKTE_ID',
            'display_name' => 'VKONTAKTE_ID',
            'value' => '',
            'details' => NULL,
            'type' => 'text',
            'order' => 12,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.VKONTAKTE_SECRET')->get())
        Setting::create(array (
            'key' => 'site.VKONTAKTE_SECRET',
            'display_name' => 'VKONTAKTE_SECRET',
            'value' => '',
            'details' => NULL,
            'type' => 'text',
            'order' => 13,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.VKONTAKTE_URL')->get())
        Setting::create(array (
            'key' => 'site.VKONTAKTE_URL',
            'display_name' => 'VKONTAKTE_URL',
            'value' => '',
            'details' => NULL,
            'type' => 'text',
            'order' => 14,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.freeShipping_UAH')->get())
        Setting::create(array (
            'key' => 'site.freeShipping_UAH',
            'display_name' => 'Бесплатная доставка от (в грн.)',
            'value' => '1000',
            'details' => NULL,
            'type' => 'text',
            'order' => 15,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.freeShipping_RUB')->get())
        Setting::create(array (
            'key' => 'site.freeShipping_RUB',
            'display_name' => 'Бесплатная доставка от (в руб.)',
            'value' => '2000',
            'details' => NULL,
            'type' => 'text',
            'order' => 16,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.analogProductsCount')->get())
        Setting::create(array (
            'key' => 'site.analogProductsCount',
            'display_name' => 'Количество продуктов в блоке Похожие товары (или Вам могут понравиться))',
            'value' => '10',
            'details' => NULL,
            'type' => 'text',
            'order' => 17,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.viewedProductsCount')->get())
        Setting::create(array (
            'key' => 'site.viewedProductsCount',
            'display_name' => 'Количество продуктов в блоке Недавно просмотренные',
            'value' => '10',
            'details' => NULL,
            'type' => 'text',
            'order' => 18,
            'group' => 'Site',
        ));


        if (!Setting::where('key', 'site.maxCompareProducts')->first())
        Setting::create(array (
            'key' => 'site.maxCompareProducts',
            'display_name' => 'Макс.количество товаров для сравнения',
            'value' => '5',
            'details' => NULL,
            'type' => 'text',
            'order' => 19,
            'group' => 'Site',
        ));


        if (!Setting::where('key', 'site.googleAnaliticsCode')->first())
        Setting::create(array (
            'key' => 'site.googleAnaliticsCode',
            'display_name' => 'Гугл аналитика',
            'value' => '',
            'details' => NULL,
            'type' => 'text',
            'order' => 20,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.mainEmail')->first())
        Setting::create(array (
            'key' => 'site.mainEmail',
            'display_name' => 'Главный e-mail (от имени которого буду отправляться письма)',
            'value' => 'tatamanilo@gmail.com',
            'details' => NULL,
            'type' => 'text',
            'order' => 21,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.paginationCount')->first())
        Setting::create(array (
            'key' => 'site.paginationCount',
            'display_name' => 'Количество товаров на странице',
            'value' => '12',
            'details' => NULL,
            'type' => 'text',
            'order' => 22,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.hitsProductsCount')->first())
        Setting::create(array (
            'key' => 'site.hitsProductsCount',
            'display_name' => 'Количество продуктов в блоке Новинки, Хиты, Бестселлеры',
            'value' => '15',
            'details' => NULL,
            'type' => 'text',
            'order' => 23,
            'group' => 'Site',
        ));

        if (!Setting::where('key', 'site.socialCode')->first())
            Setting::create(array (
                'key' => 'site.socialCode',
                'display_name' => 'Share-кнопки на соц.сети',
                'value' => '',
                'details' => NULL,
                'type' => 'text_area',
                'order' => 24,
                'group' => 'Site',
            ));

        if (!Setting::where('key', 'site.phoneNumber')->first())
            Setting::create(array (
                'key' => 'site.phoneNumber',
                'display_name' => 'Телефон',
                'value' => '0 800 888 99 22',
                'details' => NULL,
                'type' => 'text',
                'order' => 25,
                'group' => 'Site',
            ));

        if (!Setting::where('key', 'site.phoneNumbers')->first())
            Setting::create(array (
                'key' => 'site.phoneNumbers',
                'display_name' => 'Телефоны',
                'value' => '0 800 888 99 22;0 800 888 99 23;0 800 888 99 24',
                'details' => NULL,
                'type' => 'text',
                'order' => 26,
                'group' => 'Site',
            ));

        if (!Setting::where('key', 'site.socialFacebook')->first())
            Setting::create(array (
                'key' => 'site.socialFacebook',
                'display_name' => 'Ссылка на фейсбук',
                'value' => 'https://www.facebook.com/onetwofit.life/',
                'details' => NULL,
                'type' => 'text',
                'order' => 27,
                'group' => 'Site',
            ));

        if (!Setting::where('key', 'site.socialInstagram')->first())
            Setting::create(array (
                'key' => 'site.socialInstagram',
                'display_name' => 'Ссылка на инстаграм',
                'value' => 'https://www.instagram.com/onetwofit.life',
                'details' => NULL,
                'type' => 'text',
                'order' => 28,
                'group' => 'Site',
            ));

        if (!Setting::where('key', 'site.socialYoutube')->first())
            Setting::create(array (
                'key' => 'site.socialYoutube',
                'display_name' => 'Ссылка Ютуб',
                'value' => 'https://www.youtube.com/channel/UCeFYeXYfysAK0SWb39RWR6Q',
                'details' => NULL,
                'type' => 'text',
                'order' => 29,
                'group' => 'Site',
            ));


        if (!Setting::where('key', 'site.chatTelegram')->first())
            Setting::create(array (
                'key' => 'site.chatTelegram',
                'display_name' => 'Чат телеграм',
                'value' => 'https://t.me/one2fit',
                'details' => NULL,
                'type' => 'text',
                'order' => 30,
                'group' => 'Site',
            ));
        if (!Setting::where('key', 'site.chatFacebook')->first())
            Setting::create(array (
                'key' => 'site.chatFacebook',
                'display_name' => 'Чат фейсбук',
                'value' => 'https://m.me/onetwofit.life',
                'details' => NULL,
                'type' => 'text',
                'order' => 31,
                'group' => 'Site',
            ));
        if (!Setting::where('key', 'site.chatFacebookScript')->first())
            Setting::create(array (
                'key' => 'site.chatFacebookScript',
                'display_name' => 'Скрипт чата Фейсбук',
                'value' => '',
                'details' => NULL,
                'type' => 'text_area',
                'order' => 32,
                'group' => 'Site',
            ));
        if (!Setting::where('key', 'site.chatJivositeScript')->first())
            Setting::create(array (
                'key' => 'site.chatJivositeScript',
                'display_name' => 'Скрипт чата Живосайт',
                'value' => '',
                'details' => NULL,
                'type' => 'text_area',
                'order' => 33,
                'group' => 'Site',
            ));
    }
}