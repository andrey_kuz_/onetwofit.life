<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;

class ProductsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {

//        //Content
        $product = Product::firstOrNew([
            'article' => 'test1',
        ]);
        if (!$product->exists) {
            $product->fill([
                'name' => 'test product 1',
                'article' => 'test1',
            ])->save();
        }

    }

}
