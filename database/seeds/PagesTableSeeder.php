<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            2 =>
            array (
                'id' => 3,
                'author_id' => NULL,
                'title' => 'компания',
                'excerpt' => NULL,
                'body' => '<p>компания текст</p>',
                'image' => NULL,
                'slug' => 'company',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:03:40',
                'updated_at' => '2018-07-03 15:03:40',
            ),
            3 => 
            array (
                'id' => 4,
                'author_id' => NULL,
                'title' => 'зарабатывай на покупках',
                'excerpt' => NULL,
                'body' => '<p>зарабатывай на покупках</p>',
                'image' => NULL,
                'slug' => 'benefits',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:07:46',
                'updated_at' => '2018-07-03 15:07:46',
            ),
            4 => 
            array (
                'id' => 5,
                'author_id' => NULL,
                'title' => 'отзывы о магазине',
                'excerpt' => NULL,
                'body' => '<p>отзывы о магазине</p>',
                'image' => NULL,
                'slug' => 'reviews',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:08:53',
                'updated_at' => '2018-07-03 15:08:53',
            ),
            5 => 
            array (
                'id' => 6,
                'author_id' => NULL,
                'title' => 'Поддержка',
                'excerpt' => NULL,
                'body' => '<p>Поддержка</p>',
                'image' => NULL,
                'slug' => 'support',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:09:24',
                'updated_at' => '2018-07-03 15:09:24',
            ),
            6 => 
            array (
                'id' => 7,
                'author_id' => NULL,
                'title' => 'faq',
                'excerpt' => NULL,
                'body' => '<p>faq</p>',
                'image' => NULL,
                'slug' => 'faq',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:09:57',
                'updated_at' => '2018-07-03 15:09:57',
            ),
            7 => 
            array (
                'id' => 8,
                'author_id' => NULL,
                'title' => 'возврат',
                'excerpt' => NULL,
                'body' => '<p>возврат</p>',
                'image' => NULL,
                'slug' => 'return',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:10:46',
                'updated_at' => '2018-07-03 15:10:46',
            ),
            8 => 
            array (
                'id' => 9,
                'author_id' => NULL,
                'title' => 'доставка',
                'excerpt' => NULL,
                'body' => '<p>доставка</p>',
                'image' => NULL,
                'slug' => 'delivery',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:11:18',
                'updated_at' => '2018-07-03 15:11:18',
            ),
            9 => 
            array (
                'id' => 10,
                'author_id' => NULL,
                'title' => 'Политика конфиденциальности',
                'excerpt' => NULL,
                'body' => '<p>Политика конфиденциальности</p>',
                'image' => NULL,
                'slug' => 'confident',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:11:58',
                'updated_at' => '2018-07-03 15:11:58',
            ),
            10 => 
            array (
                'id' => 11,
                'author_id' => NULL,
                'title' => 'Пользовательское соглашение',
                'excerpt' => NULL,
                'body' => '<p>Пользовательское соглашение</p>',
                'image' => NULL,
                'slug' => 'agree',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-07-03 15:12:39',
                'updated_at' => '2018-07-03 15:12:39',
            ),
        ));
        
        
    }
}