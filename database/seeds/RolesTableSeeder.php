<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        if (!Role::where('id', '3')->first())
            Role::create(array (
                'id' => 3,
                'name' => 'manager',
                'display_name' => 'Manager',
                'created_at' => '2018-07-30 12:32:00',
                'updated_at' => '2018-07-30 12:32:00',
            ));
    }
}