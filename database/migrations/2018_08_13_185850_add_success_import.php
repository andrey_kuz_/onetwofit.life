<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuccessImport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('imports', function (Blueprint $table) {
            $table->boolean('success')->default(false);
            $table->string('type', 10)->default(''); // full or changes
            $table->text('error', 1000)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imports', function (Blueprint $table) {
            $table->dropColumn('success');
            $table->dropColumn('type');
            $table->dropColumn('error');
        });
    }
}
