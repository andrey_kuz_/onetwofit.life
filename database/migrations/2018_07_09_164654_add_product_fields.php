<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('is_bestseller')->default(false);
            $table->boolean('is_hit')->default(false);
            $table->boolean('is_special')->default(false);
            $table->boolean('is_new')->default(false);
            $table->boolean('is_finished')->default(false);
            $table->boolean('is_active')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('is_bestseller');
            $table->dropColumn('is_hit');
            $table->dropColumn('is_special');
            $table->dropColumn('is_new');
            $table->dropColumn('is_active');
            $table->dropColumn('is_finished');
        });
    }
}
