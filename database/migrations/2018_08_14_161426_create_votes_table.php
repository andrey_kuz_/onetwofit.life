<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('votable_id')->unsigned()->index();
            $table->string('votable_type')->index();
            $table->enum('type', ['up', 'down'])->default('up');
            $table->unique(['user_id', 'votable_id', 'votable_type']);
            $table->timestamps();
        });

        Schema::table('reviews', function (Blueprint $table) {
            $table->integer('up_votes')->unsigned()->default(0);
            $table->integer('down_votes')->unsigned()->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');

        Schema::table('reviews', function (Blueprint $table) {
            $table->dropColumn('up_votes');
            $table->dropColumn('down_votes');
        });
    }
}
