<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('shipping');
            $table->string('name', 30)->change();
            $table->string('avatar', 100)->change();
            $table->string('email', 190)->change();
            $table->string('phone', 20)->change();
            $table->string('firstname', 20)->change();
            $table->string('lastname', 20)->change();
            if (Schema::hasColumn('users', 'shipping_city')) {
                $table->string('shipping_city', 50)->change();
            } else {
                $table->string('shipping_city', 50)->nullable();
            }

            $table->string('shipping_street', 50)->change();
            $table->string('shipping_house', 10)->change();
            $table->string('shipping_room', 10)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('shipping_city');
        });
    }
}
