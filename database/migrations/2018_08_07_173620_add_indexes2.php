<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexes2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->index(['price']);
            $table->integer('discount')->nullable(false)->default(0)->change();
            $table->index(['discount']);
            $table->index(['discount2']);
            $table->index(['discount3']);
            $table->index(['discount4']);
            $table->index(['discount5']);
            $table->index(['discount6']);
            $table->index(['discount7']);
            $table->index(['discount8']);
            $table->index(['discount9']);
            $table->index(['discount10']);
            $table->index(['brand_id', 'price', 'discount']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
