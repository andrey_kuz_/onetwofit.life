<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('promocode', 32)->nullable();
            $table->renameColumn('discount', 'discount_by_qnt_combo');
            $table->integer('discount_promo')->default(0);
            $table->float('total_price_discounted_without_promo', 15, 2)->default(0);
            $table->float('total_price_discounted', 15, 2)->default(0);
        });

        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('total_price', 'total_price_discounted');
            $table->renameColumn('price', 'total_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->renameColumn('total_price_discounted', 'total_price');
            $table->renameColumn('total_price', 'price');
        });
    }
}
