<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->timestamps();
        });
        Schema::create('shipping_methods_types', function (Blueprint $table) {
            $table->integer('shipping_type_id')->unsigned();
            $table->foreign('shipping_type_id')->references('id')->on('shipping_types')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('shipping_method_id')->unsigned();
            $table->foreign('shipping_method_id')->references('id')->on('shipping_methods')->onUpdate('cascade')->onDelete('cascade');

            $table->primary(array('shipping_type_id', 'shipping_method_id'), 'stm_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_methods');
        Schema::dropIfExists('shipping_methods_types');
    }
}
