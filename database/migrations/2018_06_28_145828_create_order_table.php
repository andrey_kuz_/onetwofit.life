<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use  App\Models\Order;
class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();

            $table->enum('status', Order::$statuses)->default(Order::STATUS_PENDING);
            $table->dateTime('completed_at')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();

            $table->integer('item_count')->unsigned()->default(0);
            $table->float('total_price', 15, 2)->default(0);
            $table->float('discount', 15, 2)->default(0);

            $table->string('currency', 10);
            $table->string('currency_rate');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            //custom fields
            $table->text('shipping_status')->nullable();
            $table->text('payment_status')->nullable();
            $table->integer('shipping_type_id')->unsigned()->nullable();
            $table->integer('payment_type_id')->unsigned()->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_name')->nullable();
            $table->string('shipping_last_name')->nullable();
            $table->string('shipping_email')->nullable();
            $table->string('shipping_phone')->nullable();


        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
