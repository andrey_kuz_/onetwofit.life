<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CteatePromoCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 32)->unique();
            $table->integer('discount')->nullable();
            $table->text('data')->nullable();
            $table->boolean('is_disposable')->default(true);
            $table->boolean('is_oneuser_disposable')->default(true);
            $table->date('exp_date')->nullable();
            $table->timestamps();
        });

        Schema::create('promocode_used', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('promocode_id');

            $table->timestamp('used_at');
            $table->timestamps();
            $table->primary(['user_id', 'promocode_id']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('promocode_id')->references('id')->on(config('promocodes.table', 'promocodes'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('promocode_used');
        Schema::dropIfExists('promocodes');

    }
}
