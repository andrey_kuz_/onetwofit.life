<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('flavor', 'issue_form', 'country_code');
            $table->integer('flavor_id')->unsigned()->nullable();
            $table->integer('issue_form_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('pack_pieces')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('flavor_id');
            $table->dropColumn('issue_form_id');
            $table->dropColumn('country_id');

            $table->string('issue_form', 50)->nullable();
            $table->string('flavor', 50)->nullable();
            $table->string('country_code')->nullable();
            $table->string('pack_pieces', 50)->nullable()->change();
        });
    }
}
