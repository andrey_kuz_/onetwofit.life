<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpecialPricesToProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('price2')->nullable();
            $table->decimal('price3')->nullable();
            $table->decimal('price4')->nullable();
            $table->decimal('price5')->nullable();
            $table->decimal('price6')->nullable();
            $table->decimal('price7')->nullable();
            $table->decimal('price8')->nullable();
            $table->decimal('price9')->nullable();
            $table->decimal('price10')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('price2');
            $table->dropColumn('price3');
            $table->dropColumn('price4');
            $table->dropColumn('price5');
            $table->dropColumn('price6');
            $table->dropColumn('price7');
            $table->dropColumn('price8');
            $table->dropColumn('price9');
            $table->dropColumn('price10');
        });
    }
}
