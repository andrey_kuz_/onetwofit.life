<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropOrdersColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('orders', 'payment_id')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('payment_id');
            });
        }
        if (Schema::hasColumn('orders', 'promocode_id')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('promocode_id');
            });
        }
        if (Schema::hasColumn('orders', 'promocode')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('promocode');
            });
        }
        if (Schema::hasColumn('orders', 'shipping_last_name')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('shipping_last_name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
