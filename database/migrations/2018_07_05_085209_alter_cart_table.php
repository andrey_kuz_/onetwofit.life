<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart_lines', function (Blueprint $table) {
            $table->dropColumn('unit_price', 'total_price');
        });
        Schema::table('cart', function (Blueprint $table) {
            $table->dropColumn('total_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_lines', function (Blueprint $table) {
            $table->decimal('unit_price')->default(0.00);
            $table->decimal('total_price')->default(0.00);
        });
        Schema::table('cart', function (Blueprint $table) {
            $table->decimal('total_price')->default(0.00);
        });
    }
}
