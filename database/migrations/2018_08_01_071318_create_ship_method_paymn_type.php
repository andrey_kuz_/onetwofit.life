<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipMethodPaymnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('shipping_methods_types');
        Schema::create('shipping_type_payment_type', function (Blueprint $table) {

            $table->integer('shipping_type_id')->unsigned();
            $table->foreign('shipping_type_id')->references('id')->on('shipping_types')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('payment_type_id')->unsigned();
            $table->foreign('payment_type_id')->references('id')->on('payment_types')->onUpdate('cascade')->onDelete('cascade');

            $table->primary(array('shipping_type_id', 'payment_type_id'), 'st_pt_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_type_payment_type');
    }
}
