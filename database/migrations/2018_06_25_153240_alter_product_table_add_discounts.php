<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductTableAddDiscounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //
        Schema::table('products', function (Blueprint $table) {
            $table->integer('discount2')->nullable();
            $table->integer('discount3')->nullable();
            $table->integer('discount4')->nullable();
            $table->integer('discount5')->nullable();
            $table->integer('discount6')->nullable();
            $table->integer('discount7')->nullable();
            $table->integer('discount8')->nullable();
            $table->integer('discount9')->nullable();
            $table->integer('discount10')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('discount1');
            $table->dropColumn('discount2');
            $table->dropColumn('discount3');
            $table->dropColumn('discount4');
            $table->dropColumn('discount5');
            $table->dropColumn('discount6');
            $table->dropColumn('discount7');
            $table->dropColumn('discount8');
            $table->dropColumn('discount9');
            $table->dropColumn('discount10');
        });
    }
}
