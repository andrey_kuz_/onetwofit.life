<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->date('exp_date')->nullable();
            $table->string('country_code')->nullable();
            $table->decimal('volume', 7, 2)->nullable();
            $table->enum('volume_dim', ['l', 'ml'])->nullable();
            $table->decimal('weight', 7, 2)->nullable();
            $table->enum('weight_dim', ['g', 'kg'])->nullable();
            $table->string('pack_pieces', 50)->nullable();
            $table->string('issue_form', 50)->nullable();
            $table->string('flavor', 50)->nullable();
            $table->integer('quantity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('exp_date')->nullable(false)->change();
            $table->dropColumn('country_code');
            $table->dropColumn('volume');
            $table->dropColumn('volume_dim');
            $table->dropColumn('weight');
            $table->dropColumn('weight_dim');
            $table->dropColumn('pack_pieces');
            $table->dropColumn('issue_form');
            $table->dropColumn('flavor');
            $table->dropColumn('quantity');
        });
    }
}
