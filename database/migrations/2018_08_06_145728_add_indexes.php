<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('translations', function (Blueprint $table) {
            $table->index(['table_name', 'locale', 'foreign_key']);
        });

        Schema::table('combo_offers', function (Blueprint $table) {
            $table->index(['product_id', 'related_product_id']);
            $table->index(['related_product_id']);
        });

        Schema::table('cart_lines', function (Blueprint $table) {
            $table->index(['product_id']);
            $table->index(['product_id', 'cart_id']);
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->index(['user_id', 'name']);
        });

        Schema::table('shipping_types', function (Blueprint $table) {
            $table->index(['api']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
