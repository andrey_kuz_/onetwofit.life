let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.

 */
mix.js('resources/assets/js/app-admin.js', 'public/js/app-admin.js').
mix.js('resources/assets/js/app-front.js', 'public/js/app-front.js')
    .sass('resources/assets/sass/app.scss', 'public/css/app.css')
    .copyDirectory('resources/assets/images', 'public/images').version();






